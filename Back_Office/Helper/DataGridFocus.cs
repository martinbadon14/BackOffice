﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Controls;

namespace Back_Office.Helper
{
    public class DataGridFocus
    {
        public DataGridFocus()
        {

        }

        public DataGridCell GetDataGridCell(DataGridCellInfo cellInfo)
        {
            var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);

            if (cellContent != null)
                return ((System.Windows.Controls.DataGridCell)cellContent.Parent);

            return (null);
        }


        public SLNSet FillAcctCodeAndDesc(string SLCCode, string SLTCode, string SLECode, string SLNCode)
        {
            SLList sl = new SLList();
            sl.SLC = Convert.ToByte(SLCCode);
            sl.SLT = Convert.ToByte(SLTCode);
            sl.SLE = Convert.ToByte(SLECode);
            sl.SLN = Convert.ToByte(SLNCode);

            Back_Office.Models.SLNSet sln = new Back_Office.Models.SLNSet();

            string parsed = new JavaScriptSerializer().Serialize(sl);
            CRUXLib.Response Response = App.CRUXAPI.request("sntype/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {
                List<Models.SNType> sntype = new JavaScriptSerializer().Deserialize<List<Models.SNType>>(Response.Content);

                foreach (Models.SNType item in sntype)
                {
                    sln.AccountCode = item.GLControlACCT;
                    sln.AccountDesc = item.COADesc;
                }

                return sln;
            }

            else
                return sln;
        }


        public SLNSet FillSLNDataGrid(string SLCCode, string SLTCode, string SLECode)
        {
            SLList sl = new SLList();
            sl.SLC = Convert.ToByte(SLCCode);
            sl.SLT = Convert.ToByte(SLTCode);
            sl.SLE = Convert.ToByte(SLECode);

            Back_Office.Models.SLNSet sln = new Back_Office.Models.SLNSet();

            string parsed = new JavaScriptSerializer().Serialize(sl);
            CRUXLib.Response Response = App.CRUXAPI.request("sntype/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {

                List<Models.SNType> sntype = new JavaScriptSerializer().Deserialize<List<Models.SNType>>(Response.Content);

                foreach (Models.SNType item in sntype)
                {
                    if (item.GLControlNUM == 15 || item.GLControlNUM == 11)
                    {
                        sln.SLN = item.GLControlNUM;
                        sln.AccountCode = item.GLControlACCT;
                        sln.AccountDesc = item.COADesc;

                        break;
                    }
                    else
                    {
                        sln.SLN = 99;
                        sln.AccountCode = item.GLControlACCT;
                        sln.AccountDesc = item.COADesc;
                    }
                }

                return sln;

            }

            else
            {
                return sln;
            }

        }


    }
}
