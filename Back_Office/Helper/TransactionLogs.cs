﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Helper
{
    public class TransactionLogs
    {
        public class ActivityFields
        {
            public static Int64 NewGJ = 28;
            public static Int64 ModifyGJ = 30;
            public static Int64 Explanation = 31;
            public static Int64 ComputeSpecialTrans = 32;
            public static Int64 ReverseTrans = 33;
            public static Int64 PostTrans = 16;
            public static Int64 NewCV = 50;
            public static Int64 ComputeIntDeposits = 100;
            public static Int64 NewRemittanceJ = 149;
            public static Int64 ModifyRemittanceJ = 150;
            public static Int64 Reclassify = 161;
            public static Int64 TransactionDate = 165;
            public static Int64 GenerateClosingEntries = 169;
            public static Int64 TaggedCancelled = 170;
            public static Int64 ComputeAVGShareCap = 177;
        }

        public class ActivityList
        {
            public static Int64 NewTransAdded = 9;
            public static Int64 YrEndClosingEntries = 18;
            public static Int64 ModifyTrans = 29;
            public static Int64 PostTrans = 31;
            public static Int64 ReverseTrans = 33;
            public static Int64 GenerateControlList = 74;
        }


        public void InsertLog()
        {
            List<UtilityLogs> _UtilLogs = new List<UtilityLogs>();

        }
    }
}
