﻿using Back_Office.Models;
using Back_Office.Models.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Input;

namespace Back_Office.Helper
{
    public class ExecuteHelper
    {
        public MainWindow mww;


        public ExecuteHelper(MainWindow mw)
        {
            this.mww = mw;
        }


        public void ExecutePost(String upd, ExecutedRoutedEventArgs e, Boolean ClosedStatus)
        {
            if (String.IsNullOrEmpty(upd) && upd != "R" && upd != "P")
            {
                if (this.mww.Lock() == "SUCCESS")
                {
                    if (!ClosedStatus)
                    {
                        this.mww.Posting();
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show(CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show(this.mww, "Cannot Post Documents. Transaction is currently on Edit Mode by another User: " + this.mww.Lock());
                }
            }

            else if (upd == "P")
            {
                MessageBoxResult result = MessageBox.Show("Transaction is Already Posted!", "Posting!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else if (upd == "R")
            {
                MessageBoxResult result = MessageBox.Show("No Documents Transaction Details are eligible for Posting! Aborting...", "Posting!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else
            {
                e.Handled = true;
            }
        }


        public void ExecutePost(String upd, KeyEventArgs e, Boolean ClosedStatus)
        {
            if (String.IsNullOrEmpty(upd) && upd != "R" && upd != "P")
            {
                if (this.mww.Lock() == "SUCCESS")
                {
                    if (!ClosedStatus)
                    {
                        this.mww.Posting();
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show(CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show(this.mww, "Cannot Post Documents. Transaction is currently on Edit Mode by another User: " + this.mww.Lock());
                }
            }

            else if (upd == "P")
            {
                MessageBoxResult result = MessageBox.Show("No Documents Transaction Details are eligible for Posting! Aborting...", "Posting!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else if (upd == "R")
            {
                MessageBoxResult result = MessageBox.Show("No Documents Transaction Details are eligible for Posting! Aborting...", "Posting!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else
            {
                e.Handled = true;
            }
        }


        public void ExecuteReverse(String upd, ExecutedRoutedEventArgs e, Boolean ClosedStatus)
        {
            if (upd == "P" && upd != "R" && !String.IsNullOrEmpty(upd))
            {
                if (this.mww.Lock() == "SUCCESS")
                {
                    if (!ClosedStatus)
                    {
                        this.mww.ReverseEntry();
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show(CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show(this.mww.Lock());
                }
            }

            else if (upd == "R")
            {
                MessageBoxResult result = MessageBox.Show("No Documents Transaction Details are eligible for REVERSAL! Aborting..", "Reversing!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else
            {
                e.Handled = true;
            }
        }


        public void ExecuteReverse(String upd, KeyEventArgs e, Boolean ClosedStatus)
        {
            if (upd == "P" && upd != "R" && !String.IsNullOrEmpty(upd))
            {
                if (this.mww.Lock() == "SUCCESS")
                {
                    if (!ClosedStatus)
                    {
                        this.mww.ReverseEntry();
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show(CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show(this.mww.Lock());
                }
            }

            else if (upd == "R" || upd == null)
            {
                MessageBoxResult result = MessageBox.Show("No Documents Transaction Details are eligible for REVERSAL! Aborting..", "Reversing!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else
            {
                e.Handled = true;
            }
        }


        public void ExecuteCancel(String upd, KeyEventArgs e, Boolean ClosedStatus)
        {
            if (String.IsNullOrEmpty(upd) && upd != "R" && upd != "P")
            {
                if (this.mww.Lock() == "SUCCESS")
                {
                    if (!ClosedStatus)
                    {
                        this.mww.Cancelling();
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show(CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show(this.mww.Lock());
                }
            }

            else if (upd == "P")
            {
                MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else if (upd == "C")
            {
                MessageBoxResult result = MessageBox.Show("Transaction Document is Already Cancelled!", "Cancelled!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else if (upd == "R")
            {
                MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else
            {
                e.Handled = true;
            }
        }


        public void ExecuteCancel(String upd, ExecutedRoutedEventArgs e, Boolean ClosedStatus)
        {
            if (String.IsNullOrEmpty(upd) && upd != "R" && upd != "P")
            {
                if (this.mww.Lock() == "SUCCESS")
                {
                    if (!ClosedStatus)
                    {
                        this.mww.Cancelling();
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show(CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show(this.mww.Lock());
                }
            }

            else if (upd == "P")
            {
                MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else if (upd == "C")
            {
                MessageBoxResult result = MessageBox.Show("Transaction Document is Already Cancelled!", "Cancelled!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else if (upd == "R")
            {
                MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                this.mww.ClearValues();
                e.Handled = true;
            }

            else
            {
                e.Handled = true;
            }
        }


        public List<Models.TransactionDetails> ExecuteCheckingUpdateOnly(List<Models.TransactionDetails> dbDetails, List<Models.TransactionDetails> updatedDetails)
        {
            List<Models.TransactionDetails> updateTrans = new List<Models.TransactionDetails>();

            updateTrans = updatedDetails.Except(dbDetails).ToList();

            return updateTrans;
        }


        public List<Models.TransactionDetails> ExecuteCheckingUpdateWithDelete(List<Models.TransactionDetails> dbDetails, List<Models.TransactionDetails> updatedDetails)
        {
            List<Int32> updatedItems = new List<int>();
            List<int> dbItems = new List<int>();

            foreach (Models.TransactionDetails detail in dbDetails)
            {
                updatedItems.Add(detail.SequenceNo);
            }

            foreach (Models.TransactionDetails detail in updatedDetails)
            {
                dbItems.Add(detail.SequenceNo);
            }

            List<Int32> only = updatedItems.Except(dbItems).ToList();

            List<Models.TransactionDetails> deleteTrans = new List<Models.TransactionDetails>();

            foreach (Int32 number in only) //list values of SeqNo Deleted
            {
                Models.TransactionDetails deleteTransDetail = dbDetails.Find(c => c.SequenceNo == number);

                    deleteTrans.Add(new Models.TransactionDetails()
                    {
                        TransYear = deleteTransDetail.TransYear,
                        BranchCode = deleteTransDetail.BranchCode,
                        OfficeID = deleteTransDetail.OfficeID,
                        SequenceNo = deleteTransDetail.SequenceNo,
                        ControlNo = deleteTransDetail.ControlNo,
                        SLC_Code = deleteTransDetail.SLC_Code,
                        SLE_Code = deleteTransDetail.SLE_Code,
                        SLT_Code = deleteTransDetail.SLT_Code,
                        SLN_Code = deleteTransDetail.SLN_Code,
                        ClientID = deleteTransDetail.ClientID,
                        clientName = deleteTransDetail.clientName,
                        TransactionCode = deleteTransDetail.TransactionCode,
                        AccountCode = deleteTransDetail.AccountCode,
                        Debit = deleteTransDetail.Debit,
                        Credit = deleteTransDetail.Credit,
                        SLDate = deleteTransDetail.SLDate,
                        Amount = deleteTransDetail.Amount,
                        ReferenceNo = deleteTransDetail.ReferenceNo
                    }); //end of deleteTrans List


                } // end of list values of SeqNo Deleted

                return deleteTrans;
            }


        public String CreateAcctCodeWithSLPrompt()
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("This GL Account Code is linked to an SL Control!").AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.Append("Improper posting of GL Only Transactions would result to disbalanced SL GL Figures!").AppendLine();
            MyStringBuilder.Append("Continuing as is would be logged! Continue?");

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateAcctCodeCanGLOnlyPrompt()
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("This GL Account Code is linked to an SL Control!").AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.Append("Improper posting of GL Only Transactions would result to disbalanced SL GL Figures!").AppendLine();

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateInterestOnSavingsExplanation(List<InterestRangeForExplanation> SetupInterestExplanation, String FromDate, String ToDate)
        {
            StringBuilder MyStringBuilder = new StringBuilder();

            MyStringBuilder.Append("Computer Generated - Interest on Deposits - A.D.B. Computed for the period :");
            MyStringBuilder.AppendLine(FromDate + " to " + ToDate + ":").AppendLine();
            MyStringBuilder.Append("Parameters:").AppendLine();

            foreach (InterestRangeForExplanation item in SetupInterestExplanation)
            {
                MyStringBuilder.Append(item.COADesc + " Int. Rate: " + item.IntRate + "%");
                MyStringBuilder.Append(" W/Tax Rate:" + item.TaxIntRate + "% p.a.");
                MyStringBuilder.Append(" MinADB: " + item.MinADB + " ");

                if (item.MaxADB != 0)
                {
                    MyStringBuilder.Append(" MaxADB: " + item.MaxADB);
                }
                else
                {
                    MyStringBuilder.Append(" MaxADB: No Limit ");
                }

                MyStringBuilder.Append(" IntExpAcct: " + item.GLAcctCode + " " + item.GLAcctDesc);
                MyStringBuilder.AppendLine();
            }

            return MyStringBuilder.ToString();
        }


        public String CreateLoanReclassificationExplanation(String CutOffDate)
        {
            StringBuilder MyStringBuilder = new StringBuilder();

            MyStringBuilder.Append("Computer Generated Reclassification of Loan Status:");
            MyStringBuilder.AppendLine("Cut-Off Date : " + CutOffDate).AppendLine();
            MyStringBuilder.Append("SL Class: Loans Receivable").AppendLine();
            MyStringBuilder.Append("SL Type:  ALL").AppendLine();
            MyStringBuilder.Append("SL Status:  Current").AppendLine();

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateUnearnedAmortizationExplanation(String CutOffDate, String checkBoxContent)
        {
            StringBuilder MyStringBuilder = new StringBuilder();

            MyStringBuilder.Append("Computer Generated UID Amortization:");
            MyStringBuilder.AppendLine("Cut-Off Date: " + CutOffDate).AppendLine();
            MyStringBuilder.Append("SL Class: Loans Receivable").AppendLine();
            MyStringBuilder.Append("SL Type:  ALL").AppendLine();
            MyStringBuilder.Append("SL Status:" + checkBoxContent).AppendLine();

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }

        public String CreateClosingPrompt()
        {
            StringBuilder MyStringBuilder = new StringBuilder();

            MyStringBuilder.Append("Error: Transaction Date Falls below the latest Closing Entries!");
            MyStringBuilder.AppendLine();
            MyStringBuilder.AppendLine("Cannot make further transactions for dates belonging to an Accounting Year/Month that has been CLOSED! ").AppendLine();
            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateClosingEntriesExplanation(String CutOffDate)
        {
            StringBuilder MyStringBuilder = new StringBuilder();

            MyStringBuilder.Append("Computer Generated-");
            MyStringBuilder.AppendLine("Closing Entries for Date Ending: " + CutOffDate).AppendLine();
            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public void Posting(List<Models.TransactionDetails> slDetailList, Int16 BranchCode, Int16 TransCode, Int64 CtlNo, Int16 UpdID, String CutOffDate, String Explanation, String RandomToken)
        {
            UPDTrans updTrans = new UPDTrans();
            updTrans.UPDTagID = UpdID;
            updTrans.CtlNo = CtlNo;
            updTrans.TransCode = TransCode;
            updTrans.BranchCode = BranchCode;

            String parsedUPDTrans = new JavaScriptSerializer().Serialize(updTrans);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/postTransDetails", parsedUPDTrans);

            if (Response.Status == "SUCCESS")
            {
                // SaveTOGLTrans(slDetailList, CutOffDate, Explanation, "Post",RandomToken);

                MessageBox.Show("Module Was Successfully Posted!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show(Response.Content, "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void SaveTOGLTrans(List<TransactionDetails> insertSLDetailsList, String CutOffDate, String Explanation, String Option, String RandomToken)
        {
            PostSLGLDetails _SLGLDetails = new PostSLGLDetails();

            _SLGLDetails.TransactionCode = insertSLDetailsList.First().TransactionCode;

            int Range = 0, NoItems = 300;
            List<TransactionDetails> GLDetailsBy300 = new List<Models.TransactionDetails>();

            if (insertSLDetailsList.Count > NoItems)
            {
                while (insertSLDetailsList.Count > Range)
                {
                    var check = insertSLDetailsList.Count - Range;

                    if (check < NoItems)
                    {
                        GLDetailsBy300 = insertSLDetailsList.GetRange(Range, check).ToList();
                    }
                    else
                    {
                        GLDetailsBy300 = insertSLDetailsList.GetRange(Range, NoItems).ToList();
                    }

                    _SLGLDetails.SLDetailsList = GLDetailsBy300;
                    _SLGLDetails.Explanation = Explanation;
                    _SLGLDetails.TransactionDate = Convert.ToDateTime(CutOffDate).ToString("MM/dd/yyyy");
                    _SLGLDetails.Type = Option;
                    _SLGLDetails.RandomToken = RandomToken;

                    if (_SLGLDetails != null)
                    {
                        String parsedGLTransaction = new JavaScriptSerializer().Serialize(_SLGLDetails);
                        CRUXLib.Response Response = App.CRUXAPI.request("backoffice/postGLTrans", parsedGLTransaction, 999999999);
                    }

                    Range += 300;
                }
            }
            else
            {
                _SLGLDetails.SLDetailsList = insertSLDetailsList;
                _SLGLDetails.Explanation = Explanation;
                _SLGLDetails.TransactionDate = Convert.ToDateTime(CutOffDate).ToString("MM/dd/yyyy");
                _SLGLDetails.Type = Option;
                _SLGLDetails.RandomToken = RandomToken;

                if (_SLGLDetails != null)
                {
                    String parsedGLTransaction = new JavaScriptSerializer().Serialize(_SLGLDetails);
                    CRUXLib.Response Response = App.CRUXAPI.request("backoffice/postGLTrans", parsedGLTransaction, 999999999);
                }
            }
        }


    }
}
