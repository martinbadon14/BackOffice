﻿using Back_Office.Models.Scripts;
using System;
using System.Collections.Generic;


namespace Back_Office.Scripts
{
    public class ADBComputation
    {
        Decimal NetInterest = 0, taxInterest = 0, grossInterest = 0;

        public ADBValues Computation(List<Models.Scripts.ADBDetails> adbLists, String CutOffDate)
        {
            ADBValues adbDetails = new ADBValues();
            int i = 0;
            Decimal Balance = 0;

            foreach (ADBDetails item in adbLists)
            {
                if (i + 1 != adbLists.Count)
                {
                    TimeSpan dateDiff = Convert.ToDateTime(adbLists[i + 1].TransactionDate) - Convert.ToDateTime(adbLists[i].TransactionDate);
                    item.DateDiff = (dateDiff).Days;
                    Balance += item.Balance;

                    item.CurrentBalance = Balance * item.DateDiff;
                    adbDetails.TotalBalance += item.CurrentBalance;
                    adbDetails.TotalNoDays += item.DateDiff;
                    i++;
                }
                else
                {
                    TimeSpan dateDiff = Convert.ToDateTime(CutOffDate) - Convert.ToDateTime(adbLists[i].TransactionDate);
                    item.DateDiff = (dateDiff).Days;
                    Balance += item.Balance;

                    item.CurrentBalance = Balance * item.DateDiff;

                    adbDetails.TotalBalance += item.CurrentBalance;
                    adbDetails.TotalNoDays += item.DateDiff;
                }
            }

            return adbDetails;
        }


        public Decimal InterestComputation(Decimal ADBValue, Decimal IntRate, DateTime TransactionDate)
        {
            Decimal grosInt = ADBValue * (IntRate / 100);
            Decimal noDays = DateTime.DaysInMonth(TransactionDate.Year, TransactionDate.Month);
            NetInterest = grosInt * (noDays / 360);

            return NetInterest;
        }


        public Decimal TaxInterestComputation(Decimal ADBValue, Decimal IntRate, DateTime TransactionDate, Decimal TaxIntRate)
        {
            taxInterest = ADBValue * (IntRate / 100);
            Decimal NoDays = DateTime.DaysInMonth(TransactionDate.Year, TransactionDate.Month);
            grossInterest = taxInterest * (NoDays / 360);

            Decimal InterestAmount = grossInterest * (TaxIntRate / 100);

            return NetInterest = grossInterest - InterestAmount;
        }

    }
}
