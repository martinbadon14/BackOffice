﻿using Back_Office.Models;
using Back_Office.Models.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Back_Office.Scripts
{
    public class MonthEndScript
    {
        ScriptParameters scriptParams;
        int TransactionTypeID = 0;
        private Decimal TotalInt;
        String TransDate = "";
        DateTime TransactionDate;

        public MonthEndScript(int TransTypeID, String transDate)
        {
            scriptParams = new ScriptParameters();
            TransactionTypeID = TransTypeID;
            TransDate = transDate;
            TransactionDate = Convert.ToDateTime(TransDate);
        }


        public List<Models.TransactionDetails> SetupInterestOnSavings(InterestRange interestRange, List<SLWithADB> slWithADB)
        {
            Decimal TotalInterest = 0;
            Decimal interest = 0, GrossInterest = 0;
            Int32 SeqNo = 0, BalTypeValue = -1;
            ADBComputation adbComp = new ADBComputation();

            List<TransactionDetails> transDetailsWithSL = new List<TransactionDetails>();

            foreach (SLWithADB slDetailWithADB in slWithADB)
            {
                if (interestRange.SLC == slDetailWithADB.SLC && interestRange.SLT == slDetailWithADB.SLT)
                {

                    if (slDetailWithADB.ADBValue >= interestRange.MinADB && slDetailWithADB.ADBValue <= interestRange.MaxADB && interestRange.IntRate != 0)
                    {

                        if (interestRange.TaxIntRate != 0)
                        {
                            GrossInterest = Convert.ToDecimal(adbComp.TaxInterestComputation(slDetailWithADB.ADBValue, interestRange.IntRate, TransactionDate, interestRange.TaxIntRate).ToString("N7", CultureInfo.CreateSpecificCulture("en-US")));
                            interest = Convert.ToDecimal(GrossInterest.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")));
                            TotalInterest += interest;

                            transDetailsWithSL.Add(new TransactionDetails()
                            {
                                SLC_Code = slDetailWithADB.SLC.ToString(),
                                SLT_Code = slDetailWithADB.SLT.ToString(),
                                SLE_Code = slDetailWithADB.SLE.ToString(),
                                SLN_Code = slDetailWithADB.SLN.ToString(),
                                AccountCode = slDetailWithADB.AcctCode,
                                ClientID = slDetailWithADB.ClientID,
                                ReferenceNo = slDetailWithADB.RefNo,
                                SLDate = slDetailWithADB.TransDate,
                                SequenceNo = SeqNo,
                                TransYear = TransactionDate.Year,
                                TransactionCode = Convert.ToInt16(TransactionTypeID),
                                AdjFlag = 1,
                                Amount = BalTypeValue * interest
                            });

                            SeqNo += 1;
                        } //end if interestRange != 0

                        else
                        {
                            GrossInterest = Convert.ToDecimal(adbComp.InterestComputation(slDetailWithADB.ADBValue, interestRange.IntRate, TransactionDate).ToString("N7", CultureInfo.CreateSpecificCulture("en-US")));
                            interest = Convert.ToDecimal(GrossInterest.ToString("N2", CultureInfo.CreateSpecificCulture("en-US")));
                            TotalInterest += interest;


                            transDetailsWithSL.Add(new TransactionDetails()
                            {
                                SLC_Code = slDetailWithADB.SLC.ToString(),
                                SLT_Code = slDetailWithADB.SLT.ToString(),
                                SLE_Code = slDetailWithADB.SLE.ToString(),
                                SLN_Code = slDetailWithADB.SLN.ToString(),
                                AccountCode = slDetailWithADB.AcctCode,
                                ClientID = slDetailWithADB.ClientID,
                                ReferenceNo = slDetailWithADB.RefNo,
                                AdjFlag = 1,
                                SLDate = slDetailWithADB.TransDate,
                                SequenceNo = SeqNo,
                                TransYear = TransactionDate.Year,
                                TransactionCode = Convert.ToInt16(TransactionTypeID),
                                Amount = BalTypeValue * interest
                            });

                            SeqNo += 1;
                        } //end else

                    } //end (slDetailWithADB.ADBValue >= interestRange.MinADB && slDetailWithADB.ADBValue <= interestRange.MaxADB && interestRange.IntRate != 0)


                } //end if (interestRange.SLC == slDetailWithADB.SLC && interestRange.SLT == slDetailWithADB.SLT)

            } // end of foreach slWithADB

            SetTotalIntereset(TotalInterest);

            return transDetailsWithSL;

        }


        public Decimal SetTotalIntereset(Decimal TotalInterest)
        {
            return TotalInt = TotalInterest;
        }


        public Decimal GetTotalInterest()
        {
            return TotalInt;
        }

    }
}
