﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for BankCode.xaml
    /// </summary>
    public partial class BankCode : Window
    {
        public MainWindow mww;

        public BankCode(MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;
            FillDataGrid();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            searchTextBox.Focus();
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;

                } //end switch
            }
        }

        private void FillDataGrid()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/BankCode", "");

            if (Response.Status == "SUCCESS")
            {
                List<Models.BankCode> bankCodesList = new JavaScriptSerializer().Deserialize<List<Models.BankCode>>(Response.Content);
                BankCodeDataGrid.ItemsSource = bankCodesList;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void ClickFunction()
        {
            if (BankCodeDataGrid.SelectedItem == null) return;
            var selectedBankCode = BankCodeDataGrid.SelectedItem as Models.BankCode;

            TransactionCheck transCheck = (TransactionCheck)this.mww.COCIDataGrid.SelectedItem;
            transCheck.BankID = selectedBankCode.BankID;
            transCheck.BankCodeDesc = selectedBankCode.BankDesc;
            transCheck.AcctCode = selectedBankCode.AcctCode;
            this.Close();
            this.mww.MoveToNextUIElement();
        }

        private void BankCodeDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ClickFunction();
        }

        private void BankCodeDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ClickFunction();
            }
        }

        private void TextBoxGotFocus(object sender, RoutedEventArgs e)
        {
            String textName = (sender as TextBox).Name.ToString();

            if (textName == "searchTextBox")
            {
                searchTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                searchTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

        }


        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            String textName = (sender as TextBox).Name.ToString();

            if (textName == "searchTextBox")
            {
                searchTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                searchTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }
    }
}
