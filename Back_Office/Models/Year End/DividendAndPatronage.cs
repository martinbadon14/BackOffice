﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Year_End
{
    public class DividendAndPatronage
    {
        public Decimal TotalIntPaid { get; set; }
        public Decimal TotalAvgShareCapital { get; set; }
        public Decimal DividendAmt { get; set; }
        public Decimal PatronageRefundAmt { get; set; }
        public String FromDate { get; set; }
        public String ToDate { get; set; }
    }
}
