﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Year_End
{
    public class LoansRequestParameters
    {
        public String Cutoff { get; set; }
        public String SLC { get; set; }
        public String SLT { get; set; }

        public String DRF { get; set; }
        public String DRT { get; set; }

        public String MDF { get; set; }
        public String MDT { get; set; }

        public String LTF { get; set; }
        public String LTT { get; set; }


        public String BalanceFilter { get; set; }
        public String PrincipalFilter { get; set; }
        public String ShareCapFilter { get; set; }
        public String DepositFilter { get; set; }
        public String PPM { get; set; }
        public String IPM { get; set; }
        public String Amort { get; set; }
        public String Collect { get; set; }
        public String SLSTat { get; set; }
        public String AccStat { get; set; }
        public String Dosri { get; set; }



        public String ClientID { get; set; }
        public String ClientTypeID { get; set; }
        public String GenderID { get; set; }
        public String CivilStatusID { get; set; }

        public String CompanyType { get; set; }
        public String Company { get; set; }

        public String AlertLevel { get; set; }
        public String GeoProvince { get; set; }
        public String GeoCity { get; set; }
        public String GeoBarangay { get; set; }

        public String EmpFrom { get; set; }
        public String EmpTo { get; set; }
        public Boolean PrevEmp { get; set; }
    }
}
