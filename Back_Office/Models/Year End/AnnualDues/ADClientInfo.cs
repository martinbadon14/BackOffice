﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Year_End.AnnualDues
{
    public class ADClientInfo
    {
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
    }
}
