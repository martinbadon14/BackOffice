﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Year_End
{
    public class CheckClosingParams
    {
        public String ForwardMonth { get; set; }
        public String CurrentMonth { get; set; }
        public Boolean CurrentMonthStatus { get; set; }
        public Boolean ForwardMonthStatus { get; set; }
    }
}
