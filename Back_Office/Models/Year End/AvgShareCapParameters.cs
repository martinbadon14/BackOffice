﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Year_End
{
    public class AvgShareCapParameters
    {
        public Int16 BranchCode { get; set; }
        public Int64 ClientID { get; set; }
        public String ClientIDFormat { get; set; }
        public String ClientName { get; set; }
        public String RefNo { get; set; }
        public Decimal ShareCapitalAmt { get; set; }
        public Decimal AvgShareCapital { get; set; }
        public Decimal InterestPaid { get; set; }
        public String FromDate { get; set; }
        public String ToDate { get; set; }
        public String Explanation { get; set; }
        public Decimal Dividend { get; set; }
        public Decimal Patronage { get; set; }
        public Decimal GrossDPR { get; set; }
    }

    public class AvgShareCapitalContainer
    {
        public List<AvgShareCapParameters> AvgShareCapitalList { get; set; }
        public Decimal TotalShareCapital { get; set; }
        public Decimal TotalAvgShareCapital { get; set; }
        public Decimal TotalIntPaid { get; set; }
    }

    public  class ComputedAvgShareCapital
    {
        public Int64 ClientID { get; set; }
        public String ClientIDFormat { get; set; }
        public String ClientName { get; set; }
        public Decimal DividendAmt { get; set; }
        public Decimal PatronageRefundAmt { get; set; }
        public Decimal GrossAmt { get; set; }
    }

    public class ClientInfoAVG
    {
        public String ClientIDFormat { get; set; }
        public String ClientName { get; set; }
    }
}
