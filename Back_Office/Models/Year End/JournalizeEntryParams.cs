﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Year_End
{
    public class JournalizeEntryParams
    {
        public String TransDate { get; set; }
        public String ToDate { get; set; }
        public String DividendAcctCode { get; set; }
        public String PatronageAcctCode { get; set; }

    }

    public class ClientFinalParams
    {
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
    }
}
