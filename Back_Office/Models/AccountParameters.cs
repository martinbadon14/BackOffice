﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    class AccountParameters
    {
        public Int64 ClientID { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Byte SLE { get; set; }
        public String SLDate { get; set; }
        public Byte SLN { get; set; }
    }
}
