﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Reports
{
    public class BOHeader
    {
        public int BranchID { get; set; }
        public String OfficeName { get; set; }
        public String OfficeAddress { get; set; }
        public String TransDate { get; set; }
    }
}
