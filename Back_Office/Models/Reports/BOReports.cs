﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Reports
{
    public class BOReports
    {
        public String SLC_Code { get; set; }
        public String SLT_Code { get; set; }
        public String SLE_Code { get; set; }
        public String SLN_Code { get; set; }
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
        public Int64 AccountCode { get; set; }
        public string AccountDesc { get; set; }
        public string ReferenceNo { get; set; }
        public String SLDate { get; set; }
        public Int64 ControlNo { get; set; }
        public String CTLNoFormat
        {
            get
            {
                return String.Format("{0}-{1}", BranchCode, ControlNo);
            }
            set { }
        }
        public Decimal Debit { get; set; }
        public String DebitFormat
        {
            get
            {
                if (Debit == 0)
                {
                    return String.Empty;
                }
                else return String.Format("{0:###,##0.00}", Debit);
            }
            set { }
        }
        public Decimal Credit { get; set; }
        public String CreditFormat {
            get
            {
                if (Credit == 0)
                {
                    return String.Empty;
                }
                else return String.Format("{0:###,##0.00}", Credit);
            }
            set { }
        }
        public String EncodedBy { get; set; }
        public String ApprovedBy { get; set; }
        public String PostedBy { get; set; }
        public Int16 BranchCode { get; set; }
        public String Explanation { get; set; }
        public String ClientChckID { get; set; }
        public String ClientIDForm { get; set; }
        public String ClientIDFormat
        {
            get
            {
                if (ClientID == 0)
                {
                    return null;
                }
                else return string.Format("{0}-{1}-{2}", String.Format("{0:D2}", BranchCode), String.Format("{0:D8}", ClientID), String.Format("{0:D2}", ClientChckID));
            }

            set { }
        }

        public String GLSLCode
        {
            get
            {
                if (!String.IsNullOrEmpty(SLC_Code) && !String.IsNullOrEmpty(SLT_Code) && !String.IsNullOrEmpty(SLE_Code) && !String.IsNullOrEmpty(SLN_Code))
                {
                    return String.Format("S-{0}-{1}-{2}-{3}", SLC_Code, SLT_Code, SLE_Code, SLN_Code);
                }
                else return string.Format("G-{0}", AccountCode);
            }
            set { }
        }

    }
}
