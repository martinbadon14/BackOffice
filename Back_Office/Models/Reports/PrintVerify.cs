﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Reports
{
    public class PrintVerify
    {
        public String Username { get; set; }
        public String ComputerName { get; set; }
        public String IPAddress { get; set; }
        public String VerificationCode { get; set; }
        public DateTime DateTime { get; set; }
    }
}
