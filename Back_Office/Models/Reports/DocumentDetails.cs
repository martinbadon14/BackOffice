﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Reports
{
    public class DocumentDetails
    {
        public String LetterHead { get; set; }
        public String OfficeName { get; set; }
        public String OfficeAddress { get; set; }
        public String CDADetails { get; set; }
        public String BIRDetails { get; set; }
        public String Version { get; set; }
        public String PermitNumber { get; set; }
        public String DateIssued { get; set; }
        public String Header1 { get; set; }
        public String Header2 { get; set; }
        public String Footer1 { get; set; }
        public String Footer2 { get; set; }
        public String Footer3 { get; set; }
        public String Footer4 { get; set; }
    }
}
