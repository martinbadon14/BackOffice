﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class ScriptParameters
    {
        public Decimal MinADB  { get; set; }
        public Decimal MaxADB { get; set; }
        public Double IntRate { get; set; }
        public Decimal WithTaxRate { get; set; }
        public String IntExpAcct { get; set; }
        public String PeriodFrom { get; set; }
        public String PeriodTo { get; set; }
        public String TransDate { get; set; }
    }
}
