﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class CheckType
    {
        public Int32 CheckTypeID { get; set; }
        public String CheckTypeDesc { get; set; }
        public Int32 CheckTypeDays { get; set; }
        public String ChckDesc { get; set; }
        public String ShortCutKey { get; set; }
    }
}
