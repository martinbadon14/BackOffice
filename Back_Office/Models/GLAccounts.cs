﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class GLAccounts : INotifyPropertyChanged
    {
        public Int64 Code { get; set; }
        public String AccountTypeDesc { get; set; }
        public String Description { get; set; }

        public Int64 SeqNo { get; set; }
        public Boolean IsSelected { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }

    public class AcctCodewithSL
    {
        public String SLC_Code { get; set; }
        public String SLT_Code { get; set; }
        public String SLE_Code { get; set; }
        public String SLN_Code { get; set; }
        public Byte CanGLOnly { get; set; }
    }
}
