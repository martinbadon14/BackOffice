﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class Client
    {
        public Int64 BranchID { get; set; }
        public String BranchIDForm { get; set; }
        public Int64 clientId { get; set; }
        public Byte clientCheckID { get; set; }
        public String ClientIDFormat { get { return string.Format("{0}-{1}-{2}", BranchIDForm, ClientIDForm, clientCheckID); } }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string suffix { get; set; }
        public String Name { get { return string.Format("{0}, {1} {2} {3}", lastName, firstName, middleName, suffix); } }
        public String clientType { get; set; }
        public String status { get; set; }
        public String ClientIDForm { get; set; }
        public String RowColor { get; set; }
    }

    public class ClientParams
    {
        public String ClientID { get; set; }
        public String BranchID { get; set; }
    }

    public class SLAccountParams
    {
        public Int64 BranchID { get; set; }
        public Int64 ClientID { get; set; }
        public Byte SLCode { get; set; }
        public Byte SLTCode { get; set; }
        public String RefNo { get; set; }
        public Boolean IsF8 { get; set; }
    }

    public class DeleteParameters
    {
        public Int64 BranchID { get; set; }
        public Int16 TransactionCode { get; set; }
        public Int64 ControlNo { get; set; }
        public String DocNo { get; set; }
        public Int32 SeqNo { get; set; }
    }

    public class LoanApplicationParams
    {
        public Int64 BranchID { get; set; }
        public Int64 ClientID { get; set; }
        public String REF_NO { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public String APPLICATION_NO { get; set; }
    }
}
