﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class SLClass
    {
        public Byte slcode { get; set; }
        public string sldescription { get; set; }
        public Boolean IsSelected = false;
    }
}
