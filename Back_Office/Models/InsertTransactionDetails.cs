﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class InsertTransactionDetails : INotifyPropertyChanged
    {
        public Int16 TransactionCode { get; set; }
        public Int64 ControlNo { get; set; }
        public Int64 AccountCode { get; set; }
        public Int64 ClientID { get; set; }
        public Byte SLC_Code { get; set; }
        public Byte SLT_Code { get; set; }
        public string ReferenceNo { get; set; }
        public Byte SLE_Code { get; set; }
        public Byte SLN_Code { get; set; }
        public DateTime SLDate { get; set; }
        public Byte AdjFlag { get; set; }
        public Decimal Amount { get; set; }
        public Int16 EncodedBy { get; set; }
        public Int16 ApprovedBy { get; set; }
        public Int16 PostedBy { get; set; }
        public Int16 UPDTag { get; set; }
        public Int64 GlAccount { get; set; }
        public Decimal Credit { get; set; }
        public Decimal Debit { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
