﻿using Back_Office.Models.Purchase_Journal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace Back_Office.Models
{
    public class TransactionDetails : INotifyPropertyChanged
    {
        private String Sc_code;
        public String SLC_Code //Byte
        {
            get { return Sc_code; }
            set { if (value != Sc_code) { Sc_code = value; NotifyPropertyChanged("SLC_Code"); } }
        }

        private String sLC;
        public String SLC
        {
            get { return sLC; }
            set { if (value != sLC) { sLC = value; NotifyPropertyChanged("SLC"); } }
        }

        private String Stcode;
        public String SLT_Code
        {
            get { return Stcode; }
            set { if (value != Stcode) { Stcode = value; NotifyPropertyChanged("SLT_Code"); } }
        }

        private String Slecode;
        public String SLE_Code
        {
            get { return Slecode; }
            set { if (value != Slecode) { Slecode = value; NotifyPropertyChanged("SLE_Code"); } }
        }

        private String Sncode;
        public String SLN_Code
        {
            get { return Sncode; }
            set { if (value != Sncode) { Sncode = value; NotifyPropertyChanged("SLN_Code"); } }
        }

        private Int64 accoutCode;
        public Int64 AccountCode
        {
            get { return accoutCode; }
            set { if (value != accoutCode) { accoutCode = value; NotifyPropertyChanged("AccountCode"); } }
        }

        private String referenceNo;
        public string ReferenceNo
        {
            get { return referenceNo; }
            set { if (value != referenceNo) { referenceNo = value; NotifyPropertyChanged("ReferenceNo"); } }
        }

        private string AccountDesc;
        public string accountDesc
        {
            get { return AccountDesc; }
            set { if (value != AccountDesc) { AccountDesc = value; NotifyPropertyChanged("accountDesc"); } }
        }

        private Int64 clientID;
        public Int64 ClientID
        {
            get { return clientID; }
            set { if (value != clientID) { clientID = value; NotifyPropertyChanged("ClientID"); } }
        }

        private string ClientName;
        public string clientName
        {
            get { return ClientName; }
            set { if (value != ClientName) { ClientName = value; NotifyPropertyChanged("clientName"); } }
        }

        private Decimal debit;
        public Decimal Debit
        {
            get { return debit; }
            set
            {
                debit = value; NotifyPropertyChanged("Debit");
            }
        }

        private Decimal credit;
        public Decimal Credit
        {
            get { return credit; }
            set
            {
                credit = value; NotifyPropertyChanged("Credit");
            }
        }

        private string transactionDate;
        public String TransactionDate
        {
            get { return transactionDate; }
            set { if (value != transactionDate) { transactionDate = value; NotifyPropertyChanged("TransactionDate"); } }
        }

        private Int16 transactionCode;
        public Int16 TransactionCode
        {
            get { return transactionCode; }
            set { if (value != transactionCode) { transactionCode = value; NotifyPropertyChanged("TransactionCode"); } }
        }

        private String slDate;
        public String SLDate
        {
            get { return slDate; }
            set { if (value != slDate) { slDate = value; NotifyPropertyChanged("SLDate"); } }
        }

        Int16 _EncodedByID;
        public Int16 EncodedByID
        {
            get { return _EncodedByID; }
            set
            {
                if (value != _EncodedByID)
                {
                    _EncodedByID = value; NotifyPropertyChanged("EncodedByID");
                }
            }
        }
        public Decimal Amount { get; set; }
        public String EncodedBy { get; set; }
        public String ApprovedBy { get; set; }
        public String PostedBy { get; set; }

        public String UPDTagCode { get; set; }

        public String ClientChckID { get; set; }
        public String BranchIDFormat { get; set; }
        public String ClientIDForm { get; set; }

        private Int64 controlNo;
        public Int64 ControlNo
        {
            get { return controlNo; }
            set { if (value != controlNo) { controlNo = value; NotifyPropertyChanged("ControlNo"); } }
        }

        private String clientIDFormat;
        public String ClientIDFormat
        {
            get { return clientIDFormat; }
            set { if (value != clientIDFormat) { clientIDFormat = value; NotifyPropertyChanged("ClientIDFormat"); } }
        }

        public String Options { get; set; }
        public Int16 UpdTagID { get; set; }

        private Int16 branchCode;
        public Int16 BranchCode
        {
            get { return branchCode; }
            set { if (value != branchCode) { branchCode = value; NotifyPropertyChanged("BranchCode"); } }
        }

        private Int32 sequenceNo;
        public Int32 SequenceNo
        {
            get { return sequenceNo; }
            set { if (value != sequenceNo) { sequenceNo = value; NotifyPropertyChanged("SequenceNo"); } }
        }

        private Int16 officeID;
        public Int16 OfficeID
        {
            get { return officeID; }
            set { if (value != officeID) { officeID = value; NotifyPropertyChanged("OfficeID"); } }
        }

        private Int32 transYear;
        public Int32 TransYear
        {
            get { return transYear; }
            set { if (value != transYear) { transYear = value; NotifyPropertyChanged("TransYear"); } }
        }

        String _DocNo;
        public String DocNo
        {
            get { return _DocNo; }
            set { if (value != _DocNo) { _DocNo = value; NotifyPropertyChanged("DocNo"); } }
        }

        String _RowColor;
        public String RowColor
        {
            get { return _RowColor; }
            set
            {
                _RowColor = value;
                NotifyPropertyChanged("RowColor");
            }
        }

        Byte _AdjFlag;
        public Byte AdjFlag
        {
            get { return _AdjFlag; }
            set { if (value != _AdjFlag) { _AdjFlag = value; NotifyPropertyChanged("AdjFlag"); } }
        }

        String _AdjFlagCode;
        public String AdjFlagCode
        {
            get { return _AdjFlagCode; }
            set { if (value != _AdjFlagCode) { _AdjFlagCode = value; NotifyPropertyChanged("AdjFlagCode"); } }
        }

        String _OfficeAbv;
        public String OfficeAbv
        {
            get { return _OfficeAbv; }
            set { if (value != _OfficeAbv) { _OfficeAbv = value; NotifyPropertyChanged("OfficeAbv"); } }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }

    public class TransactionDetailContainer
    {
        public String OrigTransDate { get; set; }
        public String Options { get; set; }
        public List<TransactionDetails> _TransactionDetailsList { get; set; }
        public TransactionSummary _TransactionSummary { get; set; }
        public List<TransactionCheck> TransactionCheckList { get; set; }
        public Int64 CTLNoNew { get; set; }
        public List<PurchaseDetails> PurchaseDetails{ get; set; }
        public InsertPurchaseSummary PurchaseSummary { get; set; }

    }

    public class TransactionSummary : INotifyPropertyChanged
    {
        public Int16 BranchCode { get; set; }
        public Int16 OfficeID { get; set; }
        public Int16 TransactionCode { get; set; }
        public Int64 ControlNo { get; set; }

        String _DocNo;
        public String DocNo
        {
            get { return _DocNo; }
            set { if (value != _DocNo) { _DocNo = value; NotifyPropertyChanged("DocNo"); } }
        }
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
        public String AndORName { get; set; }

        public String _BatchNo;
        public String BatchNo
        {
            get { return _BatchNo; }
            set { if (value != _BatchNo) { _BatchNo = value; NotifyPropertyChanged("BatchNo"); } }
        }

        String _TransactionDate;
        public String TransactionDate
        {
            get { return _TransactionDate; }
            set { if (value != _TransactionDate) { _TransactionDate = value; NotifyPropertyChanged("TransactionDate"); } }
        }

        public DateTime DateTimeAdded { get; set; }

        String _Explanation;
        public String Explanation
        {
            get { return _Explanation; }
            set { if (value != _Explanation) { _Explanation = value; NotifyPropertyChanged("Explanation"); } }
        }

        public Int32 TransYear { get; set; }
        public Boolean ClosedStatus { get; set; }

        String _ClientIDTransSum;
        public String ClientIDTransSum
        {
            get { return _ClientIDTransSum; }
            set { if (value != _ClientIDTransSum) { _ClientIDTransSum = value; NotifyPropertyChanged("ClientIDTransSum"); } }
        }

        String _ClientChckIDTransSum;
        public String ClientChckIDTransSum
        {
            get { return _ClientChckIDTransSum; }
            set { if (value != _ClientChckIDTransSum) { _ClientChckIDTransSum = value; NotifyPropertyChanged("ClientChckIDTransSum"); } }
        }

        String _ClientNameTransSum;
        public String ClientNameTransSum
        {
            get { return _ClientNameTransSum; }
            set { if (value != _ClientNameTransSum) { _ClientNameTransSum = value; NotifyPropertyChanged("ClientNameTransSum"); } }
        }

        String _ClientOrTransSum;
        public String ClientOrTransSum
        {
            get { return _ClientOrTransSum; }
            set { if (value != _ClientOrTransSum) { _ClientOrTransSum = value; NotifyPropertyChanged("ClientOrTransSum"); } }
        }

        String _ClientIDFormatTransSum;
        public String ClientIDFormatTransSum
        {
            get { return _ClientIDFormatTransSum; }
            set { if (value != _ClientIDFormatTransSum) { _ClientIDFormatTransSum = value; NotifyPropertyChanged("ClientIDFormatTransSum"); } }
        }

        public String RandomToken { get; set; }

        String _DatePosted;
        public String DatePosted
        {
            get
            {
                return _DatePosted;
            }
            set
            {
                if (value != _DatePosted)
                {
                    _DatePosted = value; NotifyPropertyChanged("DatePosted");
                }
            }
        }

        public Int64 ORCTLNo { get; set; }

        String _ORNo;
        public String ORNo
        {
            get
            {
                return _ORNo;
            }
            set
            {
                if (value != _ORNo)
                {
                    _ORNo = value; NotifyPropertyChanged("ORNo");
                }
            }
        }

        String _TransTypDesc;
        public String TransTypDesc
        {
            get
            {
                return _TransTypDesc;
            }
            set
            {
                if (value != _TransTypDesc)
                {
                    _TransTypDesc = value; NotifyPropertyChanged("TransTypDesc");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }

    public class PostSLGLDetails
    {
        public Int64 ControlNo { get; set; }
        public String TransactionDate { get; set; }
        public Int16 TransactionCode { get; set; }
        public String TransactionDocNo { get; set; }
        public String Explanation { get; set; }
        public String Type { get; set; }
        public String RandomToken { get; set; }
        public List<TransactionDetails> SLDetailsList { get; set; }
    }

    public class SLDetails
    {
        public Int16 BranchCode { get; set; }
        public Int16 OfficeID { get; set; }
        public Int64 ClientID { get; set; }
        public String SLC_Code { get; set; }
        public String SLT_Code { get; set; }
        public String ReferenceNo { get; set; }
        public String SLE_Code { get; set; }
        public Int32 SequenceNo { get; set; }
        public String GLDate { get; set; } //DateTime
        public String TransactionDocNo { get; set; }
        public decimal Amount { get; set; }

        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public String AccountCode { get; set; }

        public Int16 TransactionCode { get; set; }
        public String TransactionDate { get; set; } //DateTime
        public Int64 ControlNo { get; set; }
        public String Type { get; set; }

        public String RandomToken { get; set; }
    }

    public class GLTransaction
    {
        public Int16 BranchCode { get; set; }
        public Int64 AccountCode { get; set; }
        public String TransDate { get; set; }
        public Int16 TransCode { get; set; }
        public Decimal DebitAmount { get; set; }
        public Decimal CreditAmount { get; set; }
        public Decimal DRAmountB4 { get; set; }
        public Decimal CRAmountB4 { get; set; }
        public Decimal TransSumBalanceBefor4 { get; set; }

        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public String Explanation { get; set; }

        public String Type { get; set; }
        public List<TransactionDetails> GLDetailsList { get; set; }
    }

    public class CheckBalancesParams
    {
        public Int16 BranchCode { get; set; }
        public Int64 ClientID { get; set; }
        public Byte SLC_Code { get; set; }
        public Byte SLT_Code { get; set; }
        public Byte SLE_Code { get; set; }
        public Byte SLN_Code { get; set; }
        public String RefNo { get; set; }
        public Decimal BalAmount { get; set; }
        public Decimal BalSLDate { get; set; }
        public String SLDate { get; set; }

        public Int64 AcctCode { get; set; }
        public Int32 BalTypeValue { get; set; }
        public Decimal HoldAmount { get; set; }

        public Decimal InterestDue { get; set; }
        public Decimal PenaltyDue { get; set; }
    }

    public class CheckLoanBalance
    {
        public String SLC { get; set; }
        public String SLT { get; set; }
        public String SLE { get; set; }
        public String SLN { get; set; }
        public String RefNo { get; set; }
        public Int64 ClientID { get; set; }
        public String SLDate { get; set; }
        public String WithPrompt { get; set; }
    }

    public class CheckTransactionDetails
    {
        public Int16 BranchCode { get; set; }
        public Int64 ControlNo { get; set; }
        public Int16 TransactionCode { get; set; }
        public int SequenceNo { get; set; }
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
        public string ReferenceNo { get; set; }
        public Int64 AccountCode { get; set; }
        public String SLDate { get; set; }
        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public Decimal Amount { get; set; }
        public String SLC_Code { get; set; }
        public String SLT_Code { get; set; }
        public String SLE_Code { get; set; }
        public String SLN_Code { get; set; }
        public Byte AdjFlag { get; set; }
    }


}

