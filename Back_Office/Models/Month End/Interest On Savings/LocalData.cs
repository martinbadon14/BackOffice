﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Month_End.Interest_On_Savings
{
    public class LocalData : INotifyPropertyChanged
    {

        private Int16 officeID;
        public Int16 OfficeID
        {
            get { return officeID; }
            set { if (value != officeID) { officeID = value; NotifyPropertyChanged("OfficeID"); } }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
