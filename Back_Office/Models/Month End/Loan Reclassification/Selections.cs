﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Month_End.Loan_Reclassification
{
    public class Selections
    {
        public List<SLClass> SLC { get; set; }
        public List<SLType> SLT { get; set; }
    }
}
