﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Month_End.Special_Transactions
{
    public class InsertSpecialTransaction : INotifyPropertyChanged
    {
        private Int16 _TransCode;
        public Int16 TransCode
        {
            get { return _TransCode; }
            set { if (value != _TransCode) { _TransCode = value; NotifyPropertyChanged("TransCode"); } }
        }

        private String _ToDate;
        public String ToDate
        {
            get { return _ToDate; }
            set { if (value != _ToDate) { _ToDate = value; NotifyPropertyChanged("ToDate"); } }
        }

        private String _CutOffDate;
        public String CutOffDate
        {
            get { return _CutOffDate; }
            set { if (value != _CutOffDate) { _CutOffDate = value; NotifyPropertyChanged("CutOffDate"); } }
        }

        private String _SLC;
        public String SLC
        {
            get { return _SLC; }
            set { if (value != _SLC) { _SLC = value; NotifyPropertyChanged("SLC"); } }
        }

        private String _SLT;
        public String SLT
        {
            get { return _SLT; }
            set { if (value != _SLT) { _SLT = value; NotifyPropertyChanged("SLT"); } }
        }

        private String _SLE;
        public String SLE
        {
            get { return _SLE; }
            set { if (value != _SLE) { _SLE = value; NotifyPropertyChanged("SLE"); } }
        }

        private String _SLN;
        public String SLN
        {
            get { return _SLN; }
            set { if (value != _SLN) { _SLN = value; NotifyPropertyChanged("SLN"); } }
        }

        private Int64 _SLAcctCode;
        public Int64 SLAcctCode
        {
            get { return _SLAcctCode; }
            set { if (value != _SLAcctCode) { _SLAcctCode = value; NotifyPropertyChanged("SLAcctCode"); } }
        }

        private Int64 _GLAcctCode;
        public Int64 GLAcctCode
        {
            get { return _GLAcctCode; }
            set { if (value != _GLAcctCode) { _GLAcctCode = value; NotifyPropertyChanged("GLAcctCode"); } }
        }

        private String _GLAcctDesc;
        public String GLAcctDesc
        {
            get { return _GLAcctDesc; }
            set { if (value != _GLAcctDesc) { _GLAcctDesc = value; NotifyPropertyChanged("GLAcctDesc"); } }
        }

        private String _SLDescription;
        public String SLDescription
        {
            get { return _SLDescription; }
            set { if (value != _SLDescription) { _SLDescription = value; NotifyPropertyChanged("SLDescription"); } }
        }

        private Byte _SLBalanceType;
        public Byte SLBalanceType
        {
            get { return _SLBalanceType; }
            set { if (value != _SLBalanceType) { _SLBalanceType = value; NotifyPropertyChanged("SLBalanceType"); } }
        }

        private Int16 _SpecialTransTypeID;
        public Int16 SpecialTransTypeID
        {
            get { return _SpecialTransTypeID; }
            set { if (value != _SpecialTransTypeID) { _SpecialTransTypeID = value; NotifyPropertyChanged("SpecialTransTypeID"); } }
        }

        private Decimal _DeductAmt;
        public Decimal DeductAmt
        {
            get { return _DeductAmt; }
            set { if (value != _DeductAmt) { _DeductAmt = value; NotifyPropertyChanged("DeductAmt"); } }
        }

        private Decimal _SLBalanceAmt;
        public Decimal SLBalanceAmt
        {
            get { return _SLBalanceAmt; }
            set { if (value != _SLBalanceAmt) { _SLBalanceAmt = value; NotifyPropertyChanged("SLBalanceAmt"); } }
        }

        private String _LastTransDate;
        public String LastTransDate
        {
            get { return _LastTransDate; }
            set { if (value != _LastTransDate) { _LastTransDate = value; NotifyPropertyChanged("LastTransDate"); } }
        }

        private Int16 _LastTransDateBalanceType;
        public Int16 LastTransDateBalanceType
        {
            get { return _LastTransDateBalanceType; }
            set { if (value != _LastTransDateBalanceType) { _LastTransDateBalanceType = value; NotifyPropertyChanged("LastTransDateBalanceType"); } }
        }

        private String _PMESTransDate;
        public String PMESTransDate
        {
            get { return _PMESTransDate; }
            set { if (value != _PMESTransDate) { _PMESTransDate = value; NotifyPropertyChanged("PMESTransDate"); } }
        }

        private Byte _PMESDateBalanceType;
        public Byte PMESDateBalanceType
        {
            get { return _PMESDateBalanceType; }
            set { if (value != _PMESDateBalanceType) { _PMESDateBalanceType = value; NotifyPropertyChanged("PMESDateBalanceType"); } }
        }

        private String _SLCDebit;
        public String SLCDebit
        {
            get { return _SLCDebit; }
            set { if (value != _SLCDebit) { _SLCDebit = value; NotifyPropertyChanged("SLCDebit"); } }
        }

        private String _SLTDebit;
        public String SLTDebit
        {
            get { return _SLTDebit; }
            set { if (value != _SLTDebit) { _SLTDebit = value; NotifyPropertyChanged("SLTDebit"); } }
        }

        private String _SLEDebit;
        public String SLEDebit
        {
            get { return _SLEDebit; }
            set { if (value != _SLEDebit) { _SLEDebit = value; NotifyPropertyChanged("SLEDebit"); } }
        }

        private String _SLNDebit;
        public String SLNDebit
        {
            get { return _SLNDebit; }
            set { if (value != _SLNDebit) { _SLNDebit = value; NotifyPropertyChanged("SLNDebit"); } }
        }

        private String _SLTDebitDesc;
        public String SLTDebitDesc
        {
            get { return _SLTDebitDesc; }
            set { if (value != _SLTDebitDesc) { _SLTDebitDesc = value; NotifyPropertyChanged("SLTDebitDesc"); } }
        }

        private Int64 _SLDebitGLCode;
        public Int64 SLDebitGLCode
        {
            get { return _SLDebitGLCode; }
            set { if (value != _SLDebitGLCode) { _SLDebitGLCode = value; NotifyPropertyChanged("SLDebitGLCode"); } }
        }

        private String _SLDebitGLDesc;
        public String SLDebitGLDesc
        {
            get { return _SLDebitGLDesc; }
            set { if (value != _SLDebitGLDesc) { _SLDebitGLDesc = value; NotifyPropertyChanged("SLDebitGLDesc"); } }
        }

        private String _SLCCredit; 
        public String SLCCredit
        {
            get { return _SLCCredit; }
            set { if (value != _SLCCredit) { _SLCCredit = value; NotifyPropertyChanged("SLCCredit"); } }
        }

        private String _SLTCredit;
        public String SLTCredit
        {
            get { return _SLTCredit; }
            set { if (value != _SLTCredit) { _SLTCredit = value; NotifyPropertyChanged("SLTCredit"); } }
        }

        private String _SLECredit;
        public String SLECredit
        {
            get { return _SLECredit; }
            set { if (value != _SLECredit) { _SLECredit = value; NotifyPropertyChanged("SLECredit"); } }
        }

        private String _SLNCredit;
        public String SLNCredit
        {
            get { return _SLNCredit; }
            set { if (value != _SLNCredit) { _SLNCredit = value; NotifyPropertyChanged("SLNCredit"); } }
        }

        private String _SLCreditDesc;
        public String SLCreditDesc
        {
            get { return _SLCreditDesc; }
            set { if (value != _SLCreditDesc) { _SLCreditDesc = value; NotifyPropertyChanged("SLCreditDesc"); } }
        }

        private Int64 _SLCreditGLCode;
        public Int64 SLCreditGLCode
        {
            get { return _SLCreditGLCode; }
            set { if (value != _SLCreditGLCode) { _SLCreditGLCode = value; NotifyPropertyChanged("SLCreditGLCode"); } }
        }

        private String _SLCreditGLDesc;
        public String SLCreditGLDesc
        {
            get { return _SLCreditGLDesc; }
            set { if (value != _SLCreditGLDesc) { _SLCreditGLDesc = value; NotifyPropertyChanged("SLCreditGLDesc"); } }
        }

        private String _Explanation;
        public String Explanation
        {
            get { return _Explanation; }
            set { if (value != _Explanation) { _Explanation = value; NotifyPropertyChanged("Explanation"); } }
        }

        private String _SpecialType;
        public String SpecialType
        {
            get { return _SpecialType; }
            set { if (value != _SpecialType) { _SpecialType = value; NotifyPropertyChanged("SpecialType"); } }
        }

        public Boolean HoldOuts { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
