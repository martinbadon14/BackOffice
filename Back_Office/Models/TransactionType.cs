﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class TransactionType
    {
        public int TransTypeID { get; set; }
        public String TransTypeDesc { get; set; }
        public String TransTypeCode { get; set; }
        public String TransTypeModule { get; set; }
    }

    public class SpecialTransType
    {
        public Int16 SpecialTransID { get; set; }
        public String SpecialTransDesc { get; set; }
    }

    public class InterestTransType
    {
        public Int16 InterestTransID { get; set; }
        public String InterestTransDesc { get; set; }
    }

    public class BalanceTypes
    {
        public Int16 BalTypeID { get; set; }
        public String BalTypeDesc { get; set; }
    }

}
