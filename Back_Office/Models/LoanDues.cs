﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    class LoanDues
    {
        public Decimal PrincipalDue { get; set; }
        public Decimal InterestDue { get; set; }
        public Decimal PenaltyDue { get; set; }
        public Decimal PrincipalOver { get; set; }
        public Decimal InterestOver { get; set; }
        public Decimal PenaltyOver { get; set; }
        public Decimal PrincipalBalance { get; set; }

        public String AccountStatus { get; set; }
    }
}
