﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class LockRequest
    {
        public String TableName { get; set; }
        public String ID { get; set; }
    }
}
