﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class Branches
    {
        public Int64 BranchCode { get; set; }
        public String Branch { get; set; }
        public String BranchAbb { get; set; }
    }
}
