﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class SystemConfig
    {
        public Int64 BranchCode { get; set; }
        public DateTime SystemDate { get; set; }
    }
}
