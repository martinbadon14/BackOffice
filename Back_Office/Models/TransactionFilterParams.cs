﻿using System;
using System.ComponentModel;

namespace Back_Office.Models
{
    public class TransactionFilterParams : INotifyPropertyChanged
    {
        Int16 _TransCode;
        public Int16 TransCode
        {
            get
            {
                return _TransCode;
            }
            set
            {
                _TransCode = value;
                OnPropertyChanged("TransCode");
            }
        }

        Int32 _TransYear;
        public Int32 TransYear
        {
            get
            {
                return _TransYear;
            }
            set
            {
                _TransYear = value;
                OnPropertyChanged("TransYear");
            }
        }

        String _FromDate;
        public String FromDate
        {
            get
            {
                return _FromDate;
            }
            set
            {
                _FromDate = value;
                OnPropertyChanged("FromDate");
            }
        }

        String _ToDate;
        public String ToDate
        {
            get
            {
                return _ToDate;
            }
            set
            {
                _ToDate = value;
                OnPropertyChanged("ToDate");
            }
        }

        Int64 _CTLNo;
        public Int64 CTLNo
        {
            get
            {
                return _CTLNo;
            }
            set
            {
                _CTLNo = value;
                OnPropertyChanged("CTLNo");
            }
        }

        String _DocNo;
        public String DocNo
        {
            get
            {
                return _DocNo;
            }
            set
            {
                _DocNo = value;
                OnPropertyChanged("DocNo");
            }
        }

        Boolean _IsInquiry;
        public Boolean IsInquiry
        {
            get
            {
                return _IsInquiry;
            }
            set
            {
                _IsInquiry = value;
                OnPropertyChanged("IsInquiry");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}

