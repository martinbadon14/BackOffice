﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class TransactionCheck : INotifyPropertyChanged
    {
        private Int16 branchCode;
        public Int16 BranchCode
        {
            get { return branchCode; }
            set { if (value != branchCode) { branchCode = value; NotifyPropertyChanged("BranchCode"); } }
        }

        private Int16 officeID;
        public Int16 OfficeID
        {
            get { return officeID; }
            set { if (value != officeID) { officeID = value; NotifyPropertyChanged("OfficeID"); } }
        }

        private Int16 transactionCode;
        public Int16 TransactionCode
        {
            get { return transactionCode; }
            set { if (value != transactionCode) { transactionCode = value; NotifyPropertyChanged("TransactionCode"); } }
        }

        private Int64 ctlNo;
        public Int64 CTLNo
        {
            get { return ctlNo; }
            set { if (value != ctlNo) { ctlNo = value; NotifyPropertyChanged("CTLNo"); } }
        }

        private Byte cociType;
        public Byte CociType
        {
            get { return cociType; }
            set { if (value != cociType) { cociType = value; NotifyPropertyChanged("CociType"); } }
        }

        private String cociTypeDesc;
        public String CociTypeDesc
        {
            get { return cociTypeDesc; }
            set { if (value != cociTypeDesc) { cociTypeDesc = value; NotifyPropertyChanged("CociTypeDesc"); } }
        }

        private Byte checkType;
        public Byte CheckType
        {
            get { return checkType; }
            set { if (value != checkType) { checkType = value; NotifyPropertyChanged("CheckType"); } }
        }

        private String checkTypeDesc;
        public String CheckTypeDesc
        {
            get { return checkTypeDesc; }
            set { if (value != checkTypeDesc) { checkTypeDesc = value; NotifyPropertyChanged("CheckTypeDesc"); } }
        }

        private String checkTypeFormat;
        public String CheckTypeFormat
        {
            get { return checkTypeFormat; }
            set { if (value != checkTypeFormat) { checkTypeFormat = value; NotifyPropertyChanged("CheckTypeFormat"); } }
        }

        private String clearingDays;
        public String ClearingDays
        {
            get { return clearingDays; }
            set { if (value != clearingDays) { clearingDays = value; NotifyPropertyChanged("ClearingDays"); } }
        }

        private String checkNo;
        public String CheckNo
        {
            get { return checkNo; }
            set { if (value != checkNo) { checkNo = value; NotifyPropertyChanged("CheckNo"); } }
        }

        private Int32 bankID;
        public Int32 BankID
        {
            get { return bankID; }
            set { if (value != bankID) { bankID = value; NotifyPropertyChanged("BankID"); } }
        }

        private String bankCodeDesc;
        public String BankCodeDesc
        {
            get { return bankCodeDesc; }
            set { if (value != bankCodeDesc) { bankCodeDesc = value; NotifyPropertyChanged("BankCodeDesc"); } }
        }

        private Int64 acctCode;
        public Int64 AcctCode
        {
            get { return acctCode; }
            set { if (value != acctCode) { acctCode = value; NotifyPropertyChanged("AcctCode"); } }
        }

        private String checkDate;
        public String CheckDate
        {
            get { return checkDate; }
            set { if (value != checkDate) { checkDate = value; NotifyPropertyChanged("CheckDate"); } }
        }

        private Decimal amount;
        public Decimal Amount
        {
            get { return amount; }
            set { if (value != amount) { amount = value; NotifyPropertyChanged("Amount"); } }
        }

        private Byte updTag;
        public Byte UPDTag
        {
            get { return updTag; }
            set { if (value != updTag) { updTag = value; NotifyPropertyChanged("UPDTag"); } }
        }

        private String clearingDate;
        public String ClearingDate
        {
            get { return clearingDate; }
            set { if (value != clearingDate) { clearingDate = value; NotifyPropertyChanged("ClearingDate"); } }
        }

        private String encodedBy;
        public String EncodedBy
        {
            get { return encodedBy; }
            set { if (value != encodedBy) { encodedBy = value; NotifyPropertyChanged("EncodedBy"); } }
        }

        private String postedBy;
        public String PostedBy
        {
            get { return postedBy; }
            set { if (value != postedBy) { postedBy = value; NotifyPropertyChanged("PostedBy"); } }
        }

        private String approvedBy;
        public String ApprovedBy
        {
            get { return approvedBy; }
            set { if (value != approvedBy) { approvedBy = value; NotifyPropertyChanged("ApprovedBy"); } }
        }

        public Byte IsCleared { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
