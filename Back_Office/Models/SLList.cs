﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class SLList
    {
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Byte SLE { get; set; }
        public Byte SLN { get; set; }
    }

    public class SLNSet
    {
        public Byte SLN { get; set; }
        public Int64 AccountCode { get; set; }
        public String AccountDesc { get; set; }
    }
}
