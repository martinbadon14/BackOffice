﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class SLType
    {
        public Int16 SLTypeSLC_CODE { get; set; }
        public Byte SLTypeSLT_CODE { get; set; }
        public String SLTypeM_DESC2 { get; set; }

        public String Description { get; set; }
        public Boolean IsSelected = false;
    }
}
