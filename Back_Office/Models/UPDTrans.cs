﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class UPDTrans
    {
        public Int16 BranchCode { get; set; }
        public Int16 UPDTagID { get; set; }
        public Int64 CtlNo { get; set; }
        public Int16 TransCode { get; set; }
        public int TransYear { get; set; }
        public Boolean IsProbi { get; set; }
        public Boolean IsClosed { get; set; }
        public String TransDate { get; set; }

        public Decimal TotalAmt { get; set; } // For Transaction Logs purposes
        public String Explanation { get; set; }
        public Int64 OfficeID { get; set; }

        public Boolean PostTransDetailsOnly { get; set; }
        public Boolean PostSLDetailsOnly { get; set; }
        public Boolean PostGLTransOnly { get; set; }

        public String ORNo { get; set; }
    }
}
