﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class FlexiSavingsParams
    {
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
        public String Balance { get; set; }
        public String AveDailyBalance { get; set; }
        public String InterestPriorMonth { get; set; }
        public String Withdrawal { get; set; }
        public String Difference { get; set; }
        public String IntRate { get; set; }
        public String DateSetup { get; set; }
        public String CutOff { get; set; }
        public String NoDaysCutOff { get; set; }
        public String IntRate2 { get; set; }
        public String AppIntRate { get; set; }
        public String NoDays { get; set; }
        public String IntAmount { get; set; }
    }

    public class FlexiGLParams
    {
        public Int64 ClientID { get; set; }
        public int TransYear { get; set; }
        public Int64 CTLNo { get; set; }
        public Int64 AccountCode { get; set; }
        public Decimal Amt { get; set; }
        public String SLDate { get; set; }
    }

    public class FlexiSLParams
    {
        public Int64 ClientID { get; set; }
        public int TransYear { get; set; }
        public Int64 CTLNo { get; set; }
        public Int64 AccountCode { get; set; }
        public Decimal Amt { get; set; }
        public String SLDate { get; set; }
    }
}
