﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class SNType
    {
        public byte GLControlNUM { get; set; }
        public Int64 GLControlACCT { get; set; }
        public String COADesc { get; set; }
        public String AcctStatusDesc { get; set; }
    }
}
