﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class AccountNumbers
    {
        public String RefNo { get; set; }
        public String SLTypeDesc { get; set; }
        public String Amount { get; set; }
        public String TRDate { get; set; }
        public String Principal { get; set; }
        public string AccountCode
        {
            get
            {
                Int64 value;
                if (Int64.TryParse(RefNo, out value))
                {
                    return string.Format("{0:D14}", value);
                }
                else
                {
                    return RefNo;
                }
            }
        }
        public Byte StatusID { get; set; }
        public Int64 AcctCode { get; set; }

        public Int64 ClientID { get; set; } //for checking Closed Status purposes

    }


    public class AdjFlags
    {
        public int AdjFlagID { get; set; }
        public String AdjFlagDesc { get; set; }
        public String AdjFlagCode { get; set; }
    }
}
