﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class BankCode
    {
        public Int32 BankID { get; set; }
        public String BankDesc { get; set; }
        public String BankDesc2 { get; set; }
        public Int64 AcctCode { get; set; }
    }
}
