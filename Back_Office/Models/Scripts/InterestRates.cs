﻿using System;
using System.ComponentModel;

namespace Back_Office.Models.Scripts
{
    public class InterestRates : INotifyPropertyChanged
    {
        private Byte _SLC;
        public Byte SLC
        {
            get { return _SLC; }
            set
            {
                _SLC = value;
                OnPropertyChanged("SLC");
            }
        }

        private Byte _SLT;
        public Byte SLT
        {
            get { return _SLT; }
            set
            {
                _SLT = value;
                OnPropertyChanged("SLT");
            }
        }

        private Byte _SLE;
        public Byte SLE
        {
            get { return _SLE; }
            set
            {
                _SLE = value;
                OnPropertyChanged("SLE");
            }
        }

        private Byte _SLN;
        public Byte SLN
        {
            get { return _SLN; }
            set
            {
                _SLN = value;
                OnPropertyChanged("SLN");
            }
        }

        private Int64 _AcctCode;
        public Int64 AcctCode
        {
            get { return _AcctCode; }
            set
            {
                _AcctCode = value;
                OnPropertyChanged("AcctCode");
            }
        }

        private String _COADesc;
        public String COADesc
        {
            get { return _COADesc; }
            set
            {
                _COADesc = value;
                OnPropertyChanged("COADesc");
            }
        }

        private String _MinADB;
        public String MinADB
        {
            get { return _MinADB; }
            set
            {
                _MinADB = value;
                OnPropertyChanged("MinADB");
            }
        }

        private String _MaxADB;
        public String MaxADB
        {
            get { return _MaxADB; }
            set
            {
                _MaxADB = value;
                OnPropertyChanged("MaxADB");
            }
        }

        private String _InterestRate;
        public String InterestRate
        {
            get { return _InterestRate; }
            set
            {
                _InterestRate = value;
                OnPropertyChanged("InterestRate");
            }
        }


        private Decimal _TaxInterestRate;
        public Decimal TaxInterestRate
        {
            get { return _TaxInterestRate; }
            set
            {
                _TaxInterestRate = value;
                OnPropertyChanged("TaxInterestRate");
            }
        }


        public Boolean IsActive { get; set; }

        private Int64 _glAccountCode;
        public Int64 glAccountCode
        {
            get { return _glAccountCode; }
            set
            {
                _glAccountCode = value;
                OnPropertyChanged("glAccountCode");
            }
        }

        private String _AcctDesc;
        public String AcctDesc
        {
            get { return _AcctDesc; }
            set
            {
                _AcctDesc = value;
                OnPropertyChanged("AcctDesc");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

    }


    public class InterestRateFlexi : INotifyPropertyChanged
    {
        private Byte _SLC;
        public Byte SLC
        {
            get { return _SLC; }
            set
            {
                _SLC = value;
                OnPropertyChanged("SLC");
            }
        }

        private Byte _SLT;
        public Byte SLT
        {
            get { return _SLT; }
            set
            {
                _SLT = value;
                OnPropertyChanged("SLT");
            }
        }

        private Byte _SLE;
        public Byte SLE
        {
            get { return _SLE; }
            set
            {
                _SLE = value;
                OnPropertyChanged("SLE");
            }
        }

        private Byte _SLN;
        public Byte SLN
        {
            get { return _SLN; }
            set
            {
                _SLN = value;
                OnPropertyChanged("SLN");
            }
        }

        private Int64 _AcctCode;
        public Int64 AcctCode
        {
            get { return _AcctCode; }
            set
            {
                _AcctCode = value;
                OnPropertyChanged("AcctCode");
            }
        }

        private String _COADesc;
        public String COADesc
        {
            get { return _COADesc; }
            set
            {
                _COADesc = value;
                OnPropertyChanged("COADesc");
            }
        }

        private Int16 _NoDays;
        public Int16 NoDays
        {
            get { return _NoDays; }
            set
            {
                _NoDays = value;
                OnPropertyChanged("NoDays");
            }
        }
        private Byte _SLTRange;
        public Byte SLTRange
        {
            get { return _SLTRange; }
            set
            {
                _SLTRange = value;
                OnPropertyChanged("SLTRange");
            }
        }

        private String _InterestRate;
        public String InterestRate
        {
            get { return _InterestRate; }
            set
            {
                _InterestRate = value;
                OnPropertyChanged("InterestRate");
            }
        }

        private Decimal _TaxInterestRate;
        public Decimal TaxInterestRate
        {
            get { return _TaxInterestRate; }
            set
            {
                _TaxInterestRate = value;
                OnPropertyChanged("TaxInterestRate");
            }
        }

        private Int64 _glAccountCode;
        public Int64 glAccountCode
        {
            get { return _glAccountCode; }
            set
            {
                _glAccountCode = value;
                OnPropertyChanged("glAccountCode");
            }
        }

        private String _AcctDesc;
        public String AcctDesc
        {
            get { return _AcctDesc; }
            set
            {
                _AcctDesc = value;
                OnPropertyChanged("AcctDesc");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
