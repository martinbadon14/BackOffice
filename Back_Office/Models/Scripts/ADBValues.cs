﻿using System;

namespace Back_Office.Models.Scripts
{
    public class ADBValues
    {
        public Decimal TotalBalance { get; set; }
        public int TotalNoDays { get; set; }
    }

    public class ADBDetails
    {
        public Decimal Balance { get; set; }
        public String TransactionDate { get; set; }
        public Int32 DateDiff { get; set; }
        public Decimal CurrentBalance { get; set; }
    }

    public class SLParameters
    {
        public Int16 BranchCode { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Byte SLE { get; set; }
        public Byte SLN { get; set; }
        public Int64 AcctCode { get; set; }
        public String SLDesc { get; set; }
        public Int64 ClientID { get; set; }
        public String DateTo { get; set; }
        public String DateFrom { get; set; }
        public String CutOffDate { get; set; }
        public String RefNo { get; set; }
    }

    public class SLWithADB
    {
        public Int16 BranchCode { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Byte SLE { get; set; }
        public Byte SLN { get; set; }
        public Int64 AcctCode { get; set; }
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
        public String RefNo { get; set; }
        public Decimal ADBValue { get; set; }
        public String TransDate { get; set; }
        public Decimal CurrentBalance { get; set; }
    }

    public class InterestRange
    {
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Decimal MinADB { get; set; }
        public Decimal MaxADB { get; set; }
        public Decimal IntRate { get; set; }
        public Decimal TaxIntRate { get; set; }
        public Int64 IntGLAcct { get; set; }
        public Int64 AcctCode { get; set; }
        public String AcctDesc { get; set; }

    }

    public class InterestRangeForExplanation
    {
        public String COADesc { get; set; }
        public Decimal MinADB { get; set; }
        public Decimal MaxADB { get; set; }
        public Decimal IntRate { get; set; }
        public Decimal TaxIntRate { get; set; }
        public Int64 GLAcctCode { get; set; }
        public String GLAcctDesc { get; set; }
    }

    public class ControlListing
    {
        public Int16 BranchCode { get; set; }
        public Int64 ClientID { get; set; }
        public String ClientName { get; set; }
        public Byte SLC { get; set; }
        public Byte SLT { get; set; }
        public Byte SLE { get; set; }
        public Byte SLN { get; set; }
        public Int64 AcctCode { get; set; }

        public String RefNo { get; set; }
        public Decimal ADBValue { get; set; }
        public String TransDate { get; set; }
        public Decimal CurrentBalance { get; set; }

        public Decimal MinADB { get; set; }
        public Decimal MaxADB { get; set; }
        public Decimal IntRate { get; set; }
        public Decimal TaxIntRate { get; set; }
        public String IntGLAcct { get; set; }
        public String TaxIntGLAcct { get; set; }
        public String AcctDesc { get; set; }
        public Byte IncludeTag { get; set; }
        public Decimal InterestAmt { get; set; }
        public Decimal TaxInterestAmt { get; set; }
    }
}
