﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class GetDateTime
    {
        public String GetDate { get; set; }
        public String GetTime { get; set; }
        public DateTime GetDateTimeValue { get; set; }
    }
}
