﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class UPDTag
    {
        public Int16 UPDTagID { get; set; }
        public String UPDTagDesc { get; set; }
    }
}
