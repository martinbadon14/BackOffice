﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Purchase_Journal
{
    public class PurchaseDetails : INotifyPropertyChanged
    {
        String _TransDate;
        public String TransDate
        {
            get { return _TransDate; }
            set
            {
                _TransDate = value;
                NotifyPropertyChanged("TransDate");
            }
        }

        String _Description;
        public String Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                NotifyPropertyChanged("Description");
            }
        }

        Int64 _CTLNo;
        public Int64 CTLNo
        {
            get { return _CTLNo; }
            set
            {
                _CTLNo = value;
                NotifyPropertyChanged("CTLNo");
            }
        }

        Int64 _VendorTIN;
        public Int64 VendorTIN
        {
            get { return _VendorTIN; }
            set
            {
                _VendorTIN = value;
                NotifyPropertyChanged("VendorTIN");
            }
        }

        String _VendorDesc;
        public String VendorDesc
        {
            get { return _VendorDesc; }
            set
            {
                _VendorDesc = value;
                NotifyPropertyChanged("VendorDesc");
            }
        }

        Decimal _VendorDiscountRate;
        public Decimal VendorDiscountRate
        {
            get { return _VendorDiscountRate; }
            set
            {
                _VendorDiscountRate = value;
                NotifyPropertyChanged("VendorDiscountRate");
            }
        }

        Decimal _VendorVatRate;
        public Decimal VendorVatRate
        {
            get { return _VendorVatRate; }
            set
            {
                _VendorVatRate = value;
                NotifyPropertyChanged("VendorVatRate");
            }
        }

        Decimal _ItemPrice;
        public Decimal ItemPrice
        {
            get { return _ItemPrice; }
            set
            {
                _ItemPrice = value;
                NotifyPropertyChanged("ItemPrice");
            }
        }

        int _Qty;
        public int Qty
        {
            get { return _Qty; }
            set
            {
                _Qty = value;
                NotifyPropertyChanged("Qty");
            }
        }

        String _UnitID;
        public String UnitID
        {
            get { return _UnitID; }
            set
            {
                _UnitID = value;
                NotifyPropertyChanged("UnitID");
            }
        }

        String _UnitDescription;
        public String UnitDescription
        {
            get { return _UnitDescription; }
            set
            {
                _UnitDescription = value;
                NotifyPropertyChanged("UnitDescription");
            }
        }

        Decimal _GrossIncome;
        public Decimal GrossIncome
        {
            get { return _GrossIncome; }
            set
            {
                _GrossIncome = value;
                NotifyPropertyChanged("GrossIncome");
            }
        }

        Decimal _VAT;
        public Decimal VAT
        {
            get { return _VAT; }
            set
            {
                _VAT = value;
                NotifyPropertyChanged("VAT");
            }
        }

        Decimal _Discount;
        public Decimal Discount
        {
            get { return _Discount; }
            set
            {
                _Discount = value;
                NotifyPropertyChanged("Discount");
            }
        }

        Decimal _NetAmt;
        public Decimal NetAmt
        {
            get { return _NetAmt; }
            set
            {
                _NetAmt = value;
                NotifyPropertyChanged("NetAmt");
            }
        }

        Int64 _AccountCode;
        public Int64 AccountCode
        {
            get { return _AccountCode; }
            set
            {
                _AccountCode = value;
                NotifyPropertyChanged("AccountCode");
            }
        }

        String _AccountDesc;
        public String AccountDesc
        {
            get { return _AccountDesc; }
            set
            {
                _AccountDesc = value;
                NotifyPropertyChanged("AccountDesc");
            }
        }

        int _SeqNo;
        public int SeqNo
        {
            get { return _SeqNo; }
            set
            {
                _SeqNo = value;
                NotifyPropertyChanged("SeqNo");
            }
        }

        public Int64 EncodedBy { get; set; }
        public String EncodedByName { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class InsertPurchaseSummary : INotifyPropertyChanged
    {
        Int64 _VendorID;
        public Int64 VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                NotifyPropertyChanged("VendorID");
            }
        }

        String _VendorName;
        public String VendorName
        {
            get { return _VendorName; }
            set
            {
                _VendorName = value;
                NotifyPropertyChanged("VendorName");
            }
        }

        String _VendorAddress;
        public String VendorAddress
        {
            get { return _VendorAddress; }
            set
            {
                _VendorAddress = value;
                NotifyPropertyChanged("VendorAddress");
            }
        }

        String _TransDate;
        public String TransDate
        {
            get { return _TransDate; }
            set
            {
                _TransDate = value;
                NotifyPropertyChanged("TransDate");
            }
        }

        String _ORNo;
        public String ORNo
        {
            get { return _ORNo; }
            set
            {
                _ORNo = value;
                NotifyPropertyChanged("ORNo");
            }
        }

        Int64 _CTLNo;
        public Int64 CTLNo
        {
            get { return _CTLNo; }
            set
            {
                _CTLNo = value;
                NotifyPropertyChanged("CTLNo");
            }
        }

        String _RefNo;
        public String RefNo
        {
            get { return _RefNo; }
            set
            {
                _RefNo = value;
                NotifyPropertyChanged("RefNo");
            }
        }

        String _DocNo;
        public String DocNo
        {
            get { return _DocNo; }
            set
            {
                _DocNo = value;
                NotifyPropertyChanged("DocNo");
            }
        }

        public Int64 EncodedBy { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
