﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Purchase_Journal
{
    public class VendorTINs : INotifyPropertyChanged
    {
        Int64 _VendorID;
        public Int64 VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                OnPropertyChanged("VendorID");
            }
        }

        String _VendorTIN;
        public String VendorTIN
        {
            get { return _VendorTIN; }
            set
            {
                _VendorTIN = value;
                OnPropertyChanged("VendorTIN");
            }
        }

        String _Description;
        public String Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
        }

        Decimal _VAT;
        public Decimal VAT
        {
            get { return _VAT; }
            set
            {
                _VAT = value;
                OnPropertyChanged("VAT");
            }
        }

        Int64 _AttributeID;
        public Int64 AttributeID
        {
            get { return _AttributeID; }
            set
            {
                _AttributeID = value;
                OnPropertyChanged("AttributeID");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
