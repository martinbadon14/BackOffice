﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Purchase_Journal
{
    public class CalculationValues
    {
        public Decimal Gross { get; set; }
        public Decimal GrossAfterDiscount { get; set; }
        public Decimal GrossAfterVAT { get; set; }
        public Decimal Net { get; set; }
    }
}
