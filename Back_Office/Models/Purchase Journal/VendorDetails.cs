﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models.Purchase_Journal
{
    public class VendorDetails : INotifyPropertyChanged
    {
        Int64 _VendorID;
        public Int64 VendorID
        {
            get { return _VendorID; }
            set
            {
                _VendorID = value;
                OnPropertyChanged("VendorID");
            }
        }

        String _VendorName;
        public String VendorName
        {
            get { return _VendorName; }
            set
            {
                _VendorName = value;
                OnPropertyChanged("VendorName");
            }
        }

        String _VendorAddress;
        public String VendorAddress
        {
            get { return _VendorAddress; }
            set
            {
                _VendorAddress = value;
                OnPropertyChanged("VendorAddress");
            }
        }

        String _Contact1;
        public String Contact1
        {
            get { return _Contact1; }
            set
            {
                _Contact1 = value;
                OnPropertyChanged("Contact1");
            }
        }

        String _Contact2;
        public String Contact2
        {
            get { return _Contact2; }
            set
            {
                _Contact2 = value;
                OnPropertyChanged("Contact2");
            }
        }

        String _Contact3;
        public String Contact3
        {
            get { return _Contact3; }
            set
            {
                _Contact3 = value;
                OnPropertyChanged("Contact3");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }

    public class vendorParams
    {
        public Int64 VendorID { get; set; }
        public String VendorName { get; set; }
    }

}
