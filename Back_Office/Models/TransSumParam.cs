﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class TransSumParam
    {
        public Int16 BranchCode { get; set; }
        public Int64 CtlNo { get; set; }
        public Int16 TransCode { get; set; }
        public String DocNo { get; set; }
        public String RandomToken { get; set; }
    }
}
