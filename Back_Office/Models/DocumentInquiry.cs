﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class DocumentInquiry
    {
        public Int16 BranchCode { get; set; }
        public String TransYear { get; set; }
        public Int16 TransCode { get; set; }
        public String TransAbbv { get; set; }
        public Int64 CtlNo { get; set; }
        public String DocNo { get; set; }
        public String TransDate { get; set; }
        public Decimal Amount { get; set; }
        public String EncodedBy { get; set; }
        public String UPDTag { get; set; }
        public String ApprvBy { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public String MiddleName { get; set; }
        public string ClientName
        {
            get
            {
                if (!String.IsNullOrEmpty(LastName) && !String.IsNullOrEmpty(FirstName))
                {
                    return string.Format("{0}, {1} {2}", LastName, FirstName, MiddleName);
                }
                else return " ";
            }

            set { }
        }

        public String Explanation { get; set; }
        public String RowColor { get; set; }
        public String FocusColor { get; set; }

        public Boolean Selected { get; set; }

        public Int64 OfficeID { get; set; }
        public String OfficeAbv { get; set; }
        public Int64 ORCTLNo { get; set; }
        public String DatePosted { get; set; }
    }




}
