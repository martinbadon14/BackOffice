﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class SLEType
    {
        public Byte slecode { get; set; }
        public string sledescription { get; set; }
    }
}
