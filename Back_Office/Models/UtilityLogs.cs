﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class UtilityLogs
    {
        public Int64 UserID { get; set; }
        public Int64 OverrideID { get; set; }
        public Int64 ActivityListID { get; set; }
        public DateTime DateTimeAdded { get; set; } //SqlDateTime
        public Int64 ClientID { get; set; }
        public Int64 ItemID { get; set; }
        public Int64 VendorID { get; set; }
        public Int64 POID { get; set; }
        public Int64 ActivityFieldID { get; set; }
        public String ActivityInfo { get; set; }
        public String FromData { get; set; }
        public String ToData { get; set; }
        public String FromImage { get; set; }
        public String ToImage { get; set; }
        public Int64 BranchCode { get; set; }
        public Int64 SLC_Code { get; set; }
        public Int64 SLT_Code { get; set; }
        public String RefNo { get; set; }
        public Int64 TransCode { get; set; }
        public Int64 CTLNo { get; set; }
        public String TransDate { get; set; } //SystemDate
        public String TransTime { get; set; } //
        public Decimal Amt { get; set; }
        public Int64 LoanAppID { get; set; }
    }

    public class ActivityLogsParams
    {
        public Int64 BranchCode { get; set; }
        public Int64 TR_Code { get; set; }
        public Int64 TR_CTLNO { get; set; }
        public String TR_Date { get; set; } //SystemDate
        public Int32 ModuleID { get; set; }

        public String ActivityListIDs { get; set; }
    }

}
