﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class Office
    {
        public Int64 OfficeID { get; set; }
        public String OfficeName { get; set; }
    }
}
