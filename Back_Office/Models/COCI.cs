﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    class COCI
    {
        public Int32 COCITypeID { get; set; }
        public String COCIDesc { get; set; }
    }
}
