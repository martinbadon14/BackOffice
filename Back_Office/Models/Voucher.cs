﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class Voucher
    {
        public Int64 ClientID { get; set; }
        public Int64 BranchCode { get; set; }
        public Int32 TransactionCode { get; set; }
        public Int64 CTLNo { get; set; }
        public Boolean IsVoucher { get; set; }
    }
}
