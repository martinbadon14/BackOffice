﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class GLTransactions
    {
        public Int16 BranchCode { get; set; }
        public Int64 AccountCode { get; set; }
        public String TRDate { get; set; }
        public Decimal Debit_Amount { get; set; }
        public Decimal Credit_Amount { get; set; }
        public Decimal DRBalanceB4 { get; set; }
        public Decimal CRBalanceB4 { get; set; }
        public Decimal TransSumBalanceB4 { get; set; }
    }
}
