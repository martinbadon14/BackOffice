﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class DocInquiryParameters : INotifyPropertyChanged
    {
        //public Int16 BranchCode { get; set; }
        //public Int16 TransCode { get; set; }
        //public Int64 CtlNo { get; set; }
        //public Int32 Year { get; set; }

        //public String DocNo { get; set; }
        //public String FromDate { get; set; }
        //public String ToDate { get; set; }
        //public Int64 ORCTLNo { get; set; }
        Int16 _BranchCode;
        public Int16 BranchCode {
            get
            {
                return _BranchCode;
            }
            set
            {
                _BranchCode = value;
                OnPropertyChanged("BranchCode");
            }
        }

        Int16 _TransCode;
        public Int16 TransCode
        {
            get
            {
                return _TransCode;
            }
            set
            {
                _TransCode = value;
                OnPropertyChanged("TransCode");
            }
        }

        Int32 _TransYear;
        public Int32 TransYear
        {
            get
            {
                return _TransYear;
            }
            set
            {
                _TransYear = value;
                OnPropertyChanged("TransYear");
            }
        }

        String _FromDate;
        public String FromDate
        {
            get
            {
                return this.DateFormat(_FromDate);
            }
            set
            {
                _FromDate = value;
                OnPropertyChanged("FromDate");
            }
        }

        String _ToDate;
        public String ToDate
        {
            get
            {
                return this.DateFormat(_ToDate);
            }
            set
            {
                _ToDate = value;
                OnPropertyChanged("ToDate");
            }
        }

        Int64 _CTLNo;
        public Int64 CTLNo
        {
            get
            {
                return _CTLNo;
            }
            set
            {
                _CTLNo = value;
                OnPropertyChanged("CTLNo");
            }
        }

        String _DocNo;
        public String DocNo
        {
            get
            {
                return _DocNo;
            }
            set
            {
                _DocNo = value;
                OnPropertyChanged("DocNo");
            }
        }

        Int64 _ORCTLNo;
        public Int64 ORCTLNo
        {
            get
            {
                return _ORCTLNo;
            }
            set
            {
                _ORCTLNo = value;
                OnPropertyChanged("ORCTLNo");
            }
        }

        Int64 _OfficeID;
        public Int64 OfficeID
        {
            get
            {
                return _OfficeID;
            }
            set
            {
                _OfficeID = value;
                OnPropertyChanged("OfficeID");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private String DateFormat(String Date)
        {
            if (Date == null)
            {
                Date = "";
            }

            String temp = Regex.Replace(Date, "[^0-9.]", "");
            if (temp.Length >= 8)
            {
                temp = temp.Substring(0, 8);
            }
            else
            {
                int underscore = 8 - temp.Length;

                while (underscore > 0)
                {
                    temp += "_";
                    underscore--;
                }
            }
            temp = temp.Insert(2, "/");
            temp = temp.Insert(5, "/");

            return temp;
        }
    }
}
