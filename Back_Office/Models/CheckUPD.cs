﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class CheckUPD
    {
        public Int16 BranchCode { get; set; }
        public Int16 TransactionCode { get; set; }
        public Int64 ControlNo { get; set; }
    }
}
