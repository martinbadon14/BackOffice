﻿using Back_Office.Models.Year_End;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Back_Office.Models
{
    public class LRControlList
    {
        public Int16 BranchCode { get; set; }
        public Int64 ClientID { get; set; }
        public Byte ClientChkID { get; set; }
        public String ClientName { get; set; }
        public String SLDescription { get; set; }
        public String RefNo { get; set; }
        public Decimal RemainingBal { get; set; }
        public String SetupDate { get; set; }
        public String MaturityDate { get; set; }
        public String AccountStatusDesc { get; set; }

        public String ClientIDFormat
        {
            get
            {
                if (ClientChkID == 0 && ClientID == 0)
                {
                    return null;
                }
                else return string.Format("{0}-{1}-{2}", String.Format("{0:D2}", BranchCode), String.Format("{0:D8}", ClientID), ClientChkID);
            }

            set { }
        }
    }



    public class IntDepControlList
    {
        public Int16 BranchCode { get; set; }
        public Int64 ClientID { get; set; }
        public Byte ClientChkID { get; set; }
        public String ClientName { get; set; }
        public String SLDescription { get; set; }
        public Decimal AvgBal { get; set; }
        public Decimal IntRate { get; set; }
        public Decimal IntExp { get; set; }
        public Decimal TaxWithHeld { get; set; }
        public Decimal NetCredit { get; set; }

        public String ClientIDFormat
        {
            get
            {
                if (ClientChkID == 0 && ClientID == 0)
                {
                    return null;
                }
                else return string.Format("{0}-{1}-{2}", String.Format("{0:D2}", BranchCode), String.Format("{0:D8}", ClientID), ClientChkID);
            }

            set { }
        }

        public String RefNo_Format { get; set; }
    }

    public class ControlListHolder
    {
        public List<LRControlList> LRControlList { get; set; }
        public List<IntDepControlList> IntDepControlList { get; set; }
        public List<AvgShareCapParameters> AvgShareCapitalControlList { get; set; }
    }
}