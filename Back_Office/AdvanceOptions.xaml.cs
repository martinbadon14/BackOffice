﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for AdvanceOptions.xaml
    /// </summary>
    public partial class AdvanceOptions : Window
    {
        MainWindow mw;

        public AdvanceOptions(MainWindow mww)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    Close();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void ProcessOptions()
        {
            Boolean NoAccess = false;

            if (!SaveTransTab.IsSelected)
            {
                if (App.CRUXAPI.isAllowed("Advance_Posting"))
                {
                    if (PostTransCheckBox.IsChecked == true)
                    {
                        this.mw.TransDetailPosting = true;
                    }

                    if (SLDetailsCheckBox.IsChecked == true)
                    {
                        this.mw.SLDetailPosting = true;
                    }

                    if (GLCheckBox.IsChecked == true)
                    {
                        this.mw.GLTaransOnly = true;
                    }
                }
                else
                {
                    NoAccess = true;
                }
            }
            else
            {
                if (EnableWithDiffCheckBox.IsChecked == true)
                {
                    if (App.CRUXAPI.isAllowed("save_journal_with_diff"))
                    {
                        this.mw.AllowSaveWithDifference = true;
                    }
                    else
                    {
                        NoAccess = true;
                    }
                }
                if (ReferenceCheckBox.IsChecked == true)
                {
                    if (App.CRUXAPI.isAllowed("enable_refNo_input"))
                    {
                        this.mw.RefNoField.IsReadOnly = false;
                    }
                    else
                    {
                        NoAccess = true;
                    }
                }
            }

            if (NoAccess)
            {
                MessageBox.Show(this, "Need access for this type of option. Please contact Administrator.");
            }

        }

        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult result;

                if (SaveTransTab.IsSelected)
                {
                    result = MessageBox.Show(this, "Enable saving transaction with difference. Continue??", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                }
                else
                {
                    result = MessageBox.Show(this, "Enable advance posting. Continue??", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                }

                switch (result)
                {
                    case MessageBoxResult.Yes:
                        ProcessOptions();
                        this.Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }
    }
}
