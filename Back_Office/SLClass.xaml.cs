﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Web.Script.Serialization;
using Back_Office.Models;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using Back_Office.Models.Month_End.Special_Transactions;
using static Back_Office.BMS;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Data;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for SLSelect.xaml
    /// </summary>
    public partial class SLClass : Window
    {
        public Byte temp = 0;
        public MainWindow mww;
        public BMS bww;
        private String OtherMod = "";

        int currentRow = 0, currentColumn = 1;
        Models.SLClass Selected;
        string SearchText = string.Empty;
        private ICollectionView MyData;


        public SLClass(MainWindow mw)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mww = mw;
            FillDataGrid();
        }

        public SLClass(BMS bw, String TextBoxName)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.bww = bw;
            FillDataGrid(TextBoxName);
            OtherMod = TextBoxName;
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                if (currentRow > 0)
                {
                    try
                    {
                        int previousIndex = SLClassDataGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        SLClassDataGrid.SelectedIndex = previousIndex;
                        SLClassDataGrid.ScrollIntoView(SLClassDataGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }
                }

            }
            else if (e.Key == Key.Down)
            {
                if (currentRow < SLClassDataGrid.Items.Count - 1)
                {

                    try
                    {
                        int nextIndex = SLClassDataGrid.SelectedIndex + 1;
                        if (nextIndex > SLClassDataGrid.Items.Count - 1) return;
                        SLClassDataGrid.SelectedIndex = nextIndex;
                        SLClassDataGrid.ScrollIntoView(SLClassDataGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }

                } // end if (this.SelectedOverride > 0)

            } // end else if (e.Key == Key.Down)

            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }


        private void FillDataGrid(String TextBoxName = null)
        {
            CRUXLib.Response Response;

            if (!String.IsNullOrEmpty(TextBoxName))
            {
                Response = App.CRUXAPI.request("backoffice/slclassBMS", "");

                if (Response.Status == "SUCCESS")
                {
                    List<Models.SLClass> slclass = new JavaScriptSerializer().Deserialize<List<Models.SLClass>>(Response.Content);
                    SLClassDataGrid.ItemsSource = slclass.OrderBy(c => c.sldescription);

                    MyData = CollectionViewSource.GetDefaultView(slclass);
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            else
            {
                Response = App.CRUXAPI.request("slclass/backoffice", "");

                if (Response.Status == "SUCCESS")
                {
                    List<Models.SLClass> slclass = new JavaScriptSerializer().Deserialize<List<Models.SLClass>>(Response.Content);
                    SLClassDataGrid.ItemsSource = slclass;

                    MyData = CollectionViewSource.GetDefaultView(slclass);
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
        }


        private void dataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            function(e);
        }


        private void dataGrid_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                function(e);
            }
        }


        private void function(KeyEventArgs e = null) //double Check
        {
            try
            {
                if (SLClassDataGrid.SelectedItem == null) return;
                var selectedSLC = SLClassDataGrid.SelectedItem as Models.SLClass;

                if (String.IsNullOrEmpty(OtherMod))
                {
                    TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;
                    tt.SLC_Code = selectedSLC.slcode.ToString();
                    this.Close();
                    this.mww.DG_GeneralJournal.Focus();
                    this.mww.DG_GeneralJournal.CurrentCell = new DataGridCellInfo(this.mww.DG_GeneralJournal.SelectedItem, this.mww.DG_GeneralJournal.Columns[2]);
                }
                else
                {
                    SpecialTransactionDataCon bb = (SpecialTransactionDataCon)this.bww.DataCon;

                    if (OtherMod == "SLCTextBox")
                    {
                        bb.InsertSpecialTrans.SLC = selectedSLC.slcode.ToString();
                        this.Close();
                        this.bww.SLTTextBox.Focus();
                    }
                    else if (OtherMod == "DebitSLCTextBox")
                    {
                        bb.InsertSpecialTrans.SLCDebit = selectedSLC.slcode.ToString();
                        this.Close();
                        this.bww.DebitSLTTextBox.Focus();
                    }
                    else if (OtherMod == "CreditSLCTextBox")
                    {
                        bb.InsertSpecialTrans.SLCCredit = selectedSLC.slcode.ToString();
                        this.Close();
                        this.bww.CreditSLTTextBox.Focus();
                    }
                }

            }

            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private bool FilterData(object item)
        {
            var value = (Models.SLClass)item;

            if (value.slcode == 0 || String.IsNullOrEmpty(value.sldescription))
                return false;
            else
                return value.sldescription.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.slcode).Contains(SearchText);
        }


        private void function(MouseButtonEventArgs e) //double Check
        {
            try
            {
                if (SLClassDataGrid.SelectedItem == null) return;
                var selectedSLC = SLClassDataGrid.SelectedItem as Models.SLClass;

                if (String.IsNullOrEmpty(OtherMod))
                {
                    TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;
                    tt.SLC_Code = selectedSLC.slcode.ToString();
                    this.Close();
                    this.mww.DG_GeneralJournal.Focus();
                    this.mww.DG_GeneralJournal.CurrentCell = new DataGridCellInfo(this.mww.DG_GeneralJournal.SelectedItem, this.mww.DG_GeneralJournal.Columns[2]);
                }
                else
                {
                    SpecialTransactionDataCon bb = (SpecialTransactionDataCon)this.bww.DataCon;

                    if (OtherMod == "SLCTextBox")
                    {
                        bb.InsertSpecialTrans.SLC = selectedSLC.slcode.ToString();
                        this.Close();
                        this.bww.SLTTextBox.Focus();
                    }
                    else if (OtherMod == "DebitSLCTextBox")
                    {
                        bb.InsertSpecialTrans.SLCDebit = selectedSLC.slcode.ToString();
                        this.Close();
                        this.bww.DebitSLTTextBox.Focus();
                    }
                    else if (OtherMod == "CreditSLCTextBox")
                    {
                        bb.InsertSpecialTrans.SLCCredit = selectedSLC.slcode.ToString();
                        this.Close();
                        this.bww.CreditSLTTextBox.Focus();
                    }

                }

            }

            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTextBox.Focus();
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Selected = (Models.SLClass)SLClassDataGrid.Items[currentRow];

                function(e);
            }
        }


        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Selected = (Models.SLClass)SLClassDataGrid.Items[currentRow];
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            MyData.Filter = FilterData;
            SLClassDataGrid.SelectedIndex = 0;
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }
    }
}
