﻿using Back_Office.Models.Purchase_Journal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.BIR.Purchase_Journal
{
    /// <summary>
    /// Interaction logic for VendorTIN.xaml
    /// </summary>
    public partial class VendorTIN : Window
    {
        MainWindow mw;
        private ICollectionView MyData;
        String SearchText = string.Empty;
        String DefaultColumn = "";
        int currentRow = 0, currentColumn = 1;
        VendorTINDataCon DataCon = new VendorTINDataCon();

        public VendorTIN(MainWindow mww, Int64 VendorID)
        {
            InitializeComponent();
            DataCon.vendorTINs = new List<VendorTINs>();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
            FillDataGrid(VendorID);
            this.DataContext = DataCon;
            searchTextBox.Focus();
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        try
                        {
                            int previousIndex = VendorsTINDataGrid.SelectedIndex - 1;
                            if (previousIndex < 0) return;
                            VendorsTINDataGrid.SelectedIndex = previousIndex;
                            VendorsTINDataGrid.ScrollIntoView(VendorsTINDataGrid.Items[currentRow]);

                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }
                    }

                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < VendorsTINDataGrid.Items.Count - 1)
                    {
                        try
                        {
                            int nextIndex = VendorsTINDataGrid.SelectedIndex + 1;
                            if (nextIndex > VendorsTINDataGrid.Items.Count - 1) return;
                            VendorsTINDataGrid.SelectedIndex = nextIndex;
                            VendorsTINDataGrid.ScrollIntoView(VendorsTINDataGrid.Items[currentRow]);
                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }

                    } // end if (this.SelectedOverride > 0)

                } // end else if (e.Key == Key.Down)

                else if (e.Key == Key.Escape)
                {
                    this.Close();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void FillDataGrid(Int64 VendorID)
        {
            vendorParams vendor = new vendorParams();
            vendor.VendorID = VendorID;

            String parsed = new JavaScriptSerializer().Serialize(vendor);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/selectVendorTINs", parsed);

            if (Response.Status == "SUCCESS")
            {
                this.DataCon.vendorTINs = new JavaScriptSerializer().Deserialize<List<VendorTINs>>(Response.Content);

                if (this.DataCon.vendorTINs.Count != 0)
                {
                    VendorsTINDataGrid.SelectedItem = this.DataCon.vendorTINs.First();
                }
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void SelectTIN()
        {
            if (VendorsTINDataGrid.SelectedItem == null) return;
            var selectedTIN = VendorsTINDataGrid.SelectedItem as VendorTINs;

            PurchaseDetails purchase = (PurchaseDetails)this.mw.PurchaseDataGrid.SelectedItem;
            purchase.VendorTIN = selectedTIN.AttributeID;
            purchase.VendorDesc = selectedTIN.Description;
            purchase.VendorVatRate = selectedTIN.VAT;

            Close();
            this.mw.MoveToNextUIElement();
            this.mw.MoveToNextUIElement();
        }

        private void VendorsTINDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SelectTIN();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void VendorsTINDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SelectTIN();
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }

            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

        private void searchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (VendorsTINDataGrid.Items != null)
                    {
                        SelectTIN();
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private bool FilterData(object item)
        {
            var value = (VendorTINs)item;

            try
            {
                return value.Description.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.VAT).Contains(SearchText);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox t = sender as TextBox;
                SearchText = t.Text.ToString();
                MyData.Filter = FilterData;

                VendorsTINDataGrid.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }
        private class VendorTINDataCon : INotifyPropertyChanged
        {
            List<VendorTINs> _vendorTINs;
            public List<VendorTINs> vendorTINs
            {
                get { return _vendorTINs; }
                set
                {
                    _vendorTINs = value;
                    OnPropertyChanged("vendorTINs");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
                }
            }
        }
    }
}
