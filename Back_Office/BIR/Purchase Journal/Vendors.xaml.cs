﻿using Back_Office.Models.Purchase_Journal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.BIR.Purchase_Journal
{
    /// <summary>
    /// Interaction logic for Vendors.xaml
    /// </summary>
    public partial class Vendors : Window
    {
        MainWindow mw;
        _VendorDetails DataCon = new _VendorDetails();
        private ICollectionView MyData;
        String SearchText = string.Empty;
        String DefaultColumn = "";
        int currentRow = 0, currentColumn = 1;

        public Vendors(MainWindow mww, String ID, String VendorName)
        {
            InitializeComponent();
            this.mw = mww;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            DataCon.vendors = new List<VendorDetails>();

            if (!String.IsNullOrEmpty(ID))
            {
                FillDataGrid(Convert.ToInt64(ID), VendorName);
            }
            else
            {
                FillDataGrid(0, VendorName);
            }

            this.DataContext = DataCon;

            searchTextBox.Focus();

        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        try
                        {
                            int previousIndex = VendorsDataGrid.SelectedIndex - 1;
                            if (previousIndex < 0) return;
                            VendorsDataGrid.SelectedIndex = previousIndex;
                            VendorsDataGrid.ScrollIntoView(VendorsDataGrid.Items[currentRow]);

                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }
                    }

                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < VendorsDataGrid.Items.Count - 1)
                    {
                        try
                        {
                            int nextIndex = VendorsDataGrid.SelectedIndex + 1;
                            if (nextIndex > VendorsDataGrid.Items.Count - 1) return;
                            VendorsDataGrid.SelectedIndex = nextIndex;
                            VendorsDataGrid.ScrollIntoView(VendorsDataGrid.Items[currentRow]);
                        }
                        catch (Exception ex)
                        {
                            e.Handled = true;
                        }

                    } // end if (this.SelectedOverride > 0)

                } // end else if (e.Key == Key.Down)

                else if (e.Key == Key.Escape)
                {
                    this.Close();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void FillDataGrid(Int64 ID, String Name)
        {
            vendorParams vendor = new vendorParams();
            vendor.VendorID = ID;
            vendor.VendorName = Name;

            String parsed = new JavaScriptSerializer().Serialize(vendor);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/selectVendors", parsed);

            if (Response.Status == "SUCCESS")
            {
                this.DataCon.vendors = new JavaScriptSerializer().Deserialize<List<VendorDetails>>(Response.Content);

                if (this.DataCon.vendors.Count != 0)
                {
                    VendorsDataGrid.SelectedItem = this.DataCon.vendors.First();
                }

                MyData = CollectionViewSource.GetDefaultView(this.DataCon.vendors);
            }
        }

        private void SelectVendor()
        {
            if (VendorsDataGrid.SelectedItem == null) return;
            var selectedVendor = VendorsDataGrid.SelectedItem as VendorDetails;

            this.mw.DataCon.PurchaseSummary.VendorID = selectedVendor.VendorID;

            this.mw.DataCon.TransactionSummary.ClientIDFormatTransSum = String.Format("{0:D4}", selectedVendor.VendorID);
            this.mw.DataCon.TransactionSummary.ClientNameTransSum = selectedVendor.VendorName;
            this.mw.DataCon.TransactionSummary.ClientOrTransSum = selectedVendor.VendorAddress;
            this.Close();

        }

        private void VendorsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SelectVendor();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void VendorsDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                SelectVendor();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

        private void searchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (VendorsDataGrid.Items != null)
                    {
                        SelectVendor();
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private bool FilterData(object item)
        {
            var value = (VendorDetails)item;

            try
            {
                return  value.VendorAddress.ToLower().StartsWith(SearchText.ToLower())
                        || value.VendorName.ToLower().StartsWith(SearchText.ToLower()) || Convert.ToString(value.VendorID).Contains(SearchText);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox t = sender as TextBox;
                SearchText = t.Text.ToString();
                MyData.Filter = FilterData;

                VendorsDataGrid.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        public class _VendorDetails : INotifyPropertyChanged
        {
            List<VendorDetails> _vendors;
            public List<VendorDetails> vendors
            {
                get { return _vendors; }
                set
                {
                    _vendors = value;
                    OnPropertyChanged("vendors");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
                }
            }
        }
    }
}
