﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.BIR.Purchase_Journal
{
    /// <summary>
    /// Interaction logic for VendorSearch.xaml
    /// </summary>
    public partial class VendorSearch : Window
    {
        MainWindow mw;
        Vendors _vendor;

        public VendorSearch(MainWindow mww)
        {
            InitializeComponent();
            this.mw = mww;
            vendorIDTextBox.Focus();

        }

        private void vendorNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    SelectVendor();
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }

        }

        private void vendorTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string textName = (sender as TextBox).Name.ToString();

                if (textName == "vendorIDTextBox")
                {
                    vendorNameTextBox.Clear();
                }
                else
                {
                    vendorIDTextBox.Clear();
                }

                if (e.Key == Key.Enter)
                {
                    if (!String.IsNullOrEmpty(vendorIDTextBox.Text) || !String.IsNullOrEmpty(vendorNameTextBox.Text))
                    {
                        SelectVendor();
                    }
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void SelectVendor()
        {
            _vendor = new Vendors(this.mw, this.vendorIDTextBox.Text, this.vendorNameTextBox.Text);
            _vendor.Owner = this.mw;
            Close();
            _vendor.ShowDialog();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(vendorIDTextBox.Text) || !String.IsNullOrEmpty(vendorNameTextBox.Text))
                {
                    SelectVendor();
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }
    }
}
