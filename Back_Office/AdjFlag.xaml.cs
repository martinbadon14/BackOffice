﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for AdjFlag.xaml
    /// </summary>
    public partial class AdjFlag : Window
    {
        MainWindow mww;
        int currentRow = 0;
        string SearchText = string.Empty;
        private ICollectionView MyData;

        public AdjFlag(MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;

            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            FillAdjFlag();
            searchTextBox.Focus();
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    MessageBoxResult result = MessageBox.Show("Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            Close();
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;

                    } //end switch
                }

                else if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        AdjFlagDataGrid.Focus();

                        int previousIndex = AdjFlagDataGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        currentRow = previousIndex;
                        AdjFlagDataGrid.SelectedIndex = previousIndex;
                        AdjFlagDataGrid.ScrollIntoView(AdjFlagDataGrid.Items[currentRow]);
                    }
                }

                else if (e.Key == Key.Down)
                {
                    if (currentRow < AdjFlagDataGrid.Items.Count - 1)
                    {
                        AdjFlagDataGrid.Focus();

                        int nextIndex = AdjFlagDataGrid.SelectedIndex + 1;
                        if (nextIndex > AdjFlagDataGrid.Items.Count - 1) return;
                        currentRow = nextIndex;
                        AdjFlagDataGrid.SelectedIndex = nextIndex;
                        AdjFlagDataGrid.ScrollIntoView(AdjFlagDataGrid.Items[currentRow]);
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void FillAdjFlag()
        {
            CRUXLib.Response Response;

            Response = App.CRUXAPI.request("AdjFlag/backoffice", "");

            if (Response.Status == "SUCCESS")
            {
                List<AdjFlags> adjFlagsList = new JavaScriptSerializer().Deserialize<List<AdjFlags>>(Response.Content);

                AdjFlagDataGrid.ItemsSource = adjFlagsList;

                if (adjFlagsList.Count != 0)
                {
                    AdjFlagDataGrid.SelectedItem = adjFlagsList.FirstOrDefault();
                }

                MyData = CollectionViewSource.GetDefaultView(adjFlagsList);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }


        private void function()
        {
            if (AdjFlagDataGrid.SelectedItem == null) return;
            var selectedPerson = AdjFlagDataGrid.SelectedItem as AdjFlags;

            TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;
            tt.AdjFlagCode = selectedPerson.AdjFlagCode;
            tt.AdjFlag = Convert.ToByte(selectedPerson.AdjFlagID);
            this.Close();
            this.mww.MoveToNextUIElement();
        }


        private void searchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    function();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void AdjFlagDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                function();
            }
            catch (System.Exception)
            {
                e.Handled = true;
            }
        }


        private void AdjFlagDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                {
                    function();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void searchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            MyData.Filter = FilterData;
            AdjFlagDataGrid.SelectedIndex = 0;
        }


        private bool FilterData(object item)
        {
            var value = (Models.AdjFlags)item;

            if (value.AdjFlagID == 0 || String.IsNullOrEmpty(value.AdjFlagDesc) || String.IsNullOrEmpty(value.AdjFlagCode))
                return false;

            else
                return Convert.ToString(value.AdjFlagID).Contains(SearchText) ||
                    value.AdjFlagDesc.ToLower().StartsWith(SearchText.ToLower()) ||
                    value.AdjFlagCode.ToLower().StartsWith(SearchText.ToLower());
        }
    }
}
