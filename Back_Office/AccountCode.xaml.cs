﻿using Back_Office.Helper;
using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for AccountCode.xaml
    /// </summary>
    public partial class AccountCode : Window
    {

        private static String Savings = "Savings Deposit";
        private static String Loan = "Loan";
        private static String AcctRec = "Accts Receivable";
        private static String TimeDep = "Time Deposit";
        private static String AcctPay = "Accts Payable";
        private static String ShareCap = "Share Capital";
        private static String OtherSL = "Other/SL";


        private static Byte LoanSLC = 12, AR = 13, AP = 26;
        public MainWindow mww;
        int currentRow = 0;
        private Byte SLC_Code = 0;


        private String CheckDesc(Byte SLC)
        {
            String Description = "";

            switch (SLC)
            {
                case 12:
                    Description = Loan;
                    break;
                case 13:
                    Description = AcctRec;
                    break;
                case 22:
                    Description = Savings;
                    break;
                case 24:
                    Description = TimeDep;
                    break;
                case 26:
                    Description = AcctPay;
                    break;
                case 29:
                    Description = OtherSL;
                    break;
                case 41:
                    Description = OtherSL;
                    break;
                case 31:
                    Description = ShareCap;
                    break;

                default:
                    break;

            }

            return Description;
        }


        public AccountCode(MainWindow mw, Int64 clientID, Byte SLC, Byte SLT, Byte SLE, String SLDate, Byte SLN)
        {
            InitializeComponent();
            this.mww = mw;
            if (SLC == AP || SLC == AR || SLC == LoanSLC)
            {
                WithPrincipal(CheckDesc(SLC));
            }
            else
            {
                Others(CheckDesc(SLC));
            }

            SLC_Code = SLC;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            FillDataGrid(clientID, SLC, SLT, SLE, SLDate, SLN);
            searchTextBox.Focus();

        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        accountCodeDataGrid.Focus();

                        int previousIndex = accountCodeDataGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        accountCodeDataGrid.SelectedIndex = previousIndex;
                        accountCodeDataGrid.ScrollIntoView(accountCodeDataGrid.Items[currentRow]);
                    }
                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < accountCodeDataGrid.Items.Count - 1)
                    {
                        accountCodeDataGrid.Focus();

                        int nextIndex = accountCodeDataGrid.SelectedIndex + 1;
                        if (nextIndex > accountCodeDataGrid.Items.Count - 1) return;
                        accountCodeDataGrid.SelectedIndex = nextIndex;
                        accountCodeDataGrid.ScrollIntoView(accountCodeDataGrid.Items[currentRow]);
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public void WithPrincipal(String Description)
        {
            Title = Description + " Acct Number Inquiry";
            DataGridTextColumn SLTypeDesc = new DataGridTextColumn();
            SLTypeDesc.Header = "S/L Type";
            SLTypeDesc.Binding = new Binding("SLTypeDesc");

            SLTypeDesc.Width = 150;
            SLTypeDesc.IsReadOnly = true;
            accountCodeDataGrid.Columns.Add(SLTypeDesc);

            DataGridTextColumn datAccountCode = new DataGridTextColumn();
            datAccountCode.Header = "Account Code";
            datAccountCode.Binding = new Binding("AccountCode");
            datAccountCode.Width = 150;
            datAccountCode.IsReadOnly = true;
            accountCodeDataGrid.Columns.Add(datAccountCode);

            DataGridTextColumn datDate = new DataGridTextColumn();
            datDate.Header = "Date";
            datDate.Binding = new Binding("TRDate");
            datDate.Width = 100;
            datDate.IsReadOnly = true;
            accountCodeDataGrid.Columns.Add(datDate);

            DataGridTextColumn datPrincipal = new DataGridTextColumn();
            datPrincipal.Header = "Principal";
            datPrincipal.Binding = new Binding("Principal");
            datPrincipal.Binding.StringFormat = "{0:#,##0.00;(#,##0.00);0.00}";
            datPrincipal.Width = 100;
            datPrincipal.IsReadOnly = true;

            datPrincipal.ElementStyle = ElementStyleAmt();
            accountCodeDataGrid.Columns.Add(datPrincipal);

            DataGridTextColumn datBalance = new DataGridTextColumn();
            datBalance.Header = "Balance";
            datBalance.Binding = new Binding("Amount");
            datBalance.Binding.StringFormat = "{0:#,##0.00;(#,##0.00);0.00}";
            datBalance.Width = 100;
            datBalance.IsReadOnly = true;
            datBalance.ElementStyle = ElementStyleAmt();
            accountCodeDataGrid.Columns.Add(datBalance);

            accountCodeDataGrid.CellStyle = StyleBorder();

        }


        public void Others(String Description)
        {
            Title = Description + " Acct Number Inquiry";
            DataGridTextColumn SLTypeDesc = new DataGridTextColumn();
            SLTypeDesc.Header = "S/L Type";
            SLTypeDesc.Binding = new Binding("SLTypeDesc");

            SLTypeDesc.Width = 170;
            SLTypeDesc.IsReadOnly = true;
            accountCodeDataGrid.Columns.Add(SLTypeDesc);

            DataGridTextColumn datAccountCode = new DataGridTextColumn();
            datAccountCode.Header = "Account Code";
            datAccountCode.Binding = new Binding("AccountCode");
            datAccountCode.Width = 150;
            datAccountCode.IsReadOnly = true;
            accountCodeDataGrid.Columns.Add(datAccountCode);

            DataGridTextColumn datDate = new DataGridTextColumn();
            datDate.Header = "Date";
            datDate.Binding = new Binding("TRDate");
            datDate.Width = 100;
            datDate.IsReadOnly = true;
            accountCodeDataGrid.Columns.Add(datDate);

            DataGridTextColumn datBalance = new DataGridTextColumn();
            datBalance.Header = "Balance";
            datBalance.Binding = new Binding("Amount");
            datBalance.Binding.StringFormat = "{0:#,##0.00;(#,##0.00);0.00}";
            datBalance.Width = 100;
            datBalance.IsReadOnly = true;
            datBalance.ElementStyle = ElementStyleAmt();
            accountCodeDataGrid.Columns.Add(datBalance);

            accountCodeDataGrid.CellStyle = StyleBorder();

        }


        private Style StyleBorder()
        {
            var brushConverter = new BrushConverter();
            var cellStyle = new Style { TargetType = typeof(DataGridCell) };
            var cellTrigger = new Trigger { Property = DataGridCell.IsSelectedProperty, Value = true };

            Style styleBalance = new Style(typeof(DataGridCell));
            styleBalance.Setters.Add(new Setter(ForegroundProperty, Brushes.Black));
            styleBalance.Setters.Add(new EventSetter(DataGridCell.SelectedEvent, new RoutedEventHandler(SelectedCell_Click)));
            styleBalance.Setters.Add(new Setter(BorderBrushProperty, Brushes.LightSteelBlue));
            cellTrigger.Setters.Add(new Setter(BackgroundProperty, Brushes.Aqua));
            styleBalance.Triggers.Add(cellTrigger);

            return styleBalance;
        }


        private Style ElementStyleAmt()
        {
            Style Align = new Style();
            Align.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Right));

            return Align;
        }


        private void FillDataGrid(Int64 clientID, Byte SLC, Byte SLT, Byte SLE, String SLDate, Byte SLN)
        {
            AccountParameters aNum = new AccountParameters();
            aNum.ClientID = clientID;
            aNum.SLC = SLC;
            aNum.SLT = SLT;
            aNum.SLE = SLE;
            aNum.SLDate = SLDate;
            aNum.SLN = SLN;

            String parsed = new JavaScriptSerializer().Serialize(aNum);
            CRUXLib.Response Response = App.CRUXAPI.request("accountCode/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {
                List<AccountNumbers> accountCodeList = new JavaScriptSerializer().Deserialize<List<AccountNumbers>>(Response.Content);
                accountCodeDataGrid.ItemsSource = accountCodeList;

                if (accountCodeList.Count != 0)
                {
                    accountCodeDataGrid.SelectedItem = accountCodeList.First();
                }

            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void function()
        {
            if (accountCodeDataGrid.SelectedItem == null) return;
            var selectedAccount = accountCodeDataGrid.SelectedItem as Models.AccountNumbers;

            TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;

            if (selectedAccount.Amount == "0.00")
            {
                MessageBoxResult result = MessageBox.Show("You are about to select account with ZERO Remaining Balance. Continue?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:

                        if (selectedAccount.StatusID != 0 && selectedAccount.AcctCode != 0)
                        {
                            tt.ReferenceNo = selectedAccount.RefNo;
                            tt.SLN_Code = selectedAccount.StatusID.ToString();
                            tt.AccountCode = selectedAccount.AcctCode;
                        }
                        else
                        {
                            tt.ReferenceNo = selectedAccount.RefNo;
                        }

                        this.Close();
                        this.mww.MoveToNextUIElement();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (selectedAccount.StatusID != 0 && selectedAccount.AcctCode != 0)
                {
                    tt.ReferenceNo = selectedAccount.RefNo;
                    tt.SLN_Code = selectedAccount.StatusID.ToString();
                    tt.AccountCode = selectedAccount.AcctCode;
                }
                else
                {
                    tt.ReferenceNo = selectedAccount.RefNo;
                }

                this.Close();
                this.mww.MoveToNextUIElement();
            }
        }


        private void accountCodeDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            function();
        }


        private void accountCodeDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                function();
                e.Handled = true;
            }
        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void searchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (accountCodeDataGrid.Items != null)
                    {
                        function();
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


    }
}
