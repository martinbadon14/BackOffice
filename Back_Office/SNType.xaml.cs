﻿using Back_Office.Helper;
using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for SNType.xaml
    /// </summary>
    public partial class SNType : Window
    {
        public Byte ctrlNum = 0;
        public Int64 account = 0;
        public string accDesc = "";
        public MainWindow mww;

        int currentRow = 0, currentColumn = 1;
        Models.SNType Selected;
        string SearchText = string.Empty;
        private ICollectionView MyData;


        public SNType(string a, string b, string c, MainWindow mw)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mww = mw;
            FillDataGrid(a, b, c);
        }


        private void FillDataGrid(string SLCCode, string SLTCode, string SLECode)
        {
            SLList sl = new SLList();
            sl.SLC = Convert.ToByte(SLCCode);
            sl.SLT = Convert.ToByte(SLTCode);
            sl.SLE = Convert.ToByte(SLECode);

            string parsed = new JavaScriptSerializer().Serialize(sl);
            CRUXLib.Response Response = App.CRUXAPI.request("sntype/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {
                List<Models.SNType> sntype = new JavaScriptSerializer().Deserialize<List<Models.SNType>>(Response.Content);
                SNTypeGrid.ItemsSource = sntype;

                MyData = CollectionViewSource.GetDefaultView(sntype);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

        }

        private void function()
        {
            if (SNTypeGrid.SelectedItem == null) return;

            TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;
            var selectedSLN = SNTypeGrid.SelectedItem as Models.SNType;

            tt.SLN_Code = selectedSLN.GLControlNUM.ToString();
            tt.AccountCode = selectedSLN.GLControlACCT;
            tt.accountDesc = selectedSLN.COADesc;

            var selectedTransCode = this.mww.transTypeComboBox.SelectedItem as TransactionType;

            if (selectedTransCode.TransTypeID == 22)
            {
                this.mww.FillInitialCheckDetails(selectedSLN.GLControlACCT);
            }

            this.Close();
            this.mww.DG_GeneralJournal.Focus();
            Keyboard.Focus(this.mww.GetDataGridCell(this.mww.DG_GeneralJournal.SelectedCells[7]));
            //  this.mww.DG_GeneralJournal.BeginEdit();
        }


        private void SNTypeGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void SNTypeGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                {
                    function();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SearchTextBox.Focus();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                if (currentRow > 0)
                {
                    try
                    {
                        int previousIndex = SNTypeGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        SNTypeGrid.SelectedIndex = previousIndex;
                        SNTypeGrid.ScrollIntoView(SNTypeGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }
                }

            }
            else if (e.Key == Key.Down)
            {
                if (currentRow < SNTypeGrid.Items.Count - 1)
                {

                    try
                    {
                        int nextIndex = SNTypeGrid.SelectedIndex + 1;
                        if (nextIndex > SNTypeGrid.Items.Count - 1) return;
                        SNTypeGrid.SelectedIndex = nextIndex;
                        SNTypeGrid.ScrollIntoView(SNTypeGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }

                } // end if (this.SelectedOverride > 0)

            } // end else if (e.Key == Key.Down)

            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            MyData.Filter = FilterData;
            SNTypeGrid.SelectedIndex = 0;
        }


        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    this.Selected = (Models.SNType)SNTypeGrid.Items[currentRow];

                    function();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Selected = (Models.SNType)SNTypeGrid.Items[currentRow];
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private bool FilterData(object item)
        {
            var value = (Models.SNType)item;

            if (value.GLControlNUM == 0 || String.IsNullOrEmpty(value.AcctStatusDesc) || value.GLControlACCT == 0)
                return false;
            else
                return value.AcctStatusDesc.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.GLControlNUM).Contains(SearchText) ||
                    Convert.ToString(value.GLControlACCT).Contains(SearchText);
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }
    }
}

