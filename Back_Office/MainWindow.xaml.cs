﻿using Back_Office.BIR.Purchase_Journal;
using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Purchase_Journal;
using Back_Office.Models.Reports;
using Back_Office.Month_End;
using Back_Office.Year_End;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using static Back_Office.Helper.TransactionLogs;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        String OrigDate = "";
        private bool temp = false;
        private int currentRow = 0;

        //variables that will hold data
        private Boolean tabItemGL = false;
        public Boolean TransMainTrigger = false, TransEditMaintTrigger = false;
        private String trType;
        private String trTypeCode = null;
        private Boolean AllowEditDate = true, IsInquiry = false, AllowSave = false;
        public Boolean AllowSaveWithDifference = false, TransDetailPosting = false, SLDetailPosting = true, GLTaransOnly = true;

        enum COCIShortcutKeys { L = 1, R, O, F, U, G };
        //instance of classes
        SLType slType;
        SLEType sleType;
        SLClass slClass;
        SNType snType;
        GLAccount glAccount;
        clientSearch clientsearch;
        AccountCode acctCode;
        AdjFlag _AdjFlag;
        TransactionFilter transFilter;
        InterestOnSavings monthEnd;
        COCIType cociType;
        BankCode bankCodes;
        TypeChecks typeChecks;
        FlexiSavings flexSavings;
        DocInquiryParameters docInqParams;
        private ExecuteHelper execHelper;
        VendorSearch _VendorSearch;
        VendorTIN _VendorTIN;

        public List<TransactionDetails> CheckTransactionDetailsList { get; set; }
        List<TransactionDetails> insertTransDetLists;

        public List<Models.CheckType> checkTypesList;

        private string apiCrux;
        public SLNSet slnset;
        public DataGridFocus dgFocus;
        public ClientParams clientParams;
        public SLAccountParams slAccountParams;

        private static int CheckVoucherID = 22;
        private static int PurchaseJournalID = 16;

        private static String LoanReclassCLTitle = "Loan Reclassification";
        private static String InterestCLTitle = "Interest on Savings";
        private static String InterestTitle = "Interest on Savings Transactions";
        private static String CashReceiptInquiryTitle = "Cash Receipt Inquiry";
        private static String GeneralJournalTitle = "General Journal Transactions";
        private static String RemittanceJournalTitle = "Remittance Journal Transactions";
        private static String CashCheckDisbursmentTitle = "Disbursement Transactions";
        private static String SpecialTransactionTitle = "Special Transactions";
        private static String POSTitle = "POS Transactions";
        private static String UITitle = "Unearned Interest Amortization";
        private static String ReclassTitle = "Reclassification SL Status";
        private static String ClosingTitle = "Closing Transactions";
        private static String DividendTitle = "Dividend Transactions";
        private static String PatronageTitle = "Patronage and Refund Transactions";
        private static String BankTitle = "Bank Deposits Transactions";
        private static String CashTransferTitle = "Cash Transfer Transactions";
        private static String AvgShareCapitalTitle = "Avg. Share Capital and Interest Paid";
        private static String DividendPatronageTitle = "Dividend And Patronage Refund";
        private static String PurchaseTitle = "Purchase Journal";

        public List<TransactionDetails> TempTransactionDetailsList;
        public List<PurchaseDetails> TempPurchaseDetails;
        private readonly BackgroundWorker InitializeWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateControlListWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateBOReportWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateSaveWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GenerateUpdateWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GeneratePostWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GenerateReversedWorker = new BackgroundWorker { WorkerSupportsCancellation = true };

        private readonly BackgroundWorker GenerateBalances = new BackgroundWorker();
        private readonly BackgroundWorker GenerateBalancesForPostedTrans = new BackgroundWorker();
        private readonly BackgroundWorker GeneratePostAfterCheckingCVWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GeneratePostAfterCheckingGJWithLoanWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GeneratePostAfterCheckingRMTWithLoanWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GeneratePostAfterCheckingOthersWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
        private readonly BackgroundWorker GenerateCheckInternetConn = new BackgroundWorker { WorkerSupportsCancellation = true };

        public Boolean TempPostingAfterCheckSLBalance = false;

        public BackOfficeDataCon DataCon = new BackOfficeDataCon();
        ReportDocument Report = new ReportDocument();
        ReportDocument Export = new ReportDocument();
        BOHeader boh = new BOHeader();
        DocumentDetails documentDetail = new DocumentDetails();

        List<TransactionType> TransactionTypesList = new List<TransactionType>();
        List<UPDTag> UPDTags = new List<UPDTag>();
        List<GLAccounts> GLAccounts = new List<GLAccounts>();
        List<GLAccounts> FillGLAccounts = new List<GLAccounts>();
        CrystalReport Viewer;
        TransactionDetailContainer _transDetailContainer = new TransactionDetailContainer();

        List<Models.TransactionDetails> unbalanceListWithHold;
        List<Models.TransactionDetails> unbalanceListBW;
        List<Models.TransactionDetails> withLPList;
        List<TransactionDetails> CheckClosedAccts;
        List<TransactionDetails> CheckFinesWaived;
        List<TransactionDetails> CheckInterestWaived;
        List<AccountNumbers> WithClosedAccts;
        List<Models.CheckLoanBalance> unbalancedLoanListBW;
        List<Models.CheckLoanBalance> ClientsForApprovalList;
        List<Models.TransactionDetails> unbalancedLoanPrincipal;
        List<Models.CheckLoanBalance> unbalancedRMTListBW;
        Decimal TotalPenatly = 0, TotalInterest = 0;
        Byte PostOrReverse = 0;
        int PurchaseSeqNo = 0;

        public int SavingMain = 0, PostingMain = 0, PostOthers = 0, RMTWithLoan = 0, GJWithLoan = 0, CVPosting = 0, ReverseSwitch = 0;

        public MainWindow()
        {
            //String args
            try
            {
                InitializeComponent();

                //App.CRUXAPI = new CRUXLib.API();
                //string[] aaa = args.Split(' ');
                //List<String> ll = aaa.ToList<String>();
                //App.CRUXAPI.loadParams(ll);

                InitializeDefaults();
                InitializeWorkers();
                this.PreviewKeyDown += new KeyEventHandler(HandleKeys);

                apiCrux = App.CRUXAPI.Arguments;

                if (!String.IsNullOrEmpty(apiCrux))
                {
                    DocInquiryParameters docInqParams = new JavaScriptSerializer().Deserialize<DocInquiryParameters>(apiCrux);
                    GetTransDetail(docInqParams);
                }

                this.DataContext = DataCon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
        }

        private void InitializeDefaults()
        {
            OrigDate = "";

            this.DataCon.TransYear = "";
            this.DataCon.ClosedStatus = false;
            this.DataCon.Title = String.Empty;
            this.DataCon.ReportType = String.Empty;
            this.DataCon.InsertOrUpdated = true;
            this.DataCon.TotalDebit = 0;
            this.DataCon.TotalCredit = 0;
            this.DataCon.TempTotalDebit = 0;
            this.DataCon.TempTotalCredit = 0;
            this.DataCon.TotalDifference = 0;
            this.DataCon.TotalCOCI = 0;
            this.DataCon.Response = String.Empty;
            this.DataCon.CurrentBranch = 0;
            this.DataCon.Upd = String.Empty;
            this.DataCon.TransactionCode = 0;

            this.DataCon.DateTimeNow = String.Empty;
            this.DataCon.ControlNo = 0;
            this.DataCon.ControlNew = 0;

            this.DataCon.PreparedBy = String.Empty;
            this.DataCon.ReversalExplanation = String.Empty;

            //newly added 04/23/2018
            this.DataCon.ClientNameSearch = String.Empty;
            this.DataCon.ClientIDSearch = 0;

            this.DataCon.NetTotal = 0;

            checkTypesList = new List<CheckType>();

            dgFocus = new DataGridFocus();
            slnset = new SLNSet();
            clientParams = new ClientParams();
            slAccountParams = new SLAccountParams();
            this.DataCon.UPDTrans = new UPDTrans();
            this.DataCon.TransactionChecks = new ObservableCollection<TransactionCheck>();
            this.DataCon.TransactionDetails = new List<Models.TransactionDetails>();
            this.DataCon.DeletedTransactionDetails = new List<Models.TransactionDetails>();
            this.DataCon.TransactionSummary = new TransactionSummary();
            this.DataCon.GLTransactionList = new List<GLTransaction>();

            _transDetailContainer.TransactionCheckList = new List<TransactionCheck>();
            _transDetailContainer._TransactionDetailsList = new List<Models.TransactionDetails>();
            _transDetailContainer._TransactionSummary = new TransactionSummary();

            this.DataCon.TransParameters = new TransactionFilterParams();

            this.DataCon.PurchaseDetails = new List<PurchaseDetails>();
            this.DataCon.PurchaseSummary = new InsertPurchaseSummary();
            _transDetailContainer.PurchaseDetails = new List<PurchaseDetails>();
            _transDetailContainer.PurchaseSummary = new InsertPurchaseSummary();

            DefaultFormGridSize();
        }

        private void InitializeWorkers()
        {
            InitializeWorker.DoWork += InitializeWorker_DoWork;
            InitializeWorker.RunWorkerCompleted += InitializeWorker_RunWorkerCompleted;

            GenerateControlListWorker.DoWork += GenerateControlListWorker_DoWork;
            GenerateControlListWorker.RunWorkerCompleted += GenerateControlListWorker_RunWorkerCompleted;

            GenerateBOReportWorker.DoWork += GenerateBOReportWorker_DoWork;
            GenerateBOReportWorker.RunWorkerCompleted += GenerateBOReportWorker_RunWorkerCompleted;

            GenerateSaveWorker.DoWork += GenerateSaveWorker_DoWork;
            GenerateSaveWorker.RunWorkerCompleted += GenerateSaveWorker_RunWorkerCompleted;

            GenerateUpdateWorker.DoWork += GenerateUpdateWorker_DoWork;
            GenerateUpdateWorker.RunWorkerCompleted += GenerateUpdateWorker_RunWorkerCompleted;

            GeneratePostWorker.DoWork += GeneratePostWorker_DoWork;
            GeneratePostWorker.RunWorkerCompleted += GeneratePostWorker_RunWorkerCompleted;

            GenerateReversedWorker.DoWork += GenerateReversedWorker_DoWork;
            GenerateReversedWorker.RunWorkerCompleted += GenerateReversedWorker_RunWorkerCompleted;

            GenerateBalances.WorkerSupportsCancellation = true;
            GenerateBalances.DoWork += GenerateBalances_DoWork;
            GenerateBalances.RunWorkerCompleted += GenerateBalances_RunWorkerCompleted;

            GeneratePostAfterCheckingCVWorker.DoWork += GeneratePostAfterCheckingCVWorker_DoWork;
            GeneratePostAfterCheckingCVWorker.RunWorkerCompleted += GeneratePostAfterCheckingCVWorker_RunWorkerCompleted;

            GeneratePostAfterCheckingGJWithLoanWorker.DoWork += GeneratePostAfterCheckingGJWithLoanWorker_DoWork;
            GeneratePostAfterCheckingGJWithLoanWorker.RunWorkerCompleted += GeneratePostAfterCheckingGJWithLoanWorker_RunWorkerCompleted;

            GeneratePostAfterCheckingRMTWithLoanWorker.DoWork += GeneratePostAfterCheckingRMTWithLoanWorker_DoWork;
            GeneratePostAfterCheckingRMTWithLoanWorker.RunWorkerCompleted += GeneratePostAfterCheckingRMTWithLoanWorker_RunWorkerCompleted;

            GeneratePostAfterCheckingOthersWorker.DoWork += GeneratePostAfterCheckingOthersWorker_DoWork;
            GeneratePostAfterCheckingOthersWorker.RunWorkerCompleted += GeneratePostAfterCheckingOthersWorker_RunWorkerCompleted;

            GenerateCheckInternetConn.DoWork += GenerateCheckInternetConn_DoWork;
            GenerateCheckInternetConn.RunWorkerCompleted += GenerateCheckInternetConn_RunWorkerCompleted;

            GenerateBalancesForPostedTrans.DoWork += GenerateBalancesForPostedTrans_DoWork;
            GenerateBalancesForPostedTrans.RunWorkerCompleted += GenerateBalancesForPostedTrans_RunWorkerCompleted;

            ProgressPanel.Visibility = Visibility.Visible;
            MainGrid.IsEnabled = false;
            InitializeWorker.RunWorkerAsync();
        }

        private void GenerateCheckInternetConn_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // throw new NotImplementedException();
        }

        private void GenerateCheckInternetConn_DoWork(object sender, DoWorkEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void GeneratePostAfterCheckingOthersWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (PostOthers)
            {
                case 1:
                    GeneratePostWorker.RunWorkerAsync();
                    break;

                case 2:
                    foreach (Models.TransactionDetails detail in unbalanceListBW)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }

                    ErrorDoneWorker();
                    break;

                case 3:
                    foreach (AccountNumbers detail in WithClosedAccts)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Transaction cannot be processed due to Closed Account/s in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.RefNo,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    break;
                case 4:
                    MessageBox.Show(this, "Connection Lost. Please Try Again.");
                    break;
                case 5:
                    MessageBox.Show(this, "You don't have privilege to post transaction with hold amount resulting to negative value of SL account.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;
                case 6:
                    ErrorDoneWorker();
                    break;
                default:
                    break;
            }
        }

        private void GeneratePostAfterCheckingOthersWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true;

            while (pass)
            {
                if (!GenerateBalances.IsBusy)
                {
                    try
                    {
                        if (WithClosedAccts.Count == 0)
                        {
                            if (unbalanceListBW.Count == 0)
                            {
                                if (unbalanceListWithHold.Count == 0)
                                {
                                    PostOthers = 1;
                                }
                                else
                                {
                                    MessageBoxResult withPrompt = MessageBox.Show("SL account would result to a negative value due to Hold Amount . Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                    switch (withPrompt)
                                    {
                                        case MessageBoxResult.Yes:

                                            if (App.CRUXAPI.isAllowed("post_hold_amount"))
                                            {
                                                PostOthers = 1;
                                            }
                                            else
                                            {
                                                PostOthers = 5;
                                            }
                                            break;
                                        case MessageBoxResult.No:
                                            PostOthers = 6;
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            } //end if unbalance is null
                            else
                            {
                                PostOthers = 2;
                            }
                        }
                        else
                        {
                            PostOthers = 3;
                        }

                        pass = false;
                    }
                    catch (Exception)
                    {
                        PostOthers = 4;
                    }


                }

                Thread.Sleep(50);
            } // end while
        }

        private void GeneratePostAfterCheckingRMTWithLoanWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (RMTWithLoan)
            {
                case 1:
                    GeneratePostWorker.RunWorkerAsync();
                    break;

                case 2:
                    MessageBox.Show(this, "Total Fines Waived greater than amount due.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;

                case 3:
                    MessageBox.Show(this, "Total Interest Waived greater than amount due.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;

                case 4:
                    foreach (Models.TransactionDetails detail in unbalanceListBW)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    ErrorDoneWorker();
                    break;

                case 5:
                    foreach (CheckLoanBalance detail in unbalancedRMTListBW)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Interest Paid/Penalty Fines cannot Exceed Interest Due/Penalty Due . Client ID: " + detail.ClientID, "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    ErrorDoneWorker();
                    break;

                case 6:
                    foreach (Models.TransactionDetails detail in unbalancedLoanPrincipal)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    ErrorDoneWorker();
                    break;

                case 7:
                    foreach (AccountNumbers detail in WithClosedAccts)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Transaction cannot be processed due to Closed Account/s in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.RefNo,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    break;
                case 8:
                    MessageBox.Show(this, "You don't have privilege to post transaction with hold amount resulting to negative value of SL account.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;
                case 9:
                    ErrorDoneWorker();
                    break;

                default:
                    break;
            }
        }

        private void GeneratePostAfterCheckingRMTWithLoanWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true;

            while (pass)
            {
                if (!GenerateBalances.IsBusy)
                {
                    if (WithClosedAccts.Count == 0)
                    {
                        if (unbalancedLoanPrincipal.Count == 0)
                        {
                            if (unbalancedRMTListBW.Count == 0)
                            {
                                if (unbalanceListBW.Count == 0)
                                {
                                    if (unbalanceListWithHold.Count == 0)
                                    {
                                        if (TotalPenatly == 0 && TotalInterest == 0)
                                        {
                                            RMTWithLoan = 1;
                                        } // end if totalFines = SL Total Fines
                                        else if (TotalPenatly != 0)
                                        {
                                            RMTWithLoan = 2;
                                        }
                                        else if (TotalInterest != 0)
                                        {
                                            RMTWithLoan = 3;
                                        }
                                    }
                                    else
                                    {
                                        MessageBoxResult withPrompt = MessageBox.Show("SL account would result to a negative value due to Hold Amount . Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                        switch (withPrompt)
                                        {
                                            case MessageBoxResult.Yes:

                                                if (App.CRUXAPI.isAllowed("post_hold_amount"))
                                                {
                                                    RMTWithLoan = 1;
                                                }
                                                else
                                                {
                                                    PostOthers = 8;
                                                }
                                                break;
                                            case MessageBoxResult.No:
                                                RMTWithLoan = 9;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    RMTWithLoan = 4;
                                }

                            } // end if (unbalancedLoanListBW.Count == 0)
                            else
                            {
                                RMTWithLoan = 5;
                            }
                        }
                        else
                        {
                            RMTWithLoan = 6;
                        }

                    }
                    else
                    {
                        RMTWithLoan = 7;
                    }

                    pass = false;
                }

                Thread.Sleep(50);
            } // end while
        }

        private void ErrorDoneWorker()
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            MainGrid.IsEnabled = true;
        }

        private void GeneratePostAfterCheckingGJWithLoanWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (GJWithLoan)
            {
                case 1:
                    GeneratePostWorker.RunWorkerAsync();
                    break;

                case 2:

                    MessageBox.Show(this, "Total Fines Waived greater than amount due.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;
                case 3:

                    MessageBox.Show(this, "Total Interest Waived greater than amount due.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;

                case 4:
                    foreach (Models.TransactionDetails detail in unbalanceListBW)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    ErrorDoneWorker();
                    break;

                case 5:

                    foreach (Models.CheckLoanBalance detail in unbalancedLoanListBW)
                    {
                        if (detail.WithPrompt == "Ok")
                        {
                            MessageBoxResult alert = MessageBox.Show(this, "Interest Paid/Penalty Fines cannot Exceed Interest Due/Penalty Due. Client ID: " + detail.ClientID + "SL Ref: " + detail.RefNo, "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        }
                    } //end of foreach

                    ErrorDoneWorker();

                    break;
                case 6:
                    foreach (Models.TransactionDetails detail in unbalancedLoanPrincipal)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    ErrorDoneWorker();
                    break;
                case 7:
                    foreach (AccountNumbers detail in WithClosedAccts)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Transaction cannot be processed due to Closed Account/s in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.RefNo,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    break;
                case 8:
                    MessageBox.Show(this, "You don't have privilege to post transaction with hold amount resulting to negative value of SL account.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;
                case 9:
                    ErrorDoneWorker();
                    break;
                default:
                    break;
            }

        }

        private void GeneratePostAfterCheckingGJWithLoanWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true;

            while (pass)
            {
                if (!GenerateBalances.IsBusy)
                {
                    if (WithClosedAccts.Count == 0)
                    {
                        if (unbalancedLoanPrincipal.Count == 0)
                        {
                            if (unbalancedLoanListBW.Count == 0)
                            {
                                if (unbalanceListBW.Count == 0)
                                {
                                    if (unbalanceListWithHold.Count == 0)
                                    {
                                        if (TotalPenatly == 0 && TotalInterest == 0)
                                        {
                                            GJWithLoan = 1;
                                        } // end if totalFines = SL Total Fines
                                        else if (TotalPenatly != 0)
                                        {
                                            GJWithLoan = 2;
                                        }
                                        else if (TotalInterest != 0)
                                        {
                                            GJWithLoan = 3;
                                        }
                                    }
                                    else
                                    {
                                        MessageBoxResult withPrompt = MessageBox.Show("SL account would result to a negative value due to Hold Amount . Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                        switch (withPrompt)
                                        {
                                            case MessageBoxResult.Yes:

                                                if (App.CRUXAPI.isAllowed("post_hold_amount"))
                                                {
                                                    GJWithLoan = 1;
                                                }
                                                else
                                                {
                                                    GJWithLoan = 8;
                                                }
                                                break;
                                            case MessageBoxResult.No:
                                                GJWithLoan = 9;
                                                break;
                                            default:
                                                break;
                                        }
                                    }


                                }
                                else
                                {
                                    GJWithLoan = 4;
                                }
                            }
                            else
                            {

                                var YesNoBalance = unbalancedLoanListBW.FindAll(c => c.WithPrompt == "YesNo").ToList();

                                if (YesNoBalance.Count != 0)
                                {
                                    if (unbalanceListBW.Count == 0)
                                    {
                                        if (unbalanceListWithHold.Count == 0)
                                        {
                                            if (TotalPenatly == 0 && TotalInterest == 0)
                                            {
                                                foreach (Models.CheckLoanBalance detail in YesNoBalance)
                                                {
                                                    MessageBoxResult withPrompt = MessageBox.Show("Interest Paid/Penalty Fines is less than Interest Due/Penalty Due. Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                                    switch (withPrompt)
                                                    {
                                                        case MessageBoxResult.Yes:
                                                            GJWithLoan = 1;
                                                            break;

                                                        case MessageBoxResult.No:
                                                            GJWithLoan = 8;
                                                            break;
                                                        default:
                                                            break;
                                                    }

                                                }
                                            }
                                            else if (TotalPenatly != 0)
                                            {
                                                GJWithLoan = 2;
                                            }
                                            else if (TotalInterest != 0)
                                            {
                                                GJWithLoan = 3;
                                            }
                                        }
                                        else
                                        {
                                            MessageBoxResult withPrompt = MessageBox.Show("SL account would result to a negative value due to Hold Amount . Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                            switch (withPrompt)
                                            {
                                                case MessageBoxResult.Yes:

                                                    if (App.CRUXAPI.isAllowed("post_hold_amount"))
                                                    {
                                                        GJWithLoan = 1;
                                                    }
                                                    else
                                                    {
                                                        GJWithLoan = 8;
                                                    }
                                                    break;
                                                case MessageBoxResult.No:
                                                    GJWithLoan = 9;
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        GJWithLoan = 4;
                                    }
                                }

                                var OKBalance = unbalancedLoanListBW.FindAll(c => c.WithPrompt == "Ok").ToList();

                                if (OKBalance.Count != 0)
                                {
                                    GJWithLoan = 5;
                                }
                            }
                        }
                        else
                        {
                            GJWithLoan = 6;
                        }
                    }
                    else
                    {
                        GJWithLoan = 7;
                    }
                    pass = false;
                }

                Thread.Sleep(50);
            }
        }

        private void GeneratePostAfterCheckingCVWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (CVPosting)
            {
                case 1:
                    GeneratePostWorker.RunWorkerAsync();
                    break;
                case 2:
                    foreach (Models.TransactionDetails detail in unbalanceListBW)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }

                    ErrorDoneWorker();
                    break;
                case 3:

                    foreach (Models.CheckLoanBalance detail in unbalancedLoanListBW)
                    {
                        if (detail.WithPrompt == "Ok")
                        {
                            MessageBoxResult alert = MessageBox.Show(this, "Interest Paid/Penalty Fines cannot Exceed Interest Due/Penalty Due. Client ID: " + detail.ClientID + "SL Ref: " + detail.RefNo, "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                        }
                    } //end of foreach

                    ErrorDoneWorker();
                    break;

                case 4:
                    foreach (Models.TransactionDetails detail in unbalancedLoanPrincipal)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                " Date: " + detail.SLDate,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }

                    ErrorDoneWorker();
                    break;

                case 5:
                    foreach (AccountNumbers detail in WithClosedAccts)
                    {
                        MessageBoxResult alert = MessageBox.Show(this, "Transaction cannot be processed due to Closed Account/s in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                             " SL Ref: " + detail.RefNo,
                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    break;
                case 6:
                    MessageBox.Show(this, "You don't have privilege to post transaction with hold amount resulting to negative value of SL account.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ErrorDoneWorker();
                    break;
                case 7:
                    ErrorDoneWorker();
                    break;
                default:
                    break;
            }
        }

        private void GeneratePostAfterCheckingCVWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true;

            while (pass)
            {
                //this.Dispatcher.BeginInvoke((Action)(() =>
                //{
                //    ProcessLabel.Content = "Checking SL Balances...";
                //}), System.Windows.Threading.DispatcherPriority.Background);

                if (!GenerateBalances.IsBusy)
                {
                    //this.Dispatcher.BeginInvoke((Action)(() =>
                    //{
                    //    ProcessLabel.Content = "Processing...";
                    //}), System.Windows.Threading.DispatcherPriority.Background);

                    if (WithClosedAccts.Count == 0)
                    {
                        if (unbalancedLoanPrincipal.Count == 0)
                        {
                            if (unbalancedLoanListBW.Count == 0)
                            {
                                if (unbalanceListBW.Count == 0)
                                {
                                    if (unbalanceListWithHold.Count == 0)
                                    {
                                        CVPosting = 1;
                                    }
                                    else
                                    {
                                        MessageBoxResult withPrompt = MessageBox.Show("SL account would result to a negative value due to Hold Amount . Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                        switch (withPrompt)
                                        {
                                            case MessageBoxResult.Yes:

                                                if (App.CRUXAPI.isAllowed("post_hold_amount"))
                                                {
                                                    CVPosting = 1;
                                                }
                                                else
                                                {
                                                    CVPosting = 6;
                                                }
                                                break;
                                            case MessageBoxResult.No:
                                                CVPosting = 7;
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                                else if (unbalanceListBW.Count != 0)
                                {
                                    CVPosting = 2;
                                }
                            }
                            else
                            {
                                var YesNoBalance = unbalancedLoanListBW.FindAll(c => c.WithPrompt == "YesNo").ToList();

                                if (YesNoBalance.Count != 0)
                                {
                                    if (unbalanceListBW.Count == 0)
                                    {
                                        if (unbalanceListWithHold.Count == 0)
                                        {
                                            foreach (Models.CheckLoanBalance detail in YesNoBalance)
                                            {
                                                MessageBoxResult withPrompt = MessageBox.Show("Interest Paid/Penalty Fines is less than Interest Due/Penalty Due. Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);

                                                switch (withPrompt)
                                                {
                                                    case MessageBoxResult.Yes:
                                                        CVPosting = 1;
                                                        break;

                                                    case MessageBoxResult.No:
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBoxResult withPrompt = MessageBox.Show("SL account would result to a negative value due to Hold Amount . Proceed Anyway?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                                            switch (withPrompt)
                                            {
                                                case MessageBoxResult.Yes:

                                                    if (App.CRUXAPI.isAllowed("post_hold_amount"))
                                                    {
                                                        CVPosting = 1;
                                                    }
                                                    else
                                                    {
                                                        CVPosting = 6;
                                                    }
                                                    break;
                                                case MessageBoxResult.No:
                                                    CVPosting = 7;
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        CVPosting = 2;
                                    }

                                } //end of YesNOBalance != 0

                                var OKBalance = unbalancedLoanListBW.FindAll(c => c.WithPrompt == "Ok").ToList();

                                if (OKBalance.Count != 0)
                                {
                                    CVPosting = 3;
                                }
                            }
                        }
                        else
                        {
                            CVPosting = 4;
                        }
                    }
                    else
                    {
                        CVPosting = 5;
                    }

                    pass = false;
                }

                Thread.Sleep(50);
            }
        }

        private void GenerateBalancesForPostedTrans_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                return;
            }
        }

        private void GenerateBalancesForPostedTrans_DoWork(object sender, DoWorkEventArgs e)
        {
            if (this.DataCon.TransactionCode == 21 || this.DataCon.TransactionCode == 22)
            {
                withLPList = DataCon.TransactionDetails.FindAll(l => l.SLC_Code == "12").ToList();

                if (withLPList.Count != 0)
                {
                    unbalancedLoanPrincipal = CheckLoanBalance(withLPList, e);
                }
            }
        }

        private void GenerateBalances_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GenerateBalances.WorkerReportsProgress = false;


            if (e.Cancelled)
            {
                GenerateBalances.Dispose();
                return;
            }
        }

        private void GenerateBalances_DoWork(object sender, DoWorkEventArgs e)
        {

            withLPList = DataCon.TransactionDetails.FindAll(l => l.SLC_Code == "12").ToList();
            CheckClosedAccts = DataCon.TransactionDetails.FindAll(l => !String.IsNullOrEmpty(l.SLC_Code) && l.SLC != "11").ToList();
            CheckFinesWaived = DataCon.TransactionDetails.FindAll(f => f.AccountCode == 4039999).ToList();
            CheckInterestWaived = DataCon.TransactionDetails.FindAll(f => f.AccountCode == 4019999).ToList();

            if (CheckFinesWaived.Count != 0)
            {
                TotalPenatly = CheckTotalFinesWaivedAndInterestWaived(this.DataCon.TransactionDetails, e);
            }

            if (CheckInterestWaived.Count != 0)
            {
                TotalInterest = CheckTotalInterestWaivedAndInterestWaived(this.DataCon.TransactionDetails, e);
            }

            if (CheckClosedAccts.Count != 0)
            {
                WithClosedAccts = CheckClosedAcctsBeforePosting(CheckClosedAccts);
            }

            if (this.DataCon.TransactionCode == 22)
            {
                if (withLPList.Count != 0)
                {
                    unbalancedLoanListBW = CheckLoan(withLPList, e);
                    unbalancedLoanPrincipal = CheckLoanBalance(withLPList, e);
                }

                unbalanceListBW = CheckBalance(this.DataCon.TransactionDetails.FindAll(c => c.SLC_Code != "12" && !String.IsNullOrEmpty(c.SLC_Code)).ToList(), e);
            }
            else
            {
                List<Models.TransactionDetails> chkTransClientID = new List<Models.TransactionDetails>();

                if (withLPList.Count != 0 && (this.DataCon.TransactionCode == 1 || this.DataCon.TransactionCode == 6 || this.DataCon.TransactionCode == 7))
                {
                    if (this.DataCon.TransactionCode == 1)
                    {
                        chkTransClientID = withLPList.FindAll(l => l.SLE_Code == "11" && l.AdjFlagCode == "S").ToList();

                        if (chkTransClientID.Count != 0)
                        {
                            ClientsForApprovalList = CheckAppStatus(chkTransClientID);
                        }

                        unbalancedLoanListBW = CheckLoan(withLPList, e);
                    }
                    else
                    {
                        unbalancedRMTListBW = CheckOverPaymentsRemittance(withLPList, e);
                    }

                    unbalancedLoanPrincipal = CheckLoanBalance(withLPList, e);
                    unbalanceListBW = CheckBalance(this.DataCon.TransactionDetails, e).FindAll(c => c.SLC_Code != "12" && !String.IsNullOrEmpty(c.SLC_Code)).ToList();
                }
                else
                {
                    if (this.DataCon.TransactionCode != 51 && this.DataCon.TransactionCode != 53 && this.DataCon.TransactionCode != 77 && this.DataCon.TransactionCode != 54 && this.DataCon.TransactionCode != 55 && this.DataCon.TransactionCode != 62)
                    {
                        unbalanceListBW = CheckBalance(this.DataCon.TransactionDetails, e).FindAll(c => c.SLC_Code != "12" && !String.IsNullOrEmpty(c.SLC_Code)).ToList();
                    }
                }
            }

            Thread.Sleep(100);
        }

        private void GenerateBOReportWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            MainGrid.IsEnabled = true;

            Viewer = new CrystalReport();
            Viewer.cryRpt = Export;
            Viewer._CrystalReport.ViewerCore.ReportSource = Report;
            Viewer._CrystalReport.ViewerCore.Zoom(130);
            Viewer.Owner = this;
            Viewer.ShowDialog();
        }



        private void GenerateBOReportWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<BOReports> boReportList = new List<BOReports>();
                PrintVerify verification = new Models.Reports.PrintVerify();

                foreach (TransactionDetails transDetail in this.DataCon.TransactionDetails)
                {
                    boReportList.Add(new BOReports()
                    {
                        BranchCode = transDetail.BranchCode,
                        SLC_Code = transDetail.SLC_Code,
                        SLT_Code = transDetail.SLT_Code,
                        SLE_Code = transDetail.SLE_Code,
                        SLN_Code = transDetail.SLN_Code,
                        AccountCode = transDetail.AccountCode,
                        AccountDesc = transDetail.accountDesc,
                        ClientID = transDetail.ClientID,
                        ClientChckID = transDetail.ClientChckID,
                        ClientName = transDetail.clientName,
                        ControlNo = transDetail.ControlNo,
                        ReferenceNo = transDetail.ReferenceNo.Equals(String.Empty) ? "" : String.Format("{0:D8}", Convert.ToInt64(transDetail.ReferenceNo)),
                        SLDate = transDetail.SLDate,
                        Debit = transDetail.Debit,
                        Credit = transDetail.Credit,
                        PostedBy = transDetail.PostedBy
                    });
                }

                String CTLNoFormat = "";
                CTLNoFormat = String.Format("{0}-{1}", String.Format("{0:D2}", this.DataCon.CurrentBranch), String.Format("{0:D8}", this.DataCon.ControlNo));

                Report.SetDataSource(boReportList);
                Export.SetDataSource(boReportList);

                Report.SetParameterValue("Title", this.DataCon.TransactionSummary.TransTypDesc);
                Export.SetParameterValue("Title", this.DataCon.TransactionSummary.TransTypDesc);


                //Details
                Report.SetParameterValue("BRCTLNo", CTLNoFormat);
                Report.SetParameterValue("DocNo", String.Format("{0:D8}", this.DataCon.ControlNo));
                Report.SetParameterValue("TransDate", this.DataCon.DateTimeNow);
                Report.SetParameterValue("Explanation", this.DataCon.TransactionSummary.Explanation);

                if (!String.IsNullOrEmpty(this.DataCon.PreparedBy))
                {
                    CRUXLib.Response Response = App.CRUXAPI.request("backoffice/EncodedBy", "");

                    if (Response.Status == "SUCCESS")
                    {
                        this.DataCon.PreparedBy = new JavaScriptSerializer().Deserialize<String>(Response.Content);
                    }
                    else
                    {
                        MessageBox.Show(Response.Content);
                    }
                }

                Report.SetParameterValue("PreparedByName", this.DataCon.PreparedBy);

                Export.SetParameterValue("BRCTLNo", CTLNoFormat);
                Export.SetParameterValue("DocNo", String.Format("{0:D8}", this.DataCon.ControlNo));
                Export.SetParameterValue("TransDate", this.DataCon.DateTimeNow);
                Export.SetParameterValue("Explanation", this.DataCon.TransactionSummary.Explanation);
                Export.SetParameterValue("PreparedByName", this.DataCon.PreparedBy);

                verification = PrintVerify();

                Report.SetParameterValue("GenerateDetails", "Generated by: " + verification.Username + " ON Computer:" + verification.ComputerName + ": " + verification.IPAddress);
                Export.SetParameterValue("GenerateDetails", "Generated by: " + verification.Username + " ON Computer:" + verification.ComputerName + ": " + verification.IPAddress);

                Report.SetParameterValue("Verification", "Verification Code: " + verification.VerificationCode);
                Export.SetParameterValue("Verification", "Verification Code: " + verification.VerificationCode);

                Report.SetParameterValue("LetterHead", this.documentDetail.LetterHead);
                Export.SetParameterValue("LetterHead", this.documentDetail.LetterHead);

                Report.SetParameterValue("OfficeName", this.documentDetail.OfficeName);
                Report.SetParameterValue("OfficeAddress", this.documentDetail.OfficeAddress);

                Export.SetParameterValue("OfficeName", this.documentDetail.OfficeName);
                Export.SetParameterValue("OfficeAddress", this.documentDetail.OfficeAddress);

                Report.SetParameterValue("Header1", this.documentDetail.Header1);
                Export.SetParameterValue("Header1", this.documentDetail.Header1);

                Report.SetParameterValue("Header2", this.documentDetail.Header2);
                Export.SetParameterValue("Header2", this.documentDetail.Header2);

                Report.SetParameterValue("Footer1", this.documentDetail.Footer1);
                Export.SetParameterValue("Footer1", this.documentDetail.Footer1);

                Report.SetParameterValue("Footer2", this.documentDetail.Footer2);
                Export.SetParameterValue("Footer2", this.documentDetail.Footer2);

                Report.SetParameterValue("Footer3", this.documentDetail.Footer3);
                Export.SetParameterValue("Footer3", this.documentDetail.Footer3);

                Report.SetParameterValue("Footer4", this.documentDetail.Footer4);
                Export.SetParameterValue("Footer4", this.documentDetail.Footer4);

                Report.SetParameterValue("DatePosted", this.DataCon.TransactionSummary.DatePosted);
                Export.SetParameterValue("DatePosted", this.DataCon.TransactionSummary.DatePosted);

                String DocRef = "", ORNo = "";

                if (this.DataCon.TransactionSummary.ORCTLNo != 0)
                {
                    String office = String.Concat(this.DataCon.TransactionDetails.First().OfficeAbv + String.Format("{0:D2}", this.DataCon.TransactionDetails.First().OfficeID));
                    DocRef = office + "-" + String.Format("{0:D2}", this.DataCon.TransactionCode) + "-" + this.DataCon.TransactionSummary.TransYear.ToString() + "-" + String.Format("{0:9}", this.DataCon.TransactionSummary.ORCTLNo);

                    Report.SetParameterValue("ORNumber", "DocRef#:  " + DocRef);
                    Export.SetParameterValue("ORNumber", "DocRef#:  " + DocRef);
                }
                else
                {
                    Report.SetParameterValue("ORNumber", DocRef);
                    Export.SetParameterValue("ORNumber", DocRef);
                }

                if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ORNo))
                {
                    Report.SetParameterValue("ORNo", "OR. No:  " + this.DataCon.TransactionSummary.ORNo);
                    Export.SetParameterValue("ORNo", "OR. No:  " + this.DataCon.TransactionSummary.ORNo);
                }
                else
                {
                    Report.SetParameterValue("ORNo", ORNo);
                    Export.SetParameterValue("ORNo", ORNo);
                }


            }
            catch (Exception)
            {

            }

        }

        private PrintVerify PrintVerify()
        {
            PrintVerify verification = new Models.Reports.PrintVerify();
            try
            {
                CRUXLib.Response Response = App.CRUXAPI.request("reports/printverify", "", 3600000);
                verification = new JavaScriptSerializer().Deserialize<PrintVerify>(Response.Content);

                return verification;
            }
            catch (Exception)
            {
                return verification;
            }
        }

        private void InitializeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            transTypeComboBox.ItemsSource = this.TransactionTypesList;
            ProgressPanel.Visibility = Visibility.Hidden;
            MainGrid.IsEnabled = true;
        }

        private void InitializeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;

            //HeaderDetails For Report
            Response = App.CRUXAPI.request("foreports/headerdetails", "");
            if (Response.Status == "ERROR")
            {

            }
            else
            {
                this.boh = new JavaScriptSerializer().Deserialize<BOHeader>(Response.Content);
                this.DataCon.CurrentBranch = this.boh.BranchID;
            }

            //Transaction Types
            Response = App.CRUXAPI.request("backoffice/transType", "");
            if (Response.Status == "SUCCESS")
            {
                this.TransactionTypesList = new JavaScriptSerializer().Deserialize<List<TransactionType>>(Response.Content);
            }
            else
            {
                MessageBox.Show("Could not load Transaction Types!");
            }

            //UPDTag
            Response = App.CRUXAPI.request("backoffice/UPDTag", "");
            if (Response.Status == "SUCCESS")
            {
                this.UPDTags = new JavaScriptSerializer().Deserialize<List<UPDTag>>(Response.Content);

            }
            else
            {
                MessageBox.Show("Could not load UpdTag!");
            }

            Response = App.CRUXAPI.request("backoffice/glAccount", "");

            if (Response.Status == "SUCCESS")
            {
                this.GLAccounts = new JavaScriptSerializer().Deserialize<List<Models.GLAccounts>>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

            Response = App.CRUXAPI.request("backoffice/FillAccountCodes", "");

            if (Response.Status == "SUCCESS")
            {
                this.FillGLAccounts = new JavaScriptSerializer().Deserialize<List<Models.GLAccounts>>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

            Response = App.CRUXAPI.request("backoffice/EncodedBy", "");

            if (Response.Status == "SUCCESS")
            {
                this.DataCon.PreparedBy = new JavaScriptSerializer().Deserialize<String>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

            Response = App.CRUXAPI.request("reports/docdetails", "6");

            if (Response.Status == "SUCCESS")
            {
                this.documentDetail = new JavaScriptSerializer().Deserialize<DocumentDetails>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void GenerateControlListWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String Title = "";
                CRUXLib.Response Response;

                ControlListHolder ControlListHolder = new ControlListHolder();
                PrintVerify verification = new Models.Reports.PrintVerify();

                var json = new JavaScriptSerializer().Serialize(this.DataCon.ReportType);

                Response = App.CRUXAPI.request("backoffice/GenerateControlList", json);

                if (Response.Status == "ERROR")
                {
                    MessageBox.Show(Response.Content);
                }
                else
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    ControlListHolder = serializer.Deserialize<ControlListHolder>(Response.Content);
                }

                if (this.DataCon.ReportType == "LoanReclass")
                {
                    Title = this.DataCon.Title;
                    Report.SetDataSource(ControlListHolder.LRControlList);
                    Export.SetDataSource(ControlListHolder.LRControlList);
                }
                else if (this.DataCon.ReportType == "IntSavings")
                {
                    Title = this.DataCon.Title;
                    var IntExpList = ControlListHolder.IntDepControlList.FindAll(i => i.ClientID != 0).ToList();
                    Report.SetDataSource(IntExpList);
                    Export.SetDataSource(IntExpList);

                    var explanation = ControlListHolder.IntDepControlList.FindAll(x => x.ClientID == 0).ToList().First().ClientName;

                    Report.SetParameterValue("Explanation", explanation);
                    Export.SetParameterValue("Explanation", explanation);

                    Report.SetParameterValue("SLClass", Title);
                    Export.SetParameterValue("SLClass", Title);

                    //var FromDate = new DateTime(Convert.ToDateTime(this.boh.TransDate).Year, Convert.ToDateTime(this.boh.TransDate).Month, 1);
                    //var ToDate = new DateTime(Convert.ToDateTime(this.boh.TransDate).Year, Convert.ToDateTime(this.boh.TransDate).Month, DateTime.DaysInMonth(Convert.ToDateTime(this.boh.TransDate).Year, Convert.ToDateTime(this.boh.TransDate).Month));

                    //Report.SetParameterValue("FromDate", FromDate.ToString("MM/dd/yyyy"));
                    //Export.SetParameterValue("FromDate", FromDate.ToString("MM/dd/yyyy"));

                    //Report.SetParameterValue("ToDate", ToDate.ToString("MM/dd/yyyy"));
                    //Export.SetParameterValue("ToDate", ToDate.ToString("MM/dd/yyyy"));
                }
                else if (this.DataCon.ReportType == "AvgShareCapital" || this.DataCon.ReportType == "DividendPatronageRefund")
                {
                    Title = this.DataCon.Title;

                    Report.SetDataSource(ControlListHolder.AvgShareCapitalControlList);
                    Export.SetDataSource(ControlListHolder.AvgShareCapitalControlList);

                    Report.SetParameterValue("FromDate", ControlListHolder.AvgShareCapitalControlList.First().FromDate);
                    Report.SetParameterValue("ToDate", ControlListHolder.AvgShareCapitalControlList.First().ToDate);

                    Export.SetParameterValue("FromDate", ControlListHolder.AvgShareCapitalControlList.First().FromDate);
                    Export.SetParameterValue("ToDate", ControlListHolder.AvgShareCapitalControlList.First().ToDate);

                    Report.SetParameterValue("Explanation", ControlListHolder.AvgShareCapitalControlList.First().Explanation);
                    Export.SetParameterValue("Explanation", ControlListHolder.AvgShareCapitalControlList.First().Explanation);

                    Report.SetParameterValue("PreparedByName", this.DataCon.PreparedBy);
                    Export.SetParameterValue("PreparedByName", this.DataCon.PreparedBy);

                    Report.SetParameterValue("Title", Title);
                    Export.SetParameterValue("Title", Title);
                }


                if (this.DataCon.ReportType == "LoanReclass")
                {
                    Report.SetParameterValue("CutOff", this.boh.TransDate);
                    Report.SetParameterValue("SLClass", Title);

                    Export.SetParameterValue("CutOff", this.boh.TransDate);
                    Export.SetParameterValue("SLClass", Title);
                }

                Report.SetParameterValue("OfficeName", this.boh.OfficeName);
                Report.SetParameterValue("OfficeAddress", this.boh.OfficeAddress);

                Export.SetParameterValue("OfficeName", this.boh.OfficeName);
                Export.SetParameterValue("OfficeAddress", this.boh.OfficeAddress);

                verification = PrintVerify();

                Report.SetParameterValue("GenerateDetails", "Generated by: " + verification.Username + " ON Computer:" + verification.ComputerName + ": " + verification.IPAddress);
                Export.SetParameterValue("GenerateDetails", "Generated by: " + verification.Username + " ON Computer:" + verification.ComputerName + ": " + verification.IPAddress);

                Report.SetParameterValue("Verification", "Verification Code: " + verification.VerificationCode);
                Export.SetParameterValue("Verification", "Verification Code: " + verification.VerificationCode);

                Report.SetParameterValue("LetterHead", this.documentDetail.LetterHead);
                Export.SetParameterValue("LetterHead", this.documentDetail.LetterHead);

                Report.SetParameterValue("Header1", this.documentDetail.Header1);
                Export.SetParameterValue("Header1", this.documentDetail.Header1);

                Report.SetParameterValue("Header2", this.documentDetail.Header2);
                Export.SetParameterValue("Header2", this.documentDetail.Header2);

                Report.SetParameterValue("Footer1", this.documentDetail.Footer1);
                Export.SetParameterValue("Footer1", this.documentDetail.Footer1);

                Report.SetParameterValue("Footer2", this.documentDetail.Footer2);
                Export.SetParameterValue("Footer2", this.documentDetail.Footer2);

                Report.SetParameterValue("Footer3", this.documentDetail.Footer3);
                Export.SetParameterValue("Footer3", this.documentDetail.Footer3);

                Report.SetParameterValue("Footer4", this.documentDetail.Footer4);
                Export.SetParameterValue("Footer4", this.documentDetail.Footer4);
            }

            catch (Exception ex)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(this, ex.Message);
                }), System.Windows.Threading.DispatcherPriority.Background);

            }
        }

        private void GenerateControlListWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            MainGrid.IsEnabled = true;

            Viewer = new CrystalReport();
            Viewer.cryRpt = Export;
            Viewer._CrystalReport.ViewerCore.ReportSource = Report;
            Viewer._CrystalReport.ViewerCore.Zoom(130);
            Viewer.Owner = this;
            Viewer.ShowDialog();

        }

        private void GenerateSaveWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;

            switch (SavingMain)
            {
                case 1:
                    CtlNoTextBox.Visibility = Visibility.Visible;

                    MessageBox.Show(this, "Module Was Successfully Added!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearValues();
                    break;

                case 2:
                    MessageBox.Show(this, this.DataCon.ResponseContent, "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 3:
                    MessageBox.Show(this, "Module was not Successfully Added! Please contact Administrator.", "Failed!", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
                default:
                    break;
            }

            MainGrid.IsEnabled = true;
        }

        private List<PurchaseDetails> _PurchaseDetails()
        {
            List<PurchaseDetails> purchaseDetails = new List<PurchaseDetails>();
            int SeqNo = 0;
            try
            {
                if (TempPurchaseDetails.Count != 0)
                {
                    foreach (PurchaseDetails pd in TempPurchaseDetails)
                    {
                        purchaseDetails.Add(new PurchaseDetails()
                        {
                            AccountCode = pd.AccountCode,
                            AccountDesc = pd.AccountDesc,
                            Description = pd.Description,
                            VendorTIN = pd.VendorTIN,
                            VendorDiscountRate = pd.VendorDiscountRate,
                            SeqNo = SeqNo,
                            ItemPrice = pd.ItemPrice,
                            Qty = pd.Qty,
                            GrossIncome = pd.GrossIncome,
                            Discount = pd.Discount,
                            VAT = pd.VAT,
                            NetAmt = pd.NetAmt
                        });

                        SeqNo += 1;
                    }
                }

                return purchaseDetails;
            }
            catch (Exception)
            {
                return purchaseDetails;
            }


        }

        private void GenerateSaveWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;
            TransactionDetailContainer _Transactions = new TransactionDetailContainer();
            _Transactions._TransactionDetailsList = this.DataCon.TransactionDetails;
            _Transactions._TransactionSummary = TempProcessTransactionSummary();
            _Transactions.TransactionCheckList = InsertTransactionCheck();

            _Transactions.PurchaseDetails = _PurchaseDetails();
            _Transactions.PurchaseSummary = InsertProcessPurchaseSummary();

            String parsed = new JavaScriptSerializer().Serialize(_Transactions);
            Response = App.CRUXAPI.request("backoffice/insertBackoffice", parsed, int.MaxValue);

            if (Response.Status == "SUCCESS")
            {
                Int64 CTLNo = new JavaScriptSerializer().Deserialize<Int64>(Response.Content);

                if (CTLNo != 0)
                {
                    this.DataCon.ControlNo = CTLNo;
                    SavingMain = 1;
                    this.DataCon.Response = Response.Status;
                }
                else
                {
                    SavingMain = 3;
                }
            } // end if Response = Success

            else
            {
                SavingMain = 2;
                this.DataCon.ResponseContent = Response.Content;
            }
        }

        private void GenerateReversedWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;

            switch (ReverseSwitch)
            {
                case 1:
                    MessageBox.Show(this, "Module Was Successfully Reversed!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearValues();
                    break;
                case 2:
                    MessageBox.Show(this, "Failed to Insert Transaction Check Details");
                    break;
                case 3:
                    MessageBox.Show("Transaction was not successfully reversed", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 4:
                    MessageBox.Show(this, this.DataCon.ResponseContent, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                default:
                    break;
            }

            MainGrid.IsEnabled = true;
        }

        private void GenerateReversedWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;

            String parsedUPDTrans = new JavaScriptSerializer().Serialize(this.DataCon.UPDTrans);

            Response = App.CRUXAPI.request("backoffice/postTransDetails", parsedUPDTrans);

            if (Response.Status == "SUCCESS")
            {
                if (this.DataCon.TransactionCode == CheckVoucherID)
                {
                    this.DataCon.ResponseContent = Response.Content;

                    this.DataCon.UPDTrans.UPDTagID = this.UPDTags.Find(u => u.UPDTagDesc == "Reversed").UPDTagID;
                    this.DataCon.UPDTrans.CtlNo = this.DataCon.ControlNo;
                    this.DataCon.UPDTrans.TransCode = this.DataCon.TransactionCode;
                    this.DataCon.UPDTrans.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);

                    string parsedReverse = new JavaScriptSerializer().Serialize(this.DataCon.UPDTrans);

                    Response = App.CRUXAPI.request("backoffice/postTransCheckDetails", parsedReverse);

                    if (Response.Status != "SUCCESS")
                    {
                        ReverseSwitch = 2;
                    }
                    else
                    {
                        ReverseSwitch = 1;
                    }
                }
                else
                {
                    this.DataCon.ResponseContent = Response.Content;
                    ReverseSwitch = 1;
                }

            }
            else
            {
                if (Response.Status == "Failed")
                {
                    ReverseSwitch = 4;
                    this.DataCon.ResponseContent = Response.Content;
                }
                else
                {
                    ReverseSwitch = 3;
                    this.DataCon.Response = Response.Status;
                    this.DataCon.ResponseContent = Response.Content;
                }
            }

        }

        private void GeneratePostWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;

            switch (PostingMain)
            {
                case 1:
                    MessageBox.Show(this, "Module Was Successfully Posted!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ClearValues();
                    break;
                case 2:
                    MessageBox.Show(this, this.DataCon.ResponseContent, "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 3:
                    //TODO: UNPost Transactions
                    MessageBox.Show(this, "Unable to approve loan application. Please contact Administrator.", "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 4:
                    MessageBox.Show(this, this.DataCon.ResponseContent, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                default:
                    break;
            }

            MainGrid.IsEnabled = true;
        }

        private void GeneratePostWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;
            String parsedUPDTrans = new JavaScriptSerializer().Serialize(this.DataCon.UPDTrans);
            //var MaxVal = int.MaxValue;

            Response = App.CRUXAPI.request("backoffice/postTransDetails", parsedUPDTrans, int.MaxValue);


            if (Response.Status == "SUCCESS")
            {
                if (ClientsForApprovalList.Count != 0)
                {
                    String parsedUpdateAppStatus = new JavaScriptSerializer().Serialize(ClientsForApprovalList);
                    Response = App.CRUXAPI.request("backoffice/UpdateAppStatus", parsedUpdateAppStatus, int.MaxValue);

                    if (Response.Status != "SUCCESS")
                    {
                        PostingMain = 3;
                    }

                }

                if (this.DataCon.TransactionDetails.First().TransactionCode == Convert.ToInt16(CheckVoucherID) || this.DataCon.TransactionDetails.First().TransactionCode == 16)
                {
                    Models.TransactionDetails transCheckDetail = new Models.TransactionDetails();
                    Models.UPDTrans postTransCheck = new Models.UPDTrans();

                    if (this.DataCon.TransactionDetails.First().TransactionCode != 16)
                    {
                        foreach (Models.TransactionDetails detail in this.DataCon.TransactionDetails)
                        {
                            if (detail.SLC_Code == "12" && detail.Amount > 0 && detail.Debit != 0)
                            {
                                transCheckDetail.SLC_Code = detail.SLC_Code;
                                transCheckDetail.BranchCode = detail.BranchCode;
                                transCheckDetail.SLT_Code = detail.SLT_Code;
                                transCheckDetail.ReferenceNo = detail.ReferenceNo;
                                transCheckDetail.ClientID = detail.ClientID;

                                UpdateLoanApp(transCheckDetail);
                            }
                        }
                    }

                    postTransCheck.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
                    postTransCheck.TransCode = this.DataCon.TransactionCode;
                    postTransCheck.CtlNo = this.DataCon.ControlNo;
                    postTransCheck.UPDTagID = Convert.ToByte(this.UPDTags.Find(u => u.UPDTagID == 1).UPDTagID);

                    PostTransCheck(postTransCheck);
                }

                PostingMain = 1;
                this.DataCon.Response = Response.Status;
                this.DataCon.ResponseContent = Response.Content;
            }
            else
            {
                if (Response.Status == "Failed")
                {
                    PostingMain = 4;
                    this.DataCon.ResponseContent = Response.Content;
                }
                else
                {
                    PostingMain = 2;
                    this.DataCon.Response = Response.Status;
                    this.DataCon.ResponseContent = Response.Content;
                }
            }

        }

        private void GenerateUpdateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            MainGrid.IsEnabled = true;

            if (this.DataCon.Response == "SUCCESS" && !String.IsNullOrEmpty(this.DataCon.Response))
            {
                MessageBox.Show(this, "Module Was Successfully Updated!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                ClearValues();
            }
            else if (this.DataCon.Response != "SUCCESS" && !String.IsNullOrEmpty(this.DataCon.Response))
            {
                DG_GeneralJournal.IsReadOnly = true;
                MessageBox.Show(this, this.DataCon.ResponseContent, "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GenerateUpdateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;

            TransactionDetailContainer _Transactions = new TransactionDetailContainer();
            _Transactions._TransactionDetailsList = this.DataCon.TransactionDetails;
            _Transactions._TransactionSummary = ProcessTransactionSummary(_Transactions._TransactionDetailsList.First().BranchCode, _Transactions._TransactionDetailsList.First().TransactionCode,
                    _Transactions._TransactionDetailsList.First().ControlNo, String.Format("{0:D8}", _Transactions._TransactionDetailsList.First().ControlNo));


            List<TransactionCheck> checkDetails = new List<TransactionCheck>();
            checkDetails = InsertTransactionCheck(this.DataCon.ControlNo);

            _Transactions.TransactionCheckList = checkDetails;
            _Transactions.CTLNoNew = this.DataCon.ControlNew;
            _Transactions.OrigTransDate = OrigDate;

            String parsed = new JavaScriptSerializer().Serialize(_Transactions);
            Response = App.CRUXAPI.request("backoffice/TempUpdateBackoffice", parsed, int.MaxValue);

            if (Response.Status == "SUCCESS")
            {
                this.DataCon.Response = Response.Status;

            } // end Response
            else
            {
                this.DataCon.Response = Response.Status;
                this.DataCon.ResponseContent = "Failed to Update Data. Please Contact Administrator.";
            }
        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private List<AccountNumbers> CheckClosedAcctsBeforePosting(List<TransactionDetails> TransactionDetailsList)
        {
            List<AccountNumbers> ClosedAccountsList = new List<AccountNumbers>();
            try
            {
                string parsed = new JavaScriptSerializer().Serialize(TransactionDetailsList);
                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckClosingStatus", parsed, 999999999);

                if (Response.Status == "SUCCESS")
                {
                    ClosedAccountsList = new JavaScriptSerializer().Deserialize<List<AccountNumbers>>(Response.Content);
                }

                return ClosedAccountsList;
            }
            catch (Exception ex)
            {
                return ClosedAccountsList;
            }
        }

        private void CloseWindow()
        {
            try
            {
                if (!(transTypeComboBox.SelectedIndex == -1))
                {
                    MessageBoxResult result = MessageBox.Show(this, "Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:

                            ClearValues();
                            NewTransaction();
                            newButton.IsEnabled = true;
                            saveButton.IsEnabled = false;
                            //ExplanationTextBox.IsEnabled = false;
                            this.ExplanationTextBox.IsReadOnly = true;
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;
                    }

                } // end if
                else
                {
                    MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to Exit this Module?", "Cancel", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            this.Close();
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception)
            {
                return;
            }

        }

        private void DefaultFormGridSize()
        {
            Grid.SetRow(tabControl, 2);
            Grid.SetRowSpan(tabControl, 1);

            RecDate.Visibility = Visibility.Visible;
            RecTransType.Visibility = Visibility.Visible;
            transTypeComboBox.Visibility = Visibility.Visible;
            ExplanationTextBox.Visibility = Visibility.Visible;
            ExpLabel.Visibility = Visibility.Visible;
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            ExecuteHelper exec = new ExecuteHelper(this);

            try
            {
                var selected = DG_GeneralJournal.SelectedItem as TransactionDetails;

                if (e.Key == Key.Up)
                {
                    if (!ExplanationTextBox.IsFocused)
                    {
                        if (DG_GeneralJournal.IsReadOnly)
                        {
                            if (currentRow > 0)
                            {
                                DG_GeneralJournal.Focus();

                                int previousIndex = DG_GeneralJournal.SelectedIndex - 1;
                                if (previousIndex < 0) return;
                                currentRow = previousIndex;
                                DG_GeneralJournal.SelectedIndex = previousIndex;
                                DG_GeneralJournal.ScrollIntoView(DG_GeneralJournal.Items[currentRow]);
                            }
                        }
                    }

                }
                else if (e.Key == Key.Down)
                {
                    if (!ExplanationTextBox.IsFocused)
                    {
                        if (DG_GeneralJournal.IsReadOnly)
                        {
                            if (currentRow < DG_GeneralJournal.Items.Count - 1)
                            {
                                DG_GeneralJournal.Focus();

                                int nextIndex = DG_GeneralJournal.SelectedIndex + 1;
                                if (nextIndex > DG_GeneralJournal.Items.Count - 1) return;
                                currentRow = nextIndex;
                                DG_GeneralJournal.SelectedIndex = nextIndex;
                                DG_GeneralJournal.ScrollIntoView(DG_GeneralJournal.Items[currentRow]);
                            }
                        }
                    }

                } // end else if (e.Key == Key.Down)

                else if (e.Key == Key.PageDown && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    DG_GeneralJournal.SelectedIndex = this.DataCon.TransactionDetails.Count() - 1;
                    DG_GeneralJournal.ScrollIntoView(DG_GeneralJournal.Items[DG_GeneralJournal.SelectedIndex]);
                }

                else if (e.Key == Key.PageUp && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    DG_GeneralJournal.SelectedIndex = 0;
                    DG_GeneralJournal.ScrollIntoView(DG_GeneralJournal.Items[DG_GeneralJournal.SelectedIndex]);
                }

                else if (e.Key == Key.PageUp)
                {
                    PreviousDocument();
                } //Alt + PageUp
                else if (e.Key == Key.PageDown)
                {
                    NextDocument();
                } //Alt + PageDown

                else if (e.Key == Key.F4)
                {
                    if (Grid.GetRowSpan(tabControl) == 1)
                    {
                        Grid.SetRow(tabControl, 1);
                        Grid.SetRowSpan(tabControl, 3);
                        RecDate.Visibility = Visibility.Collapsed;
                        RecTransType.Visibility = Visibility.Collapsed;
                        transTypeComboBox.Visibility = Visibility.Collapsed;
                        ExplanationTextBox.Visibility = Visibility.Collapsed;
                        ExpLabel.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        DefaultFormGridSize();
                    }
                }

                else if (e.Key == Key.Escape)
                {
                    try
                    {
                        for (int i = 0; i <= DG_GeneralJournal.Columns.Count; i++)
                        {
                            if (DG_GeneralJournal.CurrentColumn.DisplayIndex == i)
                            {
                                if (GetDataGridCell(DG_GeneralJournal.SelectedCells[i]).IsFocused)
                                {
                                    CloseWindow();
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        CloseWindow();
                    }

                }

                else if (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.E))
                {
                    if (TransEditMaintTrigger)
                    {
                        if (App.CRUXAPI.isAllowed("update_journal"))
                        {
                            if (Lock() == "SUCCESS")
                            {
                                if (editButton.IsEnabled)
                                {
                                    EnableEdit(e);
                                    editButton.IsEnabled = false;
                                    saveButton.IsEnabled = true;
                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "Transaction is currently on Edit Mode by another User: " + Lock());
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }

                else if (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G))
                {
                    GLTab.IsSelected = true;
                }
                else if (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.C))
                {
                    COCITab.IsSelected = true;
                }

                else if (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.F8))
                {
                    LoanApplicationParams loanAppParams = new LoanApplicationParams();
                    loanAppParams.ClientID = selected.ClientID;
                    loanAppParams.BranchID = Convert.ToInt64(this.boh.BranchID);
                    loanAppParams.SLC = Convert.ToByte(selected.SLC_Code);
                    loanAppParams.SLT = Convert.ToByte(selected.SLT_Code);
                    loanAppParams.REF_NO = selected.ReferenceNo;

                    String parsed = new JavaScriptSerializer().Serialize(loanAppParams);
                    App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Loans", "LoansApplication.exe", this, "Back_Office", parsed);
                    App.CRUXAPI.closeCallBack += openModule;
                }

                else if (e.Key == Key.F7)
                {
                    try
                    {
                        if (DG_GeneralJournal.SelectedItem != null)
                        {
                            if (selected.ClientID != 0 && !String.IsNullOrEmpty(selected.ClientIDFormat) && this.DataCon.CurrentBranch != 0)
                            {
                                ClientParams clientParams = new ClientParams();
                                clientParams.ClientID = selected.ClientID.ToString();
                                clientParams.BranchID = this.DataCon.CurrentBranch.ToString();
                                String parsed = new JavaScriptSerializer().Serialize(clientParams);
                                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "SL_Setup.exe", this, "Back_Office", parsed);
                                App.CRUXAPI.closeCallBack += openModule;
                            }
                            else
                            {
                                if (this.DataCon.CurrentBranch == 0)
                                {
                                    ClientParams clientParams = new ClientParams();
                                    clientParams.ClientID = selected.ClientID.ToString();
                                    clientParams.BranchID = this.boh.BranchID.ToString();
                                    String parsed = new JavaScriptSerializer().Serialize(clientParams);
                                    App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "SL_Setup.exe", this, "Back_Office", parsed);
                                    App.CRUXAPI.closeCallBack += openModule;
                                }
                            }
                        }

                        else
                        {
                            e.Handled = true;
                        }
                    }
                    catch (Exception)
                    {
                        e.Handled = true;
                    }

                } // end F7
                else if (e.Key == Key.F && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
                {
                    if (findButton.IsEnabled)
                    {
                        FindTransaction();
                    }
                }
                else if (e.Key == Key.F8 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
                {
                    try
                    {
                        clientParams.ClientID = selected.ClientID.ToString();
                        clientParams.BranchID = this.DataCon.CurrentBranch.ToString();

                        if (selected.ClientID != 0 && !String.IsNullOrEmpty(clientParams.BranchID))
                        {
                            String parsed = new JavaScriptSerializer().Serialize(clientParams);
                            App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "Members Information.exe", this, "Back_Office", parsed);
                            App.CRUXAPI.closeCallBack += openModule;
                        }

                        else
                        {
                            if (selected.ClientID != 0 && String.IsNullOrEmpty(clientParams.BranchID))
                            {
                                clientParams.BranchID = this.boh.BranchID.ToString();
                                clientParams.ClientID = selected.ClientID.ToString();

                                String parsed = new JavaScriptSerializer().Serialize(clientParams);
                                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "Members Information.exe", this, "Back_Office", parsed);
                                App.CRUXAPI.closeCallBack += openModule;
                            }
                            else
                            {
                                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "Members Information.exe", this, "Back_Office", "");
                                App.CRUXAPI.closeCallBack += openModule;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        e.Handled = true;
                        App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Client", "Members Information.exe", this, "Back_Office", "");
                        App.CRUXAPI.closeCallBack += openModule;
                    }
                } //end ctrl + f8

                else if (e.Key == Key.F8 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift &&
                    (Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
                {
                    try
                    {
                        if (this.DataCon.CurrentBranch == 0)
                        {
                            slAccountParams.BranchID = this.boh.BranchID;
                        }
                        else
                        {
                            slAccountParams.BranchID = this.DataCon.CurrentBranch;
                        }

                        slAccountParams.ClientID = selected.ClientID;
                        slAccountParams.SLCode = Convert.ToByte(selected.SLC_Code);
                        slAccountParams.SLTCode = Convert.ToByte(selected.SLT_Code);
                        slAccountParams.IsF8 = false; //false

                        if ((selected.ClientID != 0 || !String.IsNullOrEmpty(selected.ClientIDFormat)) &&
                            ((!String.IsNullOrEmpty(selected.SLC) || !String.IsNullOrEmpty(selected.SLC_Code)) &&
                            !String.IsNullOrEmpty(selected.SLT_Code)))
                        {
                            String parsed = new JavaScriptSerializer().Serialize(slAccountParams);
                            Thread.Sleep(50);
                            App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Back Office", "SLAccountSetup.exe", this, "Back_Office", parsed);
                            App.CRUXAPI.closeCallBack += openModule;
                        }

                        else
                        {
                            e.Handled = true;
                        }
                    }
                    catch (Exception)
                    {
                        e.Handled = true;
                    }

                } //end shift + F8

                else if (e.Key == Key.F8 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift &&
                    (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Back Office", "System Setup.exe", this, "Back_Office", "");
                    App.CRUXAPI.closeCallBack += openModule;
                } //end ctrl + shift + f8

                else if (e.Key == Key.T && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
                {
                    ActivityLogsParams _activityLogParams = new ActivityLogsParams();
                    _activityLogParams.BranchCode = this.boh.BranchID;
                    _activityLogParams.TR_CTLNO = this.DataCon.ControlNo;
                    _activityLogParams.TR_Code = this.DataCon.TransactionCode;
                    _activityLogParams.TR_Date = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("yyyy/MM/dd");
                    _activityLogParams.ModuleID = 9;

                    String parsed = new JavaScriptSerializer().Serialize(_activityLogParams);
                    App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "System", "AuditLog.exe", this, "Back_Office", parsed);
                    App.CRUXAPI.closeCallBack += openModule;
                } //end ctrl + f8

                else if (e.Key == Key.F8 && (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift &&
                    (Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
                {
                    try
                    {
                        if (this.DataCon.ControlNo != 0)
                        {
                            slAccountParams.BranchID = selected.BranchCode;
                        }
                        else
                        {
                            if (this.DataCon.CurrentBranch == 0)
                            {
                                slAccountParams.BranchID = this.boh.BranchID;
                            }
                            else
                            {
                                slAccountParams.BranchID = this.DataCon.CurrentBranch;
                            }
                        }

                        slAccountParams.ClientID = selected.ClientID;
                        slAccountParams.SLCode = Convert.ToByte(selected.SLC_Code);
                        slAccountParams.SLTCode = Convert.ToByte(selected.SLT_Code);
                        slAccountParams.RefNo = selected.ReferenceNo;
                        slAccountParams.IsF8 = true;

                        if ((selected.ClientID != 0 || !String.IsNullOrEmpty(selected.ClientIDFormat)) &&
                           (!String.IsNullOrEmpty(selected.SLC) || !String.IsNullOrEmpty(selected.SLC_Code)) &&
                           !String.IsNullOrEmpty(selected.SLT_Code) && !String.IsNullOrEmpty(selected.ReferenceNo))
                        {
                            if (selected.SLC_Code == "11" || selected.SLC == "11")
                            {
                                e.Handled = true;
                            }
                            else
                            {
                                String parsed = new JavaScriptSerializer().Serialize(slAccountParams);
                                App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Back Office", "SLAccountSetup.exe", this, "Back_Office", parsed);
                                App.CRUXAPI.closeCallBack += openModule;
                            }
                        }

                        else
                        {
                            e.Handled = true;
                        }
                    }
                    catch (Exception)
                    {
                        e.Handled = true;
                    }

                } //end f8

                else if (e.Key == Key.F9 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
                {
                    PrintDocument();

                } //end ctrl + f8

                else if (e.Key == Key.F3 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift) //shift + F3
                {
                    execHelper = new ExecuteHelper(this);

                    if (String.IsNullOrEmpty(referenceNumberTextBox.Text))
                    {
                        MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                    }
                    else
                    {
                        if (!DG_GeneralJournal.IsReadOnly)
                        {
                            if (this.DataCon.ControlNo == 0)
                            {
                                MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                            }
                            else
                            {
                                execHelper.ExecuteCancel(this.DataCon.Upd, e, this.DataCon.ClosedStatus);

                            } //end else refNo != ""

                        } // end if  DG_JournalGrid is readonly
                        else
                        {
                            execHelper.ExecuteCancel(this.DataCon.Upd, e, this.DataCon.ClosedStatus);
                        }
                    }
                }

                else if (e.Key == Key.F11 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift) //CTRL + F11
                {
                    try
                    {
                        execHelper = new ExecuteHelper(this);

                        if (!DG_GeneralJournal.IsReadOnly)
                        {
                            MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                        }
                        else
                        {
                            if (MainGrid.IsEnabled)
                            {
                                execHelper.ExecutePost(this.DataCon.Upd, e, this.DataCon.ClosedStatus);
                            }

                        } //end else

                    }

                    catch (Exception ex)
                    {
                        if (this.DataCon.ControlNo == 0)
                        {
                            MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                        }
                    }

                } //end ctrl+ f11

                else if (e.Key == Key.P && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift) //CTRL + P
                {
                    PrintVoucher();

                } //end ctrl+ P

                else if (e.Key == Key.F9 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift &&
                    (Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control) //Shift + F9
                {
                    try
                    {
                        if (this.DataCon.TransactionChecks.Count.Equals(0))
                        {
                            MessageBox.Show(this, "Please insert check details!");
                        }
                        else
                        {
                            PrintCheck();
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(this, "Requires document for printing!");
                    }

                } //end //Shift + F9

                else if (e.Key == Key.F11 && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift &&
                    (Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control) //shift + f11
                {
                    try
                    {
                        execHelper = new ExecuteHelper(this);

                        if (!DG_GeneralJournal.IsReadOnly)
                        {
                            if (this.DataCon.ControlNo == 0)
                            {
                                MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                            }
                            else
                            {
                                if (MainGrid.IsEnabled)
                                {
                                    execHelper.ExecuteReverse(this.DataCon.Upd, e, this.DataCon.ClosedStatus);
                                }

                            } //end else refNo != ""

                        } // end if  DG_JournalGrid is readonly

                        else
                        {
                            if (MainGrid.IsEnabled)
                            {
                                execHelper.ExecuteReverse(this.DataCon.Upd, e, this.DataCon.ClosedStatus);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        if (this.DataCon.ControlNo == 0)
                        {
                            MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                        }
                    }

                } //end if key == F11 and Modifier == shift Reversing

                else if (e.Key == Key.F11 && (Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control &&
                    (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
                {
                    if (MainGrid.IsEnabled)
                    {
                        SaveTransaction(null, e);
                    }
                } //F11

            }//end try


            catch (Exception)
            {
                e.Handled = true;
            }
        }

        public void PopulateGL()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/glAccount", "");

            if (Response.Status == "SUCCESS")
            {
                this.GLAccounts = new JavaScriptSerializer().Deserialize<List<Models.GLAccounts>>(Response.Content);
            }
            else
            {
                MessageBox.Show(this, Response.Content);
            }
        }

        class TransType
        {
            public String TransTypeMode { get; set; }
            public String TransTypeDesc { get; set; }
        }

        private void GetTransDetail(DocInquiryParameters InqParams)
        {
            TransType trType = new TransType();
            CRUXLib.Response Response;
            String parsed = new JavaScriptSerializer().Serialize(InqParams.TransCode);

            Response = App.CRUXAPI.request("backoffice/SelectTransModeType", parsed);

            if (Response.Status == "SUCCESS")
            {
                trType = new JavaScriptSerializer().Deserialize<TransType>(Response.Content);
                this.Title = trType.TransTypeDesc;

                switch (trType.TransTypeMode)
                {

                    case "2":
                    case "3":

                        clientLabel.Content = "Client ...............";
                        AndOrLabel.Content = "Or...................";

                        clientLabel.Visibility = Visibility.Visible;
                        clientIDTextBox.Visibility = Visibility.Visible;
                        AndOrLabel.Visibility = Visibility.Visible;
                        clientName.Visibility = Visibility.Visible;
                        clientORTextBox.Visibility = Visibility.Visible;
                        break;
                    case "PJ":

                        clientLabel.Content = "Vendor:";
                        AndOrLabel.Content = " ";

                        clientLabel.Visibility = Visibility.Visible;
                        clientIDTextBox.Visibility = Visibility.Visible;
                        AndOrLabel.Visibility = Visibility.Visible;
                        clientName.Visibility = Visibility.Visible;
                        clientORTextBox.Visibility = Visibility.Visible;
                        break;

                    default:
                        break;
                }

                String parsedDocInq = new JavaScriptSerializer().Serialize(InqParams);
                Response = App.CRUXAPI.request("backoffice/SelectTransDetail", parsedDocInq);

                if (Response.Status == "SUCCESS")
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = Int32.MaxValue;
                    _transDetailContainer = serializer.Deserialize<TransactionDetailContainer>(Response.Content);

                    if (_transDetailContainer._TransactionDetailsList.Count != 0)
                    {
                        IsInquiry = true;
                        this.DisplayDocInq(_transDetailContainer, null);
                        //this.ExplanationTextBox.IsEnabled = false;
                        this.ExplanationTextBox.IsReadOnly = true;
                        this.editButton.IsEnabled = true;
                        this.saveButton.IsEnabled = false;
                    }
                }
                else
                {
                    MessageBox.Show(this, "Failed to retrieve data");
                }
            }

        }

        private void scText_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 ||
                       e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                {
                    transTypeComboBox.IsEnabled = false;
                    newButton.IsEnabled = false;
                    //ExplanationTextBox.IsEnabled = true;

                    ExplanationTextBox.IsReadOnly = false;
                }
                else
                {
                    e.Handled = true;
                }


                var selectedTransaction = DG_GeneralJournal.SelectedItem as TransactionDetails;

                if (selectedTransaction != null)
                {
                    if (!DG_GeneralJournal.IsReadOnly)
                    {
                        if (e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 || e.Key == Key.Back)
                        {
                            if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 1)
                            {
                                if (!String.IsNullOrWhiteSpace(selectedTransaction.SLT_Code) ||
                                     String.IsNullOrWhiteSpace(selectedTransaction.SLE_Code) ||
                                     String.IsNullOrWhiteSpace(selectedTransaction.SLN_Code) ||
                                     selectedTransaction.AccountCode != 0 ||
                                     String.IsNullOrWhiteSpace(selectedTransaction.accountDesc))
                                {
                                    selectedTransaction.SLC = String.Empty;
                                    selectedTransaction.SLT_Code = String.Empty;
                                    selectedTransaction.SLE_Code = String.Empty;
                                    selectedTransaction.SLN_Code = String.Empty;
                                    selectedTransaction.AccountCode = 0;
                                    selectedTransaction.Debit = 0;
                                    selectedTransaction.Credit = 0;
                                    selectedTransaction.accountDesc = String.Empty;
                                    selectedTransaction.ClientID = 0;
                                    selectedTransaction.ClientIDFormat = String.Empty;
                                    selectedTransaction.clientName = String.Empty;
                                    selectedTransaction.ReferenceNo = String.Empty;

                                    selectedTransaction.AdjFlag = 0;
                                    selectedTransaction.AdjFlagCode = String.Empty;

                                    //CalculateTotalDebitCredit(0, 0);
                                    CalculateTotalPrice();
                                }
                            } // end if datagrid = 1

                            else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 2)
                            {
                                if (!String.IsNullOrWhiteSpace(selectedTransaction.SLE_Code) ||
                                    !String.IsNullOrWhiteSpace(selectedTransaction.SLN_Code) ||
                                    selectedTransaction.AccountCode != 0 ||
                                    !String.IsNullOrWhiteSpace(selectedTransaction.accountDesc))
                                {
                                    selectedTransaction.SLE_Code = String.Empty;
                                    selectedTransaction.SLN_Code = String.Empty;
                                    selectedTransaction.AccountCode = 0;
                                    selectedTransaction.Debit = 0;
                                    selectedTransaction.Credit = 0;
                                    selectedTransaction.accountDesc = String.Empty;
                                    selectedTransaction.ClientID = 0;
                                    selectedTransaction.ClientIDFormat = String.Empty;
                                    selectedTransaction.clientName = String.Empty;
                                    selectedTransaction.ReferenceNo = String.Empty;

                                    selectedTransaction.AdjFlag = 0;
                                    selectedTransaction.AdjFlagCode = String.Empty;

                                    //CalculateTotalDebitCredit(0, 0);
                                    CalculateTotalPrice();
                                }
                                else if (String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                {
                                    e.Handled = true;
                                }
                            } // end else datagrid = 2

                            else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 3)
                            {
                                if (!String.IsNullOrWhiteSpace(selectedTransaction.SLN_Code) ||
                                    selectedTransaction.AccountCode != 0 ||
                                    !String.IsNullOrWhiteSpace(selectedTransaction.accountDesc))
                                {
                                    selectedTransaction.SLN_Code = String.Empty;
                                    selectedTransaction.AccountCode = 0;
                                    selectedTransaction.Debit = 0;
                                    selectedTransaction.Credit = 0;
                                    selectedTransaction.accountDesc = String.Empty;
                                    selectedTransaction.ClientID = 0;
                                    selectedTransaction.ClientIDFormat = String.Empty;
                                    selectedTransaction.clientName = String.Empty;
                                    selectedTransaction.ReferenceNo = String.Empty;

                                    selectedTransaction.AdjFlag = 0;
                                    selectedTransaction.AdjFlagCode = String.Empty;
                                    CalculateTotalPrice();
                                }
                                else if (String.IsNullOrEmpty(selectedTransaction.SLC_Code) ||
                                    String.IsNullOrEmpty(selectedTransaction.SLT_Code))
                                {
                                    e.Handled = true;
                                }

                            } // end else datagrid = 3

                            else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 4)
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.accountDesc) ||
                                    selectedTransaction.AccountCode != 0)
                                {
                                    selectedTransaction.AccountCode = 0;
                                    selectedTransaction.Debit = 0;
                                    selectedTransaction.Credit = 0;
                                    selectedTransaction.accountDesc = String.Empty;
                                    selectedTransaction.ClientID = 0;
                                    selectedTransaction.ClientIDFormat = String.Empty;
                                    selectedTransaction.clientName = String.Empty;
                                    selectedTransaction.ReferenceNo = String.Empty;

                                    selectedTransaction.AdjFlag = 0;
                                    selectedTransaction.AdjFlagCode = String.Empty;

                                    //CalculateTotalDebitCredit(0, 0);
                                    CalculateTotalPrice();
                                }

                                else if (String.IsNullOrEmpty(selectedTransaction.SLC_Code) ||
                                    String.IsNullOrEmpty(selectedTransaction.SLT_Code) ||
                                    String.IsNullOrEmpty(selectedTransaction.SLE_Code))
                                {
                                    e.Handled = true;
                                }
                            }

                            else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 5)
                            {

                                if (!String.IsNullOrWhiteSpace(selectedTransaction.AccountCode.ToString()) ||
                                    String.IsNullOrWhiteSpace(selectedTransaction.SLC_Code) ||
                                        String.IsNullOrWhiteSpace(selectedTransaction.SLE_Code) ||
                                             String.IsNullOrWhiteSpace(selectedTransaction.SLN_Code) ||
                                                 selectedTransaction.AccountCode != 0 ||
                                                    String.IsNullOrWhiteSpace(selectedTransaction.accountDesc))
                                {

                                    selectedTransaction.SLC_Code = String.Empty;
                                    selectedTransaction.SLT_Code = String.Empty;
                                    selectedTransaction.SLE_Code = String.Empty;
                                    selectedTransaction.SLN_Code = String.Empty;

                                    selectedTransaction.Debit = 0;
                                    selectedTransaction.Credit = 0;
                                    selectedTransaction.accountDesc = String.Empty;
                                    selectedTransaction.ClientID = 0;
                                    selectedTransaction.ClientIDFormat = String.Empty;
                                    selectedTransaction.clientName = String.Empty;
                                    selectedTransaction.ReferenceNo = String.Empty;

                                    selectedTransaction.AdjFlag = 0;
                                    selectedTransaction.AdjFlagCode = String.Empty;

                                    CalculateTotalPrice();

                                } //end if SLC,SLT,SLE,SLN,AcctCode not empty

                            }//end current column = 5

                            else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 9)
                            {
                                if (!String.IsNullOrWhiteSpace(selectedTransaction.ReferenceNo))
                                {
                                    selectedTransaction.ReferenceNo = "";
                                }
                            } // end else datagrid = 9

                        }//end if key = numbers
                    }//end Datagrid is readonly
                } // end if transaction != null
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void DG_GeneralJournal_LostFocus(object sender, System.EventArgs e)
        {

            try
            {
                var selectedTransDetails = DG_GeneralJournal.SelectedItem as Models.TransactionDetails;

                if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 1)
                {
                    if (temp)
                    {
                        DG_GeneralJournal.CanUserAddRows = false;
                        temp = false;
                    }
                    DG_GeneralJournal.CanUserAddRows = false;
                }

                if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 2)
                {
                    if (!String.IsNullOrWhiteSpace(selectedTransDetails.SLT_Code))
                    {
                        if (selectedTransDetails.SLC_Code == "11")
                        {

                            selectedTransDetails.SLC = selectedTransDetails.SLC_Code + selectedTransDetails.SLT_Code;
                        }
                        else
                        {
                            selectedTransDetails.SLC_Code = selectedTransDetails.SLC_Code;
                        }
                        temp = true;
                    }
                    else
                    {
                        temp = false;
                    }

                }
                //end putting value of stcode;

                if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 3)
                {

                    (DG_GeneralJournal.SelectedItem as TransactionDetails).SLDate = this.DataCon.DateTimeNow;

                    if (!String.IsNullOrWhiteSpace(selectedTransDetails.SLE_Code) && String.IsNullOrEmpty(selectedTransDetails.SLN_Code) && selectedTransDetails.AccountCode == 0 && String.IsNullOrEmpty(selectedTransDetails.accountDesc))
                    {
                        if (String.IsNullOrWhiteSpace(selectedTransDetails.SLC))
                        {
                            slnset = dgFocus.FillSLNDataGrid(selectedTransDetails.SLC_Code, selectedTransDetails.SLT_Code, selectedTransDetails.SLE_Code);
                            selectedTransDetails.SLN_Code = slnset.SLN.ToString();
                            selectedTransDetails.AccountCode = slnset.AccountCode;
                            selectedTransDetails.accountDesc = slnset.AccountDesc;
                        }

                        if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
                        {
                            if (clientIDTextBox.Text != "")
                            {
                                if (selectedTransDetails.SLC_Code != "11")
                                {
                                    selectedTransDetails.ClientIDFormat = this.DataCon.TransactionSummary.ClientIDFormatTransSum;
                                    selectedTransDetails.ClientID = this.DataCon.TransactionSummary.ClientID;
                                    selectedTransDetails.clientName = this.DataCon.TransactionSummary.ClientNameTransSum;
                                }

                            }
                        }

                        if (!DG_GeneralJournal.IsReadOnly)
                        {
                            if ((selectedTransDetails.SLC_Code == "12" || selectedTransDetails.SLC_Code == "13") && DG_GeneralJournal.SelectedIndex == 0)
                            {
                                if (selectedTransDetails.SLE_Code == "11" || (selectedTransDetails.SLE_Code == "22" && selectedTransDetails.SLC_Code == "12"))
                                {
                                    selectedTransDetails.AdjFlagCode = "S";
                                    selectedTransDetails.AdjFlag = 2;
                                }
                                else
                                {
                                    selectedTransDetails.AdjFlagCode = "P";
                                    selectedTransDetails.AdjFlag = 1;
                                }

                            }
                            else if (selectedTransDetails.SLC_Code == "12" && DG_GeneralJournal.SelectedIndex == 1)
                            {
                                if (selectedTransDetails.SLE_Code == "22")
                                {
                                    selectedTransDetails.AdjFlagCode = "S";
                                    selectedTransDetails.AdjFlag = 2;
                                }
                                else
                                {
                                    selectedTransDetails.AdjFlagCode = "P";
                                    selectedTransDetails.AdjFlag = 1;
                                }
                            }
                            else
                            {
                                selectedTransDetails.AdjFlagCode = "P";
                                selectedTransDetails.AdjFlag = 1;
                            }
                        }

                    }
                    else
                    {
                        if (!DG_GeneralJournal.IsReadOnly)
                        {
                            if ((selectedTransDetails.SLC_Code == "12" || selectedTransDetails.SLC_Code == "13") && DG_GeneralJournal.SelectedIndex == 0)
                            {
                                if (selectedTransDetails.SLE_Code == "11" || (selectedTransDetails.SLE_Code == "22" && selectedTransDetails.SLC_Code == "12"))
                                {
                                    selectedTransDetails.AdjFlagCode = "S";
                                    selectedTransDetails.AdjFlag = 2;
                                }
                                else
                                {
                                    selectedTransDetails.AdjFlagCode = "P";
                                    selectedTransDetails.AdjFlag = 1;
                                }
                            }
                            else if (selectedTransDetails.SLC_Code == "12" && DG_GeneralJournal.SelectedIndex == 1)
                            {
                                if (selectedTransDetails.SLE_Code == "22")
                                {
                                    selectedTransDetails.AdjFlagCode = "S";
                                    selectedTransDetails.AdjFlag = 2;
                                }
                                else
                                {
                                    selectedTransDetails.AdjFlagCode = "P";
                                    selectedTransDetails.AdjFlag = 1;
                                }
                            }
                            else
                            {
                                selectedTransDetails.AdjFlagCode = "P";
                                selectedTransDetails.AdjFlag = 1;
                            }

                            return;
                        }
                    }

                }//end putting value of slecode;

                if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 5)
                {
                    selectedTransDetails.SLDate = this.DataCon.DateTimeNow;
                    DG_GeneralJournal.CanUserAddRows = false;

                }

                if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 8)
                {

                    if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID) || this.DataCon.TransactionCode == PurchaseJournalID)
                    {
                        if (selectedTransDetails.SLC_Code == "11")
                        {
                            selectedTransDetails.ReferenceNo = "1";
                            FillInitialCheckDetails(selectedTransDetails.AccountCode);

                            foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                            {
                                checkDetail.Amount = selectedTransDetails.Credit;
                            }

                            CalculateTotalCOCIAmt();
                        }

                    }
                }

                //putting value of sntype,glaccount and account description;
                if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 9)
                {
                    selectedTransDetails.TransactionDate = this.DataCon.DateTimeNow;
                }

            }
            catch (Exception)
            {
                return;
            }

        }

        public System.Windows.Controls.DataGridCell GetDataGridCell(System.Windows.Controls.DataGridCellInfo cellInfo)
        {
            var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);

            if (cellContent != null)
                return ((System.Windows.Controls.DataGridCell)cellContent.Parent);

            return (null);
        }

        public System.Windows.Controls.DataGridRow GetDataGridRow(System.Windows.Controls.DataGridCellInfo cellInfo)
        {

            var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);

            if (cellContent != null)
                return ((System.Windows.Controls.DataGridRow)((System.Windows.Controls.DataGridCell)cellContent.Parent).Parent);

            return (null);
        }

        private void DG_JournalRefresh()
        {
            DG_GeneralJournal.IsReadOnly = true;
            DG_GeneralJournal.Items.Refresh();
            DG_GeneralJournal.IsReadOnly = false;
        }

        private void DG_PurchaseRefresh()
        {
            PurchaseDataGrid.IsReadOnly = true;
            PurchaseDataGrid.Items.Refresh();
            PurchaseDataGrid.IsReadOnly = false;
        }

        private void DG_GeneralJournal_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int i = DG_GeneralJournal.CurrentColumn.DisplayIndex;
                var selectedTransaction = DG_GeneralJournal.SelectedItem as Models.TransactionDetails;

                if (!DG_GeneralJournal.IsReadOnly)
                {
                    if (e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.Tab || e.Key == Key.F2 || e.Key == Key.F12 || e.Key == Key.Back ||
                        (e.Key == Key.OemPeriod || e.Key == Key.Decimal))
                    {
                        transTypeComboBox.IsEnabled = false;
                        newButton.IsEnabled = false;
                        //  ExplanationTextBox.IsEnabled = true;
                        this.ExplanationTextBox.IsReadOnly = false;

                        if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 1 &&
                                (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[1]).IsFocused)
                                {
                                    DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.CurrentItem, DG_GeneralJournal.Columns[5]);
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Enter && (String.IsNullOrEmpty(selectedTransaction.SLC_Code)))
                                {
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Enter && (!String.IsNullOrEmpty(selectedTransaction.SLC_Code)))
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[2]));
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[1]).IsFocused)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                    {
                                        selectedTransaction.SLC = String.Empty;
                                        selectedTransaction.SLT_Code = String.Empty;
                                        selectedTransaction.SLE_Code = String.Empty;
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;
                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();
                                    }
                                }

                                else if (e.Key != Key.Back && e.Key != Key.Enter)
                                {

                                    if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                    {
                                        selectedTransaction.SLC = String.Empty;
                                        selectedTransaction.SLT_Code = String.Empty;
                                        selectedTransaction.SLE_Code = String.Empty;
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();

                                        temp = true;
                                        slClass = new SLClass(this);
                                        slClass.Owner = this;
                                        slClass.ShowDialog();
                                    }
                                    else
                                    {
                                        temp = true;
                                        slClass = new SLClass(this);
                                        slClass.Owner = this;
                                        slClass.ShowDialog();
                                    }

                                } //end else

                            }
                            catch (Exception)
                            {

                                e.Handled = true;
                            }
                        }

                        else if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 2 &&
                            (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {

                            try
                            {
                                if ((e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[2]).IsFocused) ||
                                    (String.IsNullOrEmpty(selectedTransaction.SLC_Code)))
                                {
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Enter && !String.IsNullOrEmpty(selectedTransaction.SLT_Code))
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[3]));
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[2]).IsFocused)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                    {
                                        selectedTransaction.SLT_Code = String.Empty;
                                        selectedTransaction.SLE_Code = String.Empty;
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();
                                    }
                                }

                                else if (e.Key != Key.Back && e.Key != Key.Enter)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLT_Code))
                                    {
                                        selectedTransaction.SLT_Code = String.Empty;
                                        selectedTransaction.SLE_Code = String.Empty;
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();

                                        temp = true;
                                        slType = new SLType(Int32.Parse(selectedTransaction.SLC_Code.ToString()), this);
                                        slType.Owner = this;
                                        slType.ShowDialog();
                                        e.Handled = true;
                                    }
                                    else
                                    {
                                        temp = true;
                                        slType = new SLType(Int32.Parse(selectedTransaction.SLC_Code.ToString()), this);
                                        slType.Owner = this;
                                        slType.ShowDialog();
                                        e.Handled = true;
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                e.Handled = true;
                            }

                        }

                        else if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 3 &&
                                (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            try
                            {
                                if (String.IsNullOrEmpty(selectedTransaction.SLE_Code))
                                {
                                    e.Handled = true;
                                }
                                if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[3]).IsFocused)

                                {
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Enter && !String.IsNullOrEmpty(selectedTransaction.SLE_Code))
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[4]));
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[3]).IsFocused)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                    {
                                        selectedTransaction.SLE_Code = String.Empty;
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        // CalculateTotalPrice();
                                    }
                                }

                                else if (e.Key != Key.Back && e.Key != Key.Enter)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLE_Code))
                                    {
                                        selectedTransaction.SLE_Code = String.Empty;
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();

                                        temp = true;
                                        if (selectedTransaction.SLC_Code == "11")
                                        {
                                            sleType = new SLEType(Int32.Parse(selectedTransaction.SLC), Convert.ToByte(selectedTransaction.SLT_Code), this);
                                        }
                                        else
                                        {
                                            sleType = new SLEType(Int32.Parse(selectedTransaction.SLC_Code), Convert.ToByte(selectedTransaction.SLT_Code), this);
                                        }

                                        sleType.Owner = this;
                                        sleType.ShowDialog();
                                        e.Handled = true;
                                    }
                                    else
                                    {
                                        temp = true;
                                        if (selectedTransaction.SLC_Code == "11")
                                        {
                                            sleType = new SLEType(Int32.Parse(selectedTransaction.SLC), Convert.ToByte(selectedTransaction.SLT_Code), this);
                                        }
                                        else
                                        {
                                            sleType = new SLEType(Int32.Parse(selectedTransaction.SLC_Code), Convert.ToByte(selectedTransaction.SLT_Code), this);
                                        }

                                        sleType.Owner = this;
                                        sleType.ShowDialog();
                                        e.Handled = true;
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                e.Handled = true;
                            }

                        }

                        else if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 4 &&
                                (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            try
                            {
                                if (String.IsNullOrEmpty(selectedTransaction.SLC_Code) || String.IsNullOrEmpty(selectedTransaction.SLT_Code) ||
                                        String.IsNullOrEmpty(selectedTransaction.SLE_Code))
                                { e.Handled = true; }

                                else if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[4]).IsFocused)
                                {
                                    e.Handled = true;
                                }
                                if (selectedTransaction.SLC_Code == null || selectedTransaction.SLT_Code == null || selectedTransaction.SLE_Code == null)
                                {
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Enter && (selectedTransaction.SLN_Code != "" || selectedTransaction.SLN_Code != null))
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[5]));
                                }

                                else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[4]).IsFocused)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                    {
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;
                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();
                                    }
                                }

                                else if (e.Key != Key.Back && e.Key != Key.Enter)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLN_Code))
                                    {
                                        selectedTransaction.SLN_Code = String.Empty;
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();

                                        temp = true;
                                        snType = new SNType(selectedTransaction.SLC_Code.ToString(), selectedTransaction.SLT_Code.ToString(), selectedTransaction.SLE_Code.ToString(), this);
                                        snType.Owner = this;
                                        snType.ShowDialog();
                                        e.Handled = true;
                                    }
                                    else
                                    {
                                        temp = true;
                                        snType = new SNType(selectedTransaction.SLC_Code.ToString(), selectedTransaction.SLT_Code.ToString(), selectedTransaction.SLE_Code.ToString(), this);
                                        snType.Owner = this;
                                        snType.ShowDialog();
                                        e.Handled = true;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                e.Handled = true;
                            }
                        }

                        else if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 5 &&
                                (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[5]).IsFocused)
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));
                                    e.Handled = true;
                                }

                                else if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code) && !String.IsNullOrEmpty(selectedTransaction.SLT_Code)
                                    && !String.IsNullOrEmpty(selectedTransaction.SLE_Code) && e.Key != Key.Back)
                                {
                                    e.Handled = true;
                                }
                                else if (e.Key != Key.Space && (selectedTransaction.AccountCode != 0) && e.Key != Key.Back)
                                {
                                    TempFillAccountCodeGrid(selectedTransaction.AccountCode);

                                    //selectedTransaction.AdjFlagCode = "P";
                                    //selectedTransaction.AdjFlag = 1;

                                    //FillAccountCodeGrid(selectedTransaction.AccountCode);

                                    CalculateTotalPrice();
                                    e.Handled = true;
                                }

                                else if (e.Key == Key.Enter && selectedTransaction.AccountCode != 0)
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                                    e.Handled = true;
                                }
                                else if (e.Key == Key.Space && !String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                {
                                    e.Handled = true;
                                }
                                else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[5]).IsFocused)
                                {
                                    if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                    {
                                        selectedTransaction.AccountCode = 0;
                                        selectedTransaction.Debit = 0;
                                        selectedTransaction.Credit = 0;
                                        selectedTransaction.accountDesc = String.Empty;
                                        selectedTransaction.ClientID = 0;
                                        selectedTransaction.ClientIDFormat = String.Empty;
                                        selectedTransaction.clientName = String.Empty;
                                        selectedTransaction.ReferenceNo = String.Empty;

                                        selectedTransaction.AdjFlag = 0;
                                        selectedTransaction.AdjFlagCode = String.Empty;

                                        //CalculateTotalDebitCredit(0, 0);
                                        CalculateTotalPrice();
                                    }
                                }
                                else if (e.Key != Key.Back && e.Key != Key.Enter)
                                {
                                    DG_GeneralJournal.BeginEdit();

                                    glAccount = new GLAccount(this.GLAccounts, this);
                                    glAccount.Owner = this;
                                    glAccount.ShowDialog();
                                    e.Handled = true;
                                }

                            }
                            catch (Exception)
                            {
                                temp = true;
                                glAccount = new GLAccount(this.GLAccounts, this);
                                glAccount.Owner = this;
                                glAccount.ShowDialog();
                            }

                        }

                        else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 7 && (e.Key != Key.Tab || e.Key == Key.Back))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[7]).IsFocused)
                                {
                                    e.Handled = true;

                                    if (selectedTransaction.Debit == 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[8]));
                                    }
                                    else if (selectedTransaction.Debit != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[9]));
                                    }
                                }
                                else if (e.Key == Key.Enter && !GetDataGridCell(DG_GeneralJournal.SelectedCells[7]).IsFocused)
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[9]));

                                    if (e.Key == Key.Enter && selectedTransaction.Debit == 0)
                                    {
                                        selectedTransaction.Debit = 0;
                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));

                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                                        DG_GeneralJournal.BeginEdit();
                                    }
                                    else if (e.Key == Key.Enter && selectedTransaction.Debit != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[9]));
                                        e.Handled = true;
                                    }
                                }

                                if (e.Key == Key.Space)
                                {
                                    e.Handled = true;
                                }

                                if (e.Key == Key.Back)
                                {
                                    if (selectedTransaction.Debit != 0)
                                    {
                                        selectedTransaction.Debit = 0;
                                    }
                                }

                                //CalculateTotalDebitCredit(selectedTransaction.Debit, 0);

                                CalculateTotalPrice();

                                if (e.Key == Key.F12)
                                {
                                    if (selectedTransaction.Credit != 0)
                                    {
                                        selectedTransaction.Credit = 0;
                                    }

                                    //CalculateTotalDebitCredit(selectedTransaction.Debit, 0);

                                    CalculateTotalPrice();

                                    if (selectedTransaction.Debit != 0)
                                    {
                                        if (this.DataCon.TotalDifference != 0)
                                        {
                                            if (this.DataCon.TotalDebit >= this.DataCon.TotalCredit)
                                            {
                                                selectedTransaction.Debit = selectedTransaction.Debit - this.DataCon.TotalDifference;

                                                if (selectedTransaction.Debit <= 0)
                                                {
                                                    selectedTransaction.Debit = 0;
                                                }
                                            }
                                            else
                                            {
                                                selectedTransaction.Debit = selectedTransaction.Debit + this.DataCon.TotalDifference;
                                            }
                                        }
                                    }
                                    else if (selectedTransaction.Debit == 0)
                                    {
                                        if (this.DataCon.TotalDebit >= this.DataCon.TotalCredit)
                                        {
                                            selectedTransaction.Debit = 0;
                                        }
                                        else
                                        {
                                            selectedTransaction.Debit = this.DataCon.TotalDifference;
                                        }
                                    }

                                    if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
                                    {
                                        foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                                        {
                                            checkDetail.Amount = selectedTransaction.Debit;
                                        }

                                        CalculateTotalCOCIAmt();
                                    }

                                    //CalculateTotalDebitCredit(selectedTransaction.Debit, 0);
                                    CalculateTotalPrice();
                                }
                            }

                            catch (Exception)
                            {
                                e.Handled = true;
                            }
                        }

                        else if (DG_GeneralJournal.CurrentColumn.DisplayIndex == 8 && (e.Key != Key.Tab || e.Key == Key.Back))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[8]).IsFocused)
                                {
                                    if (selectedTransaction.Credit != 0)
                                    {
                                        e.Handled = true;
                                        DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.CurrentItem, DG_GeneralJournal.Columns[9]);
                                    }
                                    else
                                    {
                                        e.Handled = true;
                                        DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.CurrentItem, DG_GeneralJournal.Columns[7]);
                                    }

                                }
                                else if (e.Key == Key.Enter && !GetDataGridCell(DG_GeneralJournal.SelectedCells[8]).IsFocused)
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[9]));

                                    if (e.Key == Key.Enter && selectedTransaction.Credit == 0)
                                    {
                                        selectedTransaction.Credit = 0;
                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));

                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[8]));
                                        DG_GeneralJournal.BeginEdit();
                                    }

                                    else if (e.Key == Key.Enter && selectedTransaction.Credit != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[9]));
                                        e.Handled = true;
                                    }
                                }

                                if (e.Key == Key.Space)
                                {
                                    e.Handled = true;
                                }

                                if (e.Key == Key.Back)
                                {
                                    if (selectedTransaction.Credit != 0)
                                    {
                                        selectedTransaction.Credit = 0;
                                    }
                                }

                                //CalculateTotalDebitCredit(0, selectedTransaction.Credit);
                                CalculateTotalPrice();

                                if (e.Key == Key.F12)
                                {
                                    if (selectedTransaction.Debit != 0)
                                    {
                                        selectedTransaction.Debit = 0;
                                    }

                                    //CalculateTotalDebitCredit(0, selectedTransaction.Credit);
                                    CalculateTotalPrice();

                                    if (selectedTransaction.Credit != 0)
                                    {
                                        if (this.DataCon.TotalDifference != 0)
                                        {
                                            if (this.DataCon.TotalCredit >= this.DataCon.TotalDebit)
                                            {
                                                selectedTransaction.Credit = selectedTransaction.Credit - this.DataCon.TotalDifference;

                                                if (selectedTransaction.Credit <= 0)
                                                {
                                                    selectedTransaction.Credit = 0;
                                                }
                                            }
                                            else
                                            {
                                                selectedTransaction.Credit = selectedTransaction.Credit + this.DataCon.TotalDifference;
                                            }
                                        }
                                    }
                                    else if (selectedTransaction.Credit == 0)
                                    {
                                        if (this.DataCon.TotalCredit >= this.DataCon.TotalDebit)
                                        {
                                            selectedTransaction.Credit = 0;
                                        }
                                        else
                                        {
                                            selectedTransaction.Credit = this.DataCon.TotalDifference;
                                        }


                                    }

                                    if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID) || this.DataCon.TransactionCode == PurchaseJournalID)
                                    {
                                        FillInitialCheckDetails(selectedTransaction.AccountCode);

                                        foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                                        {
                                            checkDetail.Amount = selectedTransaction.Credit;
                                        }

                                        CalculateTotalCOCIAmt();
                                    }

                                    //CalculateTotalDebitCredit(0, selectedTransaction.Credit);
                                    CalculateTotalPrice();
                                }
                            }

                            catch (Exception)
                            {
                                e.Handled = true;
                            }

                        }

                        else if ((e.Key == Key.Space || e.Key == Key.F2 || e.Key == Key.Enter || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 9 &&
                                (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            if ((String.IsNullOrEmpty(selectedTransaction.SLC_Code) || String.IsNullOrEmpty(selectedTransaction.SLT_Code)
                                    || String.IsNullOrEmpty(selectedTransaction.SLE_Code)) && selectedTransaction.AccountCode == 0 && e.Key != Key.Back)
                            {
                                e.Handled = true;
                            }
                            else if (e.Key == Key.Enter && GetDataGridCell(DG_GeneralJournal.SelectedCells[9]).IsFocused)
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.ClientIDFormat) && !String.IsNullOrEmpty(selectedTransaction.clientName))
                                {
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[11]));
                                }
                                else
                                {
                                    e.Handled = true;
                                }
                            }
                            else if (e.Key == Key.Enter && (selectedTransaction.ClientIDFormat != null &&
                                (!String.IsNullOrEmpty(selectedTransaction.SLC_Code) ||
                                !String.IsNullOrEmpty(selectedTransaction.SLT_Code) ||
                                !String.IsNullOrEmpty(selectedTransaction.SLE_Code) ||
                                (selectedTransaction.AccountCode != 0 && !String.IsNullOrEmpty(selectedTransaction.accountDesc))
                                ))
                                )
                            {
                                FillClientGrid(Convert.ToInt64(selectedTransaction.ClientIDFormat));
                                e.Handled = true;
                            }
                            else if (e.Key == Key.Enter && selectedTransaction.ClientIDFormat == null)
                            {
                                e.Handled = true;
                            }
                            else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[9]).IsFocused)
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.ClientIDFormat))
                                {
                                    selectedTransaction.ClientIDFormat = string.Empty;
                                    selectedTransaction.clientName = string.Empty;
                                }
                            }
                            else if (e.Key != Key.Back && e.Key != Key.Enter)
                            {
                                temp = true;
                                clientsearch = new clientSearch("GJ", this, this.boh.BranchID, this.DataCon.ClientIDSearch, this.DataCon.ClientNameSearch);
                                clientsearch.Owner = this;
                                clientsearch.ShowDialog();
                            }
                        }

                        else if ((e.Key == Key.Space || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 11)
                        {
                            if (selectedTransaction.ClientID == 0 && e.Key != Key.Back)
                            {
                                MessageBox.Show(this, "Select Client!");
                            }
                            else if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[11]).IsFocused)
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.ReferenceNo))
                                {
                                    selectedTransaction.ReferenceNo = string.Empty;
                                }
                            }
                            else if (e.Key != Key.Back && e.Key == Key.Space)
                            {
                                temp = true;
                                Thread.Sleep(10);

                                acctCode = new AccountCode(this, selectedTransaction.ClientID, Convert.ToByte(selectedTransaction.SLC_Code),
                                    Convert.ToByte(selectedTransaction.SLT_Code), Convert.ToByte(selectedTransaction.SLE_Code), selectedTransaction.SLDate,
                                    Convert.ToByte(selectedTransaction.SLN_Code));
                                acctCode.Owner = this;
                                acctCode.ShowDialog();
                            }
                        }

                        else if ((e.Key == Key.Space || e.Key == Key.Back) && DG_GeneralJournal.CurrentColumn.DisplayIndex == 12)
                        {
                            if (e.Key == Key.Back && GetDataGridCell(DG_GeneralJournal.SelectedCells[12]).IsFocused)
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.AdjFlagCode))
                                {
                                    selectedTransaction.AdjFlagCode = string.Empty;
                                }
                            }
                            else if (e.Key != Key.Back && e.Key == Key.Space)
                            {
                                temp = true;
                                Thread.Sleep(10);

                                _AdjFlag = new AdjFlag(this);
                                _AdjFlag.Owner = this;
                                _AdjFlag.ShowDialog();
                            }
                        }

                        else if (e.Key == Key.Tab)
                        {
                            if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID) || this.DataCon.TransactionCode == PurchaseJournalID)
                            {
                                MoveFocusToCOCIGrid();
                                e.Handled = true;
                            }
                            else
                            {
                                ExplanationTextBox.Focus();
                                DG_GeneralJournal.CanUserAddRows = false;
                                e.Handled = true;
                            }
                        }

                        else if (e.Key != Key.Decimal || e.Key != Key.OemPeriod)
                        {
                            e.Handled = true;
                        }

                    } // end if key : enter, space, f2, etc

                    else
                    {
                        if (e.Key == Key.Escape && DG_GeneralJournal.CurrentColumn.DisplayIndex == 9)
                        {
                            if (GetDataGridCell(DG_GeneralJournal.SelectedCells[9]).IsEditing)
                            {
                                selectedTransaction.ClientIDFormat = String.Empty;
                            }
                        }

                        else if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 ||
                             e.Key == Key.OemPeriod || e.Key == Key.Decimal) &&
                                 DG_GeneralJournal.CurrentColumn.DisplayIndex == 7)
                        {
                            try
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code) && !String.IsNullOrEmpty(selectedTransaction.SLT_Code) &&
                                    !String.IsNullOrEmpty(selectedTransaction.SLE_Code) && !String.IsNullOrEmpty(selectedTransaction.SLN_Code) ||
                                        (!String.IsNullOrEmpty(selectedTransaction.accountDesc) && selectedTransaction.AccountCode != 0))
                                {
                                    if (selectedTransaction.Debit != 0)
                                    {
                                        selectedTransaction.Credit = 0;
                                    }
                                    else if (selectedTransaction.Debit == 0)
                                    {
                                        selectedTransaction.Credit = 0;
                                    }
                                }
                                else
                                {
                                    e.Handled = true;
                                }

                            }
                            catch (Exception)
                            {
                                e.Handled = true;
                            }

                        } //end if Column 7

                        else if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 ||
                                    e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                                        && DG_GeneralJournal.CurrentColumn.DisplayIndex == 8)
                        {
                            try
                            {
                                if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code) && !String.IsNullOrEmpty(selectedTransaction.SLT_Code) &&
                                    !String.IsNullOrEmpty(selectedTransaction.SLE_Code) && !String.IsNullOrEmpty(selectedTransaction.SLN_Code) ||
                                        (!String.IsNullOrEmpty(selectedTransaction.accountDesc) && selectedTransaction.AccountCode != 0))
                                {
                                    if (selectedTransaction.Credit != 0)
                                    {
                                        selectedTransaction.Debit = 0;
                                    }
                                    else if (selectedTransaction.Credit == 0)
                                    {
                                        selectedTransaction.Debit = 0;
                                    }
                                }
                                else
                                {
                                    e.Handled = true;
                                }

                            }
                            catch (Exception)
                            {
                                e.Handled = true;
                            }

                        } //end column 8

                        else if (e.Key == Key.Delete)
                        {
                            Decimal Debit = 0, Credit = 0;

                            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to Delete this Row?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                            switch (result)
                            {
                                case MessageBoxResult.Yes:
                                    try
                                    {
                                        if (selectedTransaction != null)
                                        {
                                            var count = TempTransactionDetailsList.Count;// DG_GeneralJournal.Items.Count;

                                            if (count == 1)
                                            {
                                                if (this.DataCon.ControlNo != 0) //for editing
                                                {
                                                    TempTransactionDetailsList.Remove(selectedTransaction);

                                                    TempTransactionDetailsList.Add(new Models.TransactionDetails()
                                                    {
                                                        BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch),
                                                        TransactionCode = this.DataCon.TransactionCode,
                                                        SLDate = this.DataCon.DateTimeNow,
                                                        ControlNo = this.DataCon.ControlNo,
                                                    });

                                                    DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;
                                                    DG_JournalRefresh();
                                                    MoveFocusToFirstRow();

                                                }
                                                else //new transaction
                                                {
                                                    TempTransactionDetailsList.Remove(selectedTransaction);
                                                    TempTransactionDetailsList.Add(new Models.TransactionDetails());
                                                    DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;
                                                    DG_JournalRefresh();
                                                    MoveFocusToFirstRow();
                                                }

                                                e.Handled = true;

                                                //CalculateTotalDebitCredit(0, 0);
                                                CalculateTotalPrice();
                                            }
                                            else
                                            {
                                                Debit = selectedTransaction.Debit;
                                                Credit = selectedTransaction.Credit;

                                                var select = DG_GeneralJournal.SelectedIndex;

                                                TempTransactionDetailsList.Remove(selectedTransaction);
                                                DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;
                                                DG_JournalRefresh();

                                                if (select == 0)
                                                {
                                                    MoveFocusToFirstRow();
                                                    e.Handled = true;
                                                }
                                                else
                                                {
                                                    if (DG_GeneralJournal.Items.Count > select)
                                                    {
                                                        DG_GeneralJournal.SelectedIndex = select;
                                                    }
                                                    else
                                                    {
                                                        DG_GeneralJournal.SelectedIndex = select - 1;
                                                    }

                                                    DG_GeneralJournal.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));

                                                    DG_GeneralJournal.Focus();
                                                    DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.SelectedItem,
                                                       DG_GeneralJournal.Columns[1]);
                                                    e.Handled = true;
                                                }

                                                //CalculateDeductionDebitCredit(Debit, Credit);
                                                CalculateTotalPrice();
                                            }
                                        }
                                        else
                                        {
                                            TempTransactionDetailsList.Remove(selectedTransaction);
                                            TempTransactionDetailsList.Add(new Models.TransactionDetails());

                                            DG_JournalRefresh();
                                            DG_GeneralJournal.Focus();
                                            Keyboard.Focus(dgFocus.GetDataGridCell(DG_GeneralJournal.SelectedCells[DG_GeneralJournal.Items.Count]));
                                            e.Handled = true;

                                            //CalculateTotalDebitCredit(0, 0);
                                            CalculateTotalPrice();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        e.Handled = true;
                                    }
                                    break;

                                case MessageBoxResult.No:
                                    e.Handled = true;
                                    break;
                            }

                        }

                        else if (e.Key == Key.Insert)
                        {
                            e.Handled = true;
                            var index = DG_GeneralJournal.Items.IndexOf(DG_GeneralJournal.SelectedItem) + 1;

                            if (this.DataCon.ControlNo == 0)
                            {
                                TempTransactionDetailsList.Insert(index, new Models.TransactionDetails());

                                DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;
                                DG_JournalRefresh();
                                MoveFocusToNewRow();
                            }
                            else
                            {

                                Models.TransactionDetails selected = new Models.TransactionDetails();
                                selected.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
                                selected.TransactionCode = this.DataCon.TransactionCode;
                                selected.SLDate = this.DataCon.DateTimeNow;
                                selected.ControlNo = this.DataCon.ControlNo;

                                TempTransactionDetailsList.Insert(index, selected);
                                DG_JournalRefresh();
                                MoveFocusToNewRow();
                            }
                        }

                        else if (e.Key == Key.Up)
                        {
                            try
                            {
                                if (DG_GeneralJournal.SelectedIndex == 0)
                                {
                                    e.Handled = true;
                                }
                                else
                                {
                                    var currentCol = DG_GeneralJournal.CurrentCell.Column.DisplayIndex;

                                    DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.SelectedIndex--,
                                        DG_GeneralJournal.Columns[DG_GeneralJournal.CurrentCell.Column.DisplayIndex]);
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[currentCol]));

                                    e.Handled = true;
                                }

                            }
                            catch (Exception)
                            {

                                e.Handled = true;
                            }
                        }

                        else if (e.Key == Key.Down)
                        {
                            try
                            {

                                if (DG_GeneralJournal.SelectedIndex > DG_GeneralJournal.Items.Count - 2)
                                {
                                    e.Handled = true;
                                }
                                else
                                {
                                    var currentRow = DG_GeneralJournal.CurrentCell.Column.DisplayIndex;
                                    DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.SelectedIndex++,
                                        DG_GeneralJournal.Columns[DG_GeneralJournal.CurrentCell.Column.DisplayIndex]);
                                    Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[currentRow]));
                                    e.Handled = true;
                                }

                            }
                            catch (Exception)
                            {

                                e.Handled = true;
                            }
                        }

                        else if (e.Key == Key.Delete)
                        {
                            e.Handled = true;
                        }

                    }

                } // if read only

                else if (e.Key == Key.Tab)
                {
                    if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
                    {
                        COCITab.IsSelected = true;
                    }
                    else
                    {
                        ExplanationTextBox.Focus();
                        DG_GeneralJournal.CanUserAddRows = false;
                        e.Handled = true;
                    }

                }

                else if (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G))
                {
                    GLTab.IsSelected = true;
                }

                else if (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.C))
                {
                    COCITab.IsSelected = true;
                }

                else if (!(e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Left))
                {
                    e.Handled = true;
                }
            }

            catch (Exception ex)
            {
                e.Handled = true;
            }

        }

        private void DG_GeneralJournal_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                var selectedIndex = DG_GeneralJournal.SelectedItem as TransactionDetails;

                if (selectedIndex.SLC_Code.Count(char.IsLetterOrDigit) == 2 && DG_GeneralJournal.CurrentColumn.DisplayIndex == 1)
                {
                    if (e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up || e.Key == Key.Down)
                    {
                        return;
                    }
                    else
                    {
                        if (!(e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
                        {
                            e.Handled = true;
                        }
                        else
                        {
                            var focusedElement = Keyboard.FocusedElement as UIElement;
                            focusedElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        }
                    }
                }
                else if (selectedIndex.SLT_Code.Count(char.IsLetterOrDigit) == 2 && DG_GeneralJournal.CurrentColumn.DisplayIndex == 2)
                {
                    if (e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up || e.Key == Key.Down)
                    {
                        return;
                    }
                    else
                    {
                        if (!(e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
                        {
                            e.Handled = true;
                        }
                        else
                        {
                            var focusedElement = Keyboard.FocusedElement as UIElement;
                            focusedElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        }
                    }
                }
                else if (selectedIndex.SLE_Code.Count(char.IsLetterOrDigit) == 2 && DG_GeneralJournal.CurrentColumn.DisplayIndex == 3)
                {
                    if (e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up || e.Key == Key.Down)
                    {
                        return;
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(selectedIndex.SLC))
                        {
                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[4]));
                            e.Handled = true;
                        }
                        else
                        {
                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                        }

                    }
                } //end if displayIndex = 3
                else if (selectedIndex.SLN_Code.Count(char.IsLetterOrDigit) == 2 && DG_GeneralJournal.CurrentColumn.DisplayIndex == 4)
                {
                    if (e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up || e.Key == Key.Down)
                    {
                        return;
                    }
                    else
                    {

                        selectedIndex.SLDate = this.DataCon.DateTimeNow;

                        if (!String.IsNullOrWhiteSpace(selectedIndex.SLN_Code))
                        {
                            if (!String.IsNullOrWhiteSpace(selectedIndex.SLC))
                            {

                                slnset = dgFocus.FillAcctCodeAndDesc(selectedIndex.SLC_Code, selectedIndex.SLT_Code, selectedIndex.SLE_Code, selectedIndex.SLN_Code);

                                selectedIndex.AccountCode = slnset.AccountCode;
                                selectedIndex.accountDesc = slnset.AccountDesc;

                                if (selectedIndex.SLC_Code == "11")
                                {
                                    selectedIndex.ReferenceNo = "1";
                                }
                            }

                            if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
                            {
                                if (selectedIndex.SLC_Code != "11")
                                {
                                    selectedIndex.ClientIDFormat = this.DataCon.TransactionSummary.ClientIDFormatTransSum;
                                    selectedIndex.ClientID = this.DataCon.TransactionSummary.ClientID;
                                    selectedIndex.clientName = this.DataCon.TransactionSummary.ClientNameTransSum;

                                }
                                else if (selectedIndex.SLC_Code == "11")
                                {
                                    selectedIndex.ReferenceNo = "1";
                                    //FillInitialCheckDetails(selectedIndex.AccountCode);
                                }
                            }
                        }

                        if (slnset.AccountCode != 0 && !String.IsNullOrEmpty(slnset.AccountDesc))
                        {
                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                            e.Handled = true;
                        }
                        else
                        {
                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[5]));
                            e.Handled = true;
                        }

                        selectedIndex.AdjFlagCode = "P";
                        selectedIndex.AdjFlag = 1;
                    }
                }

            }
            catch (Exception ex)
            {

                return;
            }
        }

        private void MoveFocusToNewRow()
        {
            DG_GeneralJournal.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
            DG_GeneralJournal.SelectedIndex = DG_GeneralJournal.Items.IndexOf(DG_GeneralJournal.SelectedItem) + 1;
            DG_GeneralJournal.Focus();
            DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.SelectedItem,
               DG_GeneralJournal.Columns[1]);
        }

        private void MoveFocusToNewRowPOGrid()
        {
            PurchaseDataGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
            PurchaseDataGrid.SelectedIndex = PurchaseDataGrid.Items.IndexOf(PurchaseDataGrid.SelectedItem) + 1;
            PurchaseDataGrid.Focus();
            PurchaseDataGrid.CurrentCell = new DataGridCellInfo(PurchaseDataGrid.SelectedItem,
               PurchaseDataGrid.Columns[0]);
        }

        private void MoveFocusToFirstRow()
        {
            DG_GeneralJournal.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
            DG_GeneralJournal.SelectedIndex = 0;
            DG_GeneralJournal.Focus();
            DG_GeneralJournal.CurrentCell = new DataGridCellInfo(DG_GeneralJournal.SelectedItem,
               DG_GeneralJournal.Columns[1]);
        }

        private void MoveFocusToCOCIGrid()
        {
            COCITab.IsSelected = true;
            COCIDataGrid.SelectedIndex = 0;

            if (this.DataCon.TransactionChecks.Count == 0)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    Keyboard.Focus(GetDataGridCell(COCIDataGrid.SelectedCells[0]));
                }), System.Windows.Threading.DispatcherPriority.Background);
            }
            else
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    Keyboard.Focus(GetDataGridCell(COCIDataGrid.SelectedCells[1]));
                }), System.Windows.Threading.DispatcherPriority.Background);
            }
        }

        public void FillInitialCheckDetails(Int64 acctCode)
        {
            String parsedAcctCode = new JavaScriptSerializer().Serialize(acctCode);

            CRUXLib.Response ResponseAcctCode = App.CRUXAPI.request("backoffice/SelectBankCode", parsedAcctCode);

            if (ResponseAcctCode.Status == "SUCCESS")
            {
                Models.BankCode BankCode = new JavaScriptSerializer().Deserialize<Models.BankCode>(ResponseAcctCode.Content);

                Byte clearingDays = Convert.ToByte(checkTypesList.Find(c => c.ShortCutKey == String.Format("u").ToUpper()).CheckTypeDays);
                Byte CheckTypeID = Convert.ToByte(checkTypesList.Find(c => c.ShortCutKey == String.Format("u").ToUpper()).CheckTypeID);
                String CheckTypeDesc = checkTypesList.Find(c => c.ShortCutKey == String.Format("u").ToUpper()).CheckTypeDesc;

                if (this.DataCon.ControlNo == 0)
                {
                    if (acctCode != 0)
                    {
                        if (this.DataCon.TransactionChecks.Count == 0)
                        {
                            this.DataCon.TransactionChecks.Add(new TransactionCheck()
                            {
                                BankID = BankCode.BankID,
                                BankCodeDesc = BankCode.BankDesc,
                                AcctCode = BankCode.AcctCode,
                                CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy"),
                                CociType = 2,
                                CociTypeDesc = "Check",
                                ClearingDays = clearingDays.ToString(),
                                CheckType = CheckTypeID,
                                CheckTypeDesc = CheckTypeDesc,
                                CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays)
                            });

                            //this.DataCon.TotalCOCI =
                        }
                        else
                        {
                            foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                            {
                                checkDetail.BankID = BankCode.BankID;
                                checkDetail.BankCodeDesc = BankCode.BankDesc;
                                checkDetail.AcctCode = BankCode.AcctCode;
                                checkDetail.CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
                                checkDetail.CociType = 2;
                                checkDetail.CociTypeDesc = "Check";
                                checkDetail.ClearingDays = clearingDays.ToString();
                                checkDetail.CheckType = CheckTypeID;
                                checkDetail.CheckTypeDesc = CheckTypeDesc;
                                checkDetail.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                            }
                        }

                    }
                    else if (this.DataCon.ControlNo != 0)
                    {
                        foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                        {
                            checkDetail.BankID = BankCode.BankID;
                            checkDetail.BankCodeDesc = BankCode.BankDesc;
                            checkDetail.AcctCode = BankCode.AcctCode;
                            checkDetail.CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
                            checkDetail.CociType = 2;
                            checkDetail.CociTypeDesc = "Check";
                            checkDetail.ClearingDays = clearingDays.ToString();
                            checkDetail.CheckType = CheckTypeID;
                            checkDetail.CheckTypeDesc = CheckTypeDesc;
                            checkDetail.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                        }
                    }

                    COCIDataGrid.CanUserAddRows = false;

                }
                else
                {
                    if (this.DataCon.TransactionChecks.Count == 0)
                    {
                        this.DataCon.TransactionChecks.Add(new TransactionCheck()
                        {
                            BankID = BankCode.BankID,
                            BankCodeDesc = BankCode.BankDesc,
                            AcctCode = BankCode.AcctCode,
                            CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy"),
                            CociType = 2,
                            CociTypeDesc = "Check",
                            ClearingDays = clearingDays.ToString(),
                            CheckType = CheckTypeID,
                            CheckTypeDesc = CheckTypeDesc,
                            CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays)
                        });
                    }
                    else
                    {
                        foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                        {
                            checkDetail.BankID = BankCode.BankID;
                            checkDetail.BankCodeDesc = BankCode.BankDesc;
                            checkDetail.AcctCode = BankCode.AcctCode;
                            checkDetail.CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
                            checkDetail.CociType = 2;
                            checkDetail.CociTypeDesc = "Check";
                            checkDetail.ClearingDays = clearingDays.ToString();
                            checkDetail.CheckType = CheckTypeID;
                            checkDetail.CheckTypeDesc = CheckTypeDesc;
                            checkDetail.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Failed to Retrieve Data!");
            }
        }

        private void TempFillAccountCodeGrid(Int64 accountCode)
        {
            Models.GLAccounts selectedAccountCode = this.FillGLAccounts.Where(a => a.Code == accountCode).FirstOrDefault();

            try
            {
                if (selectedAccountCode.Code != 0)
                {
                    //to do: for PO Journal auto-fill

                    String parsedDocInq = new JavaScriptSerializer().Serialize(selectedAccountCode);

                    CRUXLib.Response ResponseGL = App.CRUXAPI.request("backoffice/checkSL", parsedDocInq);

                    if (ResponseGL.Status == "SUCCESS")
                    {
                        Models.AcctCodewithSL acctCodeWithSL = new JavaScriptSerializer().Deserialize<Models.AcctCodewithSL>(ResponseGL.Content);

                        TransactionDetails tt = (TransactionDetails)DG_GeneralJournal.SelectedItem;

                        if (!String.IsNullOrEmpty(acctCodeWithSL.SLC_Code) && !String.IsNullOrEmpty(acctCodeWithSL.SLT_Code) &&
                                !String.IsNullOrEmpty(acctCodeWithSL.SLE_Code) && !String.IsNullOrEmpty(acctCodeWithSL.SLN_Code))
                        {
                            ExecuteHelper execStringBuilder = new ExecuteHelper(this);

                            if (acctCodeWithSL.CanGLOnly == 0)
                            {
                                MessageBoxResult result = MessageBox.Show(this, execStringBuilder.CreateAcctCodeCanGLOnlyPrompt(), "Alert", MessageBoxButton.OK);
                            }
                            else
                            {
                                MessageBoxResult result = MessageBox.Show(this, execStringBuilder.CreateAcctCodeWithSLPrompt(), "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);

                                switch (result)
                                {
                                    case MessageBoxResult.Yes:

                                        if (!String.IsNullOrEmpty(referenceNumberTextBox.Text))
                                        {
                                            if (tt.TransactionCode == Convert.ToInt16(CheckVoucherID))
                                            {
                                                tt.AccountCode = selectedAccountCode.Code;

                                                String parsedAcctCode = new JavaScriptSerializer().Serialize(tt.AccountCode);

                                                CRUXLib.Response ResponseAcctCode = App.CRUXAPI.request("backoffice/SelectBankCode", parsedAcctCode);

                                                if (ResponseAcctCode.Status == "SUCCESS")
                                                {
                                                    Models.BankCode bCodes = new JavaScriptSerializer().Deserialize<Models.BankCode>(ResponseAcctCode.Content);

                                                    if (this.DataCon.TransactionChecks.Count != 0)
                                                    {
                                                        foreach (TransactionCheck checkDetail in this.DataCon.TransactionChecks)
                                                        {
                                                            checkDetail.BankID = bCodes.BankID;
                                                            checkDetail.BankCodeDesc = bCodes.BankDesc;
                                                            checkDetail.CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
                                                            checkDetail.CociType = 2;
                                                            checkDetail.CociTypeDesc = "Check";
                                                        }
                                                    }

                                                    COCIDataGrid.CanUserAddRows = false;

                                                }
                                                else
                                                {
                                                    MessageBox.Show(this, "Failed to Retrieve Data!");
                                                }
                                            }

                                            tt.AccountCode = selectedAccountCode.Code;
                                            tt.accountDesc = selectedAccountCode.AccountTypeDesc;
                                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                                        }

                                        else
                                        {
                                            var selected = transTypeComboBox.SelectedItem as TransactionType;

                                            if (selected.TransTypeID == CheckVoucherID)
                                            {
                                                tt.AccountCode = selectedAccountCode.Code;

                                                String parsedAcctCode = new JavaScriptSerializer().Serialize(tt.AccountCode);

                                                CRUXLib.Response ResponseAcctCode = App.CRUXAPI.request("backoffice/SelectBankCode", parsedAcctCode);

                                                if (ResponseAcctCode.Status == "SUCCESS")
                                                {
                                                    Models.BankCode bCodes = new JavaScriptSerializer().Deserialize<Models.BankCode>(ResponseAcctCode.Content);

                                                    this.DataCon.TransactionChecks.Add(new TransactionCheck()
                                                    {
                                                        BankID = bCodes.BankID,
                                                        BankCodeDesc = bCodes.BankDesc,
                                                        CheckDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy"),
                                                        CociType = 2,
                                                        CociTypeDesc = "Check"
                                                    });

                                                    COCIDataGrid.CanUserAddRows = false;

                                                }
                                                else
                                                {
                                                    MessageBox.Show(this, "Failed to Retrieve Data!");
                                                }
                                            }

                                            tt.AccountCode = selectedAccountCode.Code;
                                            tt.accountDesc = selectedAccountCode.AccountTypeDesc;
                                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                                        }
                                        break;

                                    case MessageBoxResult.No:
                                        break;

                                } //end of switch 

                            } // end If ResponseGL.Status

                        } // end of if SLC,SLT,SLE,SLN is not empty

                        else
                        {
                            tt.AccountCode = selectedAccountCode.Code;
                            tt.accountDesc = selectedAccountCode.AccountTypeDesc;

                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[7]));
                        }

                    }


                }
                else
                    return;

            }
            catch (Exception)
            {
                return;
            }
        }

        private void FillClientGrid(Int64 clientID)
        {

            string parsed = new JavaScriptSerializer().Serialize(clientID);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/selectedclient", parsed);

            if (Response.Status == "SUCCESS")
            {
                Client selectedClient = new JavaScriptSerializer().Deserialize<Client>(Response.Content);

                if (selectedClient.clientId != 0)
                {
                    if (!string.IsNullOrWhiteSpace(this.DataCon.TransactionSummary.ClientIDFormatTransSum))
                    {
                        if (clientIDTextBox.IsFocused)
                        {

                            this.DataCon.TransactionSummary.ClientIDFormatTransSum = selectedClient.ClientIDFormat;
                            this.DataCon.TransactionSummary.ClientNameTransSum = selectedClient.Name;
                            this.DataCon.TransactionSummary.ClientID = selectedClient.clientId;
                            clientORTextBox.Focus();
                        }
                        else
                        {
                            TransactionDetails tt = (TransactionDetails)DG_GeneralJournal.SelectedItem;
                            tt.ClientIDFormat = selectedClient.ClientIDFormat;
                            tt.clientName = selectedClient.Name;
                            tt.ClientID = selectedClient.clientId;
                            Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[11]));
                        }

                    }
                    else
                    {
                        TransactionDetails tt = (TransactionDetails)DG_GeneralJournal.SelectedItem;
                        tt.ClientIDFormat = selectedClient.ClientIDFormat;
                        tt.clientName = selectedClient.Name;
                        tt.ClientID = selectedClient.clientId;
                        Keyboard.Focus(GetDataGridCell(DG_GeneralJournal.SelectedCells[11]));
                    }

                }
                else
                    return;

            }
            else
            {
                MessageBox.Show(this, Response.Content);
            }
        }

        public void MoveToNextUIElement()
        {
            FocusNavigationDirection focusDirection = FocusNavigationDirection.Next;
            TraversalRequest request = new TraversalRequest(focusDirection);
            UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;
            if (elementWithFocus != null)
            {
                if (elementWithFocus.MoveFocus(request)) { }
            }
        }

        private void focusRow(object sender)
        {
            DG_GeneralJournal.SelectedIndex = currentRow;
            DataGrid datagrid = sender as DataGrid;
            datagrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            NewTransaction();
        }

        private void NewTransaction()
        {
            AllowSave = true;

            PostingDateLabel.Visibility = Visibility.Hidden;

            CtlNoTextBox.Visibility = Visibility.Hidden;

            transTypeComboBox.IsEnabled = true;
            saveButton.IsEnabled = true;
            transTypeComboBox.Focus();

            this.DataCon.DateTimeNow = this.boh.TransDate;
            this.DataCon.TransactionSummary.BatchNo = Convert.ToDateTime(this.boh.TransDate).ToString("yyyyMMdd");
            this.DataCon.TransYear = Convert.ToDateTime(this.boh.TransDate).Year.ToString();

            if (this.DataCon.TransactionDetails.Count == 0)
            {
                this.DataCon.TransactionDetails.Add(new Models.TransactionDetails());
                TempTransactionDetailsList = new List<Models.TransactionDetails>(this.DataCon.TransactionDetails);

                // transactionDetailsList = new ObservableCollection<Models.TransactionDetails>(DataCon.TransactionDetails);
                DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;

                if (trType == "PJ")
                {
                    this.DataCon.PurchaseDetails.Add(new PurchaseDetails()
                    {
                        TransDate = this.DataCon.DateTimeNow,
                        SeqNo = PurchaseSeqNo
                    });

                    TempPurchaseDetails = new List<PurchaseDetails>(this.DataCon.PurchaseDetails);
                    PurchaseDataGrid.ItemsSource = TempPurchaseDetails;

                    PurchaseDataGrid.IsReadOnly = false;
                }

            }

            pagePreviousButton.IsEnabled = false;
            pageNextButton.IsEnabled = false;
            newButton.IsEnabled = false;
            editButton.IsEnabled = false;

            DG_GeneralJournal.IsReadOnly = false;
            COCIDataGrid.IsReadOnly = false;
            TransDateTime.IsEnabled = true;

            pageNextButton.IsEnabled = false;
            pagePreviousButton.IsEnabled = false;

            ORTextBox.IsReadOnly = false;

            if (!String.IsNullOrEmpty(trType))
            {
                if (trType == "2")
                {
                    this.GLTab.IsSelected = true;
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "2" && t.TransTypeID == 22).ToList();
                }
                else if (trType == "1")
                {
                    this.GLTab.IsSelected = true;
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1" && (t.TransTypeID == 1 || t.TransTypeID == 6 || t.TransTypeID == 7)).ToList();
                }
                else if (trType == "PJ")
                {
                    this.POTab.IsSelected = true;
                    this.DataCon.TransactionCode = Convert.ToInt16(this.TransactionTypesList.FindAll(t => t.TransTypeModule == "PJ").First().TransTypeID);
                }
            }
        }

        //private void NewTransaction()
        //{
        //    if (App.CRUXAPI.isAllowed("update_journal"))
        //    {
        //        AllowSave = true;

        //        PostingDateLabel.Visibility = Visibility.Hidden;

        //        CtlNoTextBox.Visibility = Visibility.Hidden;
        //        this.GLTab.IsSelected = true;
        //        transTypeComboBox.IsEnabled = true;
        //        saveButton.IsEnabled = true;
        //        transTypeComboBox.Focus();

        //        this.DataCon.DateTimeNow = this.boh.TransDate;
        //        this.DataCon.TransactionSummary.BatchNo = Convert.ToDateTime(this.boh.TransDate).ToString("yyyyMMdd");
        //        this.DataCon.TransYear = Convert.ToDateTime(this.boh.TransDate).Year.ToString();

        //        if (this.DataCon.TransactionDetails.Count == 0)
        //        {
        //            this.DataCon.TransactionDetails.Add(new Models.TransactionDetails());
        //            TempTransactionDetailsList = new List<Models.TransactionDetails>(this.DataCon.TransactionDetails);

        //            // transactionDetailsList = new ObservableCollection<Models.TransactionDetails>(DataCon.TransactionDetails);
        //            DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;

        //            if (trType == "PJ")
        //            {
        //                this.DataCon.PurchaseDetails.Add(new PurchaseDetails()
        //                {
        //                    TransDate = this.DataCon.DateTimeNow,
        //                    SeqNo = PurchaseSeqNo
        //                });

        //                TempPurchaseDetails = new List<PurchaseDetails>(this.DataCon.PurchaseDetails);
        //                PurchaseDataGrid.ItemsSource = TempPurchaseDetails;

        //                PurchaseDataGrid.IsReadOnly = false;
        //            }
        //        }

        //        pagePreviousButton.IsEnabled = false;
        //        pageNextButton.IsEnabled = false;
        //        newButton.IsEnabled = false;
        //        editButton.IsEnabled = false;

        //        DG_GeneralJournal.IsReadOnly = false;
        //        COCIDataGrid.IsReadOnly = false;
        //        TransDateTime.IsEnabled = true;

        //        pageNextButton.IsEnabled = false;
        //        pagePreviousButton.IsEnabled = false;

        //        ORTextBox.IsReadOnly = false;

        //        if (!String.IsNullOrEmpty(trType))
        //        {
        //            if (trType == "2")
        //            {
        //                transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "2" && t.TransTypeID == 22).ToList();
        //            }
        //            else if (trType == "1")
        //            {
        //                transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1" && (t.TransTypeID == 1 || t.TransTypeID == 6 || t.TransTypeID == 7)).ToList();
        //            }
        //            else if (trType == "PJ")
        //            {
        //                this.POTab.IsSelected = true;
        //                this.DataCon.TransactionCode = Convert.ToInt16(this.TransactionTypesList.FindAll(t => t.TransTypeModule == "PJ").First().TransTypeID);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}

        private void transTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                TransactionType transType = (TransactionType)transTypeComboBox.SelectedItem;

                if (transType != null)
                {
                    this.DataCon.TransactionCode = Convert.ToInt16(transType.TransTypeID);
                    DG_GeneralJournal.IsEnabled = true;

                    if (trType == "PJ")
                    {
                        PurchaseDataGrid.IsEnabled = true;
                        PurchaseDataGrid.CanUserAddRows = false;
                    }
                }
                else if (transType == null)
                {
                    DG_GeneralJournal.IsEnabled = false;
                    PurchaseDataGrid.IsEnabled = false;
                }

                if (transTypeComboBox.SelectedIndex != -1)
                {
                    DG_GeneralJournal.IsEnabled = true;
                    DG_GeneralJournal.CanUserAddRows = false;


                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void transTypeComboBox_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transTypeComboBox.SelectedItem != null)
                {
                    TransactionType tt = new Models.TransactionType();
                    tt = (TransactionType)transTypeComboBox.SelectedItem;

                    if ((tt.TransTypeID == CheckVoucherID || tt.TransTypeDesc == "Check Voucher") || tt.TransTypeID == PurchaseJournalID)
                    {
                        COCIDataGrid.ItemsSource = this.DataCon.TransactionChecks;
                        checkTypesList = new List<CheckType>();
                        FillCOCICheckTypes();
                    }
                } // end if transtypeCombo not null
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void columnHeader_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedTrans = DG_GeneralJournal.SelectedItem as TransactionDetails;

                if (String.IsNullOrEmpty(selectedTrans.UPDTagCode) || !DG_GeneralJournal.IsReadOnly) // 
                {
                    //  e.Handled = true;
                    DG_GeneralJournal.CanUserSortColumns = false;
                }
                else
                {
                    DG_GeneralJournal.CanUserSortColumns = true;
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private InsertPurchaseSummary InsertProcessPurchaseSummary()
        {
            InsertPurchaseSummary purchaseSummary = new Models.Purchase_Journal.InsertPurchaseSummary();
            try
            {
                purchaseSummary.TransDate = this.DataCon.DateTimeNow;
                purchaseSummary.ORNo = this.DataCon.TransactionSummary.ORNo;
                purchaseSummary.VendorID = this.DataCon.PurchaseSummary.VendorID;

                return purchaseSummary;
            }
            catch (Exception)
            {
                return purchaseSummary;
            }
        }

        private TransactionSummary TempProcessTransactionSummary()
        {
            TransactionSummary transactionSummary = new TransactionSummary();

            if (this.DataCon.TransactionCode == 21 || this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
            {
                if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientIDFormatTransSum))
                {
                    transactionSummary.ClientID = this.DataCon.TransactionSummary.ClientID;

                    if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientOrTransSum))
                    {
                        transactionSummary.AndORName = this.DataCon.TransactionSummary.ClientOrTransSum;
                    }
                }
                else if (String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientIDFormatTransSum) && !String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientNameTransSum))
                {
                    if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientOrTransSum) && !String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientNameTransSum))
                    {
                        transactionSummary.AndORName = this.DataCon.TransactionSummary.ClientOrTransSum;
                        transactionSummary.ClientName = this.DataCon.TransactionSummary.ClientNameTransSum;
                    }
                    else
                    {
                        transactionSummary.ClientName = this.DataCon.TransactionSummary.ClientNameTransSum;
                    }
                }
            }


            transactionSummary.TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year;
            transactionSummary.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
            transactionSummary.BatchNo = this.DataCon.TransactionSummary.BatchNo;
            transactionSummary.TransactionCode = this.DataCon.TransactionCode;

            transactionSummary.TransactionDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
            transactionSummary.Explanation = this.DataCon.TransactionSummary.Explanation;
            transactionSummary.ORNo = this.DataCon.TransactionSummary.ORNo;

            return transactionSummary;
        }

        public TransactionSummary ProcessTransactionSummary(Int16 BranchCode, Int16 transCode, Int64 CtlNo, String DocNo)
        {
            TransactionSummary transactionSummary = new TransactionSummary();

            if (transCode == 21 || transCode == Convert.ToInt16(CheckVoucherID))
            {
                if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientIDFormatTransSum))
                {
                    transactionSummary.ClientID = this.DataCon.TransactionSummary.ClientID;

                    if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientOrTransSum))
                    {
                        transactionSummary.AndORName = this.DataCon.TransactionSummary.ClientOrTransSum;
                    }
                }
                else if (String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientIDFormatTransSum) && !String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientNameTransSum))
                {
                    if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientOrTransSum) && !String.IsNullOrEmpty(this.DataCon.TransactionSummary.ClientNameTransSum))
                    {
                        transactionSummary.AndORName = this.DataCon.TransactionSummary.ClientOrTransSum;
                        transactionSummary.ClientName = this.DataCon.TransactionSummary.ClientNameTransSum;
                    }
                    else
                    {
                        transactionSummary.ClientName = this.DataCon.TransactionSummary.ClientNameTransSum;
                    }
                }
            }

            transactionSummary.TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year;
            transactionSummary.BranchCode = BranchCode;
            transactionSummary.BatchNo = this.DataCon.TransactionSummary.BatchNo;
            transactionSummary.TransactionCode = transCode;
            transactionSummary.ControlNo = CtlNo;
            transactionSummary.DocNo = DocNo;
            transactionSummary.ORNo = this.DataCon.TransactionSummary.ORNo;

            transactionSummary.TransactionDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
            transactionSummary.Explanation = this.DataCon.TransactionSummary.Explanation;

            return transactionSummary;
        }

        private List<TransactionDetails> TransactionDetails()
        {
            List<TransactionDetails> insertTransDetailLists = new List<TransactionDetails>();

            try
            {
                foreach (var details in DG_GeneralJournal.ItemsSource)
                {
                    TransactionDetails insTrans = (TransactionDetails)details;

                    insertTransDetailLists.Add(new TransactionDetails()
                    {
                        Amount = insTrans.Amount,
                        BranchCode = insTrans.BranchCode,
                        AccountCode = insTrans.AccountCode,
                        TransactionCode = insTrans.TransactionCode,
                        ControlNo = Convert.ToInt64(referenceNumberTextBox.Text),
                        ClientID = insTrans.ClientID,
                        SLC_Code = insTrans.SLC_Code,
                        SLT_Code = insTrans.SLT_Code,
                        SLE_Code = insTrans.SLE_Code,
                        SLN_Code = insTrans.SLN_Code,
                        Credit = insTrans.Credit,
                        ReferenceNo = insTrans.ReferenceNo,
                        Debit = insTrans.Debit,
                        UpdTagID = insTrans.UpdTagID,
                        UPDTagCode = insTrans.UPDTagCode,
                        SLDate = insTrans.SLDate
                    });
                }

                return insertTransDetailLists;
            }
            catch (Exception ex)
            {
                throw;
                //MessageBox.Show("Error!" + ex);
                //return;
            }

        }

        private List<TransactionDetails> UpdateTransactionDetails()
        {
            int SeqNo = 0;

            List<Models.TransactionDetails> updateTransDetailList = new List<Models.TransactionDetails>();

            foreach (TransactionDetails insertTransactionDetail in DG_GeneralJournal.ItemsSource)
            {
                Decimal amount = 0;

                if (insertTransactionDetail.Debit != 0 && insertTransactionDetail.Credit == 0)
                {
                    amount = insertTransactionDetail.Debit;
                }
                else if (insertTransactionDetail.Debit == 0 && insertTransactionDetail.Credit != 0)
                {
                    amount = -(insertTransactionDetail.Credit);
                }

                updateTransDetailList.Add(new TransactionDetails()
                {
                    TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year,
                    BranchCode = insertTransactionDetail.BranchCode,
                    OfficeID = insertTransactionDetail.OfficeID,
                    SequenceNo = SeqNo,
                    ControlNo = insertTransactionDetail.ControlNo, //to be check
                    SLC_Code = insertTransactionDetail.SLC_Code,
                    SLE_Code = insertTransactionDetail.SLE_Code,
                    SLT_Code = insertTransactionDetail.SLT_Code,
                    SLN_Code = insertTransactionDetail.SLN_Code,
                    ClientID = insertTransactionDetail.ClientID,
                    clientName = insertTransactionDetail.clientName,
                    TransactionCode = insertTransactionDetail.TransactionCode,
                    AccountCode = insertTransactionDetail.AccountCode,
                    Debit = insertTransactionDetail.Debit,
                    Credit = insertTransactionDetail.Credit,
                    AdjFlag = insertTransactionDetail.AdjFlag,
                    SLDate = TransDateTime.Text,
                    Amount = amount,
                    ReferenceNo = insertTransactionDetail.ReferenceNo
                });

                SeqNo += 1;
            }

            this.DataCon.DateTimeNow = TransDateTime.Text;

            return updateTransDetailList;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                SaveTransaction(null, null, e);
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void UpdateAndDelete(List<Models.TransactionDetails> updatedList, List<Models.TransactionDetails> deletedList, KeyEventArgs keyE = null, RoutedEventArgs routedE = null, ExecutedRoutedEventArgs executedE = null)
        {

            TransactionType transType = new TransactionType();

            try
            {
                if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.Explanation))
                {

                    if (this.DataCon.TotalDifference == 0)
                    {

                        var subscribe = updatedList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 2.ToString("D2")).ToList();

                        if (subscribe.Count != 0)
                        {
                            if (subscribe.FindAll(s => -(s.Amount) > 1800).ToList().Count != 0)
                            {
                                MessageBox.Show(this, "Cannot Enter Subcribed Amount greater than 1,800.", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            else
                            {
                                UpdateTransaction(updatedList, deletedList);
                            }
                        }
                        else
                        {
                            UpdateTransaction(updatedList, deletedList);
                        }//end else

                    } //end if netTotal == 0;

                    else if (this.DataCon.TotalDifference != 0)
                    {
                        MessageBoxResult result = MessageBox.Show(this, "DEBIT & CREDIT Balances are not EQUAL!", "Alert", MessageBoxButton.OK);
                    }
                } // end if Explanation TextBox != "";
                else
                {
                    //ExplanationTextBox.IsEnabled = true;

                    ExplanationTextBox.IsReadOnly = false;
                    ExplanationTextBox.Focus();
                }
            }
            catch (Exception)
            {
                if (routedE != null)
                {
                    routedE.Handled = true;
                }
                else if (keyE != null)
                {
                    keyE.Handled = true;
                }
                else if (executedE != null)
                {
                    executedE.Handled = true;
                }
            }
        }

        private void Update()
        {
            List<TransactionDetails> InsertTransactionDetailsList = new List<TransactionDetails>();

            int SeqNo = 0;

            try
            {
                foreach (TransactionDetails insertTransactionDetail in DG_GeneralJournal.ItemsSource)
                {
                    Decimal amount = 0;

                    if (insertTransactionDetail.Debit != 0 && insertTransactionDetail.Credit == 0)
                    {
                        amount = insertTransactionDetail.Debit;
                    }
                    else if (insertTransactionDetail.Debit == 0 && insertTransactionDetail.Credit != 0)
                    {
                        amount = -(insertTransactionDetail.Credit);
                    }

                    InsertTransactionDetailsList.Add(new TransactionDetails()
                    {
                        TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year,
                        BranchCode = insertTransactionDetail.BranchCode,
                        OfficeID = insertTransactionDetail.OfficeID,
                        SequenceNo = SeqNo,
                        ControlNo = insertTransactionDetail.ControlNo, //to be check
                        SLC_Code = insertTransactionDetail.SLC_Code,
                        SLE_Code = insertTransactionDetail.SLE_Code,
                        SLT_Code = insertTransactionDetail.SLT_Code,
                        SLN_Code = insertTransactionDetail.SLN_Code,
                        ClientID = insertTransactionDetail.ClientID,
                        clientName = insertTransactionDetail.clientName,
                        TransactionCode = insertTransactionDetail.TransactionCode,
                        AccountCode = insertTransactionDetail.AccountCode,
                        Debit = insertTransactionDetail.Debit,
                        Credit = insertTransactionDetail.Credit,
                        AdjFlag = insertTransactionDetail.AdjFlag,
                        SLDate = this.DataCon.DateTimeNow,//insertTransactionDetail.SLDate,
                        Amount = amount,
                        ReferenceNo = insertTransactionDetail.ReferenceNo,
                        EncodedBy = insertTransactionDetail.EncodedBy,
                        EncodedByID = insertTransactionDetail.EncodedByID
                    });

                    SeqNo += 1;

                } //end foreach

                //this.DataCon.DateTimeNow = TransDateTime.Text;

            } //end try statement

            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }


            var subscribe = InsertTransactionDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 2.ToString("D2")).ToList();

            if (subscribe.Count != 0)
            {
                if (subscribe.FindAll(s => -(s.Amount) > 1800).ToList().Count != 0)
                {
                    MessageBox.Show(this, "Cannot Enter Subcribed Amount greater than 1,800.", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    UpdateTransaction(InsertTransactionDetailsList);
                }
            }
            else
            {
                UpdateTransaction(InsertTransactionDetailsList);
            }//end else
        }

        private void Save(String Option)
        {
            List<TransactionDetails> InsertTransactionDetailsList = new List<TransactionDetails>();
            string SLDate = "";
            int SeqNo = 0;

            try
            {
                foreach (TransactionDetails insertTransactionDetail in DG_GeneralJournal.ItemsSource)
                {
                    Decimal amount = 0;

                    if (Option == "Insert")
                    {

                        if (insertTransactionDetail.Debit != 0 && insertTransactionDetail.Credit == 0)
                        {
                            amount = insertTransactionDetail.Debit;
                        }
                        else if (insertTransactionDetail.Debit == 0 && insertTransactionDetail.Credit != 0)
                        {
                            amount = -(insertTransactionDetail.Credit);
                        }

                        if (!String.IsNullOrEmpty(insertTransactionDetail.SLDate))
                        {
                            SLDate = insertTransactionDetail.SLDate;
                        }
                        else
                        {
                            SLDate = Convert.ToDateTime(this.DataCon.DateTimeNow).ToString("MM/dd/yyyy");
                        }

                        InsertTransactionDetailsList.Add(new TransactionDetails()
                        {
                            TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year,
                            SequenceNo = SeqNo,
                            SLC_Code = insertTransactionDetail.SLC_Code,
                            SLE_Code = insertTransactionDetail.SLE_Code,
                            SLT_Code = insertTransactionDetail.SLT_Code,
                            SLN_Code = insertTransactionDetail.SLN_Code,
                            ClientID = insertTransactionDetail.ClientID,
                            clientName = insertTransactionDetail.clientName,
                            TransactionCode = this.DataCon.TransactionCode,
                            AccountCode = insertTransactionDetail.AccountCode,
                            accountDesc = insertTransactionDetail.accountDesc,
                            Debit = insertTransactionDetail.Debit,
                            Credit = insertTransactionDetail.Credit,
                            AdjFlag = insertTransactionDetail.AdjFlag,
                            SLDate = SLDate,
                            Amount = amount,
                            ReferenceNo = insertTransactionDetail.ReferenceNo,
                            Options = Option
                        });
                    }

                    SeqNo += 1;
                } //end foreach

                this.DataCon.DateTimeNow = TransDateTime.Text;

                if (this.DataCon.TransactionCode == CheckVoucherID)
                {
                    try
                    {
                        if (!String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CociTypeDesc) &&
                        !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CheckNo) &&
                            !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().BankCodeDesc) &&
                                !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CheckTypeDesc) &&
                                    !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CheckDate) &&
                                        this.DataCon.TransactionChecks.First().Amount != 0)
                        {

                            InsertTransaction(InsertTransactionDetailsList, Option);
                        }
                        else
                        {
                            MessageBoxResult result = MessageBox.Show(this, "Error! Please insert check details!", "Error", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

                            if (!COCITab.IsFocused)
                            {
                                COCITab.Focus();
                            }
                            else
                            {
                                COCITab.Focus();
                            }
                        }

                    }
                    catch (Exception)
                    {
                        return;
                    }
                } // end if TransType = 22 / Check

                else
                {
                    var subscribe = InsertTransactionDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 2.ToString("D2")).ToList();

                    if (subscribe.Count != 0)
                    {
                        if (subscribe.FindAll(s => -(s.Amount) > 1800).ToList().Count != 0)
                        {
                            MessageBox.Show(this, "Cannot Enter Subcribed Amount greater than 1,800.", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
                            InsertTransaction(InsertTransactionDetailsList, Option);
                        }
                    }
                    else
                    {
                        InsertTransaction(InsertTransactionDetailsList, Option);
                    }//end else

                } // endi f TransType not equal 22


            } // end try statement

            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }

        }

        private void UpdateTransaction(List<TransactionDetails> InsertTransactionDetailsList, List<TransactionDetails> deletedList = null)
        {
            Boolean udpate = true;
            List<TransactionDetails> removeTransDetailsList = new List<Models.TransactionDetails>(InsertTransactionDetailsList);

            var noAmount = removeTransDetailsList.FindAll(t => ((!String.IsNullOrEmpty(t.SLC_Code) &&
                                  !String.IsNullOrEmpty(t.SLT_Code) && !String.IsNullOrEmpty(t.SLE_Code) && !String.IsNullOrEmpty(t.SLN_Code)) ||
                    t.AccountCode != 0) && (t.Debit == 0 && t.Credit == 0)).ToList();

            if (noAmount.Count != 0)
            {
                if (noAmount.Count > 1)
                {
                    foreach (TransactionDetails detail in noAmount)
                    {
                        var line = detail.SequenceNo + 1;

                        MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Amount Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                        InsertTransactionDetailsList.Find(s => s.SequenceNo == detail.SequenceNo).RowColor = "IndianRed";
                    }
                }
                else
                {
                    var line = noAmount.First().SequenceNo + 1;

                    MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Amount Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                    InsertTransactionDetailsList.Find(s => s.SequenceNo == noAmount.First().SequenceNo).RowColor = "IndianRed";
                }

                udpate = false;
                DG_GeneralJournal.ItemsSource = InsertTransactionDetailsList;
            }

            foreach (TransactionDetails transDetail in InsertTransactionDetailsList)
            {
                if (transDetail.SLC_Code == "11")
                {
                    removeTransDetailsList.Remove(transDetail);
                }
            }

            var noClient = removeTransDetailsList.FindAll(t => !String.IsNullOrEmpty(t.SLC_Code) && !String.IsNullOrEmpty(t.SLT_Code) &&
                        !String.IsNullOrEmpty(t.SLE_Code) && !String.IsNullOrEmpty(t.SLN_Code) && t.ClientID == 0).ToList();

            if (noClient.Count != 0)
            {
                if (noClient.Count > 1)
                {
                    foreach (TransactionDetails detail in noClient)
                    {
                        var line = detail.SequenceNo + 1;

                        MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Client Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                        InsertTransactionDetailsList.Find(s => s.SequenceNo == detail.SequenceNo).RowColor = "IndianRed";
                    }
                }
                else
                {
                    var line = noClient.First().SequenceNo + 1;
                    MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Client Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                    InsertTransactionDetailsList.Find(s => s.SequenceNo == noClient.First().SequenceNo).RowColor = "IndianRed";
                }

                udpate = false;
                DG_GeneralJournal.ItemsSource = InsertTransactionDetailsList;
            }

            var noRefNo = removeTransDetailsList.FindAll(t => !String.IsNullOrEmpty(t.SLC_Code) && !String.IsNullOrEmpty(t.SLT_Code) &&
                    !String.IsNullOrEmpty(t.SLE_Code) && !String.IsNullOrEmpty(t.SLN_Code) && t.ClientID != 0 && String.IsNullOrEmpty(t.ReferenceNo)).ToList();

            if (noRefNo.Count != 0)
            {
                if (noRefNo.Count > 1)
                {
                    foreach (TransactionDetails detail in noRefNo)
                    {
                        var line = detail.SequenceNo + 1;

                        MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", AcctNo Field must have a value", "Module not saved", MessageBoxButton.OK, MessageBoxImage.Error);
                        InsertTransactionDetailsList.Find(s => s.SequenceNo == detail.SequenceNo).RowColor = "IndianRed";
                    }
                }
                else
                {
                    var line = noRefNo.First().SequenceNo + 1;
                    MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", AcctNo Field must have a value", "Module not saved", MessageBoxButton.OK, MessageBoxImage.Error);
                    InsertTransactionDetailsList.Find(s => s.SequenceNo == noRefNo.First().SequenceNo).RowColor = "IndianRed";
                }

                udpate = false;
                DG_GeneralJournal.ItemsSource = InsertTransactionDetailsList;
            }

            if (udpate)
            {
                this.DataCon.TransactionDetails = InsertTransactionDetailsList;
                this.DataCon.DeletedTransactionDetails = deletedList;

                ProgressPanel.Visibility = Visibility.Visible;
                MainGrid.IsEnabled = false;

                GenerateUpdateWorker.RunWorkerAsync();

            }//end update
        }

        private void InsertTransaction(List<TransactionDetails> InsertTransactionDetailsList, String Option)
        {
            Boolean insert = true;

            List<TransactionDetails> removeTransDetailsList = new List<Models.TransactionDetails>(InsertTransactionDetailsList);

            var noAmount = removeTransDetailsList.FindAll(t => ((!String.IsNullOrEmpty(t.SLC_Code) &&
                                  !String.IsNullOrEmpty(t.SLT_Code) && !String.IsNullOrEmpty(t.SLE_Code) && !String.IsNullOrEmpty(t.SLN_Code)) ||
                    t.AccountCode != 0) && (t.Debit == 0 && t.Credit == 0)).ToList();

            if (noAmount.Count != 0)
            {
                if (noAmount.Count > 1)
                {
                    foreach (TransactionDetails detail in noAmount)
                    {
                        var line = detail.SequenceNo + 1;

                        MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Amount Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                        InsertTransactionDetailsList.Find(s => s.SequenceNo == detail.SequenceNo).RowColor = "IndianRed";
                    }
                }
                else
                {
                    var line = noAmount.First().SequenceNo + 1;

                    MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Amount Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                    InsertTransactionDetailsList.Find(s => s.SequenceNo == noAmount.First().SequenceNo).RowColor = "IndianRed";
                }

                DG_GeneralJournal.ItemsSource = InsertTransactionDetailsList;
                insert = false;
            }

            foreach (TransactionDetails transDetail in InsertTransactionDetailsList)
            {
                if (transDetail.SLC_Code == "11")
                {
                    removeTransDetailsList.Remove(transDetail);
                }
            }

            var noClient = removeTransDetailsList.FindAll(t => !String.IsNullOrEmpty(t.SLC_Code) && !String.IsNullOrEmpty(t.SLT_Code) &&
                    !String.IsNullOrEmpty(t.SLE_Code) && !String.IsNullOrEmpty(t.SLN_Code) && t.ClientID == 0).ToList();

            if (noClient.Count != 0)
            {
                if (noClient.Count > 1)
                {
                    foreach (TransactionDetails detail in noClient)
                    {
                        var line = detail.SequenceNo + 1;

                        MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Client Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                        InsertTransactionDetailsList.Find(s => s.SequenceNo == detail.SequenceNo).RowColor = "IndianRed";
                    }
                }
                else
                {
                    var line = noClient.First().SequenceNo + 1;
                    MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", Client Field must have a value", "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                    InsertTransactionDetailsList.Find(s => s.SequenceNo == noClient.First().SequenceNo).RowColor = "IndianRed";
                }

                insert = false;
                DG_GeneralJournal.ItemsSource = InsertTransactionDetailsList;

            }

            var noRefNo = removeTransDetailsList.FindAll(t => !String.IsNullOrEmpty(t.SLC_Code) && !String.IsNullOrEmpty(t.SLT_Code) &&
                    !String.IsNullOrEmpty(t.SLE_Code) && !String.IsNullOrEmpty(t.SLN_Code) && t.ClientID != 0 && String.IsNullOrEmpty(t.ReferenceNo)).ToList();

            if (noRefNo.Count != 0)
            {
                if (noRefNo.Count > 1)
                {
                    foreach (TransactionDetails detail in noRefNo)
                    {
                        var line = detail.SequenceNo + 1;

                        MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", AcctNo Field must have a value", "Module not saved", MessageBoxButton.OK, MessageBoxImage.Error);
                        InsertTransactionDetailsList.Find(s => s.SequenceNo == detail.SequenceNo).RowColor = "IndianRed";
                    }
                }
                else
                {
                    var line = noRefNo.First().SequenceNo + 1;
                    MessageBox.Show(this, "Error: Required Field: SL Transaction Row Detected at Row: " + line + ", AcctNo Field must have a value", "Module not saved", MessageBoxButton.OK, MessageBoxImage.Error);
                    InsertTransactionDetailsList.Find(s => s.SequenceNo == noRefNo.First().SequenceNo).RowColor = "IndianRed";
                }

                insert = false;
                DG_GeneralJournal.ItemsSource = InsertTransactionDetailsList;

            }

            this.DataCon.InsertOrUpdated = insert;

            if (insert)
            {
                this.DataCon.TransactionDetails = InsertTransactionDetailsList;
                GenerateSaveWorker.RunWorkerAsync();
            }

        }

        private List<Models.TransactionDetails> CheckLoanBalance(List<Models.TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        {
            List<Models.CheckBalancesParams> checkBalanceParams = new List<Models.CheckBalancesParams>();
            List<Models.TransactionDetails> LoanBalanceList = new List<Models.TransactionDetails>();


            foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
            {

                if (transDetail.SLC_Code == "12" && transDetail.SLE_Code == "11")
                {
                    checkBalanceParams.Add(new Models.CheckBalancesParams()
                    {
                        BranchCode = transDetail.BranchCode,
                        SLC_Code = Convert.ToByte(transDetail.SLC_Code),
                        SLT_Code = Convert.ToByte(transDetail.SLT_Code),
                        SLE_Code = Convert.ToByte(transDetail.SLE_Code),
                        SLN_Code = Convert.ToByte(transDetail.SLN_Code),
                        ClientID = transDetail.ClientID,
                        RefNo = transDetail.ReferenceNo,
                        SLDate = Convert.ToDateTime(transDetail.SLDate).ToString("yyyy/MM/dd")
                    }); //end checkBalance list
                }

            } //end foreach

            String parsedCheckBalParams = new JavaScriptSerializer().Serialize(checkBalanceParams);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/checkSLBalance", parsedCheckBalParams, 999999999);

            List<Models.TransactionDetails> UnbalanceList = new List<Models.TransactionDetails>();

            if (Response.Status == "SUCCESS")
            {
                List<CheckBalancesParams> BalancesList = new JavaScriptSerializer().Deserialize<List<CheckBalancesParams>>(Response.Content);
                Decimal BalAmount = 0;

                foreach (CheckBalancesParams balanceDetail in BalancesList)
                {
                    Models.TransactionDetails bal = insertTransDetailsList.Find(t => t.SLT_Code == balanceDetail.SLT_Code.ToString("D2") &&
                        t.SLC_Code == balanceDetail.SLC_Code.ToString("D2") && t.SLE_Code == "11" &&
                        t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo);

                    if (PostOrReverse == 1)
                    {
                        if (bal.Debit == 0 && bal.Credit != 0)
                        {
                            BalAmount = -(bal.Amount);
                        }
                        else
                        {
                            BalAmount = bal.Amount;
                        }
                    }
                    else
                    {
                        if (bal.Debit == 0 && bal.Credit != 0)
                        {
                            BalAmount = bal.Amount;
                        }
                        else
                        {
                            BalAmount = -(bal.Amount);
                        }
                    }


                    Decimal BalRemaining = (balanceDetail.BalAmount * balanceDetail.BalTypeValue) + (BalAmount * balanceDetail.BalTypeValue);
                    Decimal BalRemainingSL = (balanceDetail.BalSLDate * balanceDetail.BalTypeValue) + (BalAmount * balanceDetail.BalTypeValue);

                    if ((BalRemaining < 0) || (BalRemainingSL < 0))
                    {
                        UnbalanceList.Add(new Models.TransactionDetails()
                        {
                            SLC_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLC_Code,

                            SLT_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLT_Code,

                            SLE_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLE_Code,

                            SLN_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLN_Code,

                            ReferenceNo = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).ReferenceNo,

                            ClientID = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).ClientID,

                            SLDate = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                         Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                         t.ReferenceNo == balanceDetail.RefNo).SLDate

                        });
                    }
                }

                return UnbalanceList;
            }
            else
            {
                return UnbalanceList;
            }
        }

        private List<Models.TransactionDetails> CheckBalance(List<Models.TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        {
            List<Models.CheckBalancesParams> checkBalanceParams = new List<Models.CheckBalancesParams>();
            List<Models.TransactionDetails> LoanBalanceList = new List<Models.TransactionDetails>();

            Boolean InsertUnbalancedToList = false;

            foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
            {
                if (GenerateBalances.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                if (transDetail.BranchCode != 0 &&
                   !String.IsNullOrEmpty(transDetail.SLC_Code) &&
                   !String.IsNullOrEmpty(transDetail.SLT_Code) &&
                   !String.IsNullOrEmpty(transDetail.ReferenceNo) &&
                   transDetail.ClientID != 0)
                {

                    if (transDetail.SLC_Code != "12")
                    {
                        checkBalanceParams.Add(new Models.CheckBalancesParams()
                        {
                            BranchCode = transDetail.BranchCode,
                            SLC_Code = Convert.ToByte(transDetail.SLC_Code),
                            SLT_Code = Convert.ToByte(transDetail.SLT_Code),
                            SLE_Code = Convert.ToByte(transDetail.SLE_Code),
                            SLN_Code = Convert.ToByte(transDetail.SLN_Code),
                            ClientID = transDetail.ClientID,
                            RefNo = transDetail.ReferenceNo,
                            SLDate = Convert.ToDateTime(transDetail.SLDate).ToString("yyyy/MM/dd")
                        }); //end checkBalance list
                    }


                } //end if

            } //end foreach


            String parsedCheckBalParams = new JavaScriptSerializer().Serialize(checkBalanceParams);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/checkSLBalance", parsedCheckBalParams, 999999999);

            List<Models.TransactionDetails> UnbalanceList = new List<Models.TransactionDetails>();

            if (Response.Status == "SUCCESS")
            {
                List<CheckBalancesParams> BalancesList = new JavaScriptSerializer().Deserialize<List<CheckBalancesParams>>(Response.Content);
                Decimal BalAmount = 0;

                foreach (CheckBalancesParams balanceDetail in BalancesList)
                {
                    Models.TransactionDetails bal = insertTransDetailsList.Find(t => t.SLT_Code == balanceDetail.SLT_Code.ToString("D2") &&
                        t.SLC_Code == balanceDetail.SLC_Code.ToString("D2") && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo);

                    if (bal.Debit == 0 && bal.Credit != 0)
                    {
                        BalAmount = -(bal.Amount);
                    }
                    else
                    {
                        BalAmount = bal.Amount;
                    }

                    Decimal BalRemaining = 0;
                    Decimal BalRemainingSL = 0;

                    if (balanceDetail.SLC_Code == 13) //if Credit
                    {
                        if (balanceDetail.SLE_Code == 11)
                        {
                            BalRemaining = (balanceDetail.BalAmount) + (BalAmount * balanceDetail.BalTypeValue);
                            BalRemainingSL = (balanceDetail.BalSLDate) + (BalAmount * balanceDetail.BalTypeValue);
                        }
                        else
                        {
                            BalRemaining = (BalAmount * balanceDetail.BalTypeValue) - (balanceDetail.BalAmount);
                            BalRemainingSL = (BalAmount * balanceDetail.BalTypeValue) - (balanceDetail.BalSLDate);
                        }

                        //else
                        //{
                        //    if (BalAmount > 0) // if Debit
                        //    {
                        //        BalRemaining = (balanceDetail.BalAmount * balanceDetail.BalTypeValue) + (BalAmount * balanceDetail.BalTypeValue);
                        //        BalRemainingSL = (balanceDetail.BalSLDate * balanceDetail.BalTypeValue) + (BalAmount * balanceDetail.BalTypeValue);
                        //    }
                        //    else if (BalAmount < 0) // if Credit
                        //    {
                        //        BalRemaining = (BalAmount * balanceDetail.BalTypeValue) - (balanceDetail.BalAmount * balanceDetail.BalTypeValue);
                        //        BalRemainingSL = (BalAmount * balanceDetail.BalTypeValue) - (balanceDetail.BalSLDate * balanceDetail.BalTypeValue);
                        //    }
                        //}

                    }
                    else
                    {
                        BalRemaining = (balanceDetail.BalAmount) + (BalAmount * balanceDetail.BalTypeValue);
                        BalRemainingSL = (balanceDetail.BalSLDate) + (BalAmount * balanceDetail.BalTypeValue);
                    }

                    if (balanceDetail.SLC_Code == 22 && balanceDetail.SLC_Code == 24)
                    {
                        if (balanceDetail.HoldAmount != 0)
                        {
                            var BalanceAfterHold = BalRemaining - balanceDetail.HoldAmount;
                            var SLBalanceAfterHold = BalRemainingSL - balanceDetail.HoldAmount;

                            if (BalanceAfterHold < 0 && SLBalanceAfterHold < 0)
                            {
                                unbalanceListWithHold.Add(new Models.TransactionDetails()
                                {
                                    SLC_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                t.ReferenceNo == balanceDetail.RefNo).SLC_Code,

                                    SLT_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                t.ReferenceNo == balanceDetail.RefNo).SLT_Code,

                                    SLE_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                t.ReferenceNo == balanceDetail.RefNo).SLE_Code,

                                    SLN_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                t.ReferenceNo == balanceDetail.RefNo).SLN_Code,

                                    ReferenceNo = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                t.ReferenceNo == balanceDetail.RefNo).ReferenceNo,

                                    ClientID = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                t.ReferenceNo == balanceDetail.RefNo).ClientID,

                                    SLDate = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                                 Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                                 t.ReferenceNo == balanceDetail.RefNo).SLDate
                                });
                            }
                        }
                    }

                    if ((BalRemaining < 0) || (BalRemainingSL < 0))
                    {
                        if (balanceDetail.SLC_Code == 31)
                        {
                            if (balanceDetail.SLT_Code != 3)
                            {
                                InsertUnbalancedToList = true;
                            }
                        }
                        else
                        {
                            InsertUnbalancedToList = true;
                        }
                    }
                    else if ((balanceDetail.SLC_Code == 31 && balanceDetail.SLT_Code == 3) && ((BalRemaining > 0) || (BalRemainingSL > 0)))
                    {
                        InsertUnbalancedToList = true;
                    }
                    else
                    {
                        InsertUnbalancedToList = false;
                    }


                    if (InsertUnbalancedToList)
                    {
                        UnbalanceList.Add(new Models.TransactionDetails()
                        {
                            SLC_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLC_Code,

                            SLT_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLT_Code,

                            SLE_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLE_Code,

                            SLN_Code = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).SLN_Code,

                            ReferenceNo = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).ReferenceNo,

                            ClientID = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                        Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                        t.ReferenceNo == balanceDetail.RefNo).ClientID,

                            SLDate = insertTransDetailsList.Find(t => Convert.ToByte(t.SLT_Code) == balanceDetail.SLT_Code &&
                         Convert.ToByte(t.SLC_Code) == balanceDetail.SLC_Code && t.ClientID == balanceDetail.ClientID &&
                         t.ReferenceNo == balanceDetail.RefNo).SLDate

                        });
                    }

                }
                return UnbalanceList;
            }
            else
            {
                return UnbalanceList;
            }

        }

        private LoanDues LoanIntDueAndPenaltyDue(Int64 ClientID, String RefNo, String SLT)
        {
            DateTime systemDate = Convert.ToDateTime(this.DataCon.DateTimeNow);
            String json = systemDate.ToString("yyyy-MM-dd") + ":" + ClientID + ":" + RefNo + ":" + SLT;
            LoanDues DueList = new LoanDues();

            CRUXLib.Response LoanInformation = App.CRUXAPI.request("loanreports/dues", json);
            if (LoanInformation.Status == "SUCCESS")
            {
                DueList = new JavaScriptSerializer().Deserialize<LoanDues>(LoanInformation.Content);

                return DueList;
            }
            else
            {
                return DueList;
            }

        }

        private void InsertTransDetailWithYesNoPrompt(List<Models.CheckLoanBalance> LoanBalanceList, Models.TransactionDetails transDetail, String WithPrompt)
        {
            LoanBalanceList.Add(new Models.CheckLoanBalance()
            {
                SLC = transDetail.SLC_Code,
                SLT = transDetail.SLT_Code,
                SLE = transDetail.SLE_Code,
                SLN = transDetail.SLN_Code,
                RefNo = transDetail.ReferenceNo,
                ClientID = transDetail.ClientID,
                SLDate = this.DataCon.DateTimeNow,
                WithPrompt = WithPrompt
            });
        }

        private List<Models.CheckLoanBalance> CheckAppStatus(List<Models.TransactionDetails> checkTrans)
        {
            List<Models.CheckLoanBalance> ClientsForUpdates = new List<Models.CheckLoanBalance>();

            String parsed = new JavaScriptSerializer().Serialize(checkTrans);
            CRUXLib.Response AppStatus = App.CRUXAPI.request("backoffice/CheckAppStatus", parsed);

            if (AppStatus.Status == "SUCCESS")
            {
                ClientsForUpdates = new JavaScriptSerializer().Deserialize<List<Models.CheckLoanBalance>>(AppStatus.Content);
            }

            return ClientsForUpdates;
        }

        private List<Models.CheckLoanBalance> CheckLoan(List<Models.TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        {
            decimal debitCount = 0, creditCount = 0;
            List<Models.CheckLoanBalance> LoanBalanceList = new List<Models.CheckLoanBalance>();
            LoanDues loanDues = new LoanDues();
            Int64 TempClientID = 0;
            Decimal SubTotalPenalty = 0, SubTotalInterest = 0;
            String TempRefNo = String.Empty;
            List<TransactionDetails> loanListWithPrincipal = new List<Models.TransactionDetails>();
            List<TransactionDetails> loanListWithInterest = new List<Models.TransactionDetails>();
            List<TransactionDetails> loanListWithPenalty = new List<Models.TransactionDetails>();

            foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
            {
                if (GenerateBalances.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                if ((TempClientID != transDetail.ClientID || TempClientID == transDetail.ClientID) && TempRefNo != transDetail.ReferenceNo)
                {
                    loanDues = LoanIntDueAndPenaltyDue(transDetail.ClientID, transDetail.ReferenceNo, transDetail.SLT_Code);

                    loanListWithPrincipal = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && l.SLE_Code == "11").ToList();
                    loanListWithInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).ToList();
                    loanListWithPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "41" || l.SLE_Code == "42")).ToList();
                }

                TempClientID = transDetail.ClientID;
                TempRefNo = transDetail.ReferenceNo;

                if (loanDues.InterestDue != 0 || loanDues.PenaltyDue != 0)
                {

                    if (transDetail.SLE_Code == "23" || transDetail.SLE_Code == "25") //for Interest Due
                    {
                        debitCount = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25") && (l.Debit > 0)).Count;
                        creditCount = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25") && (l.Credit > 0)).Count;

                        if (debitCount != 0 && creditCount != 0)
                        {
                            SubTotalInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).Sum(x => x.Debit);
                            SubTotalInterest += (-1 * insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).Sum(x => x.Credit));
                        }
                        else
                        {
                            SubTotalInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).Sum(x => x.Credit);
                        }

                        if (SubTotalInterest > loanDues.InterestDue)
                        {
                            if (loanDues.InterestOver != 0 && loanDues.InterestDue == 0)
                            {
                                if (SubTotalInterest > loanDues.InterestOver)
                                {
                                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                                }
                            }
                            else
                            {
                                InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                            }
                        }

                        if (transDetail.Amount < loanDues.InterestDue)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo"); //with YesNo = True
                        }
                    }
                    else if (transDetail.SLE_Code == "41" || transDetail.SLE_Code == "42") // for Penalty
                    {
                        debitCount = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41") && (l.Debit > 0)).Count;
                        creditCount = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41") && (l.Credit > 0)).Count;

                        if (debitCount != 0 && creditCount != 0)
                        {
                            SubTotalPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41")).Sum(x => x.Debit);
                            SubTotalPenalty += (-1 * insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41")).Sum(x => x.Credit));
                        }
                        else
                        {
                            SubTotalPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41")).Sum(x => x.Credit);
                        }

                        if (SubTotalPenalty > loanDues.PenaltyDue)
                        {
                            if (loanDues.PenaltyOver != 0 && loanDues.PenaltyDue == 0)
                            {
                                if (SubTotalPenalty > loanDues.PenaltyOver)
                                {
                                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                                }
                            }
                            else
                            {
                                InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                            }
                        }
                        if (SubTotalPenalty < loanDues.PenaltyDue)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo");
                        }

                    } //end else if
                    else if (transDetail.SLE_Code == "11")
                    {
                        if (transDetail.Amount > loanDues.PrincipalBalance)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
                        }
                    }


                    if (loanListWithPrincipal.Count != 0 || loanListWithInterest.Count != 0 || loanListWithPenalty.Count != 0)
                    {
                        if (loanDues.PenaltyDue != 0)
                        {
                            if (loanListWithPenalty.Count == 0)
                            {
                                InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo");
                            }
                        }

                        if (loanDues.InterestDue != 0)
                        {
                            if (loanListWithInterest.Count == 0)
                            {
                                InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo"); //with YesNo = True
                            }
                        }
                    }

                } //end if interest and penalty != 0

                else
                {
                    if (transDetail.SLE_Code == "11" && transDetail.AdjFlagCode == "P" && transDetail.Credit != 0)
                    {
                        if (transDetail.Amount > loanDues.PrincipalBalance)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
                        }
                    }
                }

                debitCount = 0;
                creditCount = 0;
            }

            return LoanBalanceList;
        }

        private List<Models.CheckLoanBalance> CheckOverPaymentsRemittance(List<Models.TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        {
            List<Models.CheckLoanBalance> LoanBalanceList = new List<Models.CheckLoanBalance>();
            LoanDues loanDues = new LoanDues();
            Int64 TempClientID = 0;
            String TempRefNo = String.Empty;

            foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
            {
                if (GenerateBalances.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                if (transDetail.BranchCode != 0 &&
                   !String.IsNullOrEmpty(transDetail.SLC_Code) &&
                   !String.IsNullOrEmpty(transDetail.SLT_Code) &&
                   !String.IsNullOrEmpty(transDetail.ReferenceNo) &&
                   transDetail.ClientID != 0)
                {
                    if (transDetail.SLC_Code == "12")
                    {
                        if ((TempClientID != transDetail.ClientID || TempClientID == transDetail.ClientID) && TempRefNo != transDetail.ReferenceNo)
                        {
                            loanDues = LoanIntDueAndPenaltyDue(transDetail.ClientID, transDetail.ReferenceNo, transDetail.SLT_Code);
                        }

                        TempClientID = transDetail.ClientID;
                        TempRefNo = transDetail.ReferenceNo;

                        if (loanDues.InterestDue != 0 || loanDues.PenaltyDue != 0)
                        {
                            if (transDetail.SLE_Code == "11")
                            {
                                if (transDetail.Amount > loanDues.PrincipalBalance)
                                {
                                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
                                }
                            }
                            else if (transDetail.SLE_Code == "23" || transDetail.SLE_Code == "25") //for Interest Due
                            {
                                if (transDetail.Amount > loanDues.InterestDue)
                                {
                                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
                                }
                            }
                            else if (transDetail.SLE_Code == "41" || transDetail.SLE_Code == "42") // for Penalty
                            {
                                if (transDetail.Amount > loanDues.PenaltyDue)
                                {
                                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                                }
                            } //end else if

                        } //end if interestdue != 0 || penaltyDue != 0

                    } //end slc = 12

                } // slc, sle, clientid, refno not null or emptry
            }

            return LoanBalanceList;
        }

        //private List<Models.CheckLoanBalance> CheckLoan(List<Models.TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        //{
        //    List<Models.CheckLoanBalance> LoanBalanceList = new List<Models.CheckLoanBalance>();
        //    LoanDues loanDues = new LoanDues();
        //    Int64 TempClientID = 0;
        //    Decimal SubTotalPenalty = 0, SubTotalInterest = 0;
        //    String TempRefNo = String.Empty;
        //    List<TransactionDetails> loanListWithPrincipal = new List<Models.TransactionDetails>();
        //    List<TransactionDetails> loanListWithInterest = new List<Models.TransactionDetails>();
        //    List<TransactionDetails> loanListWithPenalty = new List<Models.TransactionDetails>();

        //    foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
        //    {
        //        if (GenerateBalances.CancellationPending == true)
        //        {
        //            e.Cancel = true;
        //            break;
        //        }

        //        if ((TempClientID != transDetail.ClientID || TempClientID == transDetail.ClientID) && TempRefNo != transDetail.ReferenceNo)
        //        {
        //            loanDues = LoanIntDueAndPenaltyDue(transDetail.ClientID, transDetail.ReferenceNo, transDetail.SLT_Code);

        //            loanListWithPrincipal = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && l.SLE_Code == "11").ToList();
        //            loanListWithInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).ToList();
        //            loanListWithPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "41" || l.SLE_Code == "42")).ToList();
        //        }

        //        TempClientID = transDetail.ClientID;
        //        TempRefNo = transDetail.ReferenceNo;

        //        if (loanDues.InterestDue != 0 || loanDues.PenaltyDue != 0)
        //        {

        //            if (transDetail.SLE_Code == "23" || transDetail.SLE_Code == "25") //for Interest Due
        //            {
        //                SubTotalInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).Sum(x => x.Amount);

        //                if (SubTotalInterest > loanDues.InterestDue)
        //                {
        //                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
        //                }

        //                if (transDetail.Amount < loanDues.InterestDue)
        //                {
        //                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo"); //with YesNo = True
        //                }
        //            }
        //            else if (transDetail.SLE_Code == "41" || transDetail.SLE_Code == "42") // for Penalty
        //            {
        //                SubTotalPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41")).Sum(x => x.Amount);

        //                if (SubTotalPenalty > loanDues.PenaltyDue)
        //                {
        //                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
        //                }
        //                if (SubTotalPenalty < loanDues.PenaltyDue)
        //                {
        //                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo");
        //                }

        //            } //end else if
        //            else if (transDetail.SLE_Code == "11")
        //            {
        //                if (transDetail.Amount > loanDues.PrincipalBalance)
        //                {
        //                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
        //                }
        //            }


        //            if (loanListWithPrincipal.Count != 0 || loanListWithInterest.Count != 0 || loanListWithPenalty.Count != 0)
        //            {
        //                if (loanDues.PenaltyDue != 0)
        //                {
        //                    if (loanListWithPenalty.Count == 0)
        //                    {
        //                        InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo");
        //                    }
        //                }

        //                if (loanDues.InterestDue != 0)
        //                {
        //                    if (loanListWithInterest.Count == 0)
        //                    {
        //                        InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "YesNo"); //with YesNo = True
        //                    }
        //                }
        //            }

        //        } //end if interest and penalty != 0

        //        else
        //        {
        //            if (transDetail.SLE_Code == "11" && transDetail.AdjFlagCode == "P" && transDetail.Credit != 0)
        //            {
        //                if (transDetail.Amount > loanDues.PrincipalBalance)
        //                {
        //                    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
        //                }
        //            }
        //        }
        //    }

        //    return LoanBalanceList;
        //}

        //private List<Models.CheckLoanBalance> CheckOverPaymentsRemittance(List<Models.TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        //{
        //    List<Models.CheckLoanBalance> LoanBalanceList = new List<Models.CheckLoanBalance>();
        //    LoanDues loanDues = new LoanDues();
        //    Int64 TempClientID = 0;
        //    String TempRefNo = String.Empty;

        //    foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
        //    {
        //        if (GenerateBalances.CancellationPending == true)
        //        {
        //            e.Cancel = true;
        //            break;
        //        }

        //        if (transDetail.BranchCode != 0 &&
        //           !String.IsNullOrEmpty(transDetail.SLC_Code) &&
        //           !String.IsNullOrEmpty(transDetail.SLT_Code) &&
        //           !String.IsNullOrEmpty(transDetail.ReferenceNo) &&
        //           transDetail.ClientID != 0)
        //        {
        //            if (transDetail.SLC_Code == "12")
        //            {
        //                if ((TempClientID != transDetail.ClientID || TempClientID == transDetail.ClientID) && TempRefNo != transDetail.ReferenceNo)
        //                {
        //                    loanDues = LoanIntDueAndPenaltyDue(transDetail.ClientID, transDetail.ReferenceNo, transDetail.SLT_Code);
        //                }

        //                TempClientID = transDetail.ClientID;
        //                TempRefNo = transDetail.ReferenceNo;

        //                if (loanDues.InterestDue != 0 || loanDues.PenaltyDue != 0)
        //                {
        //                    if (transDetail.SLE_Code == "11")
        //                    {
        //                        if (transDetail.Amount > loanDues.PrincipalBalance)
        //                        {
        //                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
        //                        }
        //                    }
        //                    else if (transDetail.SLE_Code == "23") //for Interest Due
        //                    {
        //                        if (transDetail.Amount > loanDues.InterestDue)
        //                        {
        //                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok"); //withOut YesNo = False
        //                        }
        //                    }
        //                    else if (transDetail.SLE_Code == "41") // for Penalty
        //                    {
        //                        if (transDetail.Amount > loanDues.PenaltyDue)
        //                        {
        //                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
        //                        }
        //                    } //end else if

        //                } //end if interestdue != 0 || penaltyDue != 0

        //            } //end slc = 12

        //        } // slc, sle, clientid, refno not null or emptry
        //    }

        //    return LoanBalanceList;
        //}

        public void ReverseEntry()
        {
            if (App.CRUXAPI.isAllowed("update_journal"))
            {
                MessageBoxResult result = MessageBox.Show(this, "You are about to REVERSE THIS TRANSACTION! DO YOU WANT TO CONTINUE?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        bool pass = true;
                        UPDTrans updRev = new UPDTrans();
                        updRev.UPDTagID = this.UPDTags.Find(u => u.UPDTagDesc == "Reversed").UPDTagID;
                        updRev.CtlNo = this.DataCon.ControlNo;
                        updRev.TransCode = this.DataCon.TransactionCode;
                        updRev.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
                        updRev.TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year;
                        updRev.TransDate = this.DataCon.DateTimeNow;
                        updRev.TotalAmt = this.DataCon.TotalDebit;
                        updRev.OfficeID = this.DataCon.TransactionSummary.OfficeID;

                        while (pass)
                        {
                            if (!GenerateBalancesForPostedTrans.IsBusy)
                            {
                                if (unbalancedLoanPrincipal.Count == 0)
                                {
                                    ReverseExplanation _ReverseExplanation = new ReverseExplanation(this, "Reversed");
                                    _ReverseExplanation.Owner = this;
                                    _ReverseExplanation.ShowDialog();

                                    if (!String.IsNullOrEmpty(this.DataCon.ReversalExplanation))
                                    {

                                        this.DataCon.TransactionSummary.Explanation += this.DataCon.ReversalExplanation;
                                        updRev.Explanation = this.DataCon.TransactionSummary.Explanation;
                                        this.DataCon.UPDTrans = updRev;

                                        ProgressPanel.Visibility = Visibility.Visible;
                                        MainGrid.IsEnabled = false;
                                        GenerateReversedWorker.RunWorkerAsync();
                                    }
                                }
                                else
                                {
                                    foreach (Models.TransactionDetails detail in unbalancedLoanPrincipal)
                                    {
                                        MessageBoxResult alert = MessageBox.Show(this, "Entry/ies would result to a negative value in SL ClientID: " + String.Format("{0:D4}", detail.ClientID) +
                                             " SL Ref: " + detail.SLC_Code + "-" + detail.SLT_Code + "-" + detail.ReferenceNo + "-" + detail.SLE_Code + "-" + detail.SLN_Code +
                                                " Date: " + detail.SLDate,
                                                    "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                                    }

                                    ClearValues();
                                }

                                pass = false;
                            }

                            Thread.Sleep(50);
                        }

                        break;

                    case MessageBoxResult.No:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //To be Modified
        public void Cancelling()
        {
            if (App.CRUXAPI.isAllowed("update_journal"))
            {
                MessageBoxResult result = MessageBox.Show(this, "This document is about to be cancelled. Continue??", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:

                        UPDTrans updTrans = new UPDTrans();
                        updTrans.UPDTagID = this.UPDTags.Find(u => u.UPDTagDesc == "Cancelled").UPDTagID;
                        updTrans.CtlNo = this.DataCon.ControlNo;
                        updTrans.TransCode = this.DataCon.TransactionCode;
                        updTrans.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
                        updTrans.TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year;
                        updTrans.TransDate = this.DataCon.DateTimeNow;
                        updTrans.TotalAmt = this.DataCon.TotalDebit;
                        updTrans.OfficeID = this.DataCon.TransactionSummary.OfficeID;

                        ReverseExplanation _ReverseExplanation = new ReverseExplanation(this, "Cancelled");
                        _ReverseExplanation.Owner = this;
                        _ReverseExplanation.ShowDialog();

                        if (!String.IsNullOrEmpty(this.DataCon.ReversalExplanation))
                        {

                            this.DataCon.TransactionSummary.Explanation += this.DataCon.ReversalExplanation;
                            updTrans.Explanation = this.DataCon.TransactionSummary.Explanation;
                            this.DataCon.UPDTrans = updTrans;

                            String parsedUPDTrans = new JavaScriptSerializer().Serialize(updTrans);
                            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CancelTransDetails", parsedUPDTrans);

                            if (Response.Status == "SUCCESS")
                            {
                                if (this.DataCon.TransactionCode == CheckVoucherID)
                                {
                                    string parsedCancel = new JavaScriptSerializer().Serialize(updTrans);
                                    Response = App.CRUXAPI.request("backoffice/postTransCheckDetails", parsedCancel);

                                    if (Response.Status != "SUCCESS")
                                    {
                                        MessageBox.Show(this, "Failed to Modify Transaction Check Details");
                                    }
                                }

                                MessageBox.Show(this, "Module Was Successfully Cancelled!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                                ClearValues();
                            }
                            else
                            {
                                MessageBox.Show(this, Response.Content, "Module Not Posted!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }

                        break;
                    case MessageBoxResult.No:
                        break;
                }
            }
            else
            {
                MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PostingDetails(List<TransactionDetails> insertTransDetailsList, UPDTrans updTrans)
        {
            var withLoanPayment = insertTransDetailsList.FindAll(l => l.SLC_Code == "12").ToList();

            if (withLoanPayment.Count != 0 && this.DataCon.TransactionCode == 1)
            {
                ProgressPanel.Visibility = Visibility.Visible;
                MainGrid.IsEnabled = false;
                GeneratePostAfterCheckingGJWithLoanWorker.RunWorkerAsync();

            } //end if LoansPayment and TransCode = 1 "GJ"
            else if (withLoanPayment.Count != 0 && this.DataCon.TransactionCode == 6)
            {
                ProgressPanel.Visibility = Visibility.Visible;
                MainGrid.IsEnabled = false;
                GeneratePostAfterCheckingRMTWithLoanWorker.RunWorkerAsync();
            }
            else if (this.DataCon.TransactionCode == 99)
            {
                string parsedDate = new JavaScriptSerializer().Serialize(this.DataCon.TransactionSummary.TransactionDate);

                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckNotPostedDocs", "");
                if (Response.Status == "SUCCESS")
                {
                    int UnPostedDocuments = new JavaScriptSerializer().Deserialize<int>(Response.Content);

                    if (UnPostedDocuments != 0)
                    {
                        MessageBox.Show(this, "There are still UNPOSTED Documents. Cannot post transaction.", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    }
                    else
                    {
                        ProgressPanel.Visibility = Visibility.Visible;
                        MainGrid.IsEnabled = false;
                        GeneratePostAfterCheckingOthersWorker.RunWorkerAsync();
                    }
                }
            }
            else
            {
                ProgressPanel.Visibility = Visibility.Visible;
                MainGrid.IsEnabled = false;
                GeneratePostAfterCheckingOthersWorker.RunWorkerAsync();
            } // end else

        }

        public void Posting()
        {

            MessageBoxResult result = MessageBox.Show(this, "This document is about to be posted. Continue??", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:

                    List<TransactionDetails> insertTransDetailsList = new List<Models.TransactionDetails>(TempTransactionDetailsList);
                    UPDTrans updTrans = new UPDTrans();
                    updTrans.UPDTagID = this.UPDTags.Find(u => u.UPDTagDesc == "Posted").UPDTagID;
                    updTrans.CtlNo = this.DataCon.ControlNo;
                    updTrans.TransCode = this.DataCon.TransactionCode;
                    updTrans.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
                    updTrans.TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year;
                    updTrans.TransDate = this.DataCon.DateTimeNow;
                    updTrans.TotalAmt = this.DataCon.TotalDebit;
                    updTrans.OfficeID = this.DataCon.TransactionSummary.OfficeID;

                    updTrans.ORNo = this.DataCon.TransactionSummary.ORNo; // BIR Compliance

                    updTrans.PostTransDetailsOnly = TransDetailPosting;
                    updTrans.PostSLDetailsOnly = SLDetailPosting;
                    updTrans.PostGLTransOnly = GLTaransOnly;

                    var AcctCode = insertTransDetailsList.FindAll(a => a.AccountCode == 40203).ToList();
                    var SLsubscription = insertTransDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 3.ToString("D2")).ToList();

                    if (AcctCode.Count != 0 && SLsubscription.Count != 0 && updTrans.TransCode == 77)
                    {
                        updTrans.IsProbi = true;
                    }
                    else
                    {
                        updTrans.IsProbi = false;
                    }

                    var subscribe = insertTransDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 2.ToString("D2"));
                    var subscription = insertTransDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 3.ToString("D2"));

                    if (subscribe.Count != 0 && subscribe.Count != 0 && updTrans.TransCode == 77)
                    {
                        updTrans.IsClosed = true;
                    }
                    else
                    {
                        updTrans.IsClosed = false;
                    }

                    this.DataCon.UPDTrans = updTrans;
                    this.DataCon.TransactionDetails = new List<Models.TransactionDetails>(TempTransactionDetailsList);

                    if (this.DataCon.TransactionCode == 22 || this.DataCon.TransactionCode == 16)
                    {
                        if (this.DataCon.TransactionChecks.Count.Equals(0))
                        {
                            MessageBox.Show(this, "Please insert check details!", "Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
                            var gj = this.DataCon.TransactionDetails.Find(c => c.SLC_Code == "11" && c.SLT_Code == "02" && c.Credit != 0).AccountCode;
                            var tc = this.DataCon.TransactionChecks.First().AcctCode;

                            if (gj == tc) //checking of GL accountcode and its bankcode
                            {
                                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/SystemDate", "");
                                if (Response.Status == "SUCCESS")
                                {
                                    SystemConfig _systemConfig = new JavaScriptSerializer().Deserialize<SystemConfig>(Response.Content);

                                    if (_systemConfig.SystemDate.ToString("MM/dd/yyyy") == this.DataCon.TransactionChecks.FirstOrDefault().CheckDate)
                                    {
                                        ProgressPanel.Visibility = Visibility.Visible;
                                        MainGrid.IsEnabled = false;
                                        GeneratePostAfterCheckingCVWorker.RunWorkerAsync();
                                    }
                                    else
                                    {
                                        MessageBox.Show(this, "Error: Required Field: Date of Check must be equal to current system Date!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(this, "Failed to retrieve system Date. Please contact Administrator.");
                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "Error:The GL code does not reconcile with the Bank Code!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }

                    } // end transaction Code = 22

                    else
                    {
                        PostingDetails(insertTransDetailsList, updTrans);
                    }

                    break;

                case MessageBoxResult.No:
                    break;

            }

        }

        //public void Posting()
        //{
        //    if (App.CRUXAPI.isAllowed("update_journal"))
        //    {
        //        MessageBoxResult result = MessageBox.Show(this, "This document is about to be posted. Continue??", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
        //        switch (result)
        //        {
        //            case MessageBoxResult.Yes:

        //                List<TransactionDetails> insertTransDetailsList = new List<Models.TransactionDetails>(TempTransactionDetailsList);
        //                UPDTrans updTrans = new UPDTrans();
        //                updTrans.UPDTagID = this.UPDTags.Find(u => u.UPDTagDesc == "Posted").UPDTagID;
        //                updTrans.CtlNo = this.DataCon.ControlNo;
        //                updTrans.TransCode = this.DataCon.TransactionCode;
        //                updTrans.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
        //                updTrans.TransYear = Convert.ToDateTime(this.DataCon.DateTimeNow).Year;
        //                updTrans.TransDate = this.DataCon.DateTimeNow;
        //                updTrans.TotalAmt = this.DataCon.TotalDebit;
        //                updTrans.OfficeID = this.DataCon.TransactionSummary.OfficeID;

        //                updTrans.ORNo = this.DataCon.TransactionSummary.ORNo; // BIR Compliance

        //                updTrans.PostTransDetailsOnly = TransDetailPosting;
        //                updTrans.PostSLDetailsOnly = SLDetailPosting;
        //                updTrans.PostGLTransOnly = GLTaransOnly;

        //                var AcctCode = insertTransDetailsList.FindAll(a => a.AccountCode == 40203).ToList();
        //                var SLsubscription = insertTransDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 3.ToString("D2")).ToList();

        //                if (AcctCode.Count != 0 && SLsubscription.Count != 0 && updTrans.TransCode == 77)
        //                {
        //                    updTrans.IsProbi = true;
        //                }
        //                else
        //                {
        //                    updTrans.IsProbi = false;
        //                }

        //                var subscribe = insertTransDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 2.ToString("D2"));
        //                var subscription = insertTransDetailsList.FindAll(s => s.SLC_Code == "31" && s.SLT_Code == 3.ToString("D2"));

        //                if (subscribe.Count != 0 && subscribe.Count != 0 && updTrans.TransCode == 77)
        //                {
        //                    updTrans.IsClosed = true;
        //                }
        //                else
        //                {
        //                    updTrans.IsClosed = false;
        //                }

        //                this.DataCon.UPDTrans = updTrans;
        //                this.DataCon.TransactionDetails = new List<Models.TransactionDetails>(TempTransactionDetailsList);

        //                if (this.DataCon.TransactionCode == 22)
        //                {
        //                    if (this.DataCon.TransactionChecks.Count.Equals(0))
        //                    {
        //                        MessageBox.Show(this, "Please insert check details!", "Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
        //                    }
        //                    else
        //                    {
        //                        var gj = this.DataCon.TransactionDetails.Find(c => c.SLC_Code == "11" && c.SLT_Code == "02" && c.Credit != 0).AccountCode;
        //                        var tc = this.DataCon.TransactionChecks.First().AcctCode;

        //                        if (gj == tc) //checking of GL accountcode and its bankcode
        //                        {
        //                            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/SystemDate", "");
        //                            if (Response.Status == "SUCCESS")
        //                            {
        //                                SystemConfig _systemConfig = new JavaScriptSerializer().Deserialize<SystemConfig>(Response.Content);

        //                                if (_systemConfig.SystemDate.ToString("MM/dd/yyyy") == this.DataCon.TransactionChecks.FirstOrDefault().CheckDate)
        //                                {
        //                                    ProgressPanel.Visibility = Visibility.Visible;
        //                                    MainGrid.IsEnabled = false;
        //                                    GeneratePostAfterCheckingCVWorker.RunWorkerAsync();
        //                                }
        //                                else
        //                                {
        //                                    MessageBox.Show(this, "Error: Required Field: Date of Check must be equal to current system Date!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                MessageBox.Show(this, "Failed to retrieve system Date. Please contact Administrator.");
        //                            }
        //                        }
        //                        else
        //                        {
        //                            MessageBox.Show(this, "Error:The GL code does not reconcile with the Bank Code!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
        //                        }
        //                    }

        //                } // end transaction Code = 22

        //                else
        //                {
        //                    PostingDetails(insertTransDetailsList, updTrans);
        //                }

        //                break;

        //            case MessageBoxResult.No:
        //                break;

        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
        //    }
        //}

        private Decimal CheckTotalFinesWaivedAndInterestWaived(List<TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        {
            Int64 TotalFinesWaivedAcctCode = 4039999;
            Byte SLC = 12, SLE = 42;
            Decimal TotalFinesWaived = 0, TotalSLFinesWaived = 0;

            var FinesWaived = insertTransDetailsList.FindAll(w => w.AccountCode == TotalFinesWaivedAcctCode).ToList();
            var SLFinesWaived = insertTransDetailsList.FindAll(w => w.SLC_Code == SLC.ToString("D2") && w.SLE_Code == SLE.ToString("D2")).ToList();

            if (FinesWaived.Count == 1)
            {
                TotalFinesWaived = FinesWaived.First().Amount;
            }
            else if (FinesWaived.Count > 1)
            {
                foreach (TransactionDetails details in FinesWaived)
                {
                    TotalFinesWaived += details.Amount;
                }
            }

            if (SLFinesWaived.Count == 1)
            {
                TotalSLFinesWaived = SLFinesWaived.First().Amount;
            }
            else if (SLFinesWaived.Count > 1)
            {
                foreach (TransactionDetails detail in SLFinesWaived)
                {
                    TotalSLFinesWaived += detail.Amount;
                }
            }

            List<Models.CheckLoanBalance> LoanBalanceList = new List<Models.CheckLoanBalance>();
            LoanDues loanDues = new LoanDues();
            Int64 TempClientID = 0;
            Decimal SubTotalPenalty = 0;
            String TempRefNo = String.Empty;
            List<TransactionDetails> loanListWithPrincipal = new List<Models.TransactionDetails>();
            List<TransactionDetails> loanListWithPenalty = new List<Models.TransactionDetails>();

            foreach (Models.TransactionDetails transDetail in insertTransDetailsList)
            {
                if (GenerateBalances.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                if ((TempClientID != transDetail.ClientID || TempClientID == transDetail.ClientID) && TempRefNo != transDetail.ReferenceNo)
                {
                    loanDues = LoanIntDueAndPenaltyDue(transDetail.ClientID, transDetail.ReferenceNo, transDetail.SLT_Code);
                }

                TempClientID = transDetail.ClientID;
                TempRefNo = transDetail.ReferenceNo;

                if (loanDues.InterestDue != 0 || loanDues.PenaltyDue != 0)
                {
                    if (transDetail.SLE_Code == "42") // for Penalty
                    {
                        //if (transDetail.Credit > loanDues.PenaltyDue)
                        //{
                        //    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        //}

                        SubTotalPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41")).Sum(x => x.Credit);

                        if (SubTotalPenalty > loanDues.PenaltyDue)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        }
                    } //end else if
                    else if (transDetail.SLE_Code == "41") // for Penalty
                    {
                        //if (transDetail.Credit > loanDues.PenaltyDue)
                        //{
                        //    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        //}

                        SubTotalPenalty = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "42" || l.SLE_Code == "41")).Sum(x => x.Credit);

                        if (SubTotalPenalty > loanDues.PenaltyDue)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        }
                    }

                } //end if loanDues != 0

            } // end Foreach


            if (LoanBalanceList.Count != 0)
            {
                return 1;
            }
            else
            {
                if (TotalSLFinesWaived != 0)
                {
                    if (TotalFinesWaived != (-1 * TotalSLFinesWaived))
                    {
                        if (TotalFinesWaived > TotalSLFinesWaived)
                        {
                            return TotalFinesWaived - (-1 * TotalSLFinesWaived);
                        }
                        else
                        {
                            return (-1 * TotalSLFinesWaived) + TotalFinesWaived;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }

            }
        }

        private Decimal CheckTotalInterestWaivedAndInterestWaived(List<TransactionDetails> insertTransDetailsList, DoWorkEventArgs e)
        {
            Int64 TotalInterestWaivedAcctCode = 4019999;
            Byte SLC = 12, SLE = 25;
            Decimal TotalInterestWaived = 0, TotalSLInterestWaived = 0;

            var FinesWaived = insertTransDetailsList.FindAll(w => w.AccountCode == TotalInterestWaivedAcctCode).ToList();
            var SLFinesWaived = insertTransDetailsList.FindAll(w => w.SLC_Code == SLC.ToString("D2") && w.SLE_Code == SLE.ToString("D2")).ToList();

            if (FinesWaived.Count == 1)
            {
                TotalInterestWaived = FinesWaived.First().Amount;
            }
            else if (FinesWaived.Count > 1)
            {
                foreach (TransactionDetails details in FinesWaived)
                {
                    TotalInterestWaived += details.Amount;
                }
            }

            if (SLFinesWaived.Count == 1)
            {
                TotalSLInterestWaived = SLFinesWaived.First().Amount;
            }
            else if (SLFinesWaived.Count > 1)
            {
                foreach (TransactionDetails detail in SLFinesWaived)
                {
                    TotalSLInterestWaived += detail.Amount;
                }
            }


            List<Models.CheckLoanBalance> LoanBalanceList = new List<Models.CheckLoanBalance>();
            LoanDues loanDues = new LoanDues();
            Int64 TempClientID = 0;
            Decimal SubTotalInterest = 0;
            String TempRefNo = String.Empty;
            List<TransactionDetails> loanListWithPrincipal = new List<Models.TransactionDetails>();
            List<TransactionDetails> loanListWithPenalty = new List<Models.TransactionDetails>();

            foreach (Models.TransactionDetails transDetail in insertTransDetailsList.FindAll(l => l.SLC_Code == "12").ToList())
            {
                if (GenerateBalances.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }

                if ((TempClientID != transDetail.ClientID || TempClientID == transDetail.ClientID) && TempRefNo != transDetail.ReferenceNo)
                {
                    loanDues = LoanIntDueAndPenaltyDue(transDetail.ClientID, transDetail.ReferenceNo, transDetail.SLT_Code);
                }

                TempClientID = transDetail.ClientID;
                TempRefNo = transDetail.ReferenceNo;

                if (loanDues.InterestDue != 0 || loanDues.PenaltyDue != 0)
                {
                    if (transDetail.SLE_Code == "25") // for Penalty
                    {
                        //if (transDetail.Credit > loanDues.InterestDue)
                        //{
                        //    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        //}

                        SubTotalInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).Sum(x => x.Credit);

                        if (SubTotalInterest > loanDues.InterestDue)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        }
                    } //end else if
                    else if (transDetail.SLE_Code == "23") // for Penalty
                    {
                        //if (transDetail.Credit > loanDues.InterestDue) //Amount
                        //{
                        //    InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        //}

                        SubTotalInterest = insertTransDetailsList.FindAll(l => l.ClientID == transDetail.ClientID && l.ReferenceNo == transDetail.ReferenceNo && (l.SLE_Code == "23" || l.SLE_Code == "25")).Sum(x => x.Credit);

                        if (SubTotalInterest > loanDues.InterestDue)
                        {
                            InsertTransDetailWithYesNoPrompt(LoanBalanceList, transDetail, "Ok");
                        }
                    }

                } //end if loanDues != 0

            } // end Foreach



            if (LoanBalanceList.Count != 0)
            {
                return 1;
            }
            else
            {
                if (TotalSLInterestWaived != 0)
                {
                    if (TotalInterestWaived != (-1 * TotalSLInterestWaived))
                    {
                        if (TotalInterestWaived > TotalSLInterestWaived)
                        {
                            return TotalInterestWaived - (-1 * TotalSLInterestWaived);
                        }
                        else
                        {
                            return (-1 * TotalSLInterestWaived) + TotalInterestWaived;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }

        public void ClearValues()
        {
            if (this.DataCon.ControlNo != 0)
            {
                Unlock();
            }

            InitializeDefaults();

            TempTransactionDetailsList = new List<Models.TransactionDetails>(this.DataCon.TransactionDetails);
            DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;

            COCIDataGrid.ItemsSource = this.DataCon.TransactionChecks;

            TempPurchaseDetails = new List<PurchaseDetails>(this.DataCon.PurchaseDetails);
            PurchaseDataGrid.ItemsSource = TempPurchaseDetails;

            this.DataContext = this.DataCon;

            //transTypeComboBox.SelectedIndex = -1;

            reversedLabel.Visibility = Visibility.Hidden;
            findButton.Focus();

            CtlNoTextBox.Visibility = Visibility.Hidden;
            this.GLTab.IsSelected = true;
            DG_GeneralJournal.IsReadOnly = true;
            COCIDataGrid.IsReadOnly = true;
            //ExplanationTextBox.IsEnabled = false;
            this.ExplanationTextBox.IsReadOnly = true;
            editButton.IsEnabled = false;
            pagePreviousButton.IsEnabled = false;
            pageNextButton.IsEnabled = false;
            saveButton.IsEnabled = false;
            TransEditMaintTrigger = false;

            newButton.IsEnabled = true;
            pageNextButton.IsEnabled = true;
            pagePreviousButton.IsEnabled = true;
            DefaultFormGridSize();
            IsInquiry = false;
            TransMainTrigger = true;

            AllowSave = false;

            PostingDateLabel.Visibility = Visibility.Hidden;

            AllowSaveWithDifference = false;

            ORTextBox.IsReadOnly = true;

        }

        private void PostTransCheck(Models.UPDTrans transDetailLoan)
        {
            string parsedLoanApp = new JavaScriptSerializer().Serialize(transDetailLoan);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/postTransCheckDetails", parsedLoanApp);

            if (Response.Status != "SUCCESS")
            {
                MessageBox.Show(this, "Failed to Insert to Transaction Check. Please Contact Administrator. ");
            }
        }

        private void UpdateLoanApp(Models.TransactionDetails transDetailLoan)
        {
            string parsedLoanApp = new JavaScriptSerializer().Serialize(transDetailLoan);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/updateLoadApp", parsedLoanApp);

            if (Response.Status != "SUCCESS")
            {
                MessageBox.Show(this, "Failed to Insert to Loan Log. Please Contact Administrator. ");
            }
        }

        private void CalculateTotalCOCIAmt()
        {
            this.DataCon.TotalCOCI = 0;

            if (this.DataCon.TransactionChecks.Count != 0)
            {
                this.DataCon.TotalCOCI = this.DataCon.TransactionChecks.First().Amount;
            }
        }

        private void CalculateTotalPrice()
        {
            this.DataCon.TotalDebit = TempTransactionDetailsList.Sum(c => c.Debit);
            this.DataCon.TotalCredit = TempTransactionDetailsList.Sum(c => c.Credit);

            CalculateDifference();
        }

        private void CalculateNetAmount()
        {
            this.DataCon.NetTotal = TempPurchaseDetails.Sum(c => c.NetAmt);
        }

        private void CalculateDifference()
        {
            if (this.DataCon.TotalDebit != 0 || this.DataCon.TotalCredit != 0)
            {
                if (this.DataCon.TotalDebit >= this.DataCon.TotalCredit)
                {
                    this.DataCon.TotalDifference = this.DataCon.TotalDebit - this.DataCon.TotalCredit;
                }
                else if (this.DataCon.TotalDebit < this.DataCon.TotalCredit)
                {
                    this.DataCon.TotalDifference = this.DataCon.TotalCredit - this.DataCon.TotalDebit;
                }
            }
            else if (this.DataCon.TotalDebit == 0 && this.DataCon.TotalCredit == 0)
            {
                this.DataCon.TotalDifference = 0;
            }
        }

        private void debitCreditTotalTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                CalculateDifference();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void ExecuteFile_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                FileMainMenuItem.Focus();
                FileMainMenuItem.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ExecuteDoc_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                docMainMenuItem.Focus();
                docMainMenuItem.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ExecuteInquiry_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                InquiryMainMenuItem.Focus();
                InquiryMainMenuItem.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ExecuteDepUtil_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                DepUtilMainMenuItem.Focus();
                DepUtilMainMenuItem.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void ExecuteDivAndPat_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                DividendAndPatronageMenu.Focus();
                DividendAndPatronageMenu.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void ExecuteSpecialTrans_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                SpecialMenu.Focus();
                SpecialMenu.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ExecuteBatchProcess_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                BatchProcessMenu.Focus();
                BatchProcessMenu.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ExecuteSystem_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                SystemMenu.Focus();
                SystemMenu.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void ExecuteTools_DropDown(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                ToolsMenu.Focus();
                ToolsMenu.IsSubmenuOpen = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        public void Executed_New(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (TransMainTrigger)
                {
                    if (newButton.IsEnabled)
                    {
                        NewTransaction();
                    }
                }
                else
                {
                    transMainMenuItem.Focus();
                    transMainMenuItem.IsSubmenuOpen = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        public void Executed_Post(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                ExecuteHelper execHelper = new ExecuteHelper(this);
                insertTransDetLists = TransactionDetails();

                String upd = "";

                upd = insertTransDetLists.FirstOrDefault().UPDTagCode;

                if (!DG_GeneralJournal.IsReadOnly)
                {
                    MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                } // end if  DG_JournalGrid is readonly

                else
                {
                    if (MainGrid.IsEnabled)
                    {
                        execHelper.ExecutePost(upd, e, this.DataCon.ClosedStatus);
                    }
                }
            }
            catch (Exception)
            {
                if (String.IsNullOrEmpty(referenceNumberTextBox.Text))
                {
                    MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                }
            }

        }

        private void SaveTransaction(ExecutedRoutedEventArgs executedE = null, KeyEventArgs keyE = null, RoutedEventArgs routedE = null)
        {
            try
            {
                ExecuteHelper exec = new ExecuteHelper(this);
                Boolean Proceed = true, ProceedSave = false;

                if (AllowSave)
                {
                    if (!String.IsNullOrEmpty(this.DataCon.TransactionSummary.Explanation))
                    {
                        if (!AllowSaveWithDifference)
                        {
                            if (this.DataCon.TotalDifference == 0)
                            {
                                ProceedSave = true;
                            }
                            else if (this.DataCon.TotalDifference != 0)
                            {
                                MessageBoxResult result = MessageBox.Show(this, "DEBIT & CREDIT Balances are not EQUAL!", "Alert", MessageBoxButton.OK);
                            }
                        }
                        else
                        {
                            ProceedSave = true;
                        }

                        if (ProceedSave)
                        {
                            DateTime _DateNow;

                            if (DateTime.TryParse(TransDateTime.Text, out _DateNow))
                            {
                                if (DateTime.Parse(TransDateTime.Text) <= DateTime.Parse(this.boh.TransDate).AddDays(1))
                                {
                                    String parsedDate = new JavaScriptSerializer().Serialize(TransDateTime.Text);

                                    CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckTransSumIfClosed", parsedDate);

                                    if (Response.Status == "SUCCESS")
                                    {
                                        Proceed = new JavaScriptSerializer().Deserialize<Boolean>(Response.Content);

                                        if (Proceed)
                                        {
                                            MessageBoxResult result = MessageBox.Show(this, "Error! Unable to change Date. Date selected belongs to a range that has closing entry.", "Error", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(this, "Allowed Forward Date is set to 1 day only.");
                                }

                            }
                            else
                            {
                                MessageBox.Show(this, "Invalid Date! Please enter correct Date Format.");
                            }

                            if (!Proceed)
                            {
                                if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID) || this.DataCon.TransactionCode == PurchaseJournalID)
                                {
                                    if (this.DataCon.ControlNo == 0)
                                    {
                                        if (this.DataCon.TransactionChecks.Count.Equals(0))
                                        {
                                            MessageBox.Show(this, "Please insert check details!");
                                        }
                                        else
                                        {
                                            if ((!String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLC_Code) &&
                                            !String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLT_Code) &&
                                            !String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLE_Code) &&
                                            !String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLN_Code)) ||
                                            (TempTransactionDetailsList.FirstOrDefault().AccountCode != 0))
                                            {
                                                var checkingNullRows = TempTransactionDetailsList.Where(t => (String.IsNullOrEmpty(t.SLC_Code) && String.IsNullOrEmpty(t.SLT_Code)
                                                                        && String.IsNullOrEmpty(t.SLE_Code) && String.IsNullOrEmpty(t.SLN_Code) && t.AccountCode == 0) || (t.AccountCode == 0)).ToList();
                                                if (checkingNullRows.Count == 0)
                                                {
                                                    Save("Insert");
                                                }
                                                else
                                                {
                                                    MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                                }

                                            }
                                            else
                                            {
                                                MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                            }
                                        }
                                    }
                                    else // For Update transaction
                                    {
                                        TransSumParam ts = new TransSumParam();
                                        ts.CtlNo = this.DataCon.ControlNo;
                                        ts.TransCode = this.DataCon.TransactionCode;

                                        String parsedCheckIfExist = new JavaScriptSerializer().Serialize(ts);

                                        CRUXLib.Response Response = App.CRUXAPI.request("backoffice/SearchTransCheck", parsedCheckIfExist);
                                        if (Response.Status == "SUCCESS")
                                        {
                                            String ctlNo = new JavaScriptSerializer().Deserialize<String>(Response.Content);

                                            if (!String.IsNullOrEmpty(ctlNo))
                                            {
                                                this.DataCon.ControlNew = Convert.ToInt64(ctlNo);
                                            }
                                            else
                                            {
                                                this.DataCon.ControlNew = 0;
                                            }

                                            List<TransactionCheck> checkDetails = new List<TransactionCheck>();
                                            checkDetails = InsertTransactionCheck(ts.CtlNo);

                                            if (checkDetails.Count == 0)
                                            {
                                                MessageBox.Show("Please insert check details!");
                                            }
                                            else
                                            {
                                                if ((!String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLC_Code) &&
                                                !String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLT_Code) &&
                                                !String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLE_Code) &&
                                                !String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLN_Code)) ||
                                                (UpdateTransactionDetails().FirstOrDefault().AccountCode != 0))
                                                {
                                                    if (!this.DataCon.ClosedStatus)
                                                    {
                                                        var checkingNullRows = UpdateTransactionDetails().Where(t => (String.IsNullOrEmpty(t.SLC_Code) && String.IsNullOrEmpty(t.SLT_Code)
                                                                           && String.IsNullOrEmpty(t.SLE_Code) && String.IsNullOrEmpty(t.SLN_Code) && t.AccountCode == 0) || (t.AccountCode == 0)).ToList();
                                                        if (checkingNullRows.Count == 0)
                                                        {
                                                            if (!String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CociTypeDesc) &&
                                                            !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CheckNo) &&
                                                                !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().BankCodeDesc) &&
                                                                    !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CheckTypeDesc) &&
                                                                        !String.IsNullOrEmpty(this.DataCon.TransactionChecks.First().CheckDate) &&
                                                                            this.DataCon.TransactionChecks.First().Amount != 0)
                                                            {
                                                                Update();
                                                            }
                                                            else
                                                            {
                                                                MessageBoxResult result = MessageBox.Show(this, "Error! Please insert check details!", "Error", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);

                                                                if (!COCITab.IsFocused)
                                                                {
                                                                    COCITab.Focus();
                                                                }
                                                                else
                                                                {
                                                                    COCITab.Focus();
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBoxResult result = MessageBox.Show(this, exec.CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                                                    }

                                                }
                                                else
                                                {
                                                    MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Failed to Retrieve Data. Please Contact Administrator.");
                                        }

                                    }
                                } //end if transType = Check Voucher

                                else // OTHER TRANSACTIONS ASIDE Check Voucher
                                {
                                    if (this.DataCon.ControlNo == 0)
                                    {
                                        if ((!String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLC_Code) &&
                                            !String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLT_Code) &&
                                            !String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLE_Code) &&
                                            !String.IsNullOrEmpty(TempTransactionDetailsList.FirstOrDefault().SLN_Code)) ||
                                            (TempTransactionDetailsList.FirstOrDefault().AccountCode != 0))
                                        {
                                            var checkingNullRows = TempTransactionDetailsList.Where(t => (String.IsNullOrEmpty(t.SLC_Code) && String.IsNullOrEmpty(t.SLT_Code)
                                                                        && String.IsNullOrEmpty(t.SLE_Code) && String.IsNullOrEmpty(t.SLN_Code) && t.AccountCode == 0) || (t.AccountCode == 0)).ToList();
                                            if (checkingNullRows.Count == 0)
                                            {
                                                Save("Insert");
                                            }
                                            else
                                            {
                                                MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                            }
                                        }
                                        else
                                        {
                                            MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                        }

                                    }
                                    else if (this.DataCon.ControlNo != 0)
                                    {
                                        if ((!String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLC_Code) &&
                                            !String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLT_Code) &&
                                            !String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLE_Code) &&
                                            !String.IsNullOrEmpty(UpdateTransactionDetails().FirstOrDefault().SLN_Code)) ||
                                            (UpdateTransactionDetails().FirstOrDefault().AccountCode != 0))
                                        {
                                            if (!this.DataCon.ClosedStatus)
                                            {
                                                var checkingNullRows = UpdateTransactionDetails().Where(t => (String.IsNullOrEmpty(t.SLC_Code) && String.IsNullOrEmpty(t.SLT_Code)
                                                                   && String.IsNullOrEmpty(t.SLE_Code) && String.IsNullOrEmpty(t.SLN_Code) && t.AccountCode == 0) || (t.AccountCode == 0)).ToList();
                                                if (checkingNullRows.Count == 0)
                                                {
                                                    Update();
                                                }
                                                else
                                                {
                                                    MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                                }
                                            }
                                            else
                                            {
                                                MessageBoxResult result = MessageBox.Show(this, exec.CreateClosingPrompt(), "Error", MessageBoxButton.OK);
                                            }
                                        }
                                        else
                                        {
                                            MessageBoxResult result = MessageBox.Show(this, "Error: Required: GL Account Code must have a VALID VALUE! OR THIS IS A BLANK ROW THAT NEEDS TO BE DELETED!", "Error", MessageBoxButton.OK);
                                        }

                                    } //end DataCon.ContrlNo

                                } //end else
                            }
                        }



                    } // end if Explanation TextBox is empty

                    else
                    {
                        //ExplanationTextBox.IsEnabled = true;

                        ExplanationTextBox.IsReadOnly = false;
                        ExplanationTextBox.Focus();
                    }
                }
            }  //end try 

            catch (Exception ex)
            {
                if (executedE != null)
                {
                    executedE.Handled = true;
                }
                else if (keyE != null)
                {
                    keyE.Handled = true;
                }
                else if (routedE != null)
                {
                    routedE.Handled = true;
                }
            }
        }

        public void Executed_Save(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (MainGrid.IsEnabled)
                {
                    SaveTransaction(e);
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        public void Executed_Reverse(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                ExecuteHelper execHelper = new ExecuteHelper(this);
                insertTransDetLists = TransactionDetails();

                String upd = "";
                upd = insertTransDetLists.First().UPDTagCode;

                if (!DG_GeneralJournal.IsReadOnly)
                {
                    MessageBox.Show(this, "Cannot execute while in Edit Mode!");

                } // end if  DG_JournalGrid is readonly

                else
                {
                    if (referenceNumberTextBox.Text == "")
                    {
                        MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                    }
                    else
                    {
                        if (MainGrid.IsEnabled)
                        {
                            execHelper.ExecuteReverse(upd, e, this.DataCon.ClosedStatus);
                        }
                    } //end else refNo != ""
                }
            }
            catch (Exception)
            {
                if (String.IsNullOrEmpty(referenceNumberTextBox.Text))
                {
                    MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                }
            }

        }

        public void Executed_Cancelled(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                ExecuteHelper execHelper = new ExecuteHelper(this);
                execHelper = new ExecuteHelper(this);

                if (String.IsNullOrEmpty(referenceNumberTextBox.Text))
                {
                    MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                }
                else
                {
                    if (!DG_GeneralJournal.IsReadOnly)
                    {
                        if (this.DataCon.ControlNo == 0)
                        {
                            MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                        }
                        else
                        {
                            execHelper.ExecuteCancel(this.DataCon.Upd, e, this.DataCon.ClosedStatus);

                        } //end else refNo != ""

                    } // end if  DG_JournalGrid is readonly
                    else
                    {
                        execHelper.ExecuteCancel(this.DataCon.Upd, e, this.DataCon.ClosedStatus);
                    }
                }

            }
            catch (Exception)
            {
                if (String.IsNullOrEmpty(referenceNumberTextBox.Text))
                {
                    MessageBoxResult result = MessageBox.Show("No Document Transaction Details are eligible for Cancelling! Aborting...", "Cancelling!", MessageBoxButton.OK);
                    //MessageBox.Show(this, "Cannot execute while in Edit Mode!");
                }
            }

        }

        public void Executed_Edit(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (TransEditMaintTrigger)
                {
                    if (App.CRUXAPI.isAllowed("update_journal"))
                    {
                        if (Lock() == "SUCCESS")
                        {
                            if (editButton.IsEnabled)
                            {
                                EnableEdit(e);
                                editButton.IsEnabled = false;
                                saveButton.IsEnabled = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "Transaction is currently on Edit Mode by another User: " + Lock());
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        public void Executed_FocusGrid(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                DG_GeneralJournal.Focus();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void findButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FindTransaction();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void FindTransaction()
        {
            transFilter = new TransactionFilter(this, trType, trTypeCode);
            transFilter.Owner = this;
            transFilter.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (this.DataCon.ControlNo != 0)
            {
                Unlock();
            }

            this.Close();
        }

        private void InterestOnSavings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                monthEnd = new InterestOnSavings(this, this.GLAccounts);
                monthEnd.Owner = this;
                monthEnd.ShowDialog();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void Inquiry()
        {
            transTypeComboBox.IsEnabled = false;
            TransMainTrigger = true;
            findButton.IsEnabled = true;
            newButton.IsEnabled = false;
            editButton.IsEnabled = false;
            findButton.Focus();
        }

        private void MenuClick(String transTypeModule = null, String TransTypeCode = null)
        {
            transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == transTypeModule).ToList();
            trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == transTypeModule).TransTypeModule;
            trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == TransTypeCode).TransTypeCode;

            TransMainTrigger = true;
            findButton.IsEnabled = true;
            transTypeComboBox.IsEnabled = false;
            transTypeComboBox.Focus();

        }

        private void PurchaseMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                POTab.IsSelected = true;
                MenuClick("PJ", "PJ");

                this.Title = PurchaseTitle;

                clientLabel.Content = "Vendor:";
                AndOrLabel.Content = " ";

                clientLabel.Visibility = Visibility.Visible;
                AndOrLabel.Visibility = Visibility.Visible;

                clientIDTextBox.Visibility = Visibility.Visible;
                clientName.Visibility = Visibility.Visible;
                clientORTextBox.Visibility = Visibility.Visible;
                findButton.Focus();

                newButton.IsEnabled = true;

                //if (App.CRUXAPI.isAllowed("update_journal"))
                //{
                //    newButton.IsEnabled = true;
                //}
                //else
                //{
                //    newButton.IsEnabled = false;
                //}
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void disbursementMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuClick("2", "CV");

                if (GLTab.IsSelected == false)
                {
                    GLTab.IsSelected = true;
                }

                this.Title = CashCheckDisbursmentTitle; //"Disbursement Transactions";

                if (App.CRUXAPI.isAllowed("update_journal"))
                {
                    newButton.IsEnabled = true;
                }
                else
                {
                    newButton.IsEnabled = false;
                }

                clientLabel.Content = "Client ...............";
                AndOrLabel.Content = "Or...................";

                clientLabel.Visibility = Visibility.Visible;
                clientIDTextBox.Visibility = Visibility.Visible;
                AndOrLabel.Visibility = Visibility.Visible;
                clientName.Visibility = Visibility.Visible;
                clientORTextBox.Visibility = Visibility.Visible;

                findButton.Focus();
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void GJMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuClick("1", "GJ");

                this.Title = "General Journal Transactions";

                if (GLTab.IsSelected == false)
                {
                    GLTab.IsSelected = true;
                }

                newButton.IsEnabled = true;
                //if (App.CRUXAPI.isAllowed("update_journal"))
                //{
                //    newButton.IsEnabled = true;
                //}
                //else
                //{
                //    newButton.IsEnabled = false;
                //}

                ClientInfoHidden();
                findButton.Focus();

            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void MaintenanceMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuItem menuItem = (MenuItem)e.OriginalSource;

                if (menuItem.Name == "LoanRecMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "1").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "REC").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = ReclassTitle;
                }
                else if (menuItem.Name == "UnearnedInterestMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "1").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "UA").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = UITitle; // "Unearned Interest Transactions"
                }
                else if (menuItem.Name == "OtherMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "1").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "ST").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = SpecialTransactionTitle; // "Special Transaction Transactions"
                }
                else if (menuItem.Name == "IntSavingsMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "8").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "8").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "ISD").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = InterestTitle; // "Interest on Savings Transactions"
                }
                else if (menuItem.Name == "cashReceiptMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "3").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "3").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "CR").TransTypeCode;
                    IsInquiry = true;
                    ClientInfoVisible();
                    this.Title = CashReceiptInquiryTitle; // "Cash Receipt Inquiry"
                }
                else if (menuItem.Name == "GJMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "1").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "GJ").TransTypeCode;
                    IsInquiry = true;
                    ClientInfoHidden();
                    this.Title = GeneralJournalTitle; // "General Journal Inquiry"
                }
                else if (menuItem.Name == "DisbursementsMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "2").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "2").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "CV").TransTypeCode;
                    IsInquiry = true;
                    ClientInfoVisible();
                    this.Title = CashCheckDisbursmentTitle; // "Cash Disbursements Inquiry"
                }
                else if (menuItem.Name == "YrMonthMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "1").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "1").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "CLE").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = ClosingTitle;
                }
                else if (menuItem.Name == "DividendMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "6").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "6").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "DIV").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = DividendTitle;
                }
                else if (menuItem.Name == "PatronageMaintenanceMenu")
                {
                    transTypeComboBox.ItemsSource = this.TransactionTypesList.FindAll(t => t.TransTypeModule == "7").ToList();
                    trType = this.TransactionTypesList.FirstOrDefault(t => t.TransTypeModule == "7").TransTypeModule;
                    trTypeCode = this.TransactionTypesList.Find(t => t.TransTypeCode == "PR").TransTypeCode;

                    ClientInfoHidden();
                    this.Title = PatronageTitle;
                }

                Inquiry();
                findButton.Focus();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ExecutePrint_DropDown(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void PrintDocument()
        {
            MessageBoxResult result = MessageBox.Show(this, "Confirm to Print this Document?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    Print();
                    break;
                case MessageBoxResult.No:

                    break;
            }
        }

        private void Print()
        {
            if (this.DataCon.TransactionDetails.Count > 0)
            {
                Report = new Reports.BackOfficeReports();
                Export = new Reports.BackOfficeReports();

                ProgressPanel.Visibility = Visibility.Visible;
                MainGrid.IsEnabled = false;
                this.DataCon.Title = this.Title;
                GenerateBOReportWorker.RunWorkerAsync();
            }
            else
            {
                MessageBoxResult result = MessageBox.Show(this, "Please select Document.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }

        }

        private void ReportingMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuItem controlList = (MenuItem)e.OriginalSource;

                MessageBoxResult result = MessageBox.Show(this, "Do you want to generate Control List for Printing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:

                        if (controlList.Name == "LoanRecControlList")
                        {
                            this.DataCon.ReportType = "LoanReclass";
                        }
                        else if (controlList.Name == "IntSavingsControlList")
                        {
                            this.DataCon.ReportType = "IntSavings";
                        }
                        else if (controlList.Name == "FlexiOnSavingsControlListMenu")
                        {
                            this.DataCon.ReportType = "FlexiSavings";
                        }
                        else if (controlList.Name == "AverageShareCapitalControlList")
                        {
                            this.DataCon.ReportType = "AvgShareCapital";
                        }
                        else if (controlList.Name == "DividendPatronageRefundControlList")
                        {
                            this.DataCon.ReportType = "DividendPatronageRefund";
                        }

                        GenerateReport();

                        break;
                    case MessageBoxResult.No:
                        e.Handled = true;
                        break;
                } //end switch
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        public void GenerateControlList()
        {
            try
            {
                MessageBoxResult result = MessageBox.Show(this, "Do you want to generate Control List for Printing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:

                        this.DataCon.ReportType = "IntSavings";

                        GenerateReport();

                        break;
                    case MessageBoxResult.No:
                        break;
                } //end switch
            }
            catch (Exception)
            {
                return;
            }

        }

        public void GenerateReport()
        {
            switch (this.DataCon.ReportType)
            {
                case "LoanReclass":
                    Report = new Reports.LoanReclassControlList();
                    Export = new Reports.LoanReclassControlList();
                    this.DataCon.Title = LoanReclassCLTitle;
                    break;
                case "IntSavings":
                    Report = new Reports.IntDepControlList();
                    Export = new Reports.IntDepControlList();
                    this.DataCon.Title = InterestCLTitle;
                    break;
                case "FlexiSavings":
                    break;
                case "AvgShareCapital":
                    Report = new Reports.AverageShareCapital();
                    Export = new Reports.AverageShareCapital();
                    this.DataCon.Title = AvgShareCapitalTitle;
                    break;
                case "DividendPatronageRefund":
                    Report = new Reports.DividendAndPatronageRefund();
                    Export = new Reports.DividendAndPatronageRefund();
                    this.DataCon.Title = DividendPatronageTitle;
                    break;
                default:
                    break;
            }

            ProgressPanel.Visibility = Visibility.Visible;
            MainGrid.IsEnabled = false;
            GenerateControlListWorker.RunWorkerAsync();
        }

        private void ClientInfoVisible()
        {
            clientLabel.Visibility = Visibility.Visible;
            clientIDTextBox.Visibility = Visibility.Visible;
            AndOrLabel.Visibility = Visibility.Visible;
            clientName.Visibility = Visibility.Visible;
            clientORTextBox.Visibility = Visibility.Visible;
        }

        private void ClientInfoHidden()
        {
            clientLabel.Visibility = Visibility.Hidden;
            clientIDTextBox.Visibility = Visibility.Hidden;
            AndOrLabel.Visibility = Visibility.Hidden;
            clientName.Visibility = Visibility.Hidden;
            clientORTextBox.Visibility = Visibility.Hidden;
        }

        private void COCIDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int i = COCIDataGrid.CurrentColumn.DisplayIndex;
                var selectedCOCI = COCIDataGrid.SelectedItem as Models.TransactionCheck;

                if (!COCIDataGrid.IsReadOnly)
                {
                    if (e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.F12)
                    {
                        if (COCIDataGrid.CurrentColumn.DisplayIndex != 6)
                        {
                            if (e.Key == Key.Space && COCIDataGrid.CurrentColumn.DisplayIndex == 0)
                            {
                                temp = true;
                                cociType = new COCIType(this);
                                cociType.Owner = this;
                                cociType.ShowDialog();
                            }

                            else if (COCIDataGrid.CurrentColumn.DisplayIndex == 1)
                            {
                                try
                                {
                                    if (e.Key == Key.Enter)
                                    {
                                        if ((COCIDataGrid.SelectedItem as TransactionCheck).CociTypeDesc == "")
                                        {
                                            e.Handled = true;
                                        }

                                        else { MoveToNextUIElement(); }

                                    }
                                }
                                catch (Exception)
                                {

                                    e.Handled = true;
                                }
                            }

                            else if (e.Key == Key.Space && COCIDataGrid.CurrentColumn.DisplayIndex == 2)
                            {
                                try
                                {
                                    if (selectedCOCI.CociTypeDesc == "" || selectedCOCI.CociTypeDesc == null)
                                    {
                                        e.Handled = true;
                                    }
                                    else
                                    {
                                        temp = true;
                                        bankCodes = new BankCode(this);
                                        bankCodes.Owner = this;
                                        bankCodes.ShowDialog();
                                    }
                                }
                                catch (Exception)
                                {
                                    e.Handled = true;
                                }
                            }

                            else if (e.Key == Key.Space && COCIDataGrid.CurrentColumn.DisplayIndex == 3)
                            {
                                try
                                {
                                    if (selectedCOCI.CociTypeDesc == "" || selectedCOCI.CociTypeDesc == null)
                                    {
                                        e.Handled = true;
                                    }
                                    else
                                    {
                                        typeChecks = new TypeChecks(this);
                                        typeChecks.Owner = this;
                                        typeChecks.ShowDialog();
                                    }
                                }
                                catch (Exception)
                                {
                                    e.Handled = true;
                                }
                            }

                            else if (COCIDataGrid.CurrentColumn.DisplayIndex == 6)
                            {
                                try
                                {
                                    if (e.Key == Key.Enter || e.Key == Key.Return)
                                    {
                                        if ((COCIDataGrid.SelectedItem as TransactionCheck).Amount == Convert.ToDecimal(null))
                                        {
                                            e.Handled = true;
                                        }

                                        else { MoveToNextUIElement(); }

                                    }
                                }
                                catch (Exception)
                                {

                                    e.Handled = true;
                                }

                            }

                            else if (e.Key == Key.Enter && COCIDataGrid.CurrentColumn.DisplayIndex != 6)
                            {
                                e.Handled = true;
                            }

                        }
                        else
                        {
                            COCIDataGrid.CanUserAddRows = false;
                        }

                    }

                    else if (e.Key == Key.Tab)
                    {
                        ExplanationTextBox.Focus();
                        // COCIDataGrid.CanUserAddRows = false;
                        e.Handled = true;
                    }

                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void clientIDTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.F2)
                {
                    if (trType == "PJ")
                    {
                        _VendorSearch = new VendorSearch(this);
                        _VendorSearch.Owner = this;
                        _VendorSearch.ShowDialog();
                    }
                    else
                    {
                        clientsearch = new clientSearch("DB", this, this.boh.BranchID, this.DataCon.ClientIDSearch, this.DataCon.ClientNameSearch);
                        clientsearch.Owner = this;
                        clientsearch.ShowDialog();
                    }


                }

                else if (e.Key == Key.Enter)
                {
                    FillClientGrid(Convert.ToInt64(clientIDTextBox.Text));
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void COCIDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                var selectedCOCI = COCIDataGrid.SelectedItem as TransactionCheck;

                if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ||
                    e.Key == Key.L || e.Key == Key.R || e.Key == Key.O ||
                    e.Key == Key.F || e.Key == Key.U || e.Key == Key.G ||
                    e.Key == Key.Subtract || e.Key == Key.Divide)
                {

                    if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 ||
                            e.Key == Key.L || e.Key == Key.R || e.Key == Key.O || e.Key == Key.F || e.Key == Key.U || e.Key == Key.G) &&
                            COCIDataGrid.CurrentColumn.DisplayIndex == 0)
                    {
                        e.Handled = true;
                    }

                    else if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) &&
                        COCIDataGrid.CurrentColumn.DisplayIndex == 1)
                    {
                        if ((COCIDataGrid.SelectedItem as TransactionCheck).CociTypeDesc == "")
                        {
                            e.Handled = true;
                        }
                    }

                    else if ((e.Key == Key.L || e.Key == Key.R || e.Key == Key.O || e.Key == Key.F || e.Key == Key.U || e.Key == Key.G)
                        && COCIDataGrid.CurrentColumn.DisplayIndex == 3)
                    {
                        try
                        {
                            if (selectedCOCI.CociTypeDesc == "" || selectedCOCI.CociTypeDesc == null)
                            {
                                e.Handled = true;
                            }
                            else
                            {
                                Byte clearingDays = Convert.ToByte(checkTypesList.Find(c => c.ShortCutKey == e.Key.ToString()).CheckTypeDays);
                                Byte CheckTypeID = Convert.ToByte(checkTypesList.Find(c => c.ShortCutKey == e.Key.ToString()).CheckTypeID);
                                String CheckTypeDesc = checkTypesList.Find(c => c.ShortCutKey == e.Key.ToString()).CheckTypeDesc;

                                switch (e.Key)
                                {
                                    case Key.L:
                                        selectedCOCI.ClearingDays = clearingDays.ToString();
                                        selectedCOCI.CheckType = CheckTypeID;
                                        selectedCOCI.CheckTypeDesc = CheckTypeDesc;
                                        selectedCOCI.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                                        break;
                                    case Key.R:
                                        selectedCOCI.ClearingDays = clearingDays.ToString();
                                        selectedCOCI.CheckType = CheckTypeID;
                                        selectedCOCI.CheckTypeDesc = CheckTypeDesc;
                                        selectedCOCI.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                                        break;
                                    case Key.O:
                                        selectedCOCI.ClearingDays = clearingDays.ToString();
                                        selectedCOCI.CheckType = CheckTypeID;
                                        selectedCOCI.CheckTypeDesc = CheckTypeDesc;
                                        selectedCOCI.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                                        break;
                                    case Key.F:
                                        selectedCOCI.ClearingDays = clearingDays.ToString();
                                        selectedCOCI.CheckType = CheckTypeID;
                                        selectedCOCI.CheckTypeDesc = CheckTypeDesc;
                                        selectedCOCI.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                                        break;
                                    case Key.U:
                                        selectedCOCI.ClearingDays = clearingDays.ToString();
                                        selectedCOCI.CheckType = CheckTypeID;
                                        selectedCOCI.CheckTypeDesc = CheckTypeDesc;
                                        selectedCOCI.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                                        break;
                                    case Key.G:
                                        selectedCOCI.ClearingDays = clearingDays.ToString();
                                        selectedCOCI.CheckType = CheckTypeID;
                                        selectedCOCI.CheckTypeDesc = CheckTypeDesc;
                                        selectedCOCI.CheckTypeFormat = String.Format("{0} ({1}) days", CheckTypeDesc, clearingDays);
                                        break;

                                }
                                MoveToNextUIElement();
                                MoveToNextUIElement();
                            }
                        }
                        catch (Exception)
                        {
                            e.Handled = true;
                        }
                    }

                    else if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) &&
                            COCIDataGrid.CurrentColumn.DisplayIndex == 3)
                    {
                        e.Handled = true;
                    }

                    else if (!((e.Key == Key.Subtract || e.Key == Key.Divide ||
                        e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                        && COCIDataGrid.CurrentColumn.DisplayIndex == 5 || COCIDataGrid.CurrentColumn.DisplayIndex == 6))
                    {
                        e.Handled = true;
                    }

                    else if ((e.Key >= Key.D0 && e.Key <= Key.D9 || e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                         && COCIDataGrid.CurrentColumn.DisplayIndex == 6)
                    {
                        if ((COCIDataGrid.SelectedItem as TransactionCheck).CheckType == Convert.ToByte(null))

                        {
                            e.Handled = true;
                        }
                    }

                }//end if e.key

                else
                {

                    e.Handled = true;
                }

            }//end of try
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void FillCOCICheckTypes()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckTypes", "");

            if (Response.Status == "SUCCESS")
            {
                List<Models.CheckType> cTypesList = new JavaScriptSerializer().Deserialize<List<Models.CheckType>>(Response.Content);
                checkTypesList = cTypesList;
            }
            else
            {
                MessageBox.Show(this, Response.Content);
            }
        }

        private void COCIDataGrid_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (COCIDataGrid.CurrentColumn.DisplayIndex == 0)
                {
                    //if (temp) { COCIDataGrid.CanUserAddRows = false; }

                }
            }
            catch (Exception)
            {

                e.Handled = true;
            }
        }

        private List<TransactionCheck> InsertTransactionCheck(Int64 CTLNo = 0, String OptionType = null)
        {
            List<TransactionCheck> tcList = new List<TransactionCheck>();

            foreach (TransactionCheck tc in this.DataCon.TransactionChecks)
            {
                DateTime ClearingDate = Convert.ToDateTime(tc.CheckDate).AddDays(Convert.ToInt32(tc.ClearingDays));

                if (String.IsNullOrEmpty(OptionType))
                {
                    tcList.Add(new TransactionCheck()
                    {
                        TransactionCode = this.DataCon.TransactionCode,
                        CTLNo = CTLNo,
                        CheckType = tc.CheckType,
                        CociType = tc.CociType,
                        ClearingDays = tc.ClearingDays,
                        CheckNo = tc.CheckNo,
                        CheckDate = tc.CheckDate,
                        Amount = tc.Amount,
                        ClearingDate = ClearingDate.ToString("MM/dd/yyyy"),
                        BankID = tc.BankID,
                        BankCodeDesc = tc.BankCodeDesc,
                    });
                }
                else
                {
                    tcList.Add(new TransactionCheck()
                    {
                        TransactionCode = this.DataCon.TransactionCode,
                        CTLNo = CTLNo,
                        CheckType = tc.CheckType,
                        CociType = tc.CociType,
                        ClearingDays = tc.ClearingDays,
                        CheckNo = tc.CheckNo,
                        CheckDate = tc.CheckDate,
                        Amount = tc.Amount,
                        ClearingDate = ClearingDate.ToString("MM/dd/yyyy"),
                        BankID = tc.BankID,
                        BankCodeDesc = tc.BankCodeDesc,
                        UPDTag = Convert.ToByte(this.UPDTags.Find(u => u.UPDTagDesc == "Posted").UPDTagID)
                    });

                }

            }

            return tcList;
        }

        public String Lock()
        {
            String Status = "";

            LockRequest lockRequest = new LockRequest()
            {
                ID = Convert.ToDateTime(this.DataCon.DateTimeNow).Year + ":" + Convert.ToInt32(this.DataCon.TransactionCode) + ":" + String.Format("{0:D8}", this.DataCon.ControlNo),
                TableName = "tblTransactionDetails"
            };

            String parsedLock = new JavaScriptSerializer().Serialize(lockRequest);
            CRUXLib.Response Response = App.CRUXAPI.request("Lock/enable", parsedLock);

            if (Response.Status == "SUCCESS")
            {
                return Status = Response.Content;
            }
            else
            {
                return Status = Response.Content;
            }
        }

        public void Unlock()
        {
            try
            {
                LockRequest lockRequest = new LockRequest()
                {
                    ID = Convert.ToDateTime(this.DataCon.DateTimeNow).Year + ":" + Convert.ToInt32(this.DataCon.TransactionCode) + ":" + String.Format("{0:D8}", this.DataCon.ControlNo),
                    TableName = "tblTransactionDetails"
                };

                String parsedLock = new JavaScriptSerializer().Serialize(lockRequest);
                CRUXLib.Response Response = App.CRUXAPI.request("Lock/disable", parsedLock);

                if (Response.Status == "SUCCESS")
                {

                }
                else
                {

                }
            }
            catch (Exception)
            {
                return;
                //MessageBox.Show("Please enter valid date to clear values.");
            }

        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (App.CRUXAPI.isAllowed("update_journal"))
                {
                    if (Lock() == "SUCCESS")
                    {
                        EnableEdit(e);
                        e.Handled = true;
                        editButton.IsEnabled = false;
                        saveButton.IsEnabled = true;
                    }
                    else
                    {
                        MessageBox.Show(this, "Transaction is currently on Edit Mode by another User: " + Lock());
                    }
                }
                else
                {
                    MessageBox.Show(this, "You don't have access for this type of transaction.", "!!!!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void EnableEdit(RoutedEventArgs e)
        {
            try
            {
                AllowSave = true;

                Int16 chckUPD = CheckUPD(Convert.ToInt16(this.DataCon.CurrentBranch), Convert.ToInt16(this.DataCon.TransactionCode),
                    this.DataCon.ControlNo);

                if (chckUPD == 0)
                {
                    if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
                    {
                        DG_GeneralJournal.IsEnabled = true;
                        DG_GeneralJournal.IsReadOnly = false;
                        DG_GeneralJournal.CanUserAddRows = false;
                        // ExplanationTextBox.IsEnabled = true;
                        this.ExplanationTextBox.IsReadOnly = false;

                        COCIDataGrid.IsEnabled = true;
                        COCIDataGrid.IsReadOnly = false;
                        COCIDataGrid.CanUserAddRows = false;

                        checkTypesList = new List<CheckType>();
                        FillCOCICheckTypes();

                        if (this.DataCon.TransactionChecks.Count == 0)
                        {
                            COCIDataGrid.ItemsSource = null;
                            COCIDataGrid.ItemsSource = this.DataCon.TransactionChecks;
                            this.DataCon.TransactionChecks.Add(new Models.TransactionCheck());

                            FillCOCICheckTypes();
                        }
                    }
                    else
                    {
                        DG_GeneralJournal.IsEnabled = true;
                        DG_GeneralJournal.IsReadOnly = false;
                        DG_GeneralJournal.CanUserAddRows = false;
                        // ExplanationTextBox.IsEnabled = true;
                        this.ExplanationTextBox.IsReadOnly = false;
                    }

                    TransDateTime.IsEnabled = true;

                    Keyboard.Focus(dgFocus.GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));
                    e.Handled = true;

                    ORTextBox.IsReadOnly = false;

                }

                if (chckUPD == 1)
                {
                    if (this.DataCon.TransactionCode == Convert.ToInt16(CheckVoucherID))
                    {
                        this.ExplanationTextBox.IsReadOnly = false;
                        ExplanationTextBox.Focus();
                        COCIDataGrid.CanUserAddRows = false;
                    }
                    else
                    {
                        this.ExplanationTextBox.IsReadOnly = false;
                        ExplanationTextBox.Focus();
                    }

                }

                else if (chckUPD == 5 || chckUPD == 2)
                {
                    MessageBox.Show(this, "Cannot Edit a Reversed/Cancelled Document");
                }

                pageNextButton.IsEnabled = false;
                pagePreviousButton.IsEnabled = false;
                saveButton.IsEnabled = true;
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private Int16 CheckUPD(Int16 branchCode, Int16 transCode, Int64 controlNo)
        {
            try
            {
                CheckUPD ckUPD = new Models.CheckUPD();
                ckUPD.BranchCode = branchCode;
                ckUPD.TransactionCode = transCode;
                ckUPD.ControlNo = controlNo;

                string parsed = new JavaScriptSerializer().Serialize(ckUPD);
                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/checkUPD", parsed);

                if (Response.Status == "SUCCESS")
                {
                    Int16 chkUPD = new JavaScriptSerializer().Deserialize<Int16>(Response.Content);
                    return chkUPD;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                findButton.Focus();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void transTypeComboBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Tab)
                {
                    ORTextBox.Focus();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }

        private void ExplanationTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ExplanationTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
            ExplanationTextBox.IsInactiveSelectionHighlightEnabled = true;
        }

        private void ExplanationTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ExplanationTextBox.Background = System.Windows.Media.Brushes.LightGray; // WPF
            ExplanationTextBox.IsInactiveSelectionHighlightEnabled = true;
        }

        public void DisplayDocInq(TransactionDetailContainer _TransDetailContainer, RunWorkerCompletedEventArgs e)
        {
            OrigDate = _TransDetailContainer._TransactionSummary.TransactionDate; //1-5-2019 for forward transaction --new year

            this.DataCon.TransYear = _TransDetailContainer._TransactionSummary.TransYear.ToString();
            this.DataCon.CurrentBranch = _TransDetailContainer._TransactionSummary.BranchCode;
            this.DataCon.Upd = _TransDetailContainer._TransactionDetailsList.FirstOrDefault().UPDTagCode;
            this.DataCon.TransactionCode = _TransDetailContainer._TransactionDetailsList.FirstOrDefault().TransactionCode;
            this.DataCon.DateTimeNow = _TransDetailContainer._TransactionSummary.TransactionDate;
            this.DataCon.ControlNo = _TransDetailContainer._TransactionDetailsList.FirstOrDefault().ControlNo;
            this.DataCon.ClosedStatus = _TransDetailContainer._TransactionSummary.ClosedStatus;

            this.DataCon.TransactionDetails = _TransDetailContainer._TransactionDetailsList;
            this.DataCon.TransactionSummary = _TransDetailContainer._TransactionSummary;

            TempTransactionDetailsList = new List<Models.TransactionDetails>(this.DataCon.TransactionDetails);
            DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;

            //TempPurchaseDetails = new List<PurchaseDetails>(this.DataCon.PurchaseDetails);
            //PurchaseDataGrid.ItemsSource = TempPurchaseDetails;

            if (this.DataCon.TransactionDetails.Count != 0)
            {
                DG_GeneralJournal.SelectedItem = this.DataCon.TransactionDetails.FirstOrDefault();
            }

            if (_TransDetailContainer._TransactionDetailsList != null)
            {
                if (_TransDetailContainer.TransactionCheckList != null)
                {
                    this.DataCon.TransactionChecks = new ObservableCollection<Models.TransactionCheck>(_TransDetailContainer.TransactionCheckList);
                }
                COCIDataGrid.ItemsSource = this.DataCon.TransactionChecks;
                CalculateTotalCOCIAmt();
            }

            if (_TransDetailContainer.PurchaseDetails.Count != 0)
            {
                this.DataCon.PurchaseDetails = _TransDetailContainer.PurchaseDetails;
                TempPurchaseDetails = new List<PurchaseDetails>(this.DataCon.PurchaseDetails);
                PurchaseDataGrid.ItemsSource = TempPurchaseDetails;
            }

            if (this.DataCon.Upd == "R")
            {
                reversedLabel.Visibility = Visibility.Visible;
                reversedLabel.Content = "REVERSED";
            }
            else if (this.DataCon.Upd == "C")
            {
                reversedLabel.Visibility = Visibility.Visible;
                reversedLabel.Content = "CANCELLED";
            }
            else
            {
                reversedLabel.Visibility = Visibility.Hidden;
            }

            CheckTransactionDetailsList = new List<Models.TransactionDetails>(_TransDetailContainer._TransactionDetailsList); //for checking

            DG_GeneralJournal.IsEnabled = true;
            DG_GeneralJournal.IsReadOnly = true;
            COCIDataGrid.IsReadOnly = true;
            CtlNoTextBox.Visibility = Visibility.Visible;
            // this.ExplanationTextBox.IsEnabled = true;
            this.ExplanationTextBox.IsReadOnly = true;
            transTypeComboBox.IsEnabled = false;

            pageNextButton.IsEnabled = true;
            pagePreviousButton.IsEnabled = true;

            CalculateTotalPrice();

            TransDateTime.IsEnabled = false;
            TransEditMaintTrigger = true;
            DefaultFormGridSize();
            newButton.IsEnabled = false;

            if (App.CRUXAPI.isAllowed("update_journal"))
            {
                if (IsInquiry)
                { DisableButtons(); }
                else
                { EnableButtons(); }
            }
            else
            {
                DisableButtons();
            }


            withLPList = new List<Models.TransactionDetails>();

            if (String.IsNullOrEmpty(this.DataCon.Upd))
            {
                unbalanceListWithHold = new List<Models.TransactionDetails>();

                unbalanceListBW = new List<Models.TransactionDetails>();
                unbalancedLoanListBW = new List<CheckLoanBalance>();
                unbalancedLoanPrincipal = new List<Models.TransactionDetails>();
                unbalancedRMTListBW = new List<CheckLoanBalance>();

                CheckClosedAccts = new List<TransactionDetails>();
                WithClosedAccts = new List<AccountNumbers>();
                ClientsForApprovalList = new List<Models.CheckLoanBalance>();

                CheckFinesWaived = new List<Models.TransactionDetails>();
                CheckInterestWaived = new List<Models.TransactionDetails>();

                PostOrReverse = 1;
                GenerateBalances.WorkerReportsProgress = true;
                GenerateBalances.RunWorkerAsync();

                //Boolean CheckBalanceTrigger = false;

                //if (GenerateBalances.IsBusy)
                //{
                //    GenerateBalances.CancelAsync();
                //    // GenerateBalances.Dispose();

                //    //BackgroundWorker GenerateBalancesNew = new BackgroundWorker();
                //    //GenerateBalancesNew.DoWork += GenerateBalances_DoWork;
                //    //GenerateBalancesNew.RunWorkerCompleted += GenerateBalances_RunWorkerCompleted;
                //    //PostOrReverse = 1;
                //    //GenerateBalancesNew.RunWorkerAsync();
                //   // CheckBalanceTrigger = true;
                //}
                //else
                //{
                //    CheckBalanceTrigger = true;
                //}

                //if (CheckBalanceTrigger)
                //{
                //    PostOrReverse = 1;
                //    GenerateBalances.WorkerReportsProgress = true;
                //    GenerateBalances.RunWorkerAsync();
                //}
            }
            else if (this.DataCon.Upd == "P")
            {
                if (!String.IsNullOrEmpty(_TransDetailContainer._TransactionSummary.DatePosted))
                {
                    this.DataCon.TransactionSummary.DatePosted = "Date Posted: " + _TransDetailContainer._TransactionSummary.DatePosted;
                    PostingDateLabel.Visibility = Visibility.Visible;
                }
                else
                {
                    PostingDateLabel.Visibility = Visibility.Hidden;
                }

                unbalancedLoanPrincipal = new List<Models.TransactionDetails>();
                PostOrReverse = 2;
                GenerateBalancesForPostedTrans.RunWorkerAsync();
            }
        }

        public void openModule()
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke
                (
                    DispatcherPriority.Background, new Action(() => Keyboard.Focus(findButton))
                );
            }
            catch (Exception ex)
            {
            }
        }

        private void DisableButtons()
        {
            editButton.IsEnabled = false;
            saveButton.IsEnabled = false;
        }

        private void EnableButtons()
        {
            editButton.IsEnabled = true;
            // saveButton.IsEnabled = true;
        }

        private void NextDocument()
        {
            if (pageNextButton.IsEnabled)
            {
                String parsedDocInq = new JavaScriptSerializer().Serialize(SetDocInqParams());
                CRUXLib.Response Response;

                Response = App.CRUXAPI.request("backoffice/SelectNextTransDetail", parsedDocInq);

                if (Response.Status == "SUCCESS")
                {

                    DocInquiryParameters listDoc = new JavaScriptSerializer().Deserialize<DocInquiryParameters>(Response.Content);

                    if (listDoc.CTLNo == 0 && String.IsNullOrEmpty(listDoc.DocNo))
                    {
                        MessageBoxResult result = MessageBox.Show(this, "No more Documents.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    else
                    {
                        String parsedSelected = new JavaScriptSerializer().Serialize(listDoc);

                        Response = App.CRUXAPI.request("backoffice/SelectTransDetail", parsedSelected);

                        if (Response.Status == "SUCCESS")
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            serializer.MaxJsonLength = Int32.MaxValue;
                            _transDetailContainer = serializer.Deserialize<TransactionDetailContainer>(Response.Content);
                            DisplayDocInq(_transDetailContainer, null);
                        }
                    }

                }
            }
        }

        private void pageNextButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NextDocument();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private DocInquiryParameters SetDocInqParams()
        {
            DateTime dd = Convert.ToDateTime(TransDateTime.Text);

            docInqParams = new DocInquiryParameters();

            docInqParams.BranchCode = Convert.ToInt16(this.DataCon.CurrentBranch);
            docInqParams.DocNo = String.Format("{0:D8}", this.DataCon.ControlNo);
            docInqParams.TransCode = this.DataCon.TransactionCode;
            docInqParams.TransYear = Convert.ToInt32(Convert.ToDateTime(this.DataCon.DateTimeNow).Year);
            docInqParams.CTLNo = this.DataCon.ControlNo;

            return docInqParams;
        }

        private void PreviousDocument()
        {
            if (pagePreviousButton.IsEnabled)
            {
                String parsedDocInq = new JavaScriptSerializer().Serialize(SetDocInqParams());
                CRUXLib.Response Response;

                Response = App.CRUXAPI.request("backoffice/SelectPreviousTransDetail", parsedDocInq);
                if (Response.Status == "SUCCESS")
                {

                    DocInquiryParameters listDoc = new JavaScriptSerializer().Deserialize<DocInquiryParameters>(Response.Content);

                    if (listDoc.CTLNo == 0 && String.IsNullOrEmpty(listDoc.DocNo))
                    {
                        MessageBoxResult result = MessageBox.Show(this, "No more Documents.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
                    }
                    else
                    {
                        String parsedSelected = new JavaScriptSerializer().Serialize(listDoc);

                        Response = App.CRUXAPI.request("backoffice/SelectTransDetail", parsedSelected);

                        if (Response.Status == "SUCCESS")
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            serializer.MaxJsonLength = Int32.MaxValue;
                            _transDetailContainer = serializer.Deserialize<TransactionDetailContainer>(Response.Content);
                            DisplayDocInq(_transDetailContainer, null);
                        }
                    }
                }
            }

        }

        private void pagePreviousButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PreviousDocument();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void batchNoTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Tab)
                {
                    if (trType == "PJ")
                    {
                        if (!POTab.IsSelected)
                        {
                            POTab.IsSelected = true;
                        }

                        e.Handled = true;

                        PurchaseDataGrid.Focus();
                        Keyboard.Focus(dgFocus.GetDataGridCell(PurchaseDataGrid.SelectedCells[0]));
                    }
                    else
                    {
                        e.Handled = true;
                        DG_GeneralJournal.Focus();
                        Keyboard.Focus(dgFocus.GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));

                        if (!GLTab.IsFocused)
                        {
                            GLTab.Focus();
                            DG_GeneralJournal.Focus();
                            Keyboard.Focus(dgFocus.GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }

        }

        private void TransDateTime_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (AllowEditDate)
            {
                if (App.CRUXAPI.isAllowed("update_journal"))
                {
                    AllowEditDate = false;
                }
            }

            e.Handled = AllowEditDate;
        }

        private void clientORTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                e.Handled = true;
                referenceNumberTextBox.Focus();
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal value1;
            if (decimal.TryParse(((TextBox)sender).Text, out value1))
            {
                string[] a = ((TextBox)sender).Text.Split(new char[] { '.' });
                int decimals = 0;
                if (a.Length == 2)
                {
                    decimals = a[1].Length;
                }
                else
                {
                    string newString = Regex.Replace(((TextBox)sender).Text, "[^0-9]", "");
                    ((TextBox)sender).Text = string.Format("{0:#,##0}", Double.Parse(newString));
                }
                if (decimals > 2)
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
                            ((TextBox)sender).Focus();
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
            }
            else
            {
                if (((TextBox)sender).Text != "")
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
            }
        }

        private void OtherComputationsMenu_Click(object sender, RoutedEventArgs e)
        {
            BMS bms = new BMS(this, "BMS", this.boh.TransDate);
            bms.Owner = this;
            bms.ShowDialog();
        }

        private void FlexiOnSavings_Click(object sender, RoutedEventArgs e)
        {
            flexSavings = new FlexiSavings(this);
            flexSavings.Owner = this;
            flexSavings.ShowDialog();
        }

        private void ProbationaryMenu_Click(object sender, RoutedEventArgs e)
        {
            BMS bms = new BMS(this, "DPM", this.boh.TransDate);
            bms.Owner = this;
            bms.ShowDialog();
        }

        private void LoanRecGenerateMenu_Click(object sender, RoutedEventArgs e)
        {
            LoanReclassification loanRec = new LoanReclassification(this, this.boh.TransDate, this.UPDTags.Find(u => u.UPDTagID == 1).UPDTagID);
            loanRec.Owner = this;
            loanRec.ShowDialog();
        }

        private void UnearnedInterestMenu_Click(object sender, RoutedEventArgs e)
        {
            UnearnedAmortization uAmort = new UnearnedAmortization(this, this.boh.TransDate);
            uAmort.Owner = this;
            uAmort.ShowDialog();
        }

        private void AverageShareCapitalGenerate_Click(object sender, RoutedEventArgs e)
        {
            AverageShareCapital avgShareCapital = new AverageShareCapital(this, this.boh.TransDate, "Average", this.documentDetail, PrintVerify());
            avgShareCapital.Owner = this;
            avgShareCapital.ShowDialog();
        }

        private void ComputeDividendAndPatronage_Click(object sender, RoutedEventArgs e)
        {
            AverageShareCapital avgShareCapital = new AverageShareCapital(this, this.boh.TransDate, "Compute", this.documentDetail, PrintVerify());
            avgShareCapital.Owner = this;
            avgShareCapital.ShowDialog();
        }

        private void Journalize_Click(object sender, RoutedEventArgs e)
        {
            AverageShareCapital avgShareCapital = new AverageShareCapital(this, this.boh.TransDate, "Journalize", this.documentDetail, PrintVerify());
            avgShareCapital.Owner = this;
            avgShareCapital.ShowDialog();
        }

        private void AnnualDuesMenu_Click(object sender, RoutedEventArgs e)
        {
            BMS bms = new BMS(this, "AD", this.boh.TransDate);
            bms.Owner = this;
            bms.ShowDialog();
        }

        private void YrMonthGenerateMenu_Click(object sender, RoutedEventArgs e)
        {
            ClosingYearMonth closingEntries = new ClosingYearMonth(this, this.boh.TransDate);
            closingEntries.Owner = this;
            closingEntries.ShowDialog();
        }

        private void DG_GeneralJournal_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = DG_GeneralJournal.SelectedItem as TransactionDetails;

                for (int i = 0; i <= DG_GeneralJournal.Columns.Count; i++)
                {
                    if (DG_GeneralJournal.CurrentColumn.DisplayIndex == i)
                    {
                        if (GetDataGridCell(DG_GeneralJournal.SelectedCells[i]).IsEditing)
                        {
                            e.Handled = true;
                        }
                    }
                }

            }
            catch (Exception)
            {
                return;
            }

        }

        private void PrintCheck()
        {
            Voucher printVoucher = new Voucher();

            printVoucher.BranchCode = this.DataCon.CurrentBranch;
            printVoucher.CTLNo = this.DataCon.ControlNo;
            printVoucher.TransactionCode = this.DataCon.TransactionCode;
            printVoucher.ClientID = this.DataCon.TransactionSummary.ClientID;
            printVoucher.IsVoucher = true;

            if (this.DataCon.TransactionCode != 0)
            {
                if (this.DataCon.TransactionCode == 22)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Confirm to Print the Check?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:

                            String parsed = new JavaScriptSerializer().Serialize(printVoucher);
                            App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Voucher", "CheckVoucher.exe", this, "Back_Office", parsed);
                            App.CRUXAPI.closeCallBack += openModule;
                            break;
                        case MessageBoxResult.No:
                            break;
                    }
                }
                else
                {
                    MessageBox.Show(this, "No Documents to be printed.");
                }
            }
            else
            {
                MessageBox.Show(this, "No Documents to be printed.");
            }
        }

        private void ORTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Tab)
                {
                    if (trType == "2" || trType == "PJ")
                    {
                        e.Handled = true;
                        clientIDTextBox.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void MortuaryMenu_Click(object sender, RoutedEventArgs e)
        {
            BMS bms = new BMS(this, "MO", this.boh.TransDate);
            bms.Owner = this;
            bms.ShowDialog();
        }

        private void AdvanceOptionsMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AdvanceOptions _advanceOptions = new AdvanceOptions(this);
                _advanceOptions.Owner = this;
                _advanceOptions.ShowDialog();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ServiceFeeMenu_Click(object sender, RoutedEventArgs e)
        {
            BMS bms = new BMS(this, "SE", this.boh.TransDate);
            bms.Owner = this;
            bms.ShowDialog();
        }

        private void UnbalanceTransactionMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (App.CRUXAPI.isAllowed("save_journal_with_diff"))
                {
                    AllowSaveWithDifference = true;
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void PrintVoucher()
        {
            Voucher printVoucher = new Voucher();

            printVoucher.BranchCode = this.DataCon.CurrentBranch;
            printVoucher.CTLNo = this.DataCon.ControlNo;
            printVoucher.TransactionCode = this.DataCon.TransactionCode;
            printVoucher.ClientID = this.DataCon.TransactionSummary.ClientID;
            printVoucher.IsVoucher = false;

            if (this.DataCon.TransactionCode != 0)
            {
                if (this.DataCon.TransactionCode == 21 || this.DataCon.TransactionCode == 22 || this.DataCon.TransactionCode == 16)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Confirm to Print the Voucher?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:

                            String parsed = new JavaScriptSerializer().Serialize(printVoucher);
                            App.CRUXAPI.openWindowDLL(new WindowInteropHelper(this).Handle, "Voucher", "CheckVoucher.exe", this, "Back_Office", parsed);
                            App.CRUXAPI.closeCallBack += openModule;

                            break;
                        case MessageBoxResult.No:

                            break;
                    }
                }
                else
                {
                    MessageBox.Show(this, "No Documents to be printed.");
                }
            }
            else
            {
                MessageBox.Show(this, "No Documents to be printed.");
            }
        }

        private void PrintVoucherMenu_Click(object sender, RoutedEventArgs e)
        {
            PrintVoucher();
        }

        private void DocumentMenu_Click(object sender, RoutedEventArgs e)
        {
            PrintDocument();
        }

        private void TransDateTime_GotFocus(object sender, RoutedEventArgs e)
        {
            TransDateTime.Background = System.Windows.Media.Brushes.Yellow; // WPF
        }

        private void ExplanationTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab && (Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
            {
                e.Handled = true;
                DG_GeneralJournal.SelectedIndex = 0;
                DG_GeneralJournal.Focus();
                Keyboard.Focus(dgFocus.GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));

                if (!GLTab.IsFocused)
                {
                    GLTab.Focus();
                    DG_GeneralJournal.SelectedIndex = 0;
                    DG_GeneralJournal.Focus();
                    Keyboard.Focus(dgFocus.GetDataGridCell(DG_GeneralJournal.SelectedCells[1]));
                }
            }
        }

        private void TransDateTime_LostFocus(object sender, RoutedEventArgs e)
        {
            TransDateTime.Background = System.Windows.Media.Brushes.White; // WPF
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void PurchaseDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                int col = PurchaseDataGrid.CurrentColumn.DisplayIndex;
                var selectedTransaction = PurchaseDataGrid.SelectedItem as Models.Purchase_Journal.PurchaseDetails;

                if (!PurchaseDataGrid.IsReadOnly)
                {
                    if (e.Key == Key.Enter || e.Key == Key.Space || e.Key == Key.Tab || e.Key == Key.F2 || e.Key == Key.F12 || e.Key == Key.Back ||
                           (e.Key == Key.OemPeriod || e.Key == Key.Decimal))
                    {
                        if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && col == 0 &&
                                    (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            if (e.Key == Key.Enter && GetDataGridCell(PurchaseDataGrid.SelectedCells[0]).IsFocused)
                            {
                                Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[2]));
                                e.Handled = true;
                            }
                            else if (e.Key != Key.Space && (selectedTransaction.AccountCode != 0) && e.Key != Key.Back)
                            {
                                TempFillAccountCodeGrid(selectedTransaction.AccountCode);

                                e.Handled = true;
                            }

                            else if (e.Key == Key.Enter && selectedTransaction.AccountCode != 0)
                            {
                                Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[2]));
                                e.Handled = true;
                            }

                            else if (e.Key == Key.Back && GetDataGridCell(PurchaseDataGrid.SelectedCells[0]).IsFocused)
                            {
                                //if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                //{
                                //    selectedTransaction.AccountCode = 0;
                                //    selectedTransaction.Debit = 0;
                                //    selectedTransaction.Credit = 0;
                                //    selectedTransaction.accountDesc = String.Empty;
                                //    selectedTransaction.ClientID = 0;
                                //    selectedTransaction.ClientIDFormat = String.Empty;
                                //    selectedTransaction.clientName = String.Empty;
                                //    selectedTransaction.ReferenceNo = String.Empty;

                                //    selectedTransaction.AdjFlag = 0;
                                //    selectedTransaction.AdjFlagCode = String.Empty;

                                //    //CalculateTotalDebitCredit(0, 0);
                                //    CalculateTotalPrice();
                                //}
                            }
                            else if (e.Key != Key.Back && e.Key != Key.Enter)
                            {
                                DG_GeneralJournal.BeginEdit();

                                glAccount = new GLAccount(this.GLAccounts, this, true);
                                glAccount.Owner = this;
                                glAccount.ShowDialog();
                                e.Handled = true;
                                AutoPopulateGL(selectedTransaction);
                            }
                        }
                        else if ((e.Key == Key.Space || e.Key == Key.Enter || e.Key == Key.Back) && col == 3 &&
                                    (e.Key != Key.OemPeriod || e.Key != Key.Decimal))
                        {
                            if (e.Key == Key.Enter && GetDataGridCell(PurchaseDataGrid.SelectedCells[3]).IsFocused)
                            {
                                Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[3]));
                                e.Handled = true;
                            }

                            else if (e.Key == Key.Back && GetDataGridCell(PurchaseDataGrid.SelectedCells[4]).IsFocused)
                            {
                                //if (!String.IsNullOrEmpty(selectedTransaction.SLC_Code))
                                //{
                                //    selectedTransaction.AccountCode = 0;
                                //    selectedTransaction.Debit = 0;
                                //    selectedTransaction.Credit = 0;
                                //    selectedTransaction.accountDesc = String.Empty;
                                //    selectedTransaction.ClientID = 0;
                                //    selectedTransaction.ClientIDFormat = String.Empty;
                                //    selectedTransaction.clientName = String.Empty;
                                //    selectedTransaction.ReferenceNo = String.Empty;

                                //    selectedTransaction.AdjFlag = 0;
                                //    selectedTransaction.AdjFlagCode = String.Empty;

                                //    //CalculateTotalDebitCredit(0, 0);
                                //    CalculateTotalPrice();
                                //}
                            }
                            else if (e.Key != Key.Back && e.Key != Key.Enter)
                            {
                                if (this.DataCon.PurchaseSummary.VendorID != 0)
                                {
                                    _VendorTIN = new VendorTIN(this, this.DataCon.PurchaseSummary.VendorID);
                                    _VendorTIN.Owner = this;
                                    _VendorTIN.ShowDialog();
                                    e.Handled = true;
                                }
                                else
                                {
                                    MessageBox.Show(this, "Select vendor to continue. ", "Vendor is required", MessageBoxButton.OK, MessageBoxImage.Error);
                                    clientIDTextBox.Focus();
                                    e.Handled = true;
                                }
                            }
                        } // end col = 3
                        else if (col == 5 && (e.Key != Key.Tab || e.Key == Key.Back))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(PurchaseDataGrid.SelectedCells[5]).IsFocused)
                                {
                                    e.Handled = true;

                                    if (selectedTransaction.ItemPrice != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[6]));
                                    }
                                }
                                else if (e.Key == Key.Enter && !GetDataGridCell(PurchaseDataGrid.SelectedCells[5]).IsFocused)
                                {
                                    Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[9]));

                                    if (e.Key == Key.Enter && selectedTransaction.VendorDiscountRate == 0)
                                    {
                                        selectedTransaction.ItemPrice = 0;
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[1]));

                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[5]));
                                        PurchaseDataGrid.BeginEdit();
                                    }
                                    else if (e.Key == Key.Enter && selectedTransaction.VendorDiscountRate != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[6]));
                                        e.Handled = true;
                                    }
                                }

                                if (e.Key == Key.Space)
                                {
                                    e.Handled = true;
                                }

                                if (e.Key == Key.Back)
                                {
                                    if (selectedTransaction.VendorDiscountRate != 0)
                                    {
                                        selectedTransaction.VendorDiscountRate = 0;
                                    }
                                }
                            }

                            catch (Exception)
                            {
                                e.Handled = true;
                            }
                        } //end col 6
                        else if (col == 6 && (e.Key != Key.Tab || e.Key == Key.Back))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(PurchaseDataGrid.SelectedCells[6]).IsFocused)
                                {
                                    e.Handled = true;

                                    if (selectedTransaction.ItemPrice != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[7]));
                                    }
                                }
                                else if (e.Key == Key.Enter && !GetDataGridCell(PurchaseDataGrid.SelectedCells[6]).IsFocused)
                                {
                                    Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[9]));

                                    if (e.Key == Key.Enter && selectedTransaction.ItemPrice == 0)
                                    {
                                        selectedTransaction.ItemPrice = 0;
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[1]));

                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[6]));
                                        PurchaseDataGrid.BeginEdit();
                                    }
                                    else if (e.Key == Key.Enter && selectedTransaction.ItemPrice != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[7]));
                                        e.Handled = true;
                                    }
                                }

                                if (e.Key == Key.Space)
                                {
                                    e.Handled = true;
                                }

                                if (e.Key == Key.Back)
                                {
                                    if (selectedTransaction.ItemPrice != 0)
                                    {
                                        selectedTransaction.ItemPrice = 0;
                                    }
                                }

                                // CalculateTotalPrice();
                            }

                            catch (Exception)
                            {
                                e.Handled = true;
                            }
                        } //end col 6
                        else if (col == 7 && (e.Key != Key.Tab || e.Key == Key.Back))
                        {
                            try
                            {
                                if (e.Key == Key.Enter && GetDataGridCell(PurchaseDataGrid.SelectedCells[7]).IsFocused)
                                {
                                    e.Handled = true;

                                    if (selectedTransaction.ItemPrice != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[7]));
                                    }
                                }
                                else if (e.Key == Key.Enter && !GetDataGridCell(PurchaseDataGrid.SelectedCells[7]).IsFocused)
                                {
                                    Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[11]));

                                    if (e.Key == Key.Enter && selectedTransaction.ItemPrice == 0)
                                    {
                                        selectedTransaction.ItemPrice = 0;
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[1]));

                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[7]));
                                        PurchaseDataGrid.BeginEdit();
                                    }
                                    else if (e.Key == Key.Enter && selectedTransaction.ItemPrice != 0)
                                    {
                                        Keyboard.Focus(GetDataGridCell(PurchaseDataGrid.SelectedCells[11]));
                                        e.Handled = true;

                                        CalculationValues calculated = CalculatePJ(selectedTransaction);
                                        selectedTransaction.GrossIncome = calculated.Gross;
                                        selectedTransaction.VAT = calculated.GrossAfterVAT;
                                        selectedTransaction.Discount = calculated.GrossAfterDiscount;
                                        selectedTransaction.NetAmt = calculated.Net;

                                        PopulateAmt(selectedTransaction);
                                        CalculateNetAmount();
                                    }
                                }

                                if (e.Key == Key.Space)
                                {
                                    e.Handled = true;
                                }

                                if (e.Key == Key.Back)
                                {
                                    if (selectedTransaction.ItemPrice != 0)
                                    {
                                        selectedTransaction.ItemPrice = 0;
                                    }
                                }

                                // CalculateTotalPrice();
                            }

                            catch (Exception)
                            {
                                e.Handled = true;
                            }
                        } //end col 7
                    }
                    else
                    {
                        if (e.Key == Key.Insert)
                        {
                            e.Handled = true;
                            var index = PurchaseDataGrid.Items.IndexOf(PurchaseDataGrid.SelectedItem) + 1;

                            if (this.DataCon.ControlNo == 0)
                            {
                                TempPurchaseDetails.Insert(index, new PurchaseDetails()
                                {
                                    TransDate = this.DataCon.DateTimeNow
                                });

                                PurchaseDataGrid.ItemsSource = TempPurchaseDetails;
                                DG_PurchaseRefresh();
                                MoveFocusToNewRowPOGrid();
                            }
                            else
                            {
                                PurchaseDetails selected = new PurchaseDetails();
                                selected.TransDate = this.DataCon.DateTimeNow;

                                TempPurchaseDetails.Insert(index, selected);
                                DG_PurchaseRefresh();
                                MoveFocusToNewRowPOGrid();
                            }
                        }
                    }

                } //end if read Only
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }
        }

        private void PopulateAmt(PurchaseDetails selected)
        {
            try
            {
                var amtAcct = TempTransactionDetailsList.FindAll(c => c.AccountCode == selected.AccountCode).ToList();

                if (amtAcct.Count != 0)
                {
                    foreach (TransactionDetails detail in amtAcct)
                    {
                        if (detail.Debit != 0 || detail.Credit != 0)
                        {
                            detail.Debit += selected.NetAmt;
                        }
                        else
                        {
                            detail.Debit = selected.NetAmt;
                        }

                        CalculateTotalPrice();
                    }
                }
            }
            catch (Exception)
            {
                return;
            }

        }

        private CalculationValues CalculatePJ(PurchaseDetails selected)
        {
            CalculationValues calculate = new CalculationValues();

            try
            {
                calculate.Gross = (selected.ItemPrice * selected.Qty);
                calculate.GrossAfterDiscount = Convert.ToDecimal((calculate.Gross * (selected.VendorDiscountRate / 100)).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")));
                calculate.GrossAfterVAT = Convert.ToDecimal(((calculate.Gross - calculate.GrossAfterDiscount) * (selected.VendorVatRate / 100)).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")));
                calculate.Net = Convert.ToDecimal((calculate.Gross - calculate.GrossAfterVAT).ToString("N2", CultureInfo.CreateSpecificCulture("en-US")));

                return calculate;
            }
            catch (Exception)
            {
                return calculate;
            }
        }

        private void PurchaseDataGrid_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                //var selectedTransDetails = PurchaseDataGrid.SelectedItem as PurchaseDetails;

                //if (PurchaseDataGrid.CurrentColumn.DisplayIndex == 0)
                //{
                //    AutoPopulateGL(selectedTransDetails);
                //}
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void AutoPopulateGL(PurchaseDetails selected)
        {

            if (TempTransactionDetailsList.Count > 0)
            {
                var acct = TempTransactionDetailsList.FindAll(t => t.AccountCode == selected.AccountCode).ToList();

                TransactionDetails tt = (TransactionDetails)DG_GeneralJournal.SelectedItem;

                if (acct.Count != 0)
                {

                }
                else
                {
                    if (tt.AccountCode != 0)
                    {
                        TempTransactionDetailsList.Add(new Models.TransactionDetails()
                        {
                            AccountCode = selected.AccountCode,
                            accountDesc = selected.AccountDesc
                        });

                        DG_GeneralJournal.ItemsSource = TempTransactionDetailsList;
                        DG_JournalRefresh();
                    }
                    else
                    {
                        tt.AccountCode = selected.AccountCode;
                        tt.accountDesc = selected.AccountDesc;
                    }
                }
            }


        }

        private class DataGridViewHelper
        {

            static public DataGridCell GetCell(DataGrid dg, int row, int column)
            {
                DataGridRow rowContainer = GetRow(dg, row);
                if (rowContainer != null)
                {
                    DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);
                    if (presenter != null)
                    {
                        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);  // <<<------ ERROR
                        if (cell == null)
                        {
                            // now try to bring into view and retreive the cell
                            dg.ScrollIntoView(rowContainer, dg.Columns[column]);
                            cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                        }
                        return cell;
                    }
                    else
                    {

                        dg.Items.MoveCurrentToPosition(row);
                        var index = dg.SelectedIndex;
                        DataGridRow tmprow = dg.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
                        DataGridCellInfo cellInfo = new DataGridCellInfo(tmprow, dg.Columns[0]);
                        var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);
                        if (cellContent != null)
                        {
                            return (DataGridCell)cellContent.Parent;
                        }
                        return null;
                    }
                }
                return null;
            }

            static public DataGridRow GetRow(DataGrid dg, int index)
            {
                DataGridRow row = (DataGridRow)dg.ItemContainerGenerator.ContainerFromIndex(index);
                if (row == null)
                {
                    Console.WriteLine("is null");
                    // may be virtualized, bring into view and try again
                    //dg.ScrollIntoView(dg.Items[index]);
                    row = (DataGridRow)dg.ItemContainerGenerator.ContainerFromIndex(index);
                }
                if (row == null)
                {
                    Console.WriteLine("is null again");
                    // may be virtualized, bring into view and try again
                    //dg.ScrollIntoView(dg.Items[index]);
                    row = (DataGridRow)dg.CurrentItem;
                }
                return row;
            }

            static T GetVisualChild<T>(Visual parent) where T : Visual
            {
                T child = default(T);
                int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < numVisuals; i++)
                {
                    Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                    child = v as T;
                    if (child == null)
                    {
                        child = GetVisualChild<T>(v);
                    }
                    if (child != null)
                    {
                        break;
                    }
                }
                return child;
            }

            public static int GetRowIndex(DataGridCell dataGridCell)
            {
                // Use reflection to get DataGridCell.RowDataItem property value.
                PropertyInfo rowDataItemProperty = dataGridCell.GetType().GetProperty("RowDataItem", BindingFlags.Instance | BindingFlags.NonPublic);

                DataGrid dataGrid = GetDataGridFromChild(dataGridCell);

                return dataGrid.Items.IndexOf(rowDataItemProperty.GetValue(dataGridCell, null));
            }
            public static DataGrid GetDataGridFromChild(DependencyObject dataGridPart)
            {
                if (VisualTreeHelper.GetParent(dataGridPart) == null)
                {
                    throw new NullReferenceException("Control is null.");
                }
                if (VisualTreeHelper.GetParent(dataGridPart) is DataGrid)
                {
                    return (DataGrid)VisualTreeHelper.GetParent(dataGridPart);
                }
                else
                {
                    return GetDataGridFromChild(VisualTreeHelper.GetParent(dataGridPart));
                }
            }
        }

        public class BackOfficeDataCon : INotifyPropertyChanged
        {
            Boolean _ClosedStatus;
            public Boolean ClosedStatus
            {
                get { return _ClosedStatus; }
                set
                {
                    _ClosedStatus = value;
                    OnPropertyChanged("ClosedStatus");
                }
            }

            Decimal _TotalCOCI;
            public Decimal TotalCOCI
            {
                get { return _TotalCOCI; }
                set
                {
                    _TotalCOCI = value;
                    OnPropertyChanged("TotalCOCI");
                }
            }

            Decimal _TotalDifference;
            public Decimal TotalDifference
            {
                get { return _TotalDifference; }
                set
                {
                    _TotalDifference = value;
                    OnPropertyChanged("TotalDifference");
                }
            }

            Decimal _TempTotalDebit;
            public Decimal TempTotalDebit
            {
                get { return _TempTotalDebit; }
                set
                {
                    _TempTotalDebit = value;
                    OnPropertyChanged("TempTotalDebit");
                }
            }

            Decimal _TotalDebit;
            public Decimal TotalDebit
            {
                get { return _TotalDebit; }
                set
                {
                    _TotalDebit = value;
                    OnPropertyChanged("TotalDebit");
                }
            }

            Decimal _TempTotalCredit;
            public Decimal TempTotalCredit
            {
                get { return _TempTotalCredit; }
                set
                {
                    _TempTotalCredit = value;
                    OnPropertyChanged("TempTotalCredit");
                }
            }

            Decimal _TotalCredit;
            public Decimal TotalCredit
            {
                get { return _TotalCredit; }
                set
                {
                    _TotalCredit = value;
                    OnPropertyChanged("TotalCredit");
                }
            }

            Decimal _NetTotal;
            public Decimal NetTotal
            {
                get { return _NetTotal; }
                set
                {
                    _NetTotal = value;
                    OnPropertyChanged("NetTotal");
                }
            }

            Boolean _InsertOrUpdated;
            public Boolean InsertOrUpdated
            {
                get { return _InsertOrUpdated; }
                set
                {
                    _InsertOrUpdated = value;
                    OnPropertyChanged("InsertOrUpdated");
                }
            }

            String _Title;
            public String Title
            {
                get { return _Title; }
                set
                {
                    _Title = value;
                    OnPropertyChanged("Title");
                }
            }

            String _Response;
            public String Response
            {
                get { return _Response; }
                set
                {
                    _Response = value;
                    OnPropertyChanged("Response");
                }
            }

            String _ResponseContent;
            public String ResponseContent
            {
                get { return _ResponseContent; }
                set
                {
                    _ResponseContent = value;
                    OnPropertyChanged("ResponseContent");
                }
            }

            String _ReportType;
            public String ReportType
            {
                get { return _ReportType; }
                set
                {
                    _ReportType = value;
                    OnPropertyChanged("ReportType");
                }
            }

            UPDTrans _UPDTrans;
            public UPDTrans UPDTrans
            {
                get { return _UPDTrans; }
                set
                {
                    _UPDTrans = value;
                    OnPropertyChanged("UPDTrans");
                }
            }

            String _Upd;
            public String Upd
            {
                get { return _Upd; }
                set
                {
                    _Upd = value;
                    OnPropertyChanged("Upd");
                }
            }

            String _TransYear;
            public String TransYear
            {
                get { return _TransYear; }
                set
                {
                    _TransYear = value;
                    OnPropertyChanged("TransYear");
                }
            }

            Int64 _CurrentBranch;
            public Int64 CurrentBranch
            {
                get { return _CurrentBranch; }
                set
                {
                    _CurrentBranch = value;
                    OnPropertyChanged("CurrentBranch");
                }
            }

            String _DateTimeNow;
            public String DateTimeNow
            {
                get { return this.DateFormat(_DateTimeNow); }
                set
                {
                    _DateTimeNow = value;
                    OnPropertyChanged("DateTimeNow");
                }
            }

            private String DateFormat(String Date)
            {
                if (Date == null)
                {
                    Date = "";
                }

                String temp = Regex.Replace(Date, "[^0-9.]", "");
                if (temp.Length >= 8)
                {
                    temp = temp.Substring(0, 8);
                }
                else
                {
                    int underscore = 8 - temp.Length;

                    while (underscore > 0)
                    {
                        temp += "_";
                        underscore--;
                    }
                }
                temp = temp.Insert(2, "/");
                temp = temp.Insert(5, "/");

                return temp;
            }

            Int16 _TransactionCode;
            public Int16 TransactionCode
            {
                get { return _TransactionCode; }
                set
                {
                    _TransactionCode = value;
                    OnPropertyChanged("TransactionCode");
                }
            }

            Int64 _ControlNo;
            public Int64 ControlNo
            {
                get { return _ControlNo; }
                set
                {
                    _ControlNo = value;
                    OnPropertyChanged("ControlNo");
                }
            }

            Int64 _ControlNew;
            public Int64 ControlNew
            {
                get { return _ControlNew; }
                set
                {
                    _ControlNew = value;
                    OnPropertyChanged("ControlNew");
                }
            }

            String _ReversalExplanation;
            public String ReversalExplanation
            {
                get { return _ReversalExplanation; }
                set
                {
                    _ReversalExplanation = value;
                    OnPropertyChanged("ReversalExplanation");
                }
            }

            List<GLTransaction> _GLTransactionList;
            public List<GLTransaction> GLTransactionList
            {
                get { return _GLTransactionList; }
                set
                {
                    _GLTransactionList = value;
                    OnPropertyChanged("GLTransactionList");
                }
            }

            List<TransactionDetails> _TransactionDetails;
            public List<TransactionDetails> TransactionDetails
            {
                get { return _TransactionDetails; }
                set
                {
                    _TransactionDetails = value;
                    OnPropertyChanged("TransactionDetails");
                }
            }

            List<TransactionDetails> _DeletedTransactionDetails;
            public List<TransactionDetails> DeletedTransactionDetails
            {
                get { return _DeletedTransactionDetails; }
                set
                {
                    _DeletedTransactionDetails = value;
                    OnPropertyChanged("DeletedTransactionDetails");
                }
            }

            TransactionSummary _TransactionSummary;
            public TransactionSummary TransactionSummary
            {
                get { return _TransactionSummary; }
                set
                {
                    _TransactionSummary = value;
                    OnPropertyChanged("TransactionSummary");
                }
            }

            ObservableCollection<TransactionCheck> _TransactionChecks;
            public ObservableCollection<TransactionCheck> TransactionChecks
            {
                get { return _TransactionChecks; }
                set
                {
                    _TransactionChecks = value;
                    OnPropertyChanged("TransactionChecks");
                }
            }

            String _PreparedBy;
            public String PreparedBy
            {
                get { return _PreparedBy; }
                set
                {
                    _PreparedBy = value;
                    OnPropertyChanged("PreparedBy");
                }
            }

            String _ClientNameSearch;
            public String ClientNameSearch
            {
                get { return _ClientNameSearch; }
                set
                {
                    _ClientNameSearch = value;
                    OnPropertyChanged("ClientNameSearch");
                }
            }

            Int64 _ClientIDSearch;
            public Int64 ClientIDSearch
            {
                get { return _ClientIDSearch; }
                set
                {
                    _ClientIDSearch = value;
                    OnPropertyChanged("ClientIDSearch");
                }
            }

            TransactionFilterParams _TransactionFilterParams;
            public TransactionFilterParams TransParameters
            {
                get { return _TransactionFilterParams; }
                set
                {
                    _TransactionFilterParams = value;
                    OnPropertyChanged("TransParameters");
                }
            }

            List<PurchaseDetails> _PurchaseDetails;
            public List<PurchaseDetails> PurchaseDetails
            {
                get { return _PurchaseDetails; }
                set
                {
                    _PurchaseDetails = value;
                    OnPropertyChanged("PurchaseDetails");
                }
            }

            InsertPurchaseSummary _PurchaseSummary;
            public InsertPurchaseSummary PurchaseSummary
            {
                get { return _PurchaseSummary; }
                set
                {
                    _PurchaseSummary = value;
                    OnPropertyChanged("PurchaseSummary");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
                }
            }
        }
    }
}

