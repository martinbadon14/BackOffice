﻿using Back_Office.Models.Month_End.Loan_Reclassification;
using Back_Office.Models.Reports;
using Back_Office.Models.Year_End;
using Back_Office.Utilities;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.Year_End
{
    /// <summary>
    /// Interaction logic for AverageShareCapital.xaml
    /// </summary>
    public partial class AverageShareCapital : Window
    {
        MainWindow mww;
        private static Byte AvgShareCapital = 1;
        private static Byte DividendAndPatronage = 2;
        private static Byte Journalize = 3;
        Byte ActiveTab = 0;
        String DateTimeNow = "";

        AvgShareCapDataCon DataCon = new AvgShareCapDataCon();
        private readonly BackgroundWorker BasicWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateLoanDuesWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateJournalizeWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateControlListWorker = new BackgroundWorker();
        private static String AvgShareCapitalTitle = "Avg. Share Capital and Interest Paid";
        private static String DividendPatronageTitle = "Dividend And Patronage Refund";
        private static String ComputeAvgShareCapAndIPTitle = "Computation of Average Share Capital and Interest Paid";
        private static String AllocationOfShareCapAndIntPaidTitle = "Allocation of Int on Share Capital and Patronage Refund";
        private static String JournalizeTitle = "Journalize Dividend and Patronage Refund";

        ReportDocument Report = new ReportDocument();
        ReportDocument Export = new ReportDocument();
        CrystalReport Viewer;
        BOHeader boh = new BOHeader();
        StreamReader resLoanDues;
        DocumentDetails _documentDetail = new DocumentDetails();
        PrintVerify _verify = new PrintVerify();

        public AverageShareCapital(MainWindow mw, String DateTimeNow, String Type, DocumentDetails documentDetail, PrintVerify verification)
        {
            InitializeComponent();
            InitializeDefaults();
            _documentDetail = documentDetail;
            _verify = verification;
            this.DateTimeNow = DateTimeNow;
            InitializeWorkers();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.DataCon.TransDate = DateTimeNow;
            this.DataContext = this.DataCon;

            if (Type == "Average")
            {
                AvgShareCapTab.IsSelected = true;
                FromDatePicker.Focus();
                this.Title = ComputeAvgShareCapAndIPTitle;
            }
            else if (Type == "Compute")
            {
                this.Title = AllocationOfShareCapAndIntPaidTitle;
                DivAndPatronageTab.IsSelected = true;
                IntShareCapitalTextBox.Focus();
                //GenerateLoanDuesWorker.RunWorkerAsync();
            }
            else if (Type == "Journalize")
            {
                this.Title = JournalizeTitle;
                JournalizeTab.IsSelected = true;
                TransDatePicker.Focus();
                GenerateLoanDuesWorker.RunWorkerAsync();
            }

        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    MessageBoxResult result = MessageBox.Show("Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            Close();
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;

                    } //end switch

                } //end If Key = Escape

                else if (e.Key == Key.F11 || (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G)))
                {
                    if (generateButton.IsEnabled)
                    {
                        Generate();
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }


        }


        private void InitializeDefaults()
        {
            generateButton.IsEnabled = false;

            this.DataCon.Explanation = "";
            this.DataCon.PreparedBy = "";
            this.DataCon.DateNow = "";

            this.DataCon.FromShareCapitalBal = 1800;
            this.DataCon.ToShareCapitalBal = "999,999,999,999,999.99";
            this.DataCon.FromAvgShareCapital = Convert.ToDecimal(0.01);
            this.DataCon.ToAvgShareCapital = "999,999,999,999,999.99";

            this.DataCon.TotalAverageShareCapital = "";
            this.DataCon.TotalIntPaid = "";

            this.DataCon.progress = "";
            this.DataCon.processing = "";

            DataCon.Select = new Selections();
            DataCon.Selected = new Selections();

            this.DataCon.Select.SLT = new List<Models.SLType>();
            DataCon.Selected.SLT = new List<Models.SLType>();
            this.DataCon.AvgControlList = new List<AvgShareCapParameters>();
            this.DataCon.ComputedAvgShareCapList = new List<ComputedAvgShareCapital>();

            //journalize 
            this.DataCon.PatronageAcctCode = "205.08";
            this.DataCon.DividendAcctCode = "205.07";
            this.DataCon.TransDate = "";

            ListBoxUtility SLCUtil = new ListBoxUtility();

            SLCUtil.selectTo(slclist, slcselectlist, null, false, true);
            SLCUtil.selectToCallBack += new ListBoxUtility.CallbackEventHandler(selectToCallBack);
            SLCUtil.selectFromCallBack += new ListBoxUtility.CallbackEventHandler(selectFromCallBack);

            slclist.PreviewKeyDown += new KeyEventHandler(HandleSLCTo);
            slcselectlist.PreviewKeyDown += new KeyEventHandler(HandleSLCFrom);
        }


        private void InitializeWorkers()
        {
            BasicWorker.DoWork += BasicWorker_DoWork;
            BasicWorker.RunWorkerCompleted += BasicWorker_RunWorkerCompleted;
            BasicWorker.RunWorkerAsync();

            GenerateWorker.DoWork += GenerateWorker_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateWorker_RunWorkerCompleted;

            GenerateControlListWorker.DoWork += GenerateControlListWorker_DoWork;
            GenerateControlListWorker.RunWorkerCompleted += GenerateControlListWorker_RunWorkerCompleted;

            GenerateLoanDuesWorker.DoWork += GenerateLoanDuesWorker_DoWork;
            GenerateLoanDuesWorker.RunWorkerCompleted += GenerateLoanDuesWorker_RunWorkerCompleted;

            GenerateJournalizeWorker.DoWork += GenerateJournalizeWorker_DoWork;
            GenerateJournalizeWorker.RunWorkerCompleted += GenerateJournalizeWorker_RunWorkerCompleted;

        }

        private void GenerateFinalJournalizeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            this.Dispatcher.BeginInvoke((Action)(() =>
            {
                MessageBox.Show(this, "Module Was Successfully Added!", "Successfully Completed!", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            }), System.Windows.Threading.DispatcherPriority.Background);
        }


        private void GenerateJournalizeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (AfterLoanDues)
            {
                case 1:
                    GenerateWorker.RunWorkerAsync();
                    break;

                default:
                    break;
            }

        }

        Byte AfterLoanDues = 0;
        private void GenerateJournalizeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true;
            while (pass)
            {
                if (!GenerateLoanDuesWorker.IsBusy)
                {
                    try
                    {
                        AfterLoanDues = 1;
                        pass = false;
                    }
                    catch (Exception)
                    {

                    }
                }

                Thread.Sleep(50);
            } // end while
        }


        private void GenerateLoanDuesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GenerateLoanDuesWorker.WorkerReportsProgress = false;
        }


        private void GenerateLoanDuesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                LoansRequestParameters param = new LoansRequestParameters();

                param.Cutoff = this.DateTimeNow;
                param.ClientID = "";
                param.Company = "";

                var json = new JavaScriptSerializer().Serialize(param);
                resLoanDues = App.CRUXAPI.requestBIG("loanreports/streamdues", json, 7200000);

                int i = 0;
                int count = 0;
                DateTime starttime = DateTime.Now;

                while (i <= count)
                {
                    try
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        serializer.MaxJsonLength = 999999999;
                        string tmp = resLoanDues.ReadLine();
                        string tmp2 = tmp.Replace("\\\"", "\"");
                        string tmp3 = tmp2.Remove(tmp2.Length - 1);
                        //Console.WriteLine(i);

                        if (i == 0)
                        {
                            count = Convert.ToInt32(tmp);
                            Console.WriteLine(tmp);
                        }
                        else
                        {
                            this.progressPercent(i, count, starttime);
                        }

                        i++;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Thread.Sleep(500);
        }


        private void InsertControlList()
        {
            StreamReader res;

            String parsed = new JavaScriptSerializer().Serialize(this.DataCon.AvgControlList);
            res = App.CRUXAPI.requestBIG("backoffice/InsertAvgShareCapitalControlListStream", parsed, int.MaxValue);

            int i = 0;
            int count = 0;
            DateTime starttime = DateTime.Now;
            while (i <= count)
            {

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    string tmp = res.ReadLine();
                    string tmp2 = tmp.Replace("\\\"", "\"");
                    string tmp3 = tmp2.Remove(tmp2.Length - 1);

                    if (i == 0)
                    {
                        count = Convert.ToInt32(tmp);
                        Console.WriteLine(tmp);
                        i++;
                    }
                    else
                    {

                        ClientInfoAVG Avgrow = serializer.Deserialize<ClientInfoAVG>(tmp3.Substring(1, tmp3.Length - 1));

                        if (i <= count)
                        {
                            this.DataCon.processing = "Processing " + i + " of " + count + " AccountID: " + Avgrow.ClientIDFormat + " Account Name: " + Avgrow.ClientName;
                            this.progressPercent(i, count, starttime);
                        }
                        else
                        {
                            this.DataCon.processing = "Loading... ";
                        }

                        i++;

                    }

                }
                catch (Exception ex)
                {

                }

            }

            Thread.Sleep(300);
        }


        private void UpdateControlList()
        {
            StreamReader res;

            try
            {
                String parsed = new JavaScriptSerializer().Serialize(this.DataCon.ComputedAvgShareCapList);
                res = App.CRUXAPI.requestBIG("backoffice/UpdateDividenAndPatronageControlListStream", parsed, int.MaxValue);

                int i = 0;
                int count = 0;
                DateTime starttime = DateTime.Now;
                while (i <= count)
                {

                    try
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        serializer.MaxJsonLength = 999999999;
                        string tmp = res.ReadLine();
                        string tmp2 = tmp.Replace("\\\"", "\"");
                        string tmp3 = tmp2.Remove(tmp2.Length - 1);

                        if (i == 0)
                        {
                            count = Convert.ToInt32(tmp);
                            Console.WriteLine(tmp);
                            i++;
                        }
                        else
                        {

                            AvgShareCapParameters Avgrow = serializer.Deserialize<AvgShareCapParameters>(tmp3.Substring(1, tmp3.Length - 1));

                            if (i <= count)
                            {
                                this.DataCon.processing = "Processing " + i + " of " + count + " AccountID: " + Avgrow.ClientIDFormat + " Account Name: " + Avgrow.ClientName;
                                this.progressPercent(i, count, starttime);

                                this.DataCon.AvgControlList.Add(Avgrow);
                            }
                            else
                            {
                                this.DataCon.processing = "Loading Report";
                            }

                            i++;

                        }

                    }
                    catch (Exception ex)
                    {

                    }

                }

                Thread.Sleep(300);
            }
            catch
            {

            }
        }


        private void GenerateControlListWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            EnableButtons();
            HideProgressBar();

            Viewer = new CrystalReport();
            Viewer.cryRpt = Export;
            Viewer._CrystalReport.ViewerCore.ReportSource = Report;
            Viewer._CrystalReport.ViewerCore.Zoom(130);
            Viewer.Owner = this;
            Viewer.ShowDialog();

            if (AvgShareCapital == ActiveTab)
            {
                this.Close();
            }
            else if (DividendAndPatronage == ActiveTab)
            {
                processBlock.Visibility = Visibility.Hidden;
                GenerateLoanDuesWorker.RunWorkerAsync();
                JournalizeTab.IsSelected = true;
            }
        }


        private void GenerateControlListWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                String Title = "";
                if (AvgShareCapital == ActiveTab)
                {
                    InsertControlList();

                    Title = AvgShareCapitalTitle;
                }
                else if (DividendAndPatronage == ActiveTab)
                {
                    Title = DividendPatronageTitle;
                    UpdateControlList();
                }

                Report.SetDataSource(this.DataCon.AvgControlList.OrderBy(c => c.ClientName));
                Report.SetParameterValue("FromDate", this.DataCon.FromDate);
                Report.SetParameterValue("ToDate", this.DataCon.ToDate);
                Report.SetParameterValue("OfficeName", this.boh.OfficeName);
                Report.SetParameterValue("OfficeAddress", this.boh.OfficeAddress);
                Report.SetParameterValue("PreparedByName", this.DataCon.PreparedBy);
                Report.SetParameterValue("Explanation", this.DataCon.AvgControlList.First().Explanation);

                Export.SetDataSource(this.DataCon.AvgControlList.OrderBy(c => c.ClientName));
                Export.SetParameterValue("OfficeName", this.boh.OfficeName);
                Export.SetParameterValue("OfficeAddress", this.boh.OfficeAddress);
                Export.SetParameterValue("PreparedByName", this.DataCon.PreparedBy);
                Export.SetParameterValue("FromDate", this.DataCon.FromDate);
                Export.SetParameterValue("ToDate", this.DataCon.ToDate);
                Export.SetParameterValue("Explanation", this.DataCon.AvgControlList.First().Explanation);

                Report.SetParameterValue("Title", Title);
                Export.SetParameterValue("Title", Title);

                Report.SetParameterValue("GenerateDetails", "Generated by: " + _verify.Username + " ON Computer:" + _verify.ComputerName + ": " + _verify.IPAddress);
                Export.SetParameterValue("GenerateDetails", "Generated by: " + _verify.Username + " ON Computer:" + _verify.ComputerName + ": " + _verify.IPAddress);

                Report.SetParameterValue("Verification", "Verification Code: " + _verify.VerificationCode);
                Export.SetParameterValue("Verification", "Verification Code: " + _verify.VerificationCode);

                Report.SetParameterValue("LetterHead", this._documentDetail.LetterHead);
                Export.SetParameterValue("LetterHead", this._documentDetail.LetterHead);

                Report.SetParameterValue("Header1", this._documentDetail.Header1);
                Export.SetParameterValue("Header1", this._documentDetail.Header1);

                Report.SetParameterValue("Header2", this._documentDetail.Header2);
                Export.SetParameterValue("Header2", this._documentDetail.Header2);

                Report.SetParameterValue("Footer1", this._documentDetail.Footer1);
                Export.SetParameterValue("Footer1", this._documentDetail.Footer1);

                Report.SetParameterValue("Footer2", this._documentDetail.Footer2);
                Export.SetParameterValue("Footer2", this._documentDetail.Footer2);

                Report.SetParameterValue("Footer3", this._documentDetail.Footer3);
                Export.SetParameterValue("Footer3", this._documentDetail.Footer3);

                Report.SetParameterValue("Footer4", this._documentDetail.Footer4);
                Export.SetParameterValue("Footer4", this._documentDetail.Footer4);
            }
            catch (Exception ex)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(this, ex.Message);
                }), System.Windows.Threading.DispatcherPriority.Background);

            }
        }


        private void GenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (generateAvg)
            {
                case 1:
                    try
                    {
                        MessageBoxResult result = MessageBox.Show(this, "Do you want to generate Control List for Printing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                        switch (result)
                        {
                            case MessageBoxResult.Yes:

                                if (AvgShareCapital == ActiveTab)
                                {
                                    this.DataCon.ReportType = "AvgShareCapital";

                                    Report = new Reports.AverageShareCapital();
                                    Export = new Reports.AverageShareCapital();
                                }
                                else if (DividendAndPatronage == ActiveTab)
                                {
                                    this.DataCon.ReportType = "DividendAndPatronage";

                                    Report = new Reports.DividendAndPatronageRefund();
                                    Export = new Reports.DividendAndPatronageRefund();
                                }

                                GenerateReport();

                                break;
                            case MessageBoxResult.No:

                                EnableButtons();
                                HideProgressBar();
                                break;
                        } //end switch
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    break;
                case 2:
                    Thread.Sleep(500);

                    MessageBox.Show(this, "Module Was Successfully Added!", "Successfully Completed!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();

                    //this.Dispatcher.BeginInvoke((Action)(() =>
                    //{

                    //}), System.Windows.Threading.DispatcherPriority.Background);

                    break;
                default:
                    break;
            }


        }

        Byte generateAvg = 0;

        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            if (AvgShareCapital == ActiveTab)
            {
                AvgShareCapitalStream();
                generateAvg = 1;
            }
            else if (DividendAndPatronage == ActiveTab)
            {
                this.DataCon.ComputedAvgShareCapList = ComputedAvgShareCapitalStream();

                generateAvg = 1;
            }
            else if (Journalize == ActiveTab)
            {
                JournalizeStream();
                generateAvg = 2;
            }

        }


        private void JournalizeStream()
        {
            StreamReader resJournalize;

            JournalizeEntryParams _journalizeParams = new JournalizeEntryParams();
            _journalizeParams.TransDate = this.DataCon.TransDate;
            _journalizeParams.DividendAcctCode = this.DataCon.DividendAcctCode;
            _journalizeParams.PatronageAcctCode = this.DataCon.PatronageAcctCode;

            _journalizeParams.ToDate = this.DataCon.ToDate;

            String parsed = new JavaScriptSerializer().Serialize(_journalizeParams);
            resJournalize = App.CRUXAPI.requestBIG("backoffice/JournalizeStream", parsed, 7200000);

            int i = 0;
            int count = 0;

            String tmp = "", tmp3 = "";
            DateTime starttime = DateTime.Now;

            while (i <= count)
            {

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    try
                    {
                        tmp = resJournalize.ReadLine();
                        string tmp2 = tmp.Replace("\\\"", "\"");
                        tmp3 = tmp2.Remove(tmp2.Length - 1);
                    }
                    catch (Exception ex)
                    {

                    }



                    if (i == 0)
                    {
                        count = Convert.ToInt32(tmp);
                        Console.WriteLine(tmp);
                        i++;
                    }
                    else
                    {

                        ClientFinalParams balrow = serializer.Deserialize<ClientFinalParams>(tmp3.Substring(1, tmp3.Length - 1));

                        if (i <= count)
                        {
                            this.DataCon.processing = "Journalizing " + i + " of " + count + " AccountID: " + balrow.ClientID + " Account Name: " + balrow.ClientName;
                            this.progressPercent(i, count, starttime);

                        }
                        else
                        {
                            this.DataCon.processing = "Finalizing Journal";
                        }

                        i++;
                    }

                }
                catch (Exception ex)
                {

                }
            }

            resJournalize.Close();
            resJournalize.Dispose();
            Thread.Sleep(500);


        }


        private void AvgShareCapitalStream()
        {
            StreamReader res;

            AvgShareCap avgShareCapital = new AvgShareCap();
            avgShareCapital.SLT = this.DataCon.Selected.SLT.First().SLTypeSLT_CODE;
            avgShareCapital.FromDate = this.DataCon.FromDate;
            avgShareCapital.ToDate = this.DataCon.ToDate;
            avgShareCapital.FromShareCapitalBal = this.DataCon.FromShareCapitalBal;
            avgShareCapital.FromAvgShareCapital = this.DataCon.FromAvgShareCapital;

            String parsed = new JavaScriptSerializer().Serialize(avgShareCapital);
            res = App.CRUXAPI.requestBIG("backoffice/GenerateAvgShareCapital", parsed, int.MaxValue);

            Decimal TotalAvgShareCapital = 0, TotalIntPaid = 0;

            int i = 0;
            int count = 0;

            DateTime starttime = DateTime.Now;
            while (i <= count)
            {

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    string tmp = res.ReadLine();
                    string tmp2 = tmp.Replace("\\\"", "\"");
                    string tmp3 = tmp2.Remove(tmp2.Length - 1);

                    if (i == 0)
                    {
                        count = Convert.ToInt32(tmp);
                        Console.WriteLine(tmp);
                        i++;
                    }
                    else
                    {

                        AvgShareCapParameters balrow = serializer.Deserialize<AvgShareCapParameters>(tmp3.Substring(1, tmp3.Length - 1));

                        if (i <= count)
                        {
                            this.DataCon.processing = "Computing Avg Share Capital Amt " + i + " of " + count + " AccountID: " + balrow.ClientIDFormat + " Account Name: " + balrow.ClientName;
                            this.progressPercent(i, count, starttime);

                            TotalAvgShareCapital += balrow.AvgShareCapital;
                            TotalIntPaid += balrow.InterestPaid;

                            this.DataCon.AvgControlList.Add(balrow);
                        }
                        else
                        {
                            this.DataCon.processing = "Loading Report";
                        }

                        i++;
                    }

                }
                catch (Exception ex)
                {

                }
            }

            Thread.Sleep(300);

            this.DataCon.TotalAverageShareCapital = TotalAvgShareCapital.ToString("N2");
            this.DataCon.TotalIntPaid = TotalIntPaid.ToString("N2");

        }


        private List<ComputedAvgShareCapital> ComputedAvgShareCapitalStream()
        {

            List<ComputedAvgShareCapital> _ComputedAvgShareCapitalList = new List<ComputedAvgShareCapital>();
            StreamReader res;

            DividendAndPatronage divAndPat = new Models.Year_End.DividendAndPatronage();
            divAndPat.DividendAmt = this.DataCon.DividendAmt;
            divAndPat.PatronageRefundAmt = this.DataCon.PatronageRefundAmt;
            divAndPat.TotalAvgShareCapital = Convert.ToDecimal(this.DataCon.TotalAverageShareCapital);
            divAndPat.TotalIntPaid = Convert.ToDecimal(this.DataCon.TotalIntPaid);

            String parsed = new JavaScriptSerializer().Serialize(divAndPat);
            res = App.CRUXAPI.requestBIG("backoffice/GenerateDividenAndPatronageTestStream", parsed, int.MaxValue);

            int i = 0;
            int count = 0;
            DateTime starttime = DateTime.Now;
            while (i <= count)
            {

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    string tmp = res.ReadLine();
                    string tmp2 = tmp.Replace("\\\"", "\"");
                    string tmp3 = tmp2.Remove(tmp2.Length - 1);

                    if (i == 0)
                    {
                        count = Convert.ToInt32(tmp);
                        Console.WriteLine(tmp);
                        i++;
                    }
                    else
                    {

                        ComputedAvgShareCapital balrow = serializer.Deserialize<ComputedAvgShareCapital>(tmp3.Substring(1, tmp3.Length - 1));

                        if (i <= count)
                        {
                            this.DataCon.processing = "Computing Dividend and Patronage Refund Amt " + i + " of " + count + " AccountID: " + balrow.ClientIDFormat + " Account Name: " + balrow.ClientName;
                            this.progressPercent(i, count, starttime);

                            _ComputedAvgShareCapitalList.Add(balrow);
                        }
                        else
                        {
                            this.DataCon.processing = "Loading Report";
                        }

                        i++;
                    }

                }
                catch (Exception ex)
                {

                }

            }

            Thread.Sleep(300);

            return _ComputedAvgShareCapitalList;
        }


        private void progressPercent(int i, int count, DateTime starttime)
        {
            Int32 seconds = Convert.ToInt32(((DateTime.Now - starttime).TotalSeconds / i) * (count - i));

            this.DataCon.progress = " ";
            this.DataCon.progress += Math.Round((Convert.ToDecimal(i) / count) * 100, 2);
            this.DataCon.progress += "% ";


            if (seconds > 3600)
            {
                this.DataCon.progress += Convert.ToInt32(seconds / 3600) + " Hours ";
            }
            if (seconds > 60)
            {
                Int32 minutes = (seconds / 60);
                this.DataCon.progress += (minutes % 60) + " Minutes ";
            }
            if (seconds % 60 > 0)
            {
                this.DataCon.progress += (seconds % 60) + " Seconds ";
            }

            this.DataCon.progress += "Remaining";
        }


        private void GenerateReport()
        {
            DisableButtons();
            //ProgressPanel.Visibility = Visibility.Visible;
            GenerateControlListWorker.RunWorkerAsync();
        }


        private void HideProgressBar()
        {
            processBlock.Visibility = Visibility.Hidden;
            progressBlock.Visibility = Visibility.Hidden;
        }


        private void BasicWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;

            Response = App.CRUXAPI.request("backoffice/SelectSLT", "");

            if (Response.Status == "ERROR")
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(this, Response.Content);
                }), System.Windows.Threading.DispatcherPriority.Background);
            }
            else
            {
                this.DataCon.Select.SLT.AddRange(new JavaScriptSerializer().Deserialize<List<Models.SLType>>(Response.Content));
            }

            //HeaderDetails For Report
            Response = App.CRUXAPI.request("foreports/headerdetails", "");
            if (Response.Status == "ERROR")
            {

            }
            else
            {
                this.boh = new JavaScriptSerializer().Deserialize<BOHeader>(Response.Content);
            }

            Response = App.CRUXAPI.request("backoffice/EncodedBy", "");

            if (Response.Status == "SUCCESS")
            {
                this.DataCon.PreparedBy = new JavaScriptSerializer().Deserialize<String>(Response.Content);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

            String parsed = new JavaScriptSerializer().Serialize(this.DateTimeNow);
            Response = App.CRUXAPI.request("backoffice/GenerateTotalShareCapitalAndPatronage", parsed);

            if (Response.Status == "SUCCESS")
            {
                DividendAndPatronage _dividendAndPatronage = new JavaScriptSerializer().Deserialize<DividendAndPatronage>(Response.Content);
                this.DataCon.TotalAverageShareCapital = _dividendAndPatronage.TotalAvgShareCapital.ToString("N2");
                this.DataCon.TotalIntPaid = _dividendAndPatronage.TotalIntPaid.ToString("N2");

                if (this.DataCon.TotalAverageShareCapital != "0.00" && this.DataCon.TotalIntPaid != "0.00")
                {
                    this.DataCon.FromDate = _dividendAndPatronage.FromDate;
                    this.DataCon.ToDate = Convert.ToDateTime(_dividendAndPatronage.ToDate).ToString("MM/dd/yyyy");
                }

            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void BasicWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();

            ProgressPanel.Visibility = Visibility.Hidden;
            cancelButton.IsEnabled = true;
        }


        private void HandleSLCFrom(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                DeselectSLC();
            }
        }


        private void HandleSLCTo(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Space)
                {
                    selectSLCTo();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void selectToCallBack(ListBox From, ListBox To, ListBox ParentOrChild, bool IsParent, bool MultiSelect)
        {
            To.SelectedIndex = -1;
            switch (From.Name)
            {
                case "slclist": selectSLC(From, To, ParentOrChild); break;
            }
        }


        private void selectFromCallBack(ListBox From, ListBox To, ListBox ParentOrChild, bool IsParent, bool MultiSelect)
        {
            switch (From.Name)
            {
                case "slclist": deselectSLC(From, To); break;
                default:
                    break;
            }
        }


        private void deselectSLC(ListBox From, ListBox To)
        {
            foreach (Models.SLType s in To.SelectedItems)
            {
                Models.SLType tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.Select.SLT.FindAll(t => String.Compare(t.SLTypeM_DESC2, s.SLTypeM_DESC2) < 1).Count;

                DataCon.Selected.SLT.Remove(s);
                DataCon.Select.SLT.Insert(last, tpass);
            }

            From.Items.Refresh();
            To.Items.Refresh();

            if (To.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void DeselectSLC()
        {
            foreach (Models.SLType selected in slcselectlist.SelectedItems)
            {
                Models.SLType tpass = selected;
                tpass.IsSelected = false;

                int last = this.DataCon.Select.SLT.FindAll(t => String.Compare(t.SLTypeM_DESC2, selected.SLTypeM_DESC2) < 1).Count;

                this.DataCon.Selected.SLT.Remove(selected);
                this.DataCon.Select.SLT.Insert(last, tpass);
            }

            slclist.SelectedIndex = -1;
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();

            if (slcselectlist.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void selectSLCTo()
        {
            foreach (Models.SLType selected in slclist.SelectedItems)
            {
                Models.SLType tpass = selected;
                tpass.IsSelected = false;

                int last = this.DataCon.Selected.SLT.FindAll(t => String.Compare(t.SLTypeM_DESC2, selected.SLTypeM_DESC2) < 1).Count;

                this.DataCon.Select.SLT.Remove(selected);
                this.DataCon.Selected.SLT.Insert(last, tpass);
            }

            slcselectlist.SelectedIndex = -1;
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();

            if (slcselectlist.Items.Count != 0)
            {
                generateButton.IsEnabled = true;
            }
        }


        private void selectSLC(ListBox GlobalFrom, ListBox GlobalTo, ListBox Child)
        {
            foreach (Models.SLType s in GlobalFrom.SelectedItems)
            {
                Models.SLType tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.Selected.SLT.FindAll(t => String.Compare(t.SLTypeM_DESC2, s.SLTypeM_DESC2) < 1).Count;

                DataCon.Select.SLT.Remove(s);
                DataCon.Selected.SLT.Insert(last, tpass);
            }

            GlobalFrom.Items.Refresh();
            GlobalTo.Items.Refresh();


            if (GlobalTo.Items.Count != 0)
            {
                generateButton.IsEnabled = true;
            }
        }


        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "FromDatePicker")
            {
                FromDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                FromDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "ToDatePicker")
            {
                ToDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                ToDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "TransDatePicker")
            {
                TransDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                TransDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }

        }


        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "FromDatePicker")
            {
                try
                {
                    DateTime.Parse(FromDatePicker.Text);
                    FromDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    FromDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }
            else if (textName == "ToDatePicker")
            {
                try
                {
                    DateTime.Parse(ToDatePicker.Text);
                    ToDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    ToDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }
            else if (textName == "TransDatePicker")
            {
                try
                {
                    DateTime.Parse(TransDatePicker.Text);
                    TransDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    TransDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }

        }


        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal value1;
            if (decimal.TryParse(((TextBox)sender).Text, out value1))
            {
                string[] a = ((TextBox)sender).Text.Split(new char[] { '.' });
                int decimals = 0;
                if (a.Length == 2)
                {
                    decimals = a[1].Length;
                }
                else
                {
                    string newString = Regex.Replace(((TextBox)sender).Text, "[^0-9]", "");
                    ((TextBox)sender).Text = string.Format("{0:#,##0}", Double.Parse(newString));
                }
                if (decimals > 2)
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
                            ((TextBox)sender).Focus();
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
            }
            else
            {
                if (((TextBox)sender).Text != "")
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
            }
        }


        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AvgShareCapTab.IsSelected)
            {
                this.Title = ComputeAvgShareCapAndIPTitle;
                AvgShareCapitalRDButton.IsChecked = true;
                ActiveTab = AvgShareCapital;
            }
            else if (DivAndPatronageTab.IsSelected)
            {
                this.Title = AllocationOfShareCapAndIntPaidTitle;
                DividendAndPatronageRDButton.IsChecked = true;
                ActiveTab = DividendAndPatronage;
            }
            else if (JournalizeTab.IsSelected)
            {
                this.Title = JournalizeTitle;
                JournalizeRDButton.IsChecked = true;
                ActiveTab = Journalize;

                if (TransDatePicker.Text != "__/__/____" && !String.IsNullOrEmpty(this.DataCon.DividendAcctCode) && !String.IsNullOrEmpty(this.DataCon.PatronageAcctCode))
                {
                    generateButton.IsEnabled = true;
                }
            }
        }


        private void RDButton_Click(object sender, RoutedEventArgs e)
        {
            if (AvgShareCapitalRDButton.IsChecked == true)
            {
                AvgShareCapTab.IsSelected = true;
            }
            else if (DividendAndPatronageRDButton.IsChecked == true)
            {
                DivAndPatronageTab.IsSelected = true;
            }
            else if (JournalizeRDButton.IsChecked == true)
            {
                JournalizeTab.IsSelected = true;
            }

        }


        private void DisableButtons()
        {
            ProcessTabControl.IsEnabled = false;
            generateButton.IsEnabled = false;
            cancelButton.IsEnabled = false;

            AvgShareCapitalRDButton.IsEnabled = false;
            DividendAndPatronageRDButton.IsEnabled = false;
            JournalizeRDButton.IsEnabled = false;
        }


        private void EnableButtons()
        {
            ProcessTabControl.IsEnabled = true;
            generateButton.IsEnabled = true;
            cancelButton.IsEnabled = true;

            AvgShareCapitalRDButton.IsEnabled = true;
            DividendAndPatronageRDButton.IsEnabled = true;
            JournalizeRDButton.IsEnabled = true;
        }


        public void Generate()
        {
            if (AvgShareCapital == ActiveTab)
            {
                MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:

                        if (FromDatePicker.Text != "__/__/____" && ToDatePicker.Text != "__/__/____")
                        {
                            DateTime From, To;
                            From = Convert.ToDateTime(FromDatePicker.Text);
                            To = Convert.ToDateTime(ToDatePicker.Text);

                            if (From < To)
                            {
                                DisableButtons();
                                // ProgressPanel.Visibility = Visibility.Visible;
                                processBlock.Visibility = Visibility.Visible;
                                progressBlock.Visibility = Visibility.Visible;
                                this.DataCon.processing = "Loading...";

                                GenerateWorker.RunWorkerAsync();
                            }
                            else
                            {
                                MessageBox.Show(this, "From Date should be less than To Date! Thank you.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "Please Enter Date!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                        break;
                    case MessageBoxResult.No:

                        break;
                }
            }
            else if (DividendAndPatronage == ActiveTab)
            {
                MessageBoxResult result = MessageBox.Show(this, "You are about to compute the Interest on Share Capital and Patronage Refund. This process will lock the semaphor, upon starting this procedure others cannot post any documents. PROCEED?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        if (this.DataCon.DividendAmt != 0 && this.DataCon.PatronageRefundAmt != 0)
                        {
                            DisableButtons();

                            processBlock.Visibility = Visibility.Visible;
                            progressBlock.Visibility = Visibility.Visible;
                            this.DataCon.processing = "Loading...";

                            GenerateWorker.RunWorkerAsync();
                        }
                        else
                        {
                            if (this.DataCon.DividendAmt == 0)
                            {
                                MessageBox.Show(this, "Please enter Interest on Share Capital amount.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            else if (this.DataCon.DividendAmt == 0)
                            {
                                MessageBox.Show(this, "Please enter Patronage Refund amount.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        break;
                    case MessageBoxResult.No:

                        break;
                }


            }
            else if (Journalize == ActiveTab)
            {
                MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:


                        if (TransDatePicker.Text != "__/__/____")
                        {

                            DisableButtons();
                            processBlock.Visibility = Visibility.Visible;
                            progressBlock.Visibility = Visibility.Visible;
                            this.DataCon.processing = "Loading loan dues...";

                            GenerateJournalizeWorker.RunWorkerAsync();

                            //bool pass = true;
                            //while (pass)
                            //{
                            //    if (!GenerateLoanDuesWorker.IsBusy)
                            //    {
                            //        try
                            //        {

                            //            GenerateWorker.RunWorkerAsync();
                            //            //CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckLoanDues", "");
                            //            //int _documentCount = 0;

                            //            //if (Response.Status == "SUCCESS")
                            //            //{
                            //            //    _documentCount = new JavaScriptSerializer().Deserialize<int>(Response.Content);

                            //            //    if (_documentCount > 0)
                            //            //    {
                            //            //        GenerateWorker.RunWorkerAsync();
                            //            //    }
                            //            //    else
                            //            //    {
                            //            //        GenerateLoanDuesWorker.RunWorkerAsync();
                            //            //    }
                            //            //}

                            //            pass = false;
                            //        }
                            //        catch (Exception)
                            //        {

                            //        }
                            //    }

                            //    Thread.Sleep(50);
                            //} // end while

                        }
                        else
                        {
                            MessageBox.Show(this, "Please Enter Date!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }

                        break;
                    case MessageBoxResult.No:

                        break;
                }
            }
        }


        public class AvgShareCap
        {
            public Byte SLT { get; set; }
            public String FromDate { get; set; }
            public String ToDate { get; set; }
            public Decimal FromShareCapitalBal { get; set; }
            public Decimal ToShareCapitalBal { get; set; }
            public Decimal FromAvgShareCapital { get; set; }
            public Decimal ToAvgShareCapital { get; set; }
            public String Explanation { get; set; }
        }

        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Generate();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public class AvgShareCapDataCon : INotifyPropertyChanged
        {
            String _DateNow;
            public String DateNow
            {
                get { return _DateNow; }
                set
                {
                    _DateNow = value;
                    OnPropertyChanged("DateNow");
                }
            }

            String _FromDate;
            public String FromDate
            {
                get { return _FromDate; }
                set
                {
                    _FromDate = value;
                    OnPropertyChanged("FromDate");
                }
            }

            String _ToDate;
            public String ToDate
            {
                get { return _ToDate; }
                set
                {
                    _ToDate = value;
                    OnPropertyChanged("ToDate");
                }
            }

            Decimal _FromShareCapitalBal;
            public Decimal FromShareCapitalBal
            {
                get { return _FromShareCapitalBal; }
                set
                {
                    _FromShareCapitalBal = value;
                    OnPropertyChanged("FromShareCapitalBal");
                }
            }

            Decimal _FromAvgShareCapital;
            public Decimal FromAvgShareCapital
            {
                get { return _FromAvgShareCapital; }
                set
                {
                    _FromAvgShareCapital = value;
                    OnPropertyChanged("FromAvgShareCapital");
                }
            }

            String _ToShareCapitalBal;
            public String ToShareCapitalBal
            {
                get { return _ToShareCapitalBal; }
                set
                {
                    _ToShareCapitalBal = value;
                    OnPropertyChanged("ToShareCapitalBal");
                }
            }

            String _ToAvgShareCapital;
            public String ToAvgShareCapital
            {
                get { return _ToAvgShareCapital; }
                set
                {
                    _ToAvgShareCapital = value;
                    OnPropertyChanged("ToAvgShareCapital");
                }
            }

            String _TotalAverageShareCapital;
            public String TotalAverageShareCapital
            {
                get { return _TotalAverageShareCapital; }
                set
                {
                    _TotalAverageShareCapital = value;
                    OnPropertyChanged("TotalAverageShareCapital");
                }
            }

            String _TotalIntPaid;
            public String TotalIntPaid
            {
                get { return _TotalIntPaid; }
                set
                {
                    _TotalIntPaid = value;
                    OnPropertyChanged("TotalIntPaid");
                }
            }

            Decimal _DividendAmt;
            public Decimal DividendAmt
            {
                get { return _DividendAmt; }
                set
                {
                    _DividendAmt = value;
                    OnPropertyChanged("DividendAmt");
                }
            }

            Decimal _PatronageRefundAmt;
            public Decimal PatronageRefundAmt
            {
                get { return _PatronageRefundAmt; }
                set
                {
                    _PatronageRefundAmt = value;
                    OnPropertyChanged("PatronageRefundAmt");
                }
            }

            String _TransDate;
            public String TransDate
            {
                get { return _TransDate; }
                set
                {
                    _TransDate = value;
                    OnPropertyChanged("TransDate");
                }
            }

            String _TypeClosing;
            public String TypeClosing
            {
                get { return _TypeClosing; }
                set
                {
                    _TypeClosing = value;
                    OnPropertyChanged("TypeClosing");
                }
            }

            String _Explanation;
            public String Explanation
            {
                get { return _Explanation; }
                set
                {
                    _Explanation = value;
                    OnPropertyChanged("Explanation");
                }
            }

            Selections _Select = new Selections();
            public Selections Select
            {
                get
                {
                    return _Select;
                }
                set
                {
                    _Select = value;
                    OnPropertyChanged("Select");
                }
            }

            Selections _Selected = new Selections();
            public Selections Selected
            {
                get
                {
                    return _Selected;
                }
                set
                {
                    _Selected = value;
                    OnPropertyChanged("Selected");
                }
            }

            Selections _Hidden = new Selections();
            public Selections Hidden
            {
                get
                {
                    return _Hidden;
                }
                set
                {
                    _Hidden = value;
                    OnPropertyChanged("Hidden");
                }
            }

            String _ReportType;
            public String ReportType
            {
                get { return _ReportType; }
                set
                {
                    _ReportType = value;
                    OnPropertyChanged("ReportType");
                }
            }

            String _Title;
            public String Title
            {
                get { return _Title; }
                set
                {
                    _Title = value;
                    OnPropertyChanged("Title");
                }
            }

            List<AvgShareCapParameters> _AvgControlList;
            public List<AvgShareCapParameters> AvgControlList
            {
                get { return _AvgControlList; }
                set
                {
                    _AvgControlList = value;
                    OnPropertyChanged("AvgControlList");
                }
            }

            String _PreparedBy;
            public String PreparedBy
            {
                get { return _PreparedBy; }
                set
                {
                    _PreparedBy = value;
                    OnPropertyChanged("PreparedBy");
                }
            }

            String _progress;
            public String progress
            {
                get { return _progress; }
                set
                {
                    _progress = value;
                    OnPropertyChanged("progress");
                }
            }

            String _processing;
            public String processing
            {
                get
                {
                    return _processing;
                }
                set
                {
                    _processing = value;
                    OnPropertyChanged("processing");
                }
            }

            String _DividendAcctCode;
            public String DividendAcctCode
            {
                get { return _DividendAcctCode; }
                set
                {
                    _DividendAcctCode = value;
                    OnPropertyChanged("DividendAcctCode");
                }
            }

            String _PatronageAcctCode;
            public String PatronageAcctCode
            {
                get { return _PatronageAcctCode; }
                set
                {
                    _PatronageAcctCode = value;
                    OnPropertyChanged("PatronageAcctCode");
                }
            }

            List<ComputedAvgShareCapital> _ComputedAvgShareCapList;
            public List<ComputedAvgShareCapital> ComputedAvgShareCapList
            {
                get
                {
                    return _ComputedAvgShareCapList;
                }
                set
                {
                    _ComputedAvgShareCapList = value;
                    OnPropertyChanged("ComputedAvgShareCapList");
                }
            }


            public event PropertyChangedEventHandler PropertyChanged;


            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void IntShareCapitalTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.DataCon.DividendAmt != 0 && this.DataCon.PatronageRefundAmt != 0)
                {
                    this.generateButton.IsEnabled = true;
                }
                else
                {
                    this.generateButton.IsEnabled = false;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void PatronageRefundTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.DataCon.DividendAmt != 0 && this.DataCon.PatronageRefundAmt != 0)
                {
                    this.generateButton.IsEnabled = true;
                }
                else
                {
                    this.generateButton.IsEnabled = false;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //try
            //{
            //    bool pass = true;
            //    while (pass)
            //    {
            //        if (GenerateLoanDuesWorker.IsBusy)
            //        {
            //            GenerateLoanDuesWorker.Dispose();
            //            resLoanDues.Close();
            //            resLoanDues.Dispose();
            //            pass = false;

            //        }
            //        Thread.Sleep(50);
            //    } // end while
            //}
            //catch (Exception)
            //{
            //    Close();
            //}

        }
    }
}
