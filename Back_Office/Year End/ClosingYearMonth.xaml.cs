﻿using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Year_End;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.Year_End
{
    /// <summary>
    /// Interaction logic for ClosingYearMonth.xaml
    /// </summary>
    public partial class ClosingYearMonth : Window
    {
        MainWindow mww;
        private readonly BackgroundWorker GenerateWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateCheckClosingWorker = new BackgroundWorker();
        ClosingDataCon DataCon = new ClosingDataCon();

        public ClosingYearMonth(MainWindow mw, String SystemDate)
        {
            InitializeComponent();
            InitializeDefaults();
            this.DataCon.SystemDate = SystemDate;
            ComputeDate(this.DataCon.SystemDate);
            InitializeWorkers();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mww = mw;
            this.DataContext = DataCon;
        }


        private void InitializeWorkers()
        {
            GenerateWorker.DoWork += GenerateWorker_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateWorker_RunWorkerCompleted;

            GenerateCheckClosingWorker.DoWork += GenerateCheckClosingWorker_DoWork;
            GenerateCheckClosingWorker.RunWorkerCompleted += GenerateCheckClosingWorker_RunWorkerWorkerCompleted;
        }

        private void GenerateCheckClosingWorker_RunWorkerWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void GenerateCheckClosingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            String checkClosingParam = new JavaScriptSerializer().Serialize(this.DataCon.CheckClosing);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckClosingEntries", checkClosingParam);
            if (Response.Status == "SUCCESS")
            {
                this.DataCon.CheckClosing = new JavaScriptSerializer().Deserialize<CheckClosingParams>(Response.Content);
            }
        }

        private void ComputeDate(String SystemDate)
        {
            int CurrentMonthNoDays = DateTime.DaysInMonth(Convert.ToDateTime(SystemDate).Year, Convert.ToDateTime(SystemDate).AddMonths(-1).Month);
            DateTime CurrentMonth = new DateTime(Convert.ToDateTime(SystemDate).Year, Convert.ToDateTime(SystemDate).AddMonths(-1).Month, CurrentMonthNoDays);

            int ForwardMonthNoDays = DateTime.DaysInMonth(Convert.ToDateTime(SystemDate).Year, Convert.ToDateTime(SystemDate).Month);
            DateTime ForwardMonth = new DateTime(Convert.ToDateTime(SystemDate).Year, Convert.ToDateTime(SystemDate).Month, ForwardMonthNoDays);

            this.DataCon.CheckClosing.CurrentMonth = CurrentMonth.ToString("MM/dd/yyyy");
            this.DataCon.CheckClosing.ForwardMonth = ForwardMonth.ToString("MM/dd/yyyy");
        }

        private void RefreshDisplay()
        {
            ClosingGrid.IsEnabled = true;
            ProgressPanel.Visibility = Visibility.Hidden;
        }

        private void GenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (CloseProcess)
            {
                case 1:
                    ProgressPanel.Visibility = Visibility.Hidden;
                    ClosingGrid.IsEnabled = true;

                    MessageBox.Show(this, "Module Was Successfully Added!", "Successfully Completed!", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();

                    //if (this.DataCon.TypeClosing == 1)
                    //{
                    //    // GenerateUpdateTransSumWorker.RunWorkerAsync();
                    //    MessageBox.Show(this, "Module Was Successfully Added!", "Successfully Completed!", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    this.Close();
                    //}
                    //else
                    //{

                    //}

                    break;
                case 2:
                    MessageBox.Show(this, "Failed to process Closing Transaction.");
                    RefreshDisplay();
                    break;
                case 3:
                    MessageBox.Show(this, "Failed to Retrieve Data! Please Contact Administrator.");
                    Close();
                    break;
                case 4:
                    MessageBox.Show(this, "Unable to generate Closing Month End. Transaction is already processed!");
                    Close();
                    break;
                case 5:
                    MessageBox.Show(this, "Unable to generate forward date on Closing Month End. Please change the date to prior month.");
                    RefreshDisplay();
                    break;
                default:
                    break;
            }
        }

        Int16 CloseProcess = 0;

        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true, processClosing = true;
            while (pass)
            {
                if (!GenerateCheckClosingWorker.IsBusy)
                {
                    try
                    {

                        if (this.DataCon.TypeClosing == 1)
                        {
                            if (this.DataCon.CheckClosing.CurrentMonth == this.DataCon.ToDate)
                            {
                                processClosing = this.DataCon.CheckClosing.CurrentMonthStatus;
                            }
                            else if (this.DataCon.CheckClosing.ForwardMonth == this.DataCon.ToDate)
                            {
                                if (this.DataCon.CheckClosing.CurrentMonthStatus)
                                {
                                    processClosing = this.DataCon.CheckClosing.ForwardMonthStatus;
                                }
                                else
                                {
                                    CloseProcess = 5;
                                }
                            }
                            else
                            {
                                CloseProcess = 4;
                            }
                        }
                        else
                        {
                            processClosing = false;
                        }

                        if (!processClosing)
                        {
                            ExecuteHelper execHelp = new ExecuteHelper(this.mww);

                            DateTime TransactionDate = Convert.ToDateTime(this.DataCon.ToDate);
                            this.DataCon.Explanation = execHelp.CreateClosingEntriesExplanation(this.DataCon.ToDate);

                            String parsed = new JavaScriptSerializer().Serialize(this.DataCon);
                            List<TransactionDetails> insertTransDetail = new List<TransactionDetails>();

                            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/ClosingEntries", parsed, 999999999);
                            if (Response.Status == "SUCCESS")
                            {
                                if (Response.Content == "NO RECORDS FOUND!")
                                {
                                    CloseProcess = 2;
                                }
                                else if (Response.Content == "Module Was Successfully Added")
                                {
                                    CloseProcess = 1;
                                }
                            }
                            else
                            {
                                CloseProcess = 3;
                            }
                        }
                        else
                        {
                            CloseProcess = 4;
                        }
                        pass = false;
                    }
                    catch (Exception)
                    {

                    }

                }

                Thread.Sleep(50);
            }
        }


        private void InitializeDefaults()
        {
            this.DataCon.FromDate = String.Empty;
            this.DataCon.ToDate = String.Empty;
            this.DataCon.TransDate = String.Empty;
            this.DataCon.SystemDate = String.Empty;
            this.DataCon.TypeClosing = 0;
            this.DataCon.CheckClosing = new CheckClosingParams();

            LoadComboBox();
        }


        private void LoadComboBox()
        {
            List<BalanceTypes> blTypes = new List<BalanceTypes>();
            blTypes.Add(new BalanceTypes() { BalTypeID = 1, BalTypeDesc = "Month End" });
            blTypes.Add(new BalanceTypes() { BalTypeID = 2, BalTypeDesc = "Year End" });

            ClosingTypeComboBox.ItemsSource = blTypes;
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to Exit this Module?", "Close", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            this.Close();
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;
                    }
                }

                else if (e.Key == Key.F11 || (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G)))
                {
                    if (generateButton.IsEnabled)
                    {
                        this.Generate();
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        //private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        //{
        //    string textName = (sender as TextBox).Name.ToString();

        //    if (textName == "CutOffDatePicker")
        //    {
        //        CutOffDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
        //        CutOffDatePicker.IsInactiveSelectionHighlightEnabled = true;
        //    }
        //}

        void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "CutOffDatePicker")
            {
                try
                {
                    DateTime.Parse(CutOffDatePicker.Text);
                    //CutOffDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    //CutOffDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }
        }


        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Generate();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void Generate()
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:

                    if (!String.IsNullOrEmpty(this.DataCon.ToDate))
                    {
                        this.DataCon.FromDate = new DateTime(Convert.ToDateTime(this.DataCon.ToDate).Year, Convert.ToDateTime(this.DataCon.ToDate).Month, 1).ToString("yyyy/MM/dd");
                        this.DataCon.TransDate = this.DataCon.ToDate;

                        ClosingGrid.IsEnabled = false;
                        ProgressPanel.Visibility = Visibility.Visible;
                        GenerateWorker.RunWorkerAsync();
                    }
                    else
                    {

                    }
                    break;
                case MessageBoxResult.No:
                    break;
            }

        }

        private void CutOffDatePicker_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Tab)
                {
                    DateTime.Parse(CutOffDatePicker.Text);
                    generateButton.IsEnabled = true;
                }
                else if (e.Key == Key.Enter)
                {
                    if (CutOffDatePicker.Text != "__/__/____")
                    {
                        DateTime.Parse(CutOffDatePicker.Text);
                        // Generate();
                    }
                    else
                    {
                        MessageBox.Show(this, "CutOff Date Required!");
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show(this, "Please enter correct Date");
                e.Handled = true;
            }

        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Close();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void ClosingTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                generateButton.IsEnabled = true;

                if (this.DataCon.TypeClosing != 0)
                {
                    if (this.DataCon.TypeClosing == 1)
                    {
                        GenerateCheckClosingWorker.RunWorkerAsync();
                    }
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public class ClosingDataCon : INotifyPropertyChanged
        {
            String _FromDate;
            public String FromDate
            {
                get { return _FromDate; }
                set
                {
                    _FromDate = value;
                    OnPropertyChanged("FromDate");
                }
            }

            String _ToDate;
            public String ToDate
            {
                get { return this.DateFormat(_ToDate); }
                set
                {
                    _ToDate = value;
                    OnPropertyChanged("ToDate");
                }
            }

            String _TransDate;
            public String TransDate
            {
                get { return _TransDate; }
                set
                {
                    _TransDate = value;
                    OnPropertyChanged("TransDate");
                }
            }

            String _SystemDate;
            public String SystemDate
            {
                get { return _SystemDate; }
                set
                {
                    _SystemDate = value;
                    OnPropertyChanged("SystemDate");
                }
            }

            Int16 _TypeClosing;
            public Int16 TypeClosing
            {
                get { return _TypeClosing; }
                set
                {
                    _TypeClosing = value;
                    OnPropertyChanged("TypeClosing");
                }
            }

            String _Explanation;
            public String Explanation
            {
                get { return _Explanation; }
                set
                {
                    _Explanation = value;
                    OnPropertyChanged("Explanation");
                }
            }

            CheckClosingParams _CheckClosing;
            public CheckClosingParams CheckClosing
            {
                get { return _CheckClosing; }
                set
                {
                    _CheckClosing = value;
                    OnPropertyChanged("CheckClosing");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            private String DateFormat(String Date)
            {
                if (Date == null)
                {
                    Date = "";
                }

                String temp = Regex.Replace(Date, "[^0-9.]", "");
                if (temp.Length >= 8)
                {
                    temp = temp.Substring(0, 8);
                }
                else
                {
                    int underscore = 8 - temp.Length;

                    while (underscore > 0)
                    {
                        temp += "_";
                        underscore--;
                    }
                }
                temp = temp.Insert(2, "/");
                temp = temp.Insert(5, "/");

                return temp;
            }
        }
    }
}
