﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.Year_End
{
    /// <summary>
    /// Interaction logic for JournalizeDividenAndPatronageRefund.xaml
    /// </summary>
    public partial class JournalizeDividenAndPatronageRefund : Window
    {
        MainWindow mww;

        public JournalizeDividenAndPatronageRefund(MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Escape)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;

                } //end switch

            } //end If Key = Escape

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
