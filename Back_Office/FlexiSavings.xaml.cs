﻿using Back_Office.Models;
using Back_Office.Models.Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for FlexiSavings.xaml
    /// </summary>
    public partial class FlexiSavings : Window
    {
        public MainWindow mww;
        private InterestRateFlexi defFlexi;

        public FlexiSavings(MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            defFlexi = new Models.Scripts.InterestRateFlexi();
            LoadInterestRange();
            LoadDefault();
        }


        private void SelectClientList()
        {
            List<SLWithADB> slWithADB = new List<SLWithADB>();

            SLParameters slParam = new SLParameters();
            slParam.SLC = defFlexi.SLC;
            slParam.SLT = defFlexi.SLT;
            slParam.SLE = defFlexi.SLE;
            slParam.SLN = defFlexi.SLN;
            slParam.DateFrom = fromDatePicker.Text;
            slParam.DateTo = toDatePicker.Text;
            slParam.CutOffDate = transDatePicker.Text;

            String parsedSLParam = new JavaScriptSerializer().Serialize(slParam);
            CRUXLib.Response ResponseSL = App.CRUXAPI.request("backoffice/searchClients", parsedSLParam);

            if (ResponseSL.Status == "SUCCESS")
            {
                slWithADB = new JavaScriptSerializer().Deserialize<List<SLWithADB>>(ResponseSL.Content);


                CRUXLib.Response ResponseGLFlexi = App.CRUXAPI.request("backoffice/processInterestPM", parsedSLParam);
                if (ResponseGLFlexi.Status == "SUCCESS")
                {

                }

            }
        }


        private void LoadInterestRange()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/loadSLTypeRange", "");
            if (Response.Status == "SUCCESS")
            {
                ObservableCollection<InterestTransType> listIntRates = new JavaScriptSerializer().Deserialize<ObservableCollection<InterestTransType>>(Response.Content);
                IntRangeComboBox.ItemsSource = listIntRates;
                //this.DataContext = DataCont;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void LoadDefault()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/loadDefaultFlexi", "");
            if (Response.Status == "SUCCESS")
            {
                defFlexi = new JavaScriptSerializer().Deserialize<InterestRateFlexi>(Response.Content);
                this.DataContext = defFlexi;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;

                } //end switch

            } //end If Key = Escape
        }


        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "fromDatePicker")
            {
                fromDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                fromDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "toDatePicker")
            {
                toDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                toDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "transDatePicker")
            {
                transDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                transDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "ExplanationTextBox")
            {
                ExplanationTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                ExplanationTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "intRateTextBox")
            {
                intRateTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                intRateTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "intAcctCodeTextBox")
            {
                intAcctCodeTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                intAcctCodeTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "taxIntRateTextBox")
            {
                taxIntRateTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                taxIntRateTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "taxIntAcctCodeTextBox")
            {
                taxIntAcctCodeTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                taxIntAcctCodeTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "NoDaysTextBox")
            {
                NoDaysTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                NoDaysTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "fromDatePicker")
            {
                try
                {
                    DateTime.Parse(fromDatePicker.Text);
                    fromDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    fromDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }

            else if (textName == "toDatePicker")
            {
                try
                {
                    DateTime.Parse(toDatePicker.Text);
                    toDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    toDatePicker.IsInactiveSelectionHighlightEnabled = true;

                    transDatePicker.Text = toDatePicker.Text;
                }
                catch
                {
                    e.Handled = true;
                }

            } // end else if toDatePicker
            else if (textName == "transDatePicker")
            {
                try
                {
                    DateTime.Parse(transDatePicker.Text);
                    transDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    transDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            } // end else if toDatePicker

            else if (textName == "ExplanationTextBox")
            {
                ExplanationTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                ExplanationTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "intRateTextBox")
            {
                intRateTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                intRateTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "intAcctCodeTextBox")
            {
                intAcctCodeTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                intAcctCodeTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "taxIntRateTextBox")
            {
                taxIntRateTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                taxIntRateTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "taxIntAcctCodeTextBox")
            {
                taxIntAcctCodeTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                taxIntAcctCodeTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "NoDaysTextBox")
            {
                NoDaysTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                NoDaysTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void ToFromDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            String name = (sender as TextBox).Name.ToString();

            if (e.Key == Key.Tab)
            {
                try
                {
                    if (name == "fromDatePicker")
                    {
                        DateTime.Parse(fromDatePicker.Text);
                    }
                    else if (name == "toDatePicker")
                    {
                        DateTime.Parse(toDatePicker.Text);
                    }
                    else if (name == "transDatePicker")
                    {
                        DateTime.Parse(transDatePicker.Text);
                    }
                }
                catch
                {
                    e.Handled = true;
                }

            } //end if key = TAB

            else if (e.Key == Key.Enter)
            {
                try
                {
                    //if (name == "transDatePicker")
                    //{
                    //    List<SLParameters> slParams = FunctionGenerate().GroupBy(s => s.SLT).Select(g => g.First()).ToList();
                    //    SelectClientList(slParams);
                    //}
                }
                catch
                {
                    e.Handled = true;
                }
            }

        }


        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            SelectClientList();
        }
    }
}
