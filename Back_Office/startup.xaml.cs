﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for startup.xaml
    /// </summary>
    public partial class startup : Window
    {

        private readonly BackgroundWorker loadMainWindow = new BackgroundWorker();

        public startup()
        {
            InitializeComponent();
            loadMainWindow.DoWork += GenerateWorker_DoWork;
            loadMainWindow.RunWorkerCompleted += GenerateWorker_RunWorkerCompleted;
            loadMainWindow.RunWorkerAsync();
        }

        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(100);
        }

        private void GenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            MainWindow mw = new MainWindow();
            mw.Owner = this;
            mw.ShowDialog();
            this.Close();
        }

    }
}
