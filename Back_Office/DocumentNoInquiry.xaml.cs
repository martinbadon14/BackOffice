﻿using Back_Office.Helper;
using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for DocumentNoInquiry.xaml
    /// </summary>
    public partial class DocumentNoInquiry : Window
    {
        Models.DocumentInquiry Selected;
        public MainWindow mw;
        private ICollectionView MyData;
        string SearchText = string.Empty;
        String DefaultColumn = "";
        int currentRow = 0;
        DocInqDataCon DataCont = new DocInqDataCon();
        public BackgroundWorker GenerateWorker = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

        public DocInquiryParameters docNoInqParams;


        public DocumentNoInquiry(DocInquiryParameters _docInqParams,MainWindow mww)
        {
            InitializeComponent();
            InitializeDefaults();
            InitializeWorkers();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
            //docNoInqParams = new DocInquiryParameters();
            //docNoInqParams = _docInqParams;
            DisplayDocNoInquiry(_docInqParams);

            this.DataContext = DataCont;
        }

        public DocumentNoInquiry(Int16 transTypeID, MainWindow mww, Int32 Year = 0, Int64 ORControlNo = 0,Int64 ControlNo = 0, String DocNo =null)
        {
            InitializeComponent();
            InitializeDefaults();
            InitializeWorkers();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
            docNoInqParams = new DocInquiryParameters();
            docNoInqParams.TransCode = transTypeID;
            docNoInqParams.CTLNo = ControlNo;
            docNoInqParams.DocNo = DocNo;
            docNoInqParams.ORCTLNo = ORControlNo;
            docNoInqParams.TransYear = Year;
            DisplayDocNoInquiry(docNoInqParams);

            this.DataContext = DataCont;
        }


        public DocumentNoInquiry(Int16 transTypeID, MainWindow mww, Int64 ControlNo = 0, String DocNo = null, Int32 Year = 0, String FromDate = null, String ToDate = null)
        {
            InitializeComponent();
            InitializeDefaults();
            InitializeWorkers();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mw = mww;
            docNoInqParams = new DocInquiryParameters();
            docNoInqParams.TransCode = transTypeID;
            docNoInqParams.CTLNo = ControlNo;
            docNoInqParams.DocNo = DocNo;
            docNoInqParams.FromDate = FromDate;
            docNoInqParams.ToDate = ToDate;
            docNoInqParams.TransYear = Year;
            DisplayDocNoInquiry(docNoInqParams);

            this.DataContext = DataCont;
        }


        private void InitializeDefaults()
        {
            DataCont.DocInquiry = new List<DocumentInquiry>();
            DataCont.TransDetailList = new List<TransactionDetails>();
        }


        private void InitializeWorkers()
        {
            GenerateWorker.DoWork += GenerateInterest_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateInterest_RunWorkerCompleted;
        }


        private void GenerateInterest_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar.Visibility = Visibility.Hidden;

            try
            {
                if (DataCont.TransactionDetailContainer._TransactionDetailsList.Count != 0)
                {
                    this.mw.DisplayDocInq(DataCont.TransactionDetailContainer, e);
                    //this.Dispatcher.BeginInvoke((Action)(() =>
                    //{

                    //}), System.Windows.Threading.DispatcherPriority.Background);

                    this.Close();
                    this.mw.TransMainTrigger = false;
                    this.mw.GLTab.IsSelected = true;
                    this.mw.findButton.Focus();
                }
                else
                {
                    MessageBox.Show("Cannot Open Document. Please contact Administrator.");
                    GenerateWorker.CancelAsync();
                    DocInqGrid.IsEnabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please select document.");
                searchTextBox.Clear();
                DocInqGrid.IsEnabled = true;
                searchTextBox.Focus();
            }
        }


        private void DisableButtons()
        {
            progressBar.Visibility = Visibility.Visible;
            DocInqGrid.IsEnabled = false;
        }


        private void GenerateInterest_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                FunctionGo();
            }
            catch (Exception)
            {
                return;
            }

        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        docInqDataGrid.Focus();

                        int previousIndex = docInqDataGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        currentRow = previousIndex;
                        docInqDataGrid.SelectedIndex = previousIndex;
                        docInqDataGrid.ScrollIntoView(docInqDataGrid.Items[currentRow]);
                    }
                }

                else if (e.Key == Key.Down)
                {
                    if (currentRow < docInqDataGrid.Items.Count - 1)
                    {
                        docInqDataGrid.Focus();

                        int nextIndex = docInqDataGrid.SelectedIndex + 1;
                        if (nextIndex > docInqDataGrid.Items.Count - 1) return;
                        currentRow = nextIndex;
                        docInqDataGrid.SelectedIndex = nextIndex;
                        docInqDataGrid.ScrollIntoView(docInqDataGrid.Items[currentRow]);
                    }
                }

                else if (e.Key == Key.PageDown && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    docInqDataGrid.SelectedIndex = DataCont.DocInquiry.Count() - 1;
                    docInqDataGrid.ScrollIntoView(docInqDataGrid.Items[docInqDataGrid.SelectedIndex]);

                    //DataGridRow dgrow = (DataGridRow)docInqDataGrid.ItemContainerGenerator.ContainerFromItem(docInqDataGrid.Items[docInqDataGrid.Items.Count - 1]);
                    //dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Last));
                }

                else if (e.Key == Key.PageUp && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    docInqDataGrid.SelectedIndex = 0;
                    docInqDataGrid.ScrollIntoView(docInqDataGrid.Items[docInqDataGrid.SelectedIndex]);
                }

                else if (e.Key == Key.F4)
                {
                    if (this.WindowState == WindowState.Maximized)
                    {
                        this.WindowState = WindowState.Normal;
                    }
                    else
                    {
                        this.WindowState = WindowState.Maximized;
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }


        private void DisplayDocNoInquiry(DocInquiryParameters docInqParams)
        {
            String parsedDocInq = new JavaScriptSerializer().Serialize(docInqParams);

            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/DocInquiry", parsedDocInq);
            if (Response.Status == "SUCCESS")
            {
                DataCont.DocInquiry = new JavaScriptSerializer().Deserialize<List<DocumentInquiry>>(Response.Content);

                if (DataCont.DocInquiry.Count != 0)
                {
                    docInqDataGrid.SelectedItem = DataCont.DocInquiry.First();
                }

                MyData = CollectionViewSource.GetDefaultView(DataCont.DocInquiry);
            }
        }


        private void GetTransDetail(DocInquiryParameters InqParams)
        {
            String parsedDocInq = new JavaScriptSerializer().Serialize(InqParams);

            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/SelectTransDetail", parsedDocInq);
            if (Response.Status == "SUCCESS")
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = Int32.MaxValue;
                this.DataCont.TransactionDetailContainer = serializer.Deserialize<TransactionDetailContainer>(Response.Content);
            }
            else
            {
                MessageBox.Show("Failed to retrieve data. Please Contact Administrator.");
            }
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                searchTextBox.Focus();

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void columnHeader_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var columnheader = sender as DataGridColumnHeader;
                if (columnheader != null) // 
                {
                    String content = columnheader.Content.ToString();

                    if (content == "B")
                    {
                        DefaultColumn = "BranchCode";
                    }
                    else if (content == "TransYear")
                    {
                        DefaultColumn = "TransYear";
                    }
                    else if (content == "C")
                    {
                        DefaultColumn = "TransCode";
                    }
                    else if (content == "Tr-Type")
                    {
                        DefaultColumn = "TransAbbv";
                    }
                    else if (content == "Ctl.No")
                    {
                        DefaultColumn = "CtlNo";
                    }
                    else if (content == "Doc. No")
                    {
                        DefaultColumn = "DocNo";
                    }
                    else if (content == "Date")
                    {
                        DefaultColumn = "TransDate";
                    }
                    else if (content == "Amount")
                    {
                        DefaultColumn = "Amount";
                    }
                    else if (content == "EncodedBy")
                    {
                        DefaultColumn = "EncodedBy";
                    }
                    else if (content == "Tag")
                    {
                        DefaultColumn = "UPDTag";
                    }
                    else if (content == "APRVBY")
                    {
                        DefaultColumn = "ApprvBy";
                    }
                    else if (content == "Client Name")
                    {
                        DefaultColumn = "ClientName";
                    }
                    else if (content == "Explanation")
                    {
                        DefaultColumn = "Explanation";
                    }

                }
                else
                {
                    docInqDataGrid.CanUserSortColumns = true;
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private bool FilterUnpostedData(object item)
        {
            var value = (Models.DocumentInquiry)item;

            if (String.IsNullOrEmpty(value.UPDTag))
                return String.IsNullOrEmpty(value.UPDTag);

            return false;

        }


        private bool FilterData(object item)
        {
            var value = (Models.DocumentInquiry)item;

            if (String.IsNullOrEmpty(value.TransYear) || value.BranchCode == 0 || value.TransCode == 0 ||
                String.IsNullOrEmpty(value.TransAbbv) || value.CtlNo == 0 || String.IsNullOrEmpty(value.DocNo) ||
                String.IsNullOrEmpty(value.TransDate) || value.Amount == 0 || String.IsNullOrEmpty(value.EncodedBy) ||
                String.IsNullOrEmpty(value.UPDTag) || String.IsNullOrEmpty(value.ApprvBy) || String.IsNullOrEmpty(value.ClientName) ||
                String.IsNullOrEmpty(value.Explanation))
                return false;

            else
                return Convert.ToString(value.TransYear).Contains(SearchText) || Convert.ToString(value.BranchCode).Contains(SearchText) ||
                    Convert.ToString(value.TransCode).Contains(SearchText) || value.TransAbbv.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.CtlNo).Contains(SearchText.ToLower()) || value.DocNo.ToLower().StartsWith(SearchText.ToLower()) ||
                    value.TransDate.ToLower().StartsWith(SearchText.ToLower()) || Convert.ToString(value.Amount).Contains(SearchText) ||
                    value.EncodedBy.ToLower().StartsWith(SearchText.ToLower()) || value.UPDTag.ToLower().StartsWith(SearchText.ToLower()) ||
                    value.ApprvBy.ToLower().StartsWith(SearchText.ToLower()) || value.ClientName.ToLower().StartsWith(SearchText.ToLower()) ||
                    value.Explanation.ToLower().StartsWith(SearchText.ToLower());

        }


        private void searchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            if (SearchText == " ")
            {
                MyData.Filter = FilterUnpostedData;
            }
            else
            {
                MyData.Filter = FilterData;
            }

            docInqDataGrid.SelectedIndex = 0;
            // docInqDataGrid.ScrollIntoView(docInqDataGrid.Items[this.currentRow]);
        }


        private void searchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (DataCont.DocInquiry.Count != 0)
                {
                    DisableButtons();
                    GenerateWorker.RunWorkerAsync();

                    e.Handled = true;
                }
            }
        }


        private void docInqDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DisableButtons();
            GenerateWorker.RunWorkerAsync();
        }


        private void docInqDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DisableButtons();
                GenerateWorker.RunWorkerAsync();
                e.Handled = true;
            }
        }


        private void FunctionGo()
        {
            try
            {
                this.Selected = (Models.DocumentInquiry)docInqDataGrid.Items[currentRow];

                DocInquiryParameters inqParams = new DocInquiryParameters();
                inqParams.BranchCode = this.Selected.BranchCode;
                inqParams.TransCode = this.Selected.TransCode;
                inqParams.CTLNo = this.Selected.CtlNo;
                inqParams.DocNo = this.Selected.DocNo;
                inqParams.TransYear = Convert.ToInt32(this.Selected.TransYear);
                inqParams.OfficeID = this.Selected.OfficeID;

                GetTransDetail(inqParams);

            }
            catch (Exception ex)
            {
                return;
            }

        }


        private void goButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DisableButtons();
                GenerateWorker.RunWorkerAsync();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public class DocInqDataCon : INotifyPropertyChanged
        {
            private List<DocumentInquiry> _DocInquiry;
            public List<DocumentInquiry> DocInquiry
            {
                get { return _DocInquiry; }
                set
                {
                    _DocInquiry = value;
                    OnPropertyChanged("DocInquiry");
                }
            }

            private List<TransactionDetails> _TransDetailList;
            public List<TransactionDetails> TransDetailList
            {
                get { return _TransDetailList; }
                set
                {
                    _TransDetailList = value;
                    OnPropertyChanged("TransDetailList");
                }
            }

            private TransactionDetailContainer _TransactionDetailContainer;
            public TransactionDetailContainer TransactionDetailContainer
            {
                get { return _TransactionDetailContainer; }
                set
                {
                    _TransactionDetailContainer = value;
                    OnPropertyChanged("TransactionDetailContainer");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ORNoCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if(ORNoCheckBox.IsChecked == true)
            {
                ORCTLNoColumn.Visibility = Visibility.Visible;
            }
            else
            {
                ORCTLNoColumn.Visibility = Visibility.Hidden;
            }
        }
    }

}
