﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for TypeChecks.xaml
    /// </summary>
    public partial class TypeChecks : Window
    {
        public MainWindow mww;

        public TypeChecks(MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;
            FillDataGrid();
        }

        private void FillDataGrid()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/CheckTypes", "");

            if (Response.Status == "SUCCESS")
            {
                List<Models.CheckType> checkTypesList = new JavaScriptSerializer().Deserialize<List<Models.CheckType>>(Response.Content);
                CheckTypeDataGrid.ItemsSource = checkTypesList;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void ClickFuntion()
        {
            if (CheckTypeDataGrid.SelectedItem == null) return;
            var selectedCheckType = CheckTypeDataGrid.SelectedItem as Models.CheckType;

            TransactionCheck transCheck = (TransactionCheck)this.mww.COCIDataGrid.SelectedItem;
            transCheck.ClearingDays = Convert.ToByte(selectedCheckType.CheckTypeDays).ToString();
            transCheck.CheckType = Convert.ToByte(selectedCheckType.CheckTypeID);
            transCheck.CheckTypeDesc = selectedCheckType.CheckTypeDesc;
            transCheck.CheckTypeFormat = String.Format("{0} ({1}) days", selectedCheckType.CheckTypeDesc, selectedCheckType.CheckTypeDays);

            this.Close();
            this.mww.MoveToNextUIElement();
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void CheckTypeDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ClickFuntion();
        }


        private void CheckTypeDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                ClickFuntion();
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
