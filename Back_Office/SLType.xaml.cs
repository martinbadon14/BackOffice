﻿using Back_Office.Models;
using Back_Office.Models.Month_End.Special_Transactions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using static Back_Office.BMS;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for SLSelect.xaml
    /// </summary>
    public partial class SLType : Window
    {
        public Byte temp = 0;
        MainWindow mww;
        BMS bww;
        private String OtherMod = "";

        int currentRow = 0, currentColumn = 1;
        Models.SLType Selected;
        string SearchText = string.Empty;
        private ICollectionView MyData;


        public SLType(int i, MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            if (i != 0)
            {
                FillDataGrid(i);
            }
        }


        public SLType(int i, String TextBoxName, BMS bw)
        {
            InitializeComponent();
            this.bww = bw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);

            if (i != 0)
            {
                FillDataGrid(i);
            }
            OtherMod = TextBoxName;
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                if (currentRow > 0)
                {
                    try
                    {
                        int previousIndex = slTypeGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        slTypeGrid.SelectedIndex = previousIndex;
                        slTypeGrid.ScrollIntoView(slTypeGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }
                }

            }
            else if (e.Key == Key.Down)
            {
                if (currentRow < slTypeGrid.Items.Count - 1)
                {

                    try
                    {
                        int nextIndex = slTypeGrid.SelectedIndex + 1;
                        if (nextIndex > slTypeGrid.Items.Count - 1) return;
                        slTypeGrid.SelectedIndex = nextIndex;
                        slTypeGrid.ScrollIntoView(slTypeGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }

                } // end if (this.SelectedOverride > 0)

            } // end else if (e.Key == Key.Down)

            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }


        private void FillDataGrid(Object i)
        {

            string parsed = new JavaScriptSerializer().Serialize(i);
            CRUXLib.Response Response = App.CRUXAPI.request("sltype/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {
                if (Response.Content == "[]")
                {
                    MessageBox.Show("Theres no content");
                }
                else
                {
                    List<Models.SLType> sltype = new JavaScriptSerializer().Deserialize<List<Models.SLType>>(Response.Content);
                    slTypeGrid.ItemsSource = sltype;

                    MyData = CollectionViewSource.GetDefaultView(sltype);
                }
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void myDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void slTypeGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                function();
                e.Handled = true;
            }
        }


        private void function()
        {
            if (slTypeGrid.SelectedItem == null) return;
            var selectedSLT = slTypeGrid.SelectedItem as Models.SLType;

            if (String.IsNullOrEmpty(OtherMod))
            {
                TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;

                if (selectedSLT.SLTypeSLT_CODE.ToString().Length < 2)
                {
                    tt.SLT_Code = "0" + selectedSLT.SLTypeSLT_CODE.ToString();
                }
                else
                {
                    tt.SLT_Code = selectedSLT.SLTypeSLT_CODE.ToString();
                }

                this.Close();
                this.mww.DG_GeneralJournal.Focus();
                this.mww.DG_GeneralJournal.CurrentCell = new DataGridCellInfo(this.mww.DG_GeneralJournal.SelectedItem, this.mww.DG_GeneralJournal.Columns[3]);
            }
            else
            {
                SpecialTransactionDataCon bb = (SpecialTransactionDataCon)this.bww.DataCon;

                if (OtherMod == "SLTTextBox")
                {
                    if (selectedSLT.SLTypeSLT_CODE.ToString().Length < 2)
                    {
                        bb.InsertSpecialTrans.SLT = "0" + selectedSLT.SLTypeSLT_CODE.ToString();
                    }
                    else
                    {
                        bb.InsertSpecialTrans.SLT = selectedSLT.SLTypeSLT_CODE.ToString();
                    }

                    this.Close();
                    this.bww.BalTypesComboBox.Focus();
                }
                else if (OtherMod == "DebitSLTTextBox")
                {
                    if (selectedSLT.SLTypeSLT_CODE.ToString().Length < 2)
                    {
                        bb.InsertSpecialTrans.SLTDebit = "0" + selectedSLT.SLTypeSLT_CODE.ToString();
                    }
                    else
                    {
                        bb.InsertSpecialTrans.SLTDebit = selectedSLT.SLTypeSLT_CODE.ToString();
                    }

                    this.Close();
                    this.bww.CreditSLCTextBox.Focus();
                }
                else if (OtherMod == "CreditSLTTextBox")
                {
                    if (selectedSLT.SLTypeSLT_CODE.ToString().Length < 2)
                    {
                        bb.InsertSpecialTrans.SLTCredit = "0" + selectedSLT.SLTypeSLT_CODE.ToString();
                    }
                    else
                    {
                        bb.InsertSpecialTrans.SLTCredit = selectedSLT.SLTypeSLT_CODE.ToString();
                    }

                    this.Close();
                    this.bww.ExplanationTextBox.Focus();
                }
            }
        }


        private void slTypeGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTextBox.Focus();
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            MyData.Filter = FilterData;
            slTypeGrid.SelectedIndex = 0;
        }


        private bool FilterData(object item)
        {
            var value = (Models.SLType)item;

            if (value.SLTypeSLT_CODE == 0 || String.IsNullOrEmpty(value.SLTypeM_DESC2))
                return false;
            else
                return value.SLTypeM_DESC2.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.SLTypeSLT_CODE).Contains(SearchText);
        }


        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Selected = (Models.SLType)slTypeGrid.Items[currentRow];
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    this.Selected = (Models.SLType)slTypeGrid.Items[currentRow];

                    function();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }
    }
}
