﻿using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Month_End.Loan_Reclassification;
using Back_Office.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office.Month_End
{
    /// <summary>
    /// Interaction logic for LoanReclassification.xaml
    /// </summary>
    public partial class LoanReclassification : Window
    {
        public MainWindow mww;
        private readonly BackgroundWorker BasicWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateWorker = new BackgroundWorker();
        LoanReclassDataCon DataCon = new LoanReclassDataCon();

        public LoanReclassification(MainWindow mw, String DateTimeNow, Int16 UpdID)
        {
            InitializeComponent();
            InitializeDefaults();
            InitializeWorkers();

            this.mww = mw;
            DataCon.UpdID = UpdID;
            DataCon.CutOffDate = (Convert.ToDateTime(DateTimeNow)).ToString("MM/dd/yyyy");
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.DataContext = DataCon;
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Escape)
            {
                MessageBoxResult result = MessageBox.Show(this, "Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        break;

                } //end switch

            } //end If Key = Escape
            else if (e.Key == Key.F11 || (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G)))
            {
                if (generateButton.IsEnabled)
                {
                    this.Generate();
                }
            }

        }


        private void InitializeDefaults()
        {
            generateButton.IsEnabled = false;

            DataCon.SLType = new List<Models.SLType>();

            DataCon.Select = new Selections();
            DataCon.Selected = new Selections();

            DataCon.Select.SLC = new List<Models.SLClass>();
            DataCon.Selected.SLC = new List<Models.SLClass>();

            ListBoxUtility SLCUtil = new ListBoxUtility();
            ListBoxUtility SLTUtil = new ListBoxUtility();

            SLCUtil.selectTo(slclist, slcselectlist, sltlist, false, true);
            SLCUtil.selectToCallBack += new ListBoxUtility.CallbackEventHandler(selectToCallBack);
            SLCUtil.selectFromCallBack += new ListBoxUtility.CallbackEventHandler(selectFromCallBack);

            SLTUtil.selectTo(sltlist, sltselectlist);
            SLTUtil.selectToCallBack += new ListBoxUtility.CallbackEventHandler(selectToCallBack);
            SLTUtil.selectFromCallBack += new ListBoxUtility.CallbackEventHandler(selectFromCallBack);

            slclist.PreviewKeyDown += new KeyEventHandler(HandleSLCTo);
            slcselectlist.PreviewKeyDown += new KeyEventHandler(HandleSLCFrom);

            sltlist.PreviewKeyDown += new KeyEventHandler(HandleSLTTo);
            sltselectlist.PreviewKeyDown += new KeyEventHandler(HandleSLTFrom);
        }


        private void HandleSLCFrom(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                DeselectSLC();
            }
        }


        private void HandleSLCTo(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Space)
                {
                    selectSLCTo();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void HandleSLTTo(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Space)
            {
                selectSLTTo();
            }
        }


        private void HandleSLTFrom(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                DeselectSLT();
            }
        }


        private void DeselectSLC()
        {
            foreach (Models.SLClass selected in slcselectlist.SelectedItems)
            {
                Models.SLClass tpass = selected;
                tpass.IsSelected = false;


                int last = this.DataCon.Select.SLC.FindAll(t => String.Compare(t.sldescription, selected.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.SLType.FindAll(t => t.SLTypeSLC_CODE == selected.slcode))
                {
                    int rem = DataCon.SLType.IndexOf(c);
                    DataCon.SLType.RemoveAt(rem);
                }
                foreach (Models.SLType c in DataCon.SLTSelect.FindAll(t => t.SLTypeSLC_CODE == selected.slcode))
                {
                    int rem = DataCon.SLTSelect.IndexOf(c);
                    DataCon.SLTSelect.RemoveAt(rem);
                }

                this.DataCon.Selected.SLC.Remove(selected);
                this.DataCon.Select.SLC.Insert(last, tpass);
            }

            slclist.SelectedIndex = -1;
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();
            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

            if (slcselectlist.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void selectSLCTo()
        {
            foreach (Models.SLClass selected in slclist.SelectedItems)
            {
                Models.SLClass tpass = selected;
                tpass.IsSelected = false;


                int last = this.DataCon.Selected.SLC.FindAll(t => String.Compare(t.sldescription, selected.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.Hidden.SLT.FindAll(t => t.SLTypeSLC_CODE == selected.slcode))
                {
                    int lastSLT = this.DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, c.SLTypeM_DESC2) < 1).Count;
                    this.DataCon.SLType.Insert(lastSLT, c);
                }

                this.DataCon.Select.SLC.Remove(selected);
                this.DataCon.Selected.SLC.Insert(last, tpass);
            }

            slcselectlist.SelectedIndex = -1;
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();
            sltlist.Items.Refresh();

            if (slcselectlist.Items.Count != 0)
            {
                generateButton.IsEnabled = true;
            }
        }


        private void DeselectSLT()
        {
            foreach (Models.SLType selected in sltselectlist.SelectedItems)
            {
                Models.SLType tpass = selected;
                tpass.IsSelected = false;

                int last = this.DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, selected.SLTypeM_DESC2) < 1).Count;

                DataCon.SLTSelect.Remove(selected);
                DataCon.SLType.Insert(last, tpass);
            }

            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

            if (slcselectlist.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void selectSLTTo()
        {
            foreach (Models.SLType selected in sltlist.SelectedItems)
            {
                Models.SLType tpass = selected;
                tpass.IsSelected = false;

                int last = this.DataCon.Selected.SLT.FindAll(t => String.Compare(t.SLTypeM_DESC2, selected.SLTypeM_DESC2) < 1).Count;

                this.DataCon.SLType.Remove(selected);
                this.DataCon.SLTSelect.Insert(last, tpass);
            }

            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

        }


        private void selectFromCallBack(ListBox From, ListBox To, ListBox ParentOrChild, bool IsParent, bool MultiSelect)
        {
            switch (From.Name)
            {
                case "slclist": deselectSLC(From, To); break;
                case "sltlist": deselectSLT(From, To); break;
                default:
                    break;
            }
        }


        private void selectSLT(ListBox GlobalFrom, ListBox GlobalTo)
        {
            foreach (Models.SLType s in GlobalFrom.SelectedItems)
            {
                Models.SLType tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.SLTSelect.FindAll(t => String.Compare(t.SLTypeM_DESC2, s.SLTypeM_DESC2) < 1).Count;


                DataCon.SLType.Remove(s);
                DataCon.SLTSelect.Insert(last, tpass);
            }

            GlobalFrom.Items.Refresh();
            GlobalTo.Items.Refresh();
        }


        private void deselectSLT(ListBox From, ListBox To)
        {
            foreach (Models.SLType s in To.SelectedItems)
            {
                Models.SLType tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, s.SLTypeM_DESC2) < 1).Count;
                DataCon.SLTSelect.Remove(s);
                DataCon.SLType.Insert(last, tpass);
            }
            From.Items.Refresh();
            To.Items.Refresh();
        }


        private void InitializeWorkers()
        {
            BasicWorker.DoWork += BasicWorker_DoWork;
            BasicWorker.RunWorkerCompleted += BasicWorker_RunWorkerCompleted;
            BasicWorker.RunWorkerAsync();

            GenerateWorker.DoWork += GenerateWorker_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateWorker_RunWorkerCompleted;
        }


        private void GenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            LoanReclassGrid.IsEnabled = true;

            this.Close();

            switch (Process)
            {
                case 1:
                    MessageBox.Show(this, "Module Was Successfully Posted!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
                case 2:
                    MessageBox.Show(this, "Failed to execute Data.", "Failed to Retrieve Data!", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 3:
                    MessageBox.Show(this, "NO RECORD FOUND!", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
                default:
                    break;
            }
        }


        Byte Process = 0;
        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<Byte> slt = new List<byte>();

            if (DataCon.SLTSelect.Count != 0)
            {
                foreach (Models.SLType tt in this.DataCon.SLTSelect)
                {
                    slt.Add(tt.SLTypeSLT_CODE);
                }
            }

            ExecuteHelper execHelp = new ExecuteHelper(this.mww);
            String explanation = execHelp.CreateLoanReclassificationExplanation(Convert.ToDateTime(DataCon.CutOffDate).ToString("MM/dd/yyyy"));

            LoanTrans loanTrans = new LoanTrans();
            loanTrans.CutOffDate = DataCon.CutOffDate;
            loanTrans.SLC = DataCon.Selected.SLC.First().slcode;
            loanTrans.SLT = slt;
            loanTrans.Explanation = explanation;

            String parsed = new JavaScriptSerializer().Serialize(loanTrans);

            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/GenerateLoanReclass", parsed);
            if (Response.Status == "SUCCESS")
            {

                if (Response.Content.Replace("\"", "") == "Module Was Successfully Added")
                {
                    Process = 1;
                }
                else
                {
                    Process = 3;
                }
            }
            else
            {
                Process = 2;
            }
        }


        private void BasicWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            slclist.Items.Refresh();
            sltlist.Items.Refresh();
            slcselectlist.Items.Refresh();

            ProgressPanel.Visibility = Visibility.Hidden;
            cancelButton.IsEnabled = true;
        }


        private void BasicWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;

            Response = App.CRUXAPI.request("backoffice/SelectSLType", "");

            if (Response.Status == "ERROR")
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(this, Response.Content);
                }), System.Windows.Threading.DispatcherPriority.Background);
            }
            else
            {
                this.DataCon.Select.SLC.AddRange(new JavaScriptSerializer().Deserialize<List<Models.SLClass>>(Response.Content));
            }

            String parsed = new JavaScriptSerializer().Serialize(this.DataCon.Select.SLC.First().slcode);
            Response = App.CRUXAPI.request("sltype/backoffice", parsed);

            if (Response.Status == "ERROR")
            {
                MessageBox.Show(Response.Content);
            }
            else
            {
                this.DataCon.SLTSelect = new List<Models.SLType>();
                this.DataCon.Select.SLT = new List<Models.SLType>();
                this.DataCon.Selected.SLT = new List<Models.SLType>();
                this.DataCon.Hidden.SLT = new List<Models.SLType>();
                this.DataCon.Hidden.SLT = new JavaScriptSerializer().Deserialize<List<Models.SLType>>(Response.Content);
            }
        }


        private void selectSLC(ListBox GlobalFrom, ListBox GlobalTo, ListBox Child)
        {
            foreach (Models.SLClass s in GlobalFrom.SelectedItems)
            {
                Models.SLClass tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.Selected.SLC.FindAll(t => String.Compare(t.sldescription, s.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.Hidden.SLT.FindAll(t => t.SLTypeSLC_CODE == s.slcode))
                {
                    int lastSLT = this.DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, c.SLTypeM_DESC2) < 1).Count;
                    this.DataCon.SLType.Insert(lastSLT, c);
                }

                DataCon.Select.SLC.Remove(s);
                DataCon.Selected.SLC.Insert(last, tpass);
            }

            GlobalFrom.Items.Refresh();
            GlobalTo.Items.Refresh();
            Child.Items.Refresh();

            if (GlobalTo.Items.Count != 0)
            {
                generateButton.IsEnabled = true;
            }
        }


        private void deselectSLC(ListBox From, ListBox To)
        {
            foreach (Models.SLClass s in To.SelectedItems)
            {
                Models.SLClass tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.Select.SLC.FindAll(t => String.Compare(t.sldescription, s.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.SLType.FindAll(t => t.SLTypeSLC_CODE == s.slcode))
                {
                    int rem = DataCon.SLType.IndexOf(c);
                    DataCon.SLType.RemoveAt(rem);
                }
                foreach (Models.SLType c in DataCon.SLTSelect.FindAll(t => t.SLTypeSLC_CODE == s.slcode))
                {
                    int rem = DataCon.SLTSelect.IndexOf(c);
                    DataCon.SLTSelect.RemoveAt(rem);
                }

                DataCon.Selected.SLC.Remove(s);
                DataCon.Select.SLC.Insert(last, tpass);
            }

            From.Items.Refresh();
            To.Items.Refresh();
            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

            if (To.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void selectToCallBack(ListBox From, ListBox To, ListBox ParentOrChild, bool IsParent, bool MultiSelect)
        {
            To.SelectedIndex = -1;
            switch (From.Name)
            {
                case "slclist": selectSLC(From, To, ParentOrChild); break;
                case "sltlist": selectSLT(From, To); break;
            }
        }


        private void Generate()
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    if (!String.IsNullOrEmpty(DataCon.CutOffDate))
                    {
                        ProgressPanel.Visibility = Visibility.Visible;
                        LoanReclassGrid.IsEnabled = false;
                        GenerateWorker.RunWorkerAsync();
                    }
                    else
                    {
                        MessageBox.Show(this, "Please enter Cut-Off Date", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        CutOffDatePicker.Focus();
                    }
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }
        }


        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Generate();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public class LoanReclassDataCon : INotifyPropertyChanged
        {
            private Int16 _UpdID;
            public Int16 UpdID
            {
                get { return _UpdID; }
                set
                {
                    _UpdID = value;
                    OnPropertyChanged("UpdID");
                }
            }

            private String _Explanation;
            public String Explanation
            {
                get { return _Explanation; }
                set
                {
                    _Explanation = value;
                    OnPropertyChanged("Explanation");
                }
            }

            private String _CutOffDate;
            public String CutOffDate
            {
                get { return _CutOffDate; }
                set
                {
                    _CutOffDate = value;
                    OnPropertyChanged("CutOffDate");
                }
            }

            private List<Models.SLType> _SLType;
            public List<Models.SLType> SLType
            {
                get { return _SLType; }
                set
                {
                    _SLType = value;
                    OnPropertyChanged("SLType");
                }
            }

            private List<Models.SLType> _SLTSelect;
            public List<Models.SLType> SLTSelect
            {
                get
                {
                    return _SLTSelect;
                }
                set
                {
                    _SLTSelect = value;
                    OnPropertyChanged("SLTSelect");
                }
            }

            Selections _Select = new Selections();
            public Selections Select
            {
                get
                {
                    return _Select;
                }
                set
                {
                    _Select = value;
                    OnPropertyChanged("Select");
                }
            }

            Selections _Selected = new Selections();
            public Selections Selected
            {
                get
                {
                    return _Selected;
                }
                set
                {
                    _Selected = value;
                    OnPropertyChanged("Selected");
                }
            }

            Selections _Hidden = new Selections();
            public Selections Hidden
            {
                get
                {
                    return _Hidden;
                }
                set
                {
                    _Hidden = value;
                    OnPropertyChanged("Hidden");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    Close();
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            } //end switch
        }


        public class LoanTrans
        {
            public Byte SLC { get; set; }
            public List<Byte> SLT { get; set; }
            public String CutOffDate { get; set; }
            public String Explanation { get; set; }
        }

    }
}
