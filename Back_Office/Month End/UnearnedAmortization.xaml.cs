﻿using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Month_End.Loan_Reclassification;
using Back_Office.Models.Year_End;
using Back_Office.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Back_Office.Month_End.LoanReclassification;

namespace Back_Office.Month_End
{
    /// <summary>
    /// Interaction logic for UnearnedAmortization.xaml
    /// </summary>
    public partial class UnearnedAmortization : Window
    {
        MainWindow mww;
        UnearnedInterestDataCon DataCon = new UnearnedInterestDataCon();
        BackgroundWorker BasicWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateLoanDuesWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateTempWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateFinalWorker = new BackgroundWorker();

        public UnearnedAmortization(MainWindow mw, String DateTimeNow)
        {
            InitializeComponent();
            DataCon.CutOffDate = (Convert.ToDateTime(DateTimeNow)).ToString("MM/dd/yyyy");

            InitializeDefaults();
            InitializeWorkers();

            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.DataContext = DataCon;

            Thread.Sleep(50);
        }

        private void CloseForm()
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    Close();
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }
        }

        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.CloseForm();
            }
            else if (e.Key == Key.F11 || (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G)))
            {
                if (generateButton.IsEnabled)
                {
                    this.Generate();
                }
            }
        }

        private void InitializeDefaults()
        {
            this.DataCon.progress = "";
            this.DataCon.processing = "";

            generateButton.IsEnabled = false;

            DataCon.EarnAllCheckBox = true;
            DataCon.EarnDueCheckBox = false;
            DataCon.EarnAll = EarnAllCheckBox.Content.ToString();
            DataCon.EarnDue = EarnActualDueCheckBox.Content.ToString();

            DataCon.SLType = new List<Models.SLType>();

            DataCon.Select = new Selections();
            DataCon.Selected = new Selections();

            DataCon.Select.SLC = new List<Models.SLClass>();
            DataCon.Selected.SLC = new List<Models.SLClass>();

            ListBoxUtility SLCUtil = new ListBoxUtility();
            ListBoxUtility SLTUtil = new ListBoxUtility();

            SLCUtil.selectTo(slclist, slcselectlist, sltlist, false, true);
            SLCUtil.selectToCallBack += new ListBoxUtility.CallbackEventHandler(selectToCallBack);
            SLCUtil.selectFromCallBack += new ListBoxUtility.CallbackEventHandler(selectFromCallBack);

            SLTUtil.selectTo(sltlist, sltselectlist);
            SLTUtil.selectToCallBack += new ListBoxUtility.CallbackEventHandler(selectToCallBack);
            SLTUtil.selectFromCallBack += new ListBoxUtility.CallbackEventHandler(selectFromCallBack);

            slclist.PreviewKeyDown += new KeyEventHandler(HandleSLCTo);
            slcselectlist.PreviewKeyDown += new KeyEventHandler(HandleSLCFrom);

            sltlist.PreviewKeyDown += new KeyEventHandler(HandleSLTTo);
            sltselectlist.PreviewKeyDown += new KeyEventHandler(HandleSLTFrom);
        }


        private void HandleSLTFrom(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                DeselectSLT();
            }
        }


        private void DeselectSLT()
        {
            foreach (Models.SLType selected in sltselectlist.SelectedItems)
            {
                Models.SLType tpass = selected;
                tpass.IsSelected = false;

                int last = this.DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, selected.SLTypeM_DESC2) < 1).Count;

                DataCon.SLTSelect.Remove(selected);
                DataCon.SLType.Insert(last, tpass);
            }

            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

            if (slcselectlist.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void HandleSLTTo(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Space)
            {
                selectSLTTo();
            }
        }


        private void selectSLTTo()
        {
            foreach (Models.SLType selected in sltlist.SelectedItems)
            {
                Models.SLType tpass = selected;
                tpass.IsSelected = false;

                int last = this.DataCon.Selected.SLT.FindAll(t => String.Compare(t.SLTypeM_DESC2, selected.SLTypeM_DESC2) < 1).Count;

                this.DataCon.SLType.Remove(selected);
                this.DataCon.SLTSelect.Insert(last, tpass);
            }

            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

        }


        private void HandleSLCFrom(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                DeselectSLC();
            }
        }


        private void DeselectSLC()
        {
            foreach (Models.SLClass selected in slcselectlist.SelectedItems)
            {
                Models.SLClass tpass = selected;
                tpass.IsSelected = false;


                int last = this.DataCon.Select.SLC.FindAll(t => String.Compare(t.sldescription, selected.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.SLType.FindAll(t => t.SLTypeSLC_CODE == selected.slcode))
                {
                    int rem = DataCon.SLType.IndexOf(c);
                    DataCon.SLType.RemoveAt(rem);
                }
                foreach (Models.SLType c in DataCon.SLTSelect.FindAll(t => t.SLTypeSLC_CODE == selected.slcode))
                {
                    int rem = DataCon.SLTSelect.IndexOf(c);
                    DataCon.SLTSelect.RemoveAt(rem);
                }

                this.DataCon.Selected.SLC.Remove(selected);
                this.DataCon.Select.SLC.Insert(last, tpass);
            }

            slclist.SelectedIndex = -1;
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();
            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

            if (slcselectlist.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }


        private void HandleSLCTo(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Space)
                {
                    selectSLCTo();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void selectSLCTo()
        {
            foreach (Models.SLClass selected in slclist.SelectedItems)
            {
                Models.SLClass tpass = selected;
                tpass.IsSelected = false;


                int last = this.DataCon.Selected.SLC.FindAll(t => String.Compare(t.sldescription, selected.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.Hidden.SLT.FindAll(t => t.SLTypeSLC_CODE == selected.slcode))
                {
                    int lastSLT = this.DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, c.SLTypeM_DESC2) < 1).Count;
                    this.DataCon.SLType.Insert(lastSLT, c);
                }

                this.DataCon.Select.SLC.Remove(selected);
                this.DataCon.Selected.SLC.Insert(last, tpass);
            }

            slcselectlist.SelectedIndex = -1;
            slclist.Items.Refresh();
            slcselectlist.Items.Refresh();
            sltlist.Items.Refresh();

            if (slcselectlist.Items.Count != 0)
            {
                generateButton.IsEnabled = true;
            }
        }


        private void selectFromCallBack(ListBox From, ListBox To, ListBox ParentOrChild, bool IsParent, bool MultiSelect)
        {
            switch (From.Name)
            {
                case "slclist": deselectSLC(From, To); break;
                case "sltlist": deselectSLT(From, To); break;
                default:
                    break;
            }
        }


        private void deselectSLT(ListBox From, ListBox To)
        {
            foreach (Models.SLType s in To.SelectedItems)
            {
                Models.SLType tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, s.SLTypeM_DESC2) < 1).Count;
                DataCon.SLTSelect.Remove(s);
                DataCon.SLType.Insert(last, tpass);
            }
            From.Items.Refresh();
            To.Items.Refresh();
        }


        private void InitializeWorkers()
        {
            ProgressPanel.Visibility = Visibility.Visible;

            BasicWorker.DoWork += BasicWorker_DoWork;
            BasicWorker.RunWorkerCompleted += BasicWorker_RunWorkerCompleted;
            BasicWorker.RunWorkerAsync();

            GenerateWorker.DoWork += GenerateWorker_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateWorker_RunWorkerCompleted;

            GenerateLoanDuesWorker.DoWork += GenerateLoanDuesWorker_DoWork;
            GenerateLoanDuesWorker.RunWorkerCompleted += GenerateLoanDuesWorker_RunWorkerCompleted;

            GenerateTempWorker.DoWork += GenerateTempWorker_DoWork;
            GenerateTempWorker.RunWorkerCompleted += GenerateTempWorker_RunWorkerCompleted;

            GenerateFinalWorker.DoWork += GenerateFinalWorker_DoWork;
            GenerateFinalWorker.RunWorkerCompleted += GenerateFinalWorker_RunWorkerCompleted;
        }

        private void GenerateTempWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            switch (AfterLoanDues)
            {
                case 1:
                    this.DataCon.processing = "Journalizing Entries...";
                    GenerateFinalWorker.RunWorkerAsync();
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }

        Byte AfterLoanDues = 0;

        private void GenerateTempWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool pass = true;
            while (pass)
            {
                if (!GenerateLoanDuesWorker.IsBusy && !GenerateWorker.IsBusy)
                {
                    try
                    {
                        AfterLoanDues = 1;
                        pass = false;
                    }
                    catch (Exception)
                    {

                    }
                }

                Thread.Sleep(50);
            } // end while
        }


        private void GenerateFinalWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UIGrid.IsEnabled = true;
            processBlock.Visibility = Visibility.Hidden;
            progressBlock.Visibility = Visibility.Hidden;

            MessageBox.Show(this, "Module Was Successfully Added!", "Successfully Completed!", MessageBoxButton.OK, MessageBoxImage.Information);
            Close();

        }

        private void GenerateFinalWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            StreamReader res;

            ExecuteHelper execHelp = new ExecuteHelper(this.mww);

            LoanTrans loanTrans = new LoanTrans();
            loanTrans.CutOffDate = DataCon.CutOffDate;

            String checkBoxContent = "";

            if (DataCon.EarnAllCheckBox && !DataCon.EarnDueCheckBox)
            {
                checkBoxContent = DataCon.EarnAll;
            }
            else if (DataCon.EarnDueCheckBox && !DataCon.EarnAllCheckBox)
            {
                checkBoxContent = DataCon.EarnDue;
            }

            loanTrans.Explanation = execHelp.CreateUnearnedAmortizationExplanation(Convert.ToDateTime(DataCon.CutOffDate).ToString("yyyyMMdd"), checkBoxContent);

            String parsed = new JavaScriptSerializer().Serialize(loanTrans);

            res = App.CRUXAPI.requestBIG("backoffice/GenerateUnearnedInterestFinalStream", parsed, 7200000);

            int i = 0;
            int count = 0;

            String tmp = "", tmp3 = "";
            DateTime starttime = DateTime.Now;

            while (i <= count)
            {

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    try
                    {
                        tmp = res.ReadLine();
                        string tmp2 = tmp.Replace("\\\"", "\"");
                        tmp3 = tmp2.Remove(tmp2.Length - 1);
                    }
                    catch (Exception ex)
                    {

                    }

                    if (i == 0)
                    {
                        count = Convert.ToInt32(tmp);
                        Console.WriteLine(tmp);
                        i++;
                    }
                    else
                    {

                        ClientFinalParams balrow = serializer.Deserialize<ClientFinalParams>(tmp3.Substring(1, tmp3.Length - 1));

                        if (i <= count)
                        {
                            this.DataCon.processing = "Journalizing " + i + " of " + count + " AccountID: " + balrow.ClientID + " Account Name: " + balrow.ClientName;
                            this.progressPercent(i, count, starttime);

                        }
                        else
                        {
                            this.DataCon.processing = "Finalizing Journal";
                        }

                        i++;
                    }

                }
                catch (Exception ex)
                {

                }
            }

            res.Close();
            res.Dispose();
            Thread.Sleep(300);
        }

        private void GenerateLoanDuesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GenerateLoanDuesWorker.WorkerReportsProgress = false;
        }


        private void GenerateLoanDuesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                LoansRequestParameters param = new LoansRequestParameters();

                if (!String.IsNullOrEmpty(this.DataCon.CutOffDate))
                {
                    param.Cutoff = this.DataCon.CutOffDate;
                    param.ClientID = "";
                    param.Company = "";

                    var json = new JavaScriptSerializer().Serialize(param);
                    StreamReader resLoanDues = App.CRUXAPI.requestBIG("loanreports/streamduesOver", json, 7200000);

                    int i = 0;
                    int count = 0;
                    DateTime starttime = DateTime.Now;

                    while (i <= count)
                    {
                        try
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            serializer.MaxJsonLength = 999999999;
                            string tmp = resLoanDues.ReadLine();
                            string tmp2 = tmp.Replace("\\\"", "\"");
                            string tmp3 = tmp2.Remove(tmp2.Length - 1);
                            //Console.WriteLine(i);

                            if (i == 0)
                            {
                                count = Convert.ToInt32(tmp);
                                Console.WriteLine(tmp);
                            }
                            else
                            {
                                this.progressPercent(i, count, starttime);
                            }

                            i++;
                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Transaction Date needed.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Thread.Sleep(500);
        }


        private void progressPercent(int i, int count, DateTime starttime)
        {
            Int32 seconds = Convert.ToInt32(((DateTime.Now - starttime).TotalSeconds / i) * (count - i));

            this.DataCon.progress = " ";
            this.DataCon.progress += Math.Round((Convert.ToDecimal(i) / count) * 100, 2);
            this.DataCon.progress += "% ";


            if (seconds > 3600)
            {
                this.DataCon.progress += Convert.ToInt32(seconds / 3600) + " Hours ";
            }
            if (seconds > 60)
            {
                Int32 minutes = (seconds / 60);
                this.DataCon.progress += (minutes % 60) + " Minutes ";
            }
            if (seconds % 60 > 0)
            {
                this.DataCon.progress += (seconds % 60) + " Seconds ";
            }

            this.DataCon.progress += "Remaining";
        }

        Int16 UIInsert = 0;

        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<Byte> slt = new List<byte>();

                if (DataCon.SLTSelect.Count != 0)
                {
                    foreach (Models.SLType tt in this.DataCon.SLTSelect)
                    {
                        slt.Add(tt.SLTypeSLT_CODE);
                    }
                }

                ExecuteHelper execHelp = new ExecuteHelper(this.mww);

                LoanTrans loanTrans = new LoanTrans();
                loanTrans.CutOffDate = DataCon.CutOffDate;
                loanTrans.SLC = DataCon.Selected.SLC.First().slcode;
                loanTrans.SLT = slt;

                String checkBoxContent = "";

                if (DataCon.EarnAllCheckBox && !DataCon.EarnDueCheckBox)
                {
                    checkBoxContent = DataCon.EarnAll;
                }
                else if (DataCon.EarnDueCheckBox && !DataCon.EarnAllCheckBox)
                {
                    checkBoxContent = DataCon.EarnDue;
                }

                loanTrans.Explanation = execHelp.CreateUnearnedAmortizationExplanation(Convert.ToDateTime(DataCon.CutOffDate).ToString("yyyyMMdd"), checkBoxContent);

                String parsed = new JavaScriptSerializer().Serialize(loanTrans);

                // CRUXLib.Response Response = App.CRUXAPI.request("backoffice/GenerateUnearnedInterest", parsed, 720000);
                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/GenerateUnearnedInterestStream", parsed, 720000);
                if (Response.Status != "SUCCESS")
                {
                    UIInsert = 1;
                }
            }
            catch (Exception)
            {
                UIInsert = 1;
            }
        }

        private void GenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GenerateWorker.WorkerReportsProgress = false;

            switch (UIInsert)
            {
                case 1:
                    MessageBox.Show(this, "Failed to insert Data. Please contact Administrator.", "Module not saved", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                default:
                    break;
            }
        }

        private void BasicWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            slclist.Items.Refresh();
            sltlist.Items.Refresh();
            slcselectlist.Items.Refresh();

            ProgressPanel.Visibility = Visibility.Hidden;
            //generateButton.IsEnabled = true;
            cancelButton.IsEnabled = true;
        }

        private void BasicWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CRUXLib.Response Response;

            try
            {
                Response = App.CRUXAPI.request("backoffice/SelectSLType", "");

                if (Response.Status == "ERROR")
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MessageBox.Show(this, Response.Content);
                    }), System.Windows.Threading.DispatcherPriority.Background);
                }
                else
                {
                    this.DataCon.Select.SLC.AddRange(new JavaScriptSerializer().Deserialize<List<Models.SLClass>>(Response.Content));
                }

                String parsed = new JavaScriptSerializer().Serialize(this.DataCon.Select.SLC.First().slcode);
                Response = App.CRUXAPI.request("sltype/backoffice", parsed);

                if (Response.Status == "ERROR")
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MessageBox.Show(this, Response.Content);
                    }), System.Windows.Threading.DispatcherPriority.Background);
                }
                else
                {
                    this.DataCon.SLTSelect = new List<Models.SLType>();
                    this.DataCon.Select.SLT = new List<Models.SLType>();
                    this.DataCon.Selected.SLT = new List<Models.SLType>();
                    this.DataCon.Hidden.SLT = new List<Models.SLType>();
                    this.DataCon.Hidden.SLT = new JavaScriptSerializer().Deserialize<List<Models.SLType>>(Response.Content);
                }
            }
            catch (Exception)
            {
                this.Dispatcher.BeginInvoke((Action)(() =>
                {
                    MessageBox.Show(this, "Connection Lost. Please Try Again.");
                }), System.Windows.Threading.DispatcherPriority.Background);
            }

        }

        private void deselectSLC(ListBox From, ListBox To)
        {
            foreach (Models.SLClass s in To.SelectedItems)
            {
                Models.SLClass tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.Select.SLC.FindAll(t => String.Compare(t.sldescription, s.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.SLType.FindAll(t => t.SLTypeSLC_CODE == s.slcode))
                {
                    int rem = DataCon.SLType.IndexOf(c);
                    DataCon.SLType.RemoveAt(rem);
                }
                foreach (Models.SLType c in DataCon.SLTSelect.FindAll(t => t.SLTypeSLC_CODE == s.slcode))
                {
                    int rem = DataCon.SLTSelect.IndexOf(c);
                    DataCon.SLTSelect.RemoveAt(rem);
                }

                DataCon.Selected.SLC.Remove(s);
                DataCon.Select.SLC.Insert(last, tpass);
            }

            From.Items.Refresh();
            To.Items.Refresh();
            sltlist.Items.Refresh();
            sltselectlist.Items.Refresh();

            if (To.Items.Count == 0)
            {
                generateButton.IsEnabled = false;
            }
        }

        private void selectToCallBack(ListBox From, ListBox To, ListBox ParentOrChild, bool IsParent, bool MultiSelect)
        {
            To.SelectedIndex = -1;
            switch (From.Name)
            {
                case "slclist": selectSLC(From, To, ParentOrChild); break;
                case "sltlist": selectSLT(From, To); break;
            }
        }

        private void selectSLT(ListBox GlobalFrom, ListBox GlobalTo)
        {
            foreach (Models.SLType s in GlobalFrom.SelectedItems)
            {
                Models.SLType tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.SLTSelect.FindAll(t => String.Compare(t.SLTypeM_DESC2, s.SLTypeM_DESC2) < 1).Count;


                DataCon.SLType.Remove(s);
                DataCon.SLTSelect.Insert(last, tpass);
            }

            GlobalFrom.Items.Refresh();
            GlobalTo.Items.Refresh();
        }

        private void selectSLC(ListBox GlobalFrom, ListBox GlobalTo, ListBox Child)
        {
            foreach (Models.SLClass s in GlobalFrom.SelectedItems)
            {
                Models.SLClass tpass = s;
                tpass.IsSelected = false;
                int last = DataCon.Selected.SLC.FindAll(t => String.Compare(t.sldescription, s.sldescription) < 1).Count;

                foreach (Models.SLType c in DataCon.Hidden.SLT.FindAll(t => t.SLTypeSLC_CODE == s.slcode))
                {
                    int lastSLT = this.DataCon.SLType.FindAll(t => String.Compare(t.SLTypeM_DESC2, c.SLTypeM_DESC2) < 1).Count;
                    this.DataCon.SLType.Insert(lastSLT, c);
                }

                DataCon.Select.SLC.Remove(s);
                DataCon.Selected.SLC.Insert(last, tpass);
            }

            GlobalFrom.Items.Refresh();
            GlobalTo.Items.Refresh();
            Child.Items.Refresh();

            if (GlobalTo.Items.Count != 0)
            {
                generateButton.IsEnabled = true;
            }
        }

        public class UnearnedInterestDataCon : INotifyPropertyChanged
        {
            private Int16 _UpdID;
            public Int16 UpdID
            {
                get { return _UpdID; }
                set
                {
                    _UpdID = value;
                    OnPropertyChanged("UpdID");
                }
            }

            private String _Explanation;
            public String Explanation
            {
                get { return _Explanation; }
                set
                {
                    _Explanation = value;
                    OnPropertyChanged("Explanation");
                }
            }


            private String _EarnAll;
            public String EarnAll
            {
                get { return _EarnAll; }
                set
                {
                    _EarnAll = value;
                    OnPropertyChanged("EarnAll");
                }
            }

            private String _EarnDue;
            public String EarnDue
            {
                get { return _EarnDue; }
                set
                {
                    _EarnDue = value;
                    OnPropertyChanged("EarnDue");
                }
            }

            private Boolean _EarnAllCheckBox;
            public Boolean EarnAllCheckBox
            {
                get { return _EarnAllCheckBox; }
                set
                {
                    _EarnAllCheckBox = value;
                    OnPropertyChanged("EarnAllCheckBox");
                }
            }

            private Boolean _EarnDueCheckBox;
            public Boolean EarnDueCheckBox
            {
                get { return _EarnDueCheckBox; }
                set
                {
                    _EarnDueCheckBox = value;
                    OnPropertyChanged("EarnDueCheckBox");
                }
            }

            private String _CutOffDate;
            public String CutOffDate
            {
                get { return _CutOffDate; }
                set
                {
                    _CutOffDate = value;
                    OnPropertyChanged("CutOffDate");
                }
            }

            private List<Models.SLType> _SLType;
            public List<Models.SLType> SLType
            {
                get { return _SLType; }
                set
                {
                    _SLType = value;
                    OnPropertyChanged("SLType");
                }
            }

            private List<Models.SLType> _SLTSelect;
            public List<Models.SLType> SLTSelect
            {
                get
                {
                    return _SLTSelect;
                }
                set
                {
                    _SLTSelect = value;
                    OnPropertyChanged("SLTSelect");
                }
            }

            Selections _Select = new Selections();
            public Selections Select
            {
                get
                {
                    return _Select;
                }
                set
                {
                    _Select = value;
                    OnPropertyChanged("Select");
                }
            }

            Selections _Selected = new Selections();
            public Selections Selected
            {
                get
                {
                    return _Selected;
                }
                set
                {
                    _Selected = value;
                    OnPropertyChanged("Selected");
                }
            }

            Selections _Hidden = new Selections();
            public Selections Hidden
            {
                get
                {
                    return _Hidden;
                }
                set
                {
                    _Hidden = value;
                    OnPropertyChanged("Hidden");
                }
            }

            String _progress;
            public String progress
            {
                get { return _progress; }
                set
                {
                    _progress = value;
                    OnPropertyChanged("progress");
                }
            }

            String _processing;
            public String processing
            {
                get
                {
                    return _processing;
                }
                set
                {
                    _processing = value;
                    OnPropertyChanged("processing");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            this.Generate();
        }

        /// <summary>
        /// Process Unearned Amortization
        /// </summary>
        private void Generate()
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    if (!String.IsNullOrEmpty(DataCon.CutOffDate))
                    {
                        processBlock.Visibility = Visibility.Visible;
                        progressBlock.Visibility = Visibility.Visible;
                        this.DataCon.processing = "Loading Interest Over...";

                        UIGrid.IsEnabled = false;
                        GenerateLoanDuesWorker.RunWorkerAsync();
                        GenerateWorker.RunWorkerAsync();
                        GenerateTempWorker.RunWorkerAsync();
                    }
                    else
                    {
                        MessageBox.Show(this, "Please enter Cut-Off Date", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        CutOffDatePicker.Focus();
                    }
                    break;
                case MessageBoxResult.No:
                    break;
                default:
                    break;
            }

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.CloseForm();
        }
    }
}
