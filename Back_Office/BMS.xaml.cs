﻿using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Month_End.Special_Transactions;
using Back_Office.Models.Year_End.AnnualDues;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for BMS.xaml
    /// </summary>
    public partial class BMS : Window
    {
        public MainWindow mww;
        SLClass slClass;
        SLType slType;

        String transType = "";
        public SpecialTransactionDataCon DataCon = new SpecialTransactionDataCon();
        private readonly BackgroundWorker GenerateWorker = new BackgroundWorker();
        private readonly BackgroundWorker GenerateADWorker = new BackgroundWorker();
        private static Int16 Insert = 0;

        public BMS(MainWindow mw, String TransType, String DateTimeNow)
        {
            InitializeComponent();

            DataCon.AsOffDate = (Convert.ToDateTime(DateTimeNow)).ToString("MM/dd/yyyy");
            DataCon.TransDate = (Convert.ToDateTime(DateTimeNow)).ToString("MM/dd/yyyy");

            InitializedDefaults(TransType);
            InitializedWorkers();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);

            this.DataContext = DataCon;
        }


        private void SetFocus()
        {
            if (DataCon.InsertSpecialTrans.SpecialType == "BMS" ||
                DataCon.InsertSpecialTrans.SpecialType == "AD" ||
                DataCon.InsertSpecialTrans.SpecialType == "SE")
            {
                SLTTextBox.Focus();
            }
            else if (DataCon.InsertSpecialTrans.SpecialType == "DPM")
            {
                ExplanationTextBox.Focus();
            }
            else if (DataCon.InsertSpecialTrans.SpecialType == "MO")
            {
                ExplanationTextBox.Focus();
            }
        }


        private void InitializedWorkers()
        {
            GenerateWorker.DoWork += GenerateWorker_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateWorker_RunWorkerCompleted;

            GenerateADWorker.DoWork += GenerateADWorker_DoWork;
            GenerateADWorker.RunWorkerCompleted += GenerateADWorker_RunWorkerCompleted;
        }


        private void progressPercent(int i, int count, DateTime starttime)
        {
            Int32 seconds = Convert.ToInt32(((DateTime.Now - starttime).TotalSeconds / i) * (count - i));

            this.DataCon.progress = " ";
            this.DataCon.progress += Math.Round((Convert.ToDecimal(i) / count) * 100, 2);
            this.DataCon.progress += "% ";


            if (seconds > 3600)
            {
                this.DataCon.progress += Convert.ToInt32(seconds / 3600) + " Hours ";
            }
            if (seconds > 60)
            {
                Int32 minutes = (seconds / 60);
                this.DataCon.progress += (minutes % 60) + " Minutes ";
            }
            if (seconds % 60 > 0)
            {
                this.DataCon.progress += (seconds % 60) + " Seconds ";
            }

            this.DataCon.progress += "Remaining";
        }


        private void GenerateADWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(this, "Module Was Successfully Added!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);

            processBlock.Visibility = Visibility.Hidden;
            progressBlock.Visibility = Visibility.Hidden;
            this.Close();
        }


        private void GenerateADWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            StreamReader res;

            String parsed = new JavaScriptSerializer().Serialize(this.DataCon);
            res = App.CRUXAPI.requestBIG("backoffice/ComputeAnnualAndMortuary", parsed, int.MaxValue);

            int i = 0;
            int count = 0;
            DateTime starttime = DateTime.Now;
            while (i <= count)
            {

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = 999999999;
                    string tmp = res.ReadLine();
                    string tmp2 = tmp.Replace("\\\"", "\"");
                    string tmp3 = tmp2.Remove(tmp2.Length - 1);

                    if (i == 0)
                    {
                        count = Convert.ToInt32(tmp);
                        Console.WriteLine(tmp);
                        i++;
                    }
                    else
                    {

                        ADClientInfo Avgrow = serializer.Deserialize<ADClientInfo>(tmp3.Substring(1, tmp3.Length - 1));

                        if (i <= count)
                        {
                            this.DataCon.processing = "Processing " + i + " of " + count + " AccountID: " + Avgrow.ClientID + " Account Name: " + Avgrow.ClientName;
                            this.progressPercent(i, count, starttime);
                        }
                        else
                        {
                            this.DataCon.processing = "Loading... ";
                        }

                        i++;

                    }

                }
                catch (Exception ex)
                {

                }

            }

            res.Close();
            res.Dispose();
            Thread.Sleep(300);
        }


        private void GenerateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            SpecialTransGrid.IsEnabled = true;

            switch (Insert)
            {
                case 1:

                    MessageBox.Show(this, "Module Was Successfully Added!", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);

                    this.Close();
                    break;
                case 2:
                    MessageBox.Show(this, "Failed to insert Data. Please contact Administrator.", "Module not saved", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 3:

                    MessageBox.Show(this, "NO RECORD FOUND!", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.Close();
                    break;

                case 4:

                    GenerateADWorker.RunWorkerAsync();

                    break;
                default:
                    break;
            }
        }


        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLCDebit) && !String.IsNullOrEmpty(this.DataCon.InsertSpecialTrans.SLTDebit) &&
              !String.IsNullOrEmpty(this.DataCon.InsertSpecialTrans.SLAcctCode.ToString()) && !String.IsNullOrEmpty(this.DataCon.InsertSpecialTrans.SLCreditGLCode.ToString()))
            {
                this.DataCon.InsertSpecialTrans.GLAcctCode = this.DataCon.InsertSpecialTrans.SLCreditGLCode;
            }
            else
            {
                this.DataCon.InsertSpecialTrans.GLAcctCode = this.DataCon.InsertSpecialTrans.SLAcctCode;
            }

            if (DataCon.InsertSpecialTrans.SpecialType == "AD" || DataCon.InsertSpecialTrans.SpecialType == "MO")
            {
                Insert = 4;
            }
            else
            {
                String parsed = new JavaScriptSerializer().Serialize(this.DataCon);

                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/ComputeBMS", parsed, 999999999);
                if (Response.Status == "SUCCESS")
                {
                    if (Response.Content == "NO RECORD FOUND!")
                    {
                        Insert = 3;
                    }
                    else if (Response.Content == "Module Was Successfully Added")
                    {
                        Insert = 1;
                    }
                    else
                    {
                        Insert = 2;

                    }
                }
                else
                {
                    this.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        MessageBox.Show(this, Response.Content);
                    }), System.Windows.Threading.DispatcherPriority.Background);
                }
            }

        }


        private void InitializedDefaults(String TransType)
        {
            this.DataCon.progress = "";
            this.DataCon.processing = "";

            DataCon.InsertSpecialTrans = new InsertSpecialTransaction();
            DataCon.InsertSpecialTrans.SpecialType = TransType;

            loadTransactionTypeSpecialTrans(TransType);
            loadSpecialTransTypes();
            loadBalance();
            loadDefaultTransaction(TransType);

            if (DataCon.InsertSpecialTrans.SpecialType == "BMS")
            {
                PMESComboBox.IsEnabled = false;
                PMESDatePicker.IsEnabled = false;

                UseBalanceCheckBox.IsChecked = true;
                PostSkipSLCheckBox.IsChecked = true;
                SLRadioButton.IsChecked = true;

                PostNewSLCheckBox.Visibility = Visibility.Visible;
                SkipClientSLCheckBox.Visibility = Visibility.Hidden;

                this.DataCon.InsertSpecialTrans.SLC = "22";
            }
            else if (DataCon.InsertSpecialTrans.SpecialType == "DPM")
            {
                UseBalanceCheckBox.IsChecked = true;

                PMESComboBox.IsEnabled = true;
                PMESDatePicker.IsEnabled = true;
                SkipClientSLCheckBox.IsChecked = true;

                PostNewSLCheckBox.Visibility = Visibility.Hidden;
                SkipClientSLCheckBox.Visibility = Visibility.Visible;

                this.DataCon.Explanation = CreateProbiExplanation(this.DataCon.InsertSpecialTrans.SLDescription);
            }
            else if (DataCon.InsertSpecialTrans.SpecialType == "AD")
            {
                PMESComboBox.IsEnabled = false;
                PMESDatePicker.IsEnabled = false;

                SLRadioButton.IsChecked = true;
                UseBalanceCheckBox.IsChecked = true;
                PostNewSLCheckBox.IsChecked = true;

                PostNewSLCheckBox.Visibility = Visibility.Visible;
                SkipClientSLCheckBox.Visibility = Visibility.Hidden;

                this.DataCon.InsertSpecialTrans.SLC = "31";
            }
            else if (DataCon.InsertSpecialTrans.SpecialType == "MO")
            {
                UseBalanceCheckBox.IsChecked = true;
                PostNewSLCheckBox.IsChecked = true;

                PMESComboBox.IsEnabled = true;
                PMESDatePicker.IsEnabled = true;

                PostNewSLCheckBox.Visibility = Visibility.Visible;
                SkipClientSLCheckBox.Visibility = Visibility.Hidden;
                TransDateLabel.Content = "Cut-Off Date: ";
                TransDateLabel.HorizontalContentAlignment = HorizontalAlignment.Right;
                this.DataCon.InsertSpecialTrans.LastTransDate = "2017/03/03";
                this.DataCon.Explanation = CreateMOExplanation(this.DataCon.InsertSpecialTrans.SLDescription);
            }
            else if (DataCon.InsertSpecialTrans.SpecialType == "SE")
            {
                PMESComboBox.IsEnabled = false;
                PMESDatePicker.IsEnabled = false;

                UseBalanceCheckBox.IsChecked = true;
                PostSkipSLCheckBox.IsChecked = true;
                SLRadioButton.IsChecked = true;

                PostNewSLCheckBox.Visibility = Visibility.Visible;
                SkipClientSLCheckBox.Visibility = Visibility.Hidden;

                this.DataCon.InsertSpecialTrans.SLC = "22";
            }

            DisableCheckBoxes();
        }


        private void DisableCheckBoxes()
        {
            UseBalanceCheckBox.IsEnabled = false;
            SkipClientCheckBox.IsEnabled = false;
            BothCheckBox.IsEnabled = false;

            PostNewSLCheckBox.IsEnabled = false;
            PostSkipSLCheckBox.IsEnabled = false;
            PostNewSkipSLCheckBox.IsEnabled = false;
            PostAlwaysSLCheckBox.IsEnabled = false;

            SkipClientSLCheckBox.IsEnabled = false;

            AllClientsRadioButton.IsEnabled = false;
            SLRadioButton.IsEnabled = false;
        }


        public class BMSParams
        {
            public Int16 SLC { get; set; }
            public Int16 SLT { get; set; }
            public String TransType { get; set; }
        }


        private void AddZeroToSLT()
        {

            if (!String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLTDebit))
            {
                if (DataCon.InsertSpecialTrans.SLTDebit.ToString().Length < 2)
                {
                    DataCon.InsertSpecialTrans.SLTDebit = "0" + DataCon.InsertSpecialTrans.SLTDebit;
                }
                else if (DataCon.InsertSpecialTrans.SLTDebit.ToString().Length == 2)
                {
                    DataCon.InsertSpecialTrans.SLTDebit = DataCon.InsertSpecialTrans.SLTDebit;
                }
            }

            if (!String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLTCredit))
            {
                if (DataCon.InsertSpecialTrans.SLTCredit.ToString().Length < 2)
                {
                    DataCon.InsertSpecialTrans.SLTCredit = "0" + DataCon.InsertSpecialTrans.SLTCredit;
                }
                else if (DataCon.InsertSpecialTrans.SLTCredit.ToString().Length == 2)
                {
                    DataCon.InsertSpecialTrans.SLTCredit = DataCon.InsertSpecialTrans.SLTCredit;
                }
            }

        }


        private void loadDefaultTransaction(String TransType)
        {
            String parsed = new JavaScriptSerializer().Serialize(TransType);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/InitializeSpecialTrans", parsed);
            if (Response.Status == "SUCCESS")
            {
                this.DataCon.InsertSpecialTrans = new JavaScriptSerializer().Deserialize<InsertSpecialTransaction>(Response.Content);

                if (this.DataCon.InsertSpecialTrans.SpecialType == "DPM")
                {
                    if (!String.IsNullOrEmpty(this.DataCon.InsertSpecialTrans.SLC) && !String.IsNullOrEmpty(this.DataCon.InsertSpecialTrans.SLT))
                    {
                        Disable();
                        //AsOfDatePicker.Focus();
                        this.DataCon.InsertSpecialTrans.SLCDebit = this.DataCon.InsertSpecialTrans.SLC;
                        this.DataCon.InsertSpecialTrans.SLTDebit = this.DataCon.InsertSpecialTrans.SLT;
                        this.DataCon.InsertSpecialTrans.SLDebitGLDesc = this.DataCon.InsertSpecialTrans.SLDescription;
                        this.DataCon.InsertSpecialTrans.SLAcctCode = this.DataCon.InsertSpecialTrans.SLAcctCode;
                        this.DataCon.InsertSpecialTrans.SLCreditGLCode = this.DataCon.InsertSpecialTrans.GLAcctCode;
                        this.DataCon.InsertSpecialTrans.GLAcctDesc = this.DataCon.InsertSpecialTrans.GLAcctDesc;
                    }
                    else
                    {
                        Enable();

                        this.DataCon.InsertSpecialTrans.SLDescription = string.Empty;
                        this.DataCon.InsertSpecialTrans.SLAcctCode = 0;
                        this.DataCon.InsertSpecialTrans.SLCreditGLCode = 0;
                        this.DataCon.InsertSpecialTrans.GLAcctDesc = string.Empty;
                    }
                }
                else if (this.DataCon.InsertSpecialTrans.SpecialType == "MO")
                {
                    String SLCCreditDefault = "29", SLTCreditDefault = "01", SLECreditDefault = "11", SLNCreditDefault = "15";

                    this.DataCon.InsertSpecialTrans.SLCDebit = this.DataCon.InsertSpecialTrans.SLC;
                    this.DataCon.InsertSpecialTrans.SLTDebit = this.DataCon.InsertSpecialTrans.SLT;
                    this.DataCon.InsertSpecialTrans.SLEDebit = this.DataCon.InsertSpecialTrans.SLE;
                    this.DataCon.InsertSpecialTrans.SLNDebit = this.DataCon.InsertSpecialTrans.SLN;
                    this.DataCon.InsertSpecialTrans.SLDebitGLDesc = this.DataCon.InsertSpecialTrans.SLDescription;
                    this.DataCon.InsertSpecialTrans.SLAcctCode = this.DataCon.InsertSpecialTrans.SLAcctCode;

                    this.DataCon.InsertSpecialTrans.SLCCredit = SLCCreditDefault;
                    this.DataCon.InsertSpecialTrans.SLTCredit = SLTCreditDefault;
                    this.DataCon.InsertSpecialTrans.SLECredit = SLECreditDefault;
                    this.DataCon.InsertSpecialTrans.SLNCredit = SLNCreditDefault;
                    this.DataCon.InsertSpecialTrans.SLCreditDesc = this.DataCon.InsertSpecialTrans.GLAcctDesc;
                    this.DataCon.InsertSpecialTrans.SLCreditGLCode = DataCon.InsertSpecialTrans.GLAcctCode;

                    this.DataCon.InsertSpecialTrans.LastTransDateBalanceType = 5;
                }

            }
        }


        public String CreateServiceFeeExplanation(String SLDescription)
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("Service Fee on Dormant: " + SLDescription).AppendLine();
            MyStringBuilder.Append("As of: " + this.DataCon.TransDate).AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.AppendLine("Parameters:");

            //If Balance of SL
            if (UseBalanceCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(UseBalanceCheckBox.Content.ToString());
            }

            if (SkipClientCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(SkipClientCheckBox.Content.ToString());
            }

            if (BothCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(BothCheckBox.Content.ToString());
            }

            if (PostNewSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSLCheckBox.Content.ToString());
            }

            if (PostSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostSkipSLCheckBox.Content.ToString());
            }

            if (PostNewSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSkipSLCheckBox.Content.ToString());
            }
            if (PostAlwaysSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostAlwaysSLCheckBox.Content.ToString());
            }

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateBMSExplanation(String SLDescription)
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("BMS: " + SLDescription).AppendLine();
            MyStringBuilder.Append("As of: " + this.DataCon.TransDate).AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.AppendLine("Parameters:");

            //If Balance of SL
            if (UseBalanceCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(UseBalanceCheckBox.Content.ToString());
            }

            if (SkipClientCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(SkipClientCheckBox.Content.ToString());
            }

            if (BothCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(BothCheckBox.Content.ToString());
            }

            if (PostNewSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSLCheckBox.Content.ToString());
            }

            if (PostSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostSkipSLCheckBox.Content.ToString());
            }

            if (PostNewSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSkipSLCheckBox.Content.ToString());
            }
            if (PostAlwaysSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostAlwaysSLCheckBox.Content.ToString());
            }

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateADExplanation(String SLDescription)
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("Annual Dues: " + SLDescription).AppendLine();
            MyStringBuilder.Append("As of: " + this.DataCon.TransDate).AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.AppendLine("Parameters:");

            //If Balance of SL
            if (UseBalanceCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(UseBalanceCheckBox.Content.ToString());
            }

            if (SkipClientCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(SkipClientCheckBox.Content.ToString());
            }

            if (BothCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(BothCheckBox.Content.ToString());
            }

            if (PostNewSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSLCheckBox.Content.ToString());
            }

            if (PostSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostSkipSLCheckBox.Content.ToString());
            }

            if (PostNewSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSkipSLCheckBox.Content.ToString());
            }
            if (PostAlwaysSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostAlwaysSLCheckBox.Content.ToString());
            }

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateProbiExplanation(String SLDescription)
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("Dormant Probi: " + SLDescription).AppendLine();
            MyStringBuilder.Append("As of: " + this.DataCon.TransDate).AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.AppendLine("Parameters:");

            //If Balance of SL
            if (UseBalanceCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(UseBalanceCheckBox.Content.ToString());
            }

            if (SkipClientCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(SkipClientCheckBox.Content.ToString());
            }

            if (BothCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(BothCheckBox.Content.ToString());
            }

            //Other Conditions
            if (SkipClientSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(SkipClientSLCheckBox.Content.ToString());
            }

            if (PostSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostSkipSLCheckBox.Content.ToString());
            }

            if (PostNewSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSkipSLCheckBox.Content.ToString());
            }
            if (PostAlwaysSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostAlwaysSLCheckBox.Content.ToString());
            }

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        public String CreateMOExplanation(String SLDescription)
        {
            StringBuilder MyStringBuilder = new StringBuilder();
            MyStringBuilder.Append("Mortuary And Contribution: " + SLDescription).AppendLine();
            MyStringBuilder.Append("As of: " + this.DataCon.TransDate).AppendLine();
            MyStringBuilder.AppendLine();
            MyStringBuilder.AppendLine("Parameters:");

            //If Balance of SL
            if (UseBalanceCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(UseBalanceCheckBox.Content.ToString());
            }

            if (SkipClientCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(SkipClientCheckBox.Content.ToString());
            }

            if (BothCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(BothCheckBox.Content.ToString());
            }

            //Other Conditions
            if (PostNewSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSLCheckBox.Content.ToString());
            }

            if (PostSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostSkipSLCheckBox.Content.ToString());
            }

            if (PostNewSkipSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostNewSkipSLCheckBox.Content.ToString());
            }
            if (PostAlwaysSLCheckBox.IsChecked == true)
            {
                MyStringBuilder.AppendLine(PostAlwaysSLCheckBox.Content.ToString());
            }

            String stringBuilder = MyStringBuilder.ToString();

            return stringBuilder;
        }


        private void loadDefault(String TransType)
        {
            if (this.DataCon.InsertSpecialTrans.SpecialType == "BMS" || this.DataCon.InsertSpecialTrans.SpecialType == "AD" ||
                this.DataCon.InsertSpecialTrans.SpecialType == "SE")
            {
                BMSParams bmsParams = new BMSParams();
                bmsParams.SLC = Convert.ToByte(DataCon.InsertSpecialTrans.SLC);
                bmsParams.SLT = Convert.ToByte(DataCon.InsertSpecialTrans.SLT);
                bmsParams.TransType = DataCon.InsertSpecialTrans.SpecialType;


                String parsed = new JavaScriptSerializer().Serialize(bmsParams);
                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/SPDefaultBMS", parsed);
                if (Response.Status == "SUCCESS")
                {
                    InsertSpecialTransaction insertTransDetails = new InsertSpecialTransaction();

                    insertTransDetails = new JavaScriptSerializer().Deserialize<InsertSpecialTransaction>(Response.Content);
                    this.DataCon.InsertSpecialTrans = insertTransDetails;

                    if (!String.IsNullOrEmpty(insertTransDetails.SLC) && !String.IsNullOrEmpty(insertTransDetails.SLT))
                    {
                        Disable();
                        AsOfDatePicker.Focus();

                        this.DataCon.InsertSpecialTrans.SLE = insertTransDetails.SLE;
                        this.DataCon.InsertSpecialTrans.SLN = insertTransDetails.SLN;

                        this.DataCon.InsertSpecialTrans.SLCreditGLCode = insertTransDetails.GLAcctCode;
                        this.DataCon.InsertSpecialTrans.GLAcctDesc = insertTransDetails.GLAcctDesc;

                        if (this.DataCon.InsertSpecialTrans.SpecialType == "BMS")
                        {
                            this.DataCon.InsertSpecialTrans.SLCDebit = insertTransDetails.SLC;
                            this.DataCon.InsertSpecialTrans.SLTDebit = insertTransDetails.SLT;
                            this.DataCon.InsertSpecialTrans.SLDebitGLDesc = insertTransDetails.SLDescription;

                            this.DataCon.InsertSpecialTrans.SLAcctCode = insertTransDetails.SLAcctCode;
                            this.DataCon.Explanation = CreateBMSExplanation(insertTransDetails.SLDescription);
                        }
                        else if (this.DataCon.InsertSpecialTrans.SpecialType == "AD")
                        {
                            String parsedSLT = new JavaScriptSerializer().Serialize(insertTransDetails.SLT);
                            CRUXLib.Response ResponseSL = App.CRUXAPI.request("backoffice/selectRegularSavings", parsedSLT);
                            if (Response.Status == "SUCCESS")
                            {
                                InsertSpecialTransaction insertTrans = new InsertSpecialTransaction();
                                insertTrans = new JavaScriptSerializer().Deserialize<InsertSpecialTransaction>(ResponseSL.Content);

                                this.DataCon.InsertSpecialTrans.SLCDebit = insertTrans.SLCDebit;
                                this.DataCon.InsertSpecialTrans.SLTDebit = insertTrans.SLTDebit;
                                this.DataCon.InsertSpecialTrans.SLEDebit = insertTrans.SLEDebit;
                                this.DataCon.InsertSpecialTrans.SLNDebit = insertTrans.SLNDebit;
                                this.DataCon.InsertSpecialTrans.SLDebitGLDesc = insertTrans.SLDebitGLDesc;
                                this.DataCon.InsertSpecialTrans.SLAcctCode = insertTrans.SLAcctCode;

                                this.DataCon.InsertSpecialTrans.SLCCredit = insertTrans.SLCCredit;
                                this.DataCon.InsertSpecialTrans.SLTCredit = insertTrans.SLTCredit;
                                this.DataCon.InsertSpecialTrans.SLECredit = insertTrans.SLECredit;
                                this.DataCon.InsertSpecialTrans.SLNCredit = insertTrans.SLNCredit;
                                this.DataCon.InsertSpecialTrans.SLCreditDesc = this.DataCon.InsertSpecialTrans.GLAcctDesc;
                                this.DataCon.InsertSpecialTrans.SLCreditGLCode = DataCon.InsertSpecialTrans.GLAcctCode;

                                this.DataCon.Explanation = CreateADExplanation(insertTransDetails.SLDescription);
                            }
                        }
                        else if (this.DataCon.InsertSpecialTrans.SpecialType == "SE")
                        {
                            this.DataCon.InsertSpecialTrans.SLCDebit = insertTransDetails.SLC;
                            this.DataCon.InsertSpecialTrans.SLTDebit = insertTransDetails.SLT;
                            this.DataCon.InsertSpecialTrans.SLDebitGLDesc = insertTransDetails.SLDescription;

                            this.DataCon.InsertSpecialTrans.SLAcctCode = insertTransDetails.SLAcctCode;
                            this.DataCon.Explanation = CreateServiceFeeExplanation(insertTransDetails.SLDescription);

                            this.DataCon.InsertSpecialTrans.LastTransDateBalanceType = 6;
                            this.DataCon.InsertSpecialTrans.LastTransDate = this.DataCon.TransDate;

                        }

                        AddZeroToSLT();
                    }
                    else
                    {
                        Enable();

                        this.DataCon.InsertSpecialTrans.SLDescription = string.Empty;
                        this.DataCon.InsertSpecialTrans.SLAcctCode = 0;
                        this.DataCon.InsertSpecialTrans.SLCreditGLCode = 0;
                        this.DataCon.InsertSpecialTrans.GLAcctDesc = string.Empty;
                    }
                }
            }
        }


        private void Disable()
        {
            BalTypesComboBox.IsEnabled = false;
            BalAmtTextBox.IsEnabled = false;
            specialTransTypeComboBox.IsEnabled = false;
            DeductAmtTextBox.IsEnabled = false;

            PMESComboBox.IsEnabled = false;
            PMESDatePicker.IsEnabled = false;
        }


        private void Enable()
        {
            BalTypesComboBox.IsEnabled = true;
            BalAmtTextBox.IsEnabled = true;
            specialTransTypeComboBox.IsEnabled = true;
            DeductAmtTextBox.IsEnabled = true;

            PMESComboBox.IsEnabled = true;
            PMESDatePicker.IsEnabled = true;
        }


        private void GenerateTrans()
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    if (!String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLC) &&
                        !String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLT) &&
                        !String.IsNullOrEmpty(DataCon.AsOffDate) &&
                        !String.IsNullOrEmpty(DataCon.Explanation))
                    {


                        if (DataCon.InsertSpecialTrans.SpecialType == "AD" || DataCon.InsertSpecialTrans.SpecialType == "MO")
                        {
                            processBlock.Visibility = Visibility.Visible;
                            progressBlock.Visibility = Visibility.Visible;
                            this.DataCon.processing = "Loading data...";
                        }
                        else
                        {
                            ProgressPanel.Visibility = Visibility.Visible;
                        }

                        SpecialTransGrid.IsEnabled = false;

                        GenerateWorker.RunWorkerAsync();
                    }
                    else if (String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLC))
                    {
                        MessageBox.Show(this, "Please enter SLC", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        SLCTextBox.Focus();
                    }
                    else if (String.IsNullOrEmpty(DataCon.InsertSpecialTrans.SLT))
                    {
                        MessageBox.Show(this, "Please enter SLT", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        SLTTextBox.Focus();
                    }
                    else if (String.IsNullOrEmpty(DataCon.AsOffDate))
                    {
                        MessageBox.Show(this, "Please enter As of Date", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        AsOfDatePicker.Focus();
                    }
                    else if (String.IsNullOrEmpty(DataCon.Explanation))
                    {
                        MessageBox.Show(this, "Please enter Explanation", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        ExplanationTextBox.Focus();
                    }
                    break;
                case MessageBoxResult.No:

                    break;
            }

        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Are you sure to Cancel current work?", "Below Maintaining Surcharge", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            Close();
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;

                    } //end switch

                } //end If Key = Escape

                else if (e.Key == Key.F11 || (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G)))
                {
                    if (generateButton.IsEnabled)
                    {
                        GenerateTrans();
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void loadSpecialTransTypes()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/specialTransType", "");
            if (Response.Status == "SUCCESS")
            {
                List<SpecialTransType> listTransTypes = new JavaScriptSerializer().Deserialize<List<SpecialTransType>>(Response.Content);
                specialTransTypeComboBox.ItemsSource = listTransTypes;
            }
            else
            {
                MessageBox.Show(this, "Please contact Administrator.");
            }
        }


        private void loadBalance()
        {
            List<BalanceTypes> blTypes = new List<BalanceTypes>();
            blTypes.Add(new BalanceTypes() { BalTypeID = 1, BalTypeDesc = "Equal To" });
            blTypes.Add(new BalanceTypes() { BalTypeID = 2, BalTypeDesc = "Not Equal To" });
            blTypes.Add(new BalanceTypes() { BalTypeID = 3, BalTypeDesc = "Greater Than" });
            blTypes.Add(new BalanceTypes() { BalTypeID = 4, BalTypeDesc = "Greater Than or Equal To" });
            blTypes.Add(new BalanceTypes() { BalTypeID = 5, BalTypeDesc = "Less Than" });
            blTypes.Add(new BalanceTypes() { BalTypeID = 6, BalTypeDesc = "Less Than or Equal To" });

            BalTypesComboBox.ItemsSource = blTypes;
            PMESComboBox.ItemsSource = blTypes;
            LastTransDateComboBox.ItemsSource = blTypes;
        }


        private void loadTransactionTypeSpecialTrans(String TransType)
        {
            int SpecialTransID = 0;

            if (TransType == "MO")
            {
                SpecialTransID = 62;
            }
            else
            {
                SpecialTransID = 77;
            }


            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/transType", "");
            if (Response.Status == "SUCCESS")
            {
                List<TransactionType> listTransTypes = new JavaScriptSerializer().Deserialize<List<TransactionType>>(Response.Content);
                TransTypeComboBox.ItemsSource = listTransTypes.Where(t => t.TransTypeID == SpecialTransID).ToList();
            }
            else
            {
                MessageBox.Show(this, Response.Content);
            }
        }


        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "AsOfDatePicker")
            {
                AsOfDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                AsOfDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "LastTransDatePicker")
            {
                LastTransDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                LastTransDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "PMESDatePicker")
            {
                PMESDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                PMESDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "TransDatePicker")
            {
                TransDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                TransDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "SLCTextBox")
            {
                SLCTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                SLCTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "SLTTextBox")
            {
                SLTTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                SLTTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "DebitSLCTextBox")
            {
                DebitSLCTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                DebitSLCTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "DebitSLTTextBox")
            {
                DebitSLTTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                DebitSLTTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "CreditSLCTextBox")
            {
                CreditSLCTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                CreditSLCTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "CreditSLTTextBox")
            {
                CreditSLTTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                CreditSLTTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

            else if (textName == "DeductAmtTextBox")
            {
                DeductAmtTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                DeductAmtTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "BalAmtTextBox")
            {
                BalAmtTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                BalAmtTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

            else if (textName == "DebitGLAcctTextBox")
            {
                DebitGLAcctTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                DebitGLAcctTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "CreditGLAcctTextBox")
            {
                CreditGLAcctTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                CreditGLAcctTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

            else if (textName == "ExplanationTextBox")
            {
                ExplanationTextBox.Background = System.Windows.Media.Brushes.Yellow; // WPF
                ExplanationTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "AsOfDatePicker")
            {
                try
                {
                    DateTime.Parse(AsOfDatePicker.Text);
                    AsOfDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    AsOfDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }

            else if (textName == "LastTransDatePicker")
            {
                try
                {
                    DateTime.Parse(LastTransDatePicker.Text);
                    LastTransDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    LastTransDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }

            } // end else if toDatePicker
            else if (textName == "PMESDatePicker")
            {
                try
                {
                    DateTime.Parse(PMESDatePicker.Text);
                    PMESDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    PMESDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            } // end else if toDatePicker

            else if (textName == "TransDatePicker")
            {
                try
                {
                    DateTime.Parse(TransDatePicker.Text);
                    TransDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    TransDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }
            else if (textName == "SLCTextBox")
            {
                SLCTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                SLCTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "SLTTextBox")
            {
                SLTTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                SLTTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "DebitSLCTextBox")
            {
                DebitSLCTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                DebitSLCTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "DebitSLTTextBox")
            {
                DebitSLTTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                DebitSLTTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "CreditSLCTextBox")
            {
                CreditSLCTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                CreditSLCTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "CreditSLTTextBox")
            {
                CreditSLTTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                CreditSLTTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

            else if (textName == "DeductAmtTextBox")
            {
                DeductAmtTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                DeductAmtTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "BalAmtTextBox")
            {
                BalAmtTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                BalAmtTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

            else if (textName == "DebitGLAcctTextBox")
            {
                DebitGLAcctTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                DebitGLAcctTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "CreditGLAcctTextBox")
            {
                CreditGLAcctTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                CreditGLAcctTextBox.IsInactiveSelectionHighlightEnabled = true;
            }

            else if (textName == "ExplanationTextBox")
            {
                ExplanationTextBox.Background = System.Windows.Media.Brushes.White; // WPF
                ExplanationTextBox.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void SelectSLC(String TextName)
        {
            slClass = new SLClass(this, TextName);
            slClass.Owner = this;
            slClass.ShowDialog();
        }


        private void SelectSLT(String TextName, String SLC)
        {
            slType = new SLType(Int32.Parse(SLC), TextName, this);
            slType.Owner = this;
            slType.ShowDialog();

        }


        private void SLC_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (e.Key == Key.Space)
            {
                if (textName == "SLCTextBox")
                {
                    SelectSLC(SLCTextBox.Name);
                    e.Handled = true;
                }
                else if (textName == "DebitSLCTextBox")
                {
                    SelectSLC(DebitSLCTextBox.Name);
                    e.Handled = true;
                }
                else if (textName == "CreditSLCTextBox")
                {
                    SelectSLC(CreditSLCTextBox.Name);
                    e.Handled = true;
                }

                else if (textName == "SLTTextBox")
                {
                    if (String.IsNullOrEmpty(SLCTextBox.Text))
                    {
                        e.Handled = true;
                    }
                    else
                    {
                        SelectSLT(SLTTextBox.Name, SLCTextBox.Text);
                        e.Handled = true;
                    }
                }
                else if (textName == "DebitSLTTextBox")
                {
                    if (String.IsNullOrEmpty(DebitSLCTextBox.Text))
                    {
                        e.Handled = true;
                    }
                    else
                    {
                        SelectSLT(DebitSLTTextBox.Name, DebitSLCTextBox.Text);
                        e.Handled = true;
                    }

                }
                else if (textName == "CreditSLTTextBox")
                {
                    if (String.IsNullOrEmpty(CreditSLCTextBox.Text))
                    {
                        e.Handled = true;
                    }
                    else
                    {
                        SelectSLT(CreditSLTTextBox.Name, CreditSLCTextBox.Text);
                        e.Handled = true;
                    }
                }// end else if textName = CreditSLTTextBox
            }
            else if (e.Key == Key.Tab)
            {

                try
                {
                    if (textName == "AsOfDatePicker")
                    {
                        DateTime.Parse(AsOfDatePicker.Text);
                    }
                    else if (textName == "LastTransDatePicker")
                    {
                        DateTime.Parse(LastTransDatePicker.Text);
                    }
                    else if (textName == "PMESDatePicker")
                    {
                        DateTime.Parse(PMESDatePicker.Text);
                    }
                    else if (textName == "TransDatePicker")
                    {
                        DateTime.Parse(TransDatePicker.Text);
                    }
                }
                catch (Exception)
                {
                    e.Handled = true;
                }


            }
        }


        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            decimal value1;
            if (decimal.TryParse(((TextBox)sender).Text, out value1))
            {
                string[] a = ((TextBox)sender).Text.Split(new char[] { '.' });
                int decimals = 0;
                if (a.Length == 2)
                {
                    decimals = a[1].Length;
                }
                else
                {
                    string newString = Regex.Replace(((TextBox)sender).Text, "[^0-9]", "");
                    ((TextBox)sender).Text = string.Format("{0:#,##0}", Double.Parse(newString));
                }
                if (decimals > 2)
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
                            ((TextBox)sender).Focus();
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
            }
            else
            {
                if (((TextBox)sender).Text != "")
                {
                    ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(0, ((TextBox)sender).Text.Length - 1);
                }
            }
        }


        private void UseBalanceCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (UseBalanceCheckBox.IsChecked == true)
            {
                UseBalanceCheckBox.IsChecked = true;
                SkipClientCheckBox.IsChecked = false;
                BothCheckBox.IsChecked = false;
            }
            else
            {
                UseBalanceCheckBox.IsChecked = false;
            }
        }


        private void SkipClientCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (SkipClientCheckBox.IsChecked == true)
            {
                SkipClientCheckBox.IsChecked = true;
                UseBalanceCheckBox.IsChecked = false;
                BothCheckBox.IsChecked = false;
            }
            else
            {
                SkipClientCheckBox.IsChecked = false;
            }
        }


        private void BothCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (BothCheckBox.IsChecked == true)
            {
                BothCheckBox.IsChecked = true;
                SkipClientCheckBox.IsChecked = false;
                UseBalanceCheckBox.IsChecked = false;
            }
            else
            {
                BothCheckBox.IsChecked = false;
            }
        }


        private void PostNewSLCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (PostNewSLCheckBox.IsChecked == true)
            {
                PostNewSLCheckBox.IsChecked = true;
                PostSkipSLCheckBox.IsChecked = false;
                PostNewSkipSLCheckBox.IsChecked = false;
                PostAlwaysSLCheckBox.IsChecked = false;
            }
            else
            {
                PostNewSLCheckBox.IsChecked = false;
            }
        }


        private void PostSkipSLCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (PostSkipSLCheckBox.IsChecked == true)
            {
                PostSkipSLCheckBox.IsChecked = true;
                PostNewSLCheckBox.IsChecked = false;
                PostNewSkipSLCheckBox.IsChecked = false;
                PostAlwaysSLCheckBox.IsChecked = false;
            }
            else
            {
                PostSkipSLCheckBox.IsChecked = false;
            }
        }


        private void PostNewSkipSLCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (PostNewSkipSLCheckBox.IsChecked == true)
            {
                PostNewSkipSLCheckBox.IsChecked = true;
                PostNewSLCheckBox.IsChecked = false;
                PostSkipSLCheckBox.IsChecked = false;
                PostAlwaysSLCheckBox.IsChecked = false;
            }
            else
            {
                PostNewSkipSLCheckBox.IsChecked = false;
            }
        }


        private void PostAlwaysSLCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (PostAlwaysSLCheckBox.IsChecked == true)
            {
                PostAlwaysSLCheckBox.IsChecked = true;
                PostNewSLCheckBox.IsChecked = false;
                PostSkipSLCheckBox.IsChecked = false;
                PostNewSkipSLCheckBox.IsChecked = false;
            }
            else
            {
                PostAlwaysSLCheckBox.IsChecked = false;
            }
        }


        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GenerateTrans();

            }
            catch (Exception)
            {

                e.Handled = true;
            }

        }


        private void SLTTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(this.DataCon.InsertSpecialTrans.SLT))
                {
                    loadDefault(transType);
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private TransactionSummary ProcessTransactionSummary(Int16 BranchCode, String batchNo, Int16 transCode, Int64 CtlNo, String DocNo, Int32 TransYear, String TransDate, String RandomToken)
        {
            TransactionSummary transactionSummary = new TransactionSummary();
            ExecuteHelper execHelp = new ExecuteHelper(this.mww);

            transactionSummary.TransYear = TransYear;
            transactionSummary.BranchCode = BranchCode;
            transactionSummary.BatchNo = batchNo;
            transactionSummary.TransactionCode = transCode;
            transactionSummary.ControlNo = CtlNo;
            transactionSummary.DocNo = DocNo;
            transactionSummary.TransactionDate = TransDate;
            transactionSummary.Explanation = DataCon.Explanation;
            transactionSummary.RandomToken = RandomToken;

            return transactionSummary;
        }


        public class SpecialTransactionDataCon : INotifyPropertyChanged
        {

            String _progress;
            public String progress
            {
                get { return _progress; }
                set
                {
                    _progress = value;
                    OnPropertyChanged("progress");
                }
            }

            String _processing;
            public String processing
            {
                get
                {
                    return _processing;
                }
                set
                {
                    _processing = value;
                    OnPropertyChanged("processing");
                }
            }

            private String _TransTypeDesc;
            public String TransTypeDesc
            {
                get
                {
                    return _TransTypeDesc;
                }
                set
                {
                    _TransTypeDesc = value;
                    OnPropertyChanged("TransTypeDesc");
                }
            }

            private InsertSpecialTransaction _InsertSpecialTrans;
            public InsertSpecialTransaction InsertSpecialTrans
            {
                get
                {
                    return _InsertSpecialTrans;
                }
                set
                {
                    _InsertSpecialTrans = value;
                    OnPropertyChanged("InsertSpecialTrans");
                }
            }

            private String _Explanation;
            public String Explanation
            {
                get
                {
                    return _Explanation;
                }
                set
                {
                    _Explanation = value;
                    OnPropertyChanged("Explanation");
                }
            }

            private String _AsOffDate;
            public String AsOffDate
            {
                get
                {
                    return _AsOffDate;
                }
                set
                {
                    _AsOffDate = value;
                    OnPropertyChanged("AsOffDate");
                }
            }

            private String _TransDate;
            public String TransDate
            {
                get
                {
                    return _TransDate;
                }
                set
                {
                    _TransDate = value;
                    OnPropertyChanged("TransDate");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetFocus();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }
    }
}
