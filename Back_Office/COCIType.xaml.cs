﻿using Back_Office.Helper;
using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for COCIType.xaml
    /// </summary>
    public partial class COCIType : Window
    {
        Int32 cociID;
        String cociDesc;
        public MainWindow mww;

        public COCIType(MainWindow mw)
        {
            InitializeComponent();
            this.mww = mw;
            FillCOCIData();
        }

        private void FillCOCIData()
        {
            //string parsed = new JavaScriptSerializer().Serialize(dm);
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/cociType", "");

            if (Response.Status == "SUCCESS")
            {
                List<COCI> COCILists = new JavaScriptSerializer().Deserialize<List<COCI>>(Response.Content);
                Console.WriteLine(Response.Content);
                formDataGrid.ItemsSource = COCILists;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }

        private void function()
        {
            if (formDataGrid.SelectedItem == null) return;

            var selectedPerson = formDataGrid.SelectedItem as COCI;

            // TransactionCheck tt = (TransactionCheck)this.mww.COCIDataGrid.SelectedItem;
            if (this.mww.DataCon.TransactionChecks.Count == 0)
            {
                this.mww.DataCon.TransactionChecks.Add(new TransactionCheck()
                {
                    CociType = Convert.ToByte(selectedPerson.COCITypeID),
                    CociTypeDesc = selectedPerson.COCIDesc
                });

                this.mww.COCIDataGrid.CanUserAddRows = false;
            }
            else
            {
                foreach (TransactionCheck checkDetail in this.mww.DataCon.TransactionChecks)
                {
                    checkDetail.CociType = Convert.ToByte(selectedPerson.COCITypeID);
                    checkDetail.CociTypeDesc = selectedPerson.COCIDesc;
                }
            }

            this.Close();
            this.mww.COCIDataGrid.SelectedIndex = 0;
            this.mww.Dispatcher.BeginInvoke((Action)(() =>
            {
                Keyboard.Focus(this.mww.GetDataGridCell(this.mww.COCIDataGrid.SelectedCells[1]));
            }), System.Windows.Threading.DispatcherPriority.Background);

        }

        public void setCOCI(Int32 cociID, String cociDesc)
        {
            this.cociID = cociID;
            this.cociDesc = cociDesc;
        }

        public Int32 GetCOCIID()
        {
            return cociID;
        }

        public String GetCOCIDesc()
        {
            return cociDesc;
        }

        private void formDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            function();
        }

        private void formDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                function();
            }
        }

        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridFocus dgFocus = new DataGridFocus();
                if (formDataGrid.ItemsSource != null)
                {
                    formDataGrid.SelectedIndex = 0;
                    formDataGrid.Focus();
                    Keyboard.Focus(dgFocus.GetDataGridCell(formDataGrid.SelectedCells[0]));
                }
                else
                {
                    searchTextBox.Focus();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
