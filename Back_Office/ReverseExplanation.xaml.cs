﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Back_Office.MainWindow;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for ReverseExplanation.xaml
    /// </summary>
    public partial class ReverseExplanation : Window
    {
        MainWindow mww;
        String ProcessType = "";

        public ReverseExplanation(MainWindow mw, String Type)
        {
            InitializeComponent();
            ProcessType = Type;
            this.mww = mw;
            this.ExplanationTextBox.Focus();
        }

        private void Function()
        {
            if (String.IsNullOrEmpty(ExplanationTextBox.Text))
            {
                MessageBoxResult result = MessageBox.Show(this, "Explanation field is required for reversal. Thank you.", "Alert", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
            }
            else
            {
                BackOfficeDataCon _BOContainer = (BackOfficeDataCon)this.mww.DataCon;
                StringBuilder _TextFormat = new StringBuilder();
                _TextFormat.AppendLine();
                _TextFormat.AppendLine();

                if(ProcessType == "Reversed")
                {
                    _TextFormat.Append("Reversed: ");
                }
                else
                {
                    _TextFormat.Append("Cancelled: ");
                }
               
                _TextFormat.Append(ExplanationTextBox.Text);

                _BOContainer.ReversalExplanation += _TextFormat;

                this.Close();
            }
        }

        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
