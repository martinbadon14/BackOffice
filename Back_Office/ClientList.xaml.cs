﻿using Back_Office.Helper;
using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for ClientList.xaml
    /// </summary>
    public partial class ClientList : Window
    {
        String TransOption;
        public MainWindow mww;
        int currentRow = 0;
        string SearchText = string.Empty;
        private ICollectionView MyData;


        public ClientList()
        {
            InitializeComponent();
        }


        public ClientList(Int64 BranchID, String id, String clientName, String Option, MainWindow mw, String ClosedOrAllOption = null)
        {
            InitializeComponent();
            this.mww = mw;
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            FillDataGrid(BranchID, id, clientName, ClosedOrAllOption);
            TransOption = Option;
            SearchTextBox.Focus();
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Up)
                {
                    if (currentRow > 0)
                    {
                        clientListGrid.Focus();

                        int previousIndex = clientListGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        currentRow = previousIndex;
                        clientListGrid.SelectedIndex = previousIndex;
                        clientListGrid.ScrollIntoView(clientListGrid.Items[currentRow]);
                    }
                }
                else if (e.Key == Key.Down)
                {
                    if (currentRow < clientListGrid.Items.Count - 1)
                    {
                        clientListGrid.Focus();

                        int nextIndex = clientListGrid.SelectedIndex + 1;
                        if (nextIndex > clientListGrid.Items.Count - 1) return;
                        currentRow = nextIndex;
                        clientListGrid.SelectedIndex = nextIndex;
                        clientListGrid.ScrollIntoView(clientListGrid.Items[currentRow]);
                    }
                }
                else if (e.Key == Key.Escape)
                {
                    this.Close();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }

        class sender
        {
            public Int64 BranchID { get; set; }
            public string id { get; set; }
            public string name { get; set; }
            public String ClientStatusID { get; set; }
        }


        private bool FilterData(object item)
        {
            var value = (Models.Client)item;

            if (value.clientId == 0 || String.IsNullOrEmpty(value.Name) || String.IsNullOrEmpty(value.middleName) ||
                String.IsNullOrEmpty(value.status) || String.IsNullOrEmpty(value.clientType))
                return false;

            else
                return Convert.ToString(value.clientId).Contains(SearchText) || Convert.ToString(value.clientCheckID).Contains(SearchText) ||
                    value.middleName.ToLower().StartsWith(SearchText.ToLower()) ||
                    value.Name.ToLower().StartsWith(SearchText.ToLower()) || value.status.ToLower().StartsWith(SearchText.ToLower()) ||
                    value.clientType.ToLower().StartsWith(SearchText.ToLower());

        }


        private void FillDataGrid(Int64 BranchID, string Id, string Name, String Option = null)
        {
            sender dm = new sender();
            dm.BranchID = BranchID;
            dm.id = Id;
            dm.name = Name;

            string parsed = "";

            if (!String.IsNullOrEmpty(Option))
            {
                if (Option == "Closed")
                {
                    dm.ClientStatusID = "Closed";
                    parsed = new JavaScriptSerializer().Serialize(dm);
                }

                else if (Option == "All")
                {
                    dm.ClientStatusID = "All";
                    parsed = new JavaScriptSerializer().Serialize(dm);
                }
            }
            else
            {
                parsed = new JavaScriptSerializer().Serialize(dm);
            }

            CRUXLib.Response Response = App.CRUXAPI.request("client/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {
                List<Client> client = new JavaScriptSerializer().Deserialize<List<Client>>(Response.Content);
                clientListGrid.ItemsSource = client;

                if (client.Count != 0)
                {
                    clientListGrid.SelectedItem = client.FirstOrDefault();
                }

                MyData = CollectionViewSource.GetDefaultView(client);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void function()
        {
            if (clientListGrid.SelectedItem == null) return;
            var selectedPerson = clientListGrid.SelectedItem as Client;

            if (TransOption == "GJ")
            {
                TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;
                tt.ClientIDFormat = selectedPerson.ClientIDFormat;
                tt.clientName = selectedPerson.Name;
                tt.ClientID = selectedPerson.clientId;
                tt.ReferenceNo = String.Empty;
                this.Close();
                this.mww.MoveToNextUIElement();
                this.mww.MoveToNextUIElement();
            }
            else if (TransOption == "DB")
            {
                this.Close();
                this.mww.DataCon.TransactionSummary.ClientIDFormatTransSum = selectedPerson.ClientIDFormat.ToString();
                this.mww.DataCon.TransactionSummary.ClientID = selectedPerson.clientId;
                this.mww.DataCon.TransactionSummary.ClientNameTransSum = selectedPerson.Name;
            }
        }


        private void clientListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            function();
        }


        private void clientListGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                function();
                e.Handled = true;
            }
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            MyData.Filter = FilterData;
            clientListGrid.SelectedIndex = 0;
        }


        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (clientListGrid.Items != null)
                {
                    function();
                    e.Handled = true;
                }
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
