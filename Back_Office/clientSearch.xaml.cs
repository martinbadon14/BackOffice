﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for clientSearch.xaml
    /// </summary>
    public partial class clientSearch : Window
    {
        public ClientList cl;
        String TransOption = "";
        public MainWindow mww;
        ClientSearchDataCon DataCon = new ClientSearchDataCon();


        public clientSearch(String Option, MainWindow mw, Int64 BranchID = 0, Int64 ClientID = 0, String ClientName = "")
        {
            InitializeComponent();
            //  CheckCreateFile();
            this.mww = mw;
            TransOption = Option;
            PopulateBranch();
            this.DataCon.BranchID = BranchID;
            this.DataCon.ClientID = ClientID;
            //  this.DataCon.ClientName = ClientName;

            this.DataCon.ClientName = App.CRUXAPI.getUserParameter("ClientName");
            DataContext = this.DataCon;

            if (!String.IsNullOrEmpty(this.DataCon.ClientName))
            {
                txt_clientName.Focus();
                txt_clientName.SelectAll();
            }
            else
            {
                txt_clientID.Focus();
            }

        }


        private void CheckCreateFile()
        {
            string targetPath = @"\\10.15.40.211\e\Text Files\ClientName.text";

            if (!File.Exists(targetPath))
            {
                File.Create(targetPath);
                using (var stream = File.Open(targetPath, FileMode.Open, FileAccess.Write, FileShare.Read))
                {
                    
                    TextWriter tw = new StreamWriter(targetPath);
                    tw.WriteLine(this.DataCon.ClientName);
                    tw.Close();
                }
                
            }
            else if (File.Exists(targetPath))
            {
                using (StreamReader sr = File.OpenText(targetPath))
                {
                    String input;
                    while ((input = sr.ReadLine()) != null)
                    {
                        this.DataCon.ClientName = input;
                    }

                }
            }
        }


        private void PopulateBranch()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("client/allbranches", "");
            if (Response.Status == "SUCCESS")
            {
                List<Branches> listSuffix = new JavaScriptSerializer().Deserialize<List<Branches>>(Response.Content);
                cmb_branchID.ItemsSource = listSuffix;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            SelectClient(TransOption);
        }


        private void SelectClient(String Option)
        {
            if ((!String.IsNullOrEmpty(this.DataCon.ClientID.ToString()) || !String.IsNullOrEmpty(this.DataCon.ClientName)) &&
                closedCheckBox.IsChecked == false && allCheckBox.IsChecked == false)
            {
                cl = new ClientList(this.DataCon.BranchID, this.DataCon.ClientID.ToString(), this.DataCon.ClientName, Option, this.mww);
                this.Close();
                cl.ShowDialog();

            }

            else if ((!String.IsNullOrEmpty(this.DataCon.ClientID.ToString()) || !String.IsNullOrEmpty(this.DataCon.ClientName)) &&
                (closedCheckBox.IsChecked == true && allCheckBox.IsChecked == false))
            {
                cl = new ClientList(this.DataCon.BranchID, this.DataCon.ClientID.ToString(), this.DataCon.ClientName, Option, this.mww, "Closed");
                this.Close();
                cl.ShowDialog();
            }

            else if ((!String.IsNullOrEmpty(this.DataCon.ClientID.ToString()) || !String.IsNullOrEmpty(this.DataCon.ClientName)) &&
                (closedCheckBox.IsChecked == false && allCheckBox.IsChecked == true))
            {
                cl = new ClientList(this.DataCon.BranchID, this.DataCon.ClientID.ToString(), this.DataCon.ClientName, Option, this.mww, "All");
                this.Close();
                cl.ShowDialog();
            }

            else
            {
                MessageBox.Show("Must supply valid search in one field");
            }
        }


        private void txt_clientName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (Key.Enter == e.Key)
                {
                    SelectClient(TransOption);
                }
            }
            catch (Exception ex)
            {
                e.Handled = true;
            }

        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(this.DataCon.ClientName))
            {
                txt_clientName.Focus();
                txt_clientName.SelectAll();
            }
            else
            {
                txt_clientID.Focus();
            }
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void closedCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (closedCheckBox.IsChecked == true)
            {
                closedCheckBox.IsChecked = true;
                allCheckBox.IsChecked = false;
            }
            else if (closedCheckBox.IsChecked == false)
            {
                closedCheckBox.IsChecked = false;
            }
        }


        private void allCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (allCheckBox.IsChecked == true)
            {
                allCheckBox.IsChecked = true;
                closedCheckBox.IsChecked = false;
            }
            else if (allCheckBox.IsChecked == false)
            {
                allCheckBox.IsChecked = false;

            }
        }

        public class ClientSearchDataCon : INotifyPropertyChanged
        {
            Int64 _BranchID;
            public Int64 BranchID
            {
                get { return _BranchID; }
                set
                {
                    _BranchID = value;
                    OnPropertyChanged("BranchID");
                }
            }


            Int64 _ClientID;
            public Int64 ClientID
            {
                get { return _ClientID; }
                set
                {
                    _ClientID = value;
                    OnPropertyChanged("ClientID");
                }
            }

            String _ClientName;
            public String ClientName
            {
                get { return _ClientName; }
                set
                {
                    _ClientName = value;
                    OnPropertyChanged("ClientName");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.CRUXAPI.setUserParameter("ClientName", txt_clientName.Text);
        }
    }
}
