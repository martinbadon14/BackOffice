﻿using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Purchase_Journal;
using Back_Office.Models.Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for GLAccount.xaml
    /// </summary>
    public partial class GLAccount : Window
    {
        public MainWindow mww;
        public InterestOnSavings mee;

        public Int64 GLCode;
        public String COADesc = "";

        public ICollectionView MyData;

        string SearchText = string.Empty;

        String DefaultColumn = "";
        private Int64 SelectedOverride = 0;

        int currentRow = 0, currentColumn = 1;
        glAccountDataCon DataCont = new glAccountDataCon();
        public Models.GLAccounts Selected;
        Boolean IsPurchase = false;

        public GLAccount(List<Models.GLAccounts> glAccounts, MainWindow mw, Boolean PJ = false)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);


            DataCont.GLAccounts = glAccounts; // glAccountsList;
            MyData = CollectionViewSource.GetDefaultView(glAccounts);
            IsPurchase = PJ;
            this.DataContext = DataCont;
            this.mww = mw;

            SearchTextBox.Focus();
        }

        public GLAccount(List<Models.GLAccounts> glAccounts, InterestOnSavings me)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            // glAccountsList = new ObservableCollection<Models.GLAccounts>(FillDataWithSequence(glAccounts));
            DataCont.GLAccounts = glAccounts; //glAccountsList;
            MyData = CollectionViewSource.GetDefaultView(glAccounts);
            this.DataContext = DataCont;

            this.mee = me;
            SearchTextBox.Focus();
        }

        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                if (currentRow > 0)
                {
                    try
                    {
                        int previousIndex = GLAccountGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        GLAccountGrid.SelectedIndex = previousIndex;
                        GLAccountGrid.ScrollIntoView(GLAccountGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }
                }

            }
            else if (e.Key == Key.Down)
            {
                if (currentRow < GLAccountGrid.Items.Count - 1)
                {

                    try
                    {
                        int nextIndex = GLAccountGrid.SelectedIndex + 1;
                        if (nextIndex > GLAccountGrid.Items.Count - 1) return;
                        GLAccountGrid.SelectedIndex = nextIndex;
                        GLAccountGrid.ScrollIntoView(GLAccountGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }

                } // end if (this.SelectedOverride > 0)

            } // end else if (e.Key == Key.Down)

            else if (e.Key == Key.End && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                GLAccountGrid.SelectedItem = DataCont.GLAccounts.Last();
                GLAccountGrid.ScrollIntoView(GLAccountGrid.Items[GLAccountGrid.SelectedIndex]);
            }
            else if (e.Key == Key.Home && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                GLAccountGrid.SelectedItem = DataCont.GLAccounts.First();
                GLAccountGrid.ScrollIntoView(GLAccountGrid.Items[GLAccountGrid.SelectedIndex]);
            }

            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        public void SelectedDataEnter(Models.GLAccounts glAccounts, KeyEventArgs e = null)
        {
            try
            {
                if (glAccounts == null) return;

                if (this.mww != null)
                {

                    if (!IsPurchase)
                    {
                        TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;

                        String parsedDocInq = new JavaScriptSerializer().Serialize(glAccounts);

                        CRUXLib.Response Response = App.CRUXAPI.request("backoffice/checkSL", parsedDocInq);

                        if (Response.Status == "SUCCESS")
                        {
                            Models.AcctCodewithSL acctCodeWithSL = new JavaScriptSerializer().Deserialize<Models.AcctCodewithSL>(Response.Content);

                            if (!String.IsNullOrEmpty(acctCodeWithSL.SLC_Code) && !String.IsNullOrEmpty(acctCodeWithSL.SLT_Code) &&
                                    !String.IsNullOrEmpty(acctCodeWithSL.SLE_Code) && !String.IsNullOrEmpty(acctCodeWithSL.SLN_Code))
                            {
                                ExecuteHelper execStringBuilder = new ExecuteHelper(this.mww);
                                this.Close();

                                if (acctCodeWithSL.CanGLOnly == 0)
                                {
                                    MessageBoxResult result = MessageBox.Show(execStringBuilder.CreateAcctCodeCanGLOnlyPrompt(), "Alert", MessageBoxButton.OK);
                                }
                                else
                                {
                                    MessageBoxResult result = MessageBox.Show(execStringBuilder.CreateAcctCodeWithSLPrompt(), "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);

                                    switch (result)
                                    {
                                        case MessageBoxResult.Yes:

                                            tt.AccountCode = glAccounts.Code;
                                            tt.accountDesc = glAccounts.AccountTypeDesc;

                                            this.mww.MoveToNextUIElement();
                                            this.mww.MoveToNextUIElement();
                                            e.Handled = true;
                                            break;

                                        case MessageBoxResult.No:

                                            this.Close();
                                            break;

                                    } //end of switch 
                                }

                            } // end of if SLC,SLT,SLE,SLN is not empty
                            else
                            {

                                tt.AccountCode = glAccounts.Code;
                                tt.accountDesc = glAccounts.AccountTypeDesc;

                                this.Close();
                                this.mww.MoveToNextUIElement();
                                this.mww.MoveToNextUIElement();
                            }

                        }
                        else
                        {
                            MessageBox.Show("Failed to retrieve data. Please Contact Administrator.");
                        }
                    }
                    else
                    {
                        PurchaseDetails purchase = (PurchaseDetails)this.mww.PurchaseDataGrid.SelectedItem;
                        purchase.AccountCode = glAccounts.Code;
                        purchase.AccountDesc = glAccounts.AccountTypeDesc;

                        e.Handled = true;
                        this.Close();
                        this.mww.MoveToNextUIElement();
                        this.mww.MoveToNextUIElement();

                    }
                }
                else if (this.mee != null)
                {
                    InterestRates intRates = (InterestRates)this.mee.monthEndDataGrid.SelectedItem;

                    intRates.glAccountCode = glAccounts.Code;
                    intRates.AcctDesc = glAccounts.AccountTypeDesc;

                    this.Close();
                }
            }
            catch (Exception)
            {
                if (e != null)
                {
                    e.Handled = true;
                }
                else
                {
                    return;
                }
            }

        }

        private void AutoPopulateGL(Models.GLAccounts glAccounts)
        {

            if (this.mww.TempTransactionDetailsList.Count > 0)
            {
                var acct = this.mww.TempTransactionDetailsList.FindAll(t => t.AccountCode == glAccounts.Code).ToList();

                TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;

                if (acct.Count != 0)
                {

                }
                else
                {
                    tt.AccountCode = glAccounts.Code;
                    tt.accountDesc = glAccounts.AccountTypeDesc;
                }
            }


        }

        public void SelectedData(KeyEventArgs e = null)
        {

            if (GLAccountGrid.SelectedItem == null) return;
            var selectedGL = GLAccountGrid.SelectedItem as Models.GLAccounts;

            if (this.mww != null)
            {
                if (!IsPurchase)
                {
                    TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;

                    String parsedDocInq = new JavaScriptSerializer().Serialize(selectedGL);

                    CRUXLib.Response Response = App.CRUXAPI.request("backoffice/checkSL", parsedDocInq);

                    if (Response.Status == "SUCCESS")
                    {
                        Models.AcctCodewithSL acctCodeWithSL = new JavaScriptSerializer().Deserialize<Models.AcctCodewithSL>(Response.Content);

                        if (!String.IsNullOrEmpty(acctCodeWithSL.SLC_Code) && !String.IsNullOrEmpty(acctCodeWithSL.SLT_Code) &&
                                !String.IsNullOrEmpty(acctCodeWithSL.SLE_Code) && !String.IsNullOrEmpty(acctCodeWithSL.SLN_Code))
                        {
                            ExecuteHelper execStringBuilder = new ExecuteHelper(this.mww);
                            this.Close();

                            if (acctCodeWithSL.CanGLOnly == 0)
                            {
                                MessageBoxResult result = MessageBox.Show(execStringBuilder.CreateAcctCodeCanGLOnlyPrompt(), "Alert", MessageBoxButton.OK);
                            }
                            else
                            {
                                MessageBoxResult result = MessageBox.Show(execStringBuilder.CreateAcctCodeWithSLPrompt(), "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);

                                switch (result)
                                {
                                    case MessageBoxResult.Yes:

                                        tt.AccountCode = selectedGL.Code;
                                        tt.accountDesc = selectedGL.AccountTypeDesc;

                                        Keyboard.Focus(this.mww.GetDataGridCell(this.mww.DG_GeneralJournal.SelectedCells[7]));
                                        e.Handled = true;
                                        break;

                                    case MessageBoxResult.No:

                                        this.Close();
                                        break;

                                } //end of switch 
                            }

                        } // end of if SLC,SLT,SLE,SLN is not empty
                        else
                        {

                            tt.AccountCode = selectedGL.Code;
                            tt.accountDesc = selectedGL.AccountTypeDesc;

                            //e.Handled = true;
                            this.Close();
                            this.mww.MoveToNextUIElement();
                            this.mww.MoveToNextUIElement();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Failed to retrieve data. Please Contact Administrator.");
                    }
                }
                else
                {
                    PurchaseDetails purchase = (PurchaseDetails)this.mww.PurchaseDataGrid.SelectedItem;
                    purchase.AccountCode = selectedGL.Code;
                    purchase.AccountDesc = selectedGL.AccountTypeDesc;

                    this.Close();
                    this.mww.MoveToNextUIElement();
                    this.mww.MoveToNextUIElement();
                }
            }
            else if (this.mee != null)
            {
                InterestRates intRates = (InterestRates)this.mee.monthEndDataGrid.SelectedItem;

                intRates.glAccountCode = selectedGL.Code;
                intRates.AcctDesc = selectedGL.AccountTypeDesc;

                this.Close();
            }

        }


        public void FillSearchDataGrid()
        {

            String search = SearchTextBox.Text;
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/searchGlAccount", search);

            if (Response.Status == "SUCCESS")
            {
                List<Models.GLAccounts> glAccountLists = new JavaScriptSerializer().Deserialize<List<Models.GLAccounts>>(Response.Content);
                GLAccountGrid.ItemsSource = glAccountLists;
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        public void SetGLAccount(Int64 code, string coaDesc)
        {
            GLCode = code;
            COADesc = coaDesc;
        }


        public Int64 getGLCode()
        {
            return GLCode;
        }


        public String getCOADesc()
        {
            return COADesc;
        }


        private void GLAccountGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SelectedData();
        }


        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Selected = (Models.GLAccounts)GLAccountGrid.Items[currentRow];
                SelectedDataEnter(this.Selected);
            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Selected = (Models.GLAccounts)GLAccountGrid.Items[currentRow];

                SelectedDataEnter(this.Selected, e);
            }
        }


        private bool FilterData(object item)
        {
            var value = (Models.GLAccounts)item;

            if (DefaultColumn == "Code")
            {
                return Convert.ToString(value.Code).Contains(SearchText);
            }
            else if (DefaultColumn == "AccountTypeDesc")
            {
                return Convert.ToString(value.AccountTypeDesc).Contains(SearchText.ToLower()) || value.AccountTypeDesc.ToLower().StartsWith(SearchText.ToLower());
            }
            else if (DefaultColumn == "Description")
            {
                return Convert.ToString(value.Description).Contains(SearchText.ToLower()) || value.Description.ToLower().StartsWith(SearchText.ToLower());
            }
            else
                return value.AccountTypeDesc.ToLower().StartsWith(SearchText.ToLower()) || value.Description.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.Code).Contains(SearchText);

        }


        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();
            MyData.Filter = FilterData;

            GLAccountGrid.SelectedIndex = 0;
        }


        private void GLAccountGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SelectedData(e);
            }
        }

        private void SelectRow_Click(object sender, RoutedEventArgs e)
        {

            DataGridRow selectedRow = sender as DataGridRow;
            int rowIndex = GLAccountGrid.Items.IndexOf(selectedRow.DataContext);

            currentRow = (rowIndex >= 0 ? rowIndex : 0);
            this.Selected = (Models.GLAccounts)GLAccountGrid.Items[rowIndex];

            GLAccountGrid.Items.MoveCurrentToPosition(currentRow);

        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Selected = DataCont.GLAccounts.FirstOrDefault();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void columnHeader_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var columnheader = sender as DataGridColumnHeader;
                if (columnheader != null) // 
                {
                    String content = columnheader.Content.ToString();

                    if (content == "Code")
                    {
                        DefaultColumn = "Code";
                    }
                    else if (content == "GL Type")
                    {
                        DefaultColumn = "AccountTypeDesc";
                    }
                    else if (content == "Description")
                    {
                        DefaultColumn = "Description";
                    }

                }
                else
                {
                    GLAccountGrid.CanUserSortColumns = true;
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }



        public class glAccountDataCon : INotifyPropertyChanged
        {
            List<Models.GLAccounts> _GLAccount;

            public List<Models.GLAccounts> GLAccounts
            {
                get { return _GLAccount; }
                set
                {
                    _GLAccount = value;
                    OnPropertyChanged("GLAccounts");

                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SearchText = "";
            MyData.Refresh();
        }

        static class DataGridViewHelper
        {

            static public DataGridCell GetCell(DataGrid dg, int row, int column)
            {
                DataGridRow rowContainer = GetRow(dg, row);
                if (rowContainer != null)
                {
                    DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);
                    if (presenter != null)
                    {
                        DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);  // <<<------ ERROR
                        if (cell == null)
                        {
                            // now try to bring into view and retreive the cell
                            dg.ScrollIntoView(rowContainer, dg.Columns[column]);
                            cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                        }
                        return cell;
                    }
                    else
                    {

                        dg.Items.MoveCurrentToPosition(row);
                        var index = dg.SelectedIndex;
                        DataGridRow tmprow = dg.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;
                        DataGridCellInfo cellInfo = new DataGridCellInfo(tmprow, dg.Columns[0]);
                        var cellContent = cellInfo.Column.GetCellContent(cellInfo.Item);
                        if (cellContent != null)
                        {
                            return (DataGridCell)cellContent.Parent;
                        }
                        return null;
                    }
                }
                return null;
            }

            static public DataGridRow GetRow(DataGrid dg, int index)
            {
                DataGridRow row = (DataGridRow)dg.ItemContainerGenerator.ContainerFromIndex(index);
                if (row == null)
                {
                    Console.WriteLine("is null");
                    // may be virtualized, bring into view and try again
                    //dg.ScrollIntoView(dg.Items[index]);
                    row = (DataGridRow)dg.ItemContainerGenerator.ContainerFromIndex(index);
                }
                if (row == null)
                {
                    Console.WriteLine("is null again");
                    // may be virtualized, bring into view and try again
                    //dg.ScrollIntoView(dg.Items[index]);
                    row = (DataGridRow)dg.CurrentItem;
                }
                return row;
            }

            static T GetVisualChild<T>(Visual parent) where T : Visual
            {
                T child = default(T);
                int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < numVisuals; i++)
                {
                    Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                    child = v as T;
                    if (child == null)
                    {
                        child = GetVisualChild<T>(v);
                    }
                    if (child != null)
                    {
                        break;
                    }
                }
                return child;
            }

            public static int GetRowIndex(DataGridCell dataGridCell)
            {
                // Use reflection to get DataGridCell.RowDataItem property value.
                PropertyInfo rowDataItemProperty = dataGridCell.GetType().GetProperty("RowDataItem", BindingFlags.Instance | BindingFlags.NonPublic);

                DataGrid dataGrid = GetDataGridFromChild(dataGridCell);

                return dataGrid.Items.IndexOf(rowDataItemProperty.GetValue(dataGridCell, null));
            }
            public static DataGrid GetDataGridFromChild(DependencyObject dataGridPart)
            {
                if (VisualTreeHelper.GetParent(dataGridPart) == null)
                {
                    throw new NullReferenceException("Control is null.");
                }
                if (VisualTreeHelper.GetParent(dataGridPart) is DataGrid)
                {
                    return (DataGrid)VisualTreeHelper.GetParent(dataGridPart);
                }
                else
                {
                    return GetDataGridFromChild(VisualTreeHelper.GetParent(dataGridPart));
                }
            }
        }

    }
}
