﻿using Back_Office.Helper;
using Back_Office.Models;
using Back_Office.Models.Scripts;
using Back_Office.Scripts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for MonthEndPeriod.xaml
    /// </summary>
    public partial class InterestOnSavings : Window
    {
        MainWindow mww;
        private List<Models.GLAccounts> glAccounts;
        private GLAccount GLAccounts;
        MonthEndDataCon DataCont = new MonthEndDataCon();
        public BackgroundWorker GenerateInterest = new BackgroundWorker { WorkerReportsProgress = true };
        BackgroundWorker GenerateFunctionParams = new BackgroundWorker();
        private Boolean AllowEdit = true;

        public InterestOnSavings(MainWindow mw, List<Models.GLAccounts> glAccountsList)
        {
            InitializeComponent();
            InitializeDefaults();
            InitializeWorkers();

            LoadInterestRates();
            LoadTransType();
            glAccounts = glAccountsList;
            this.mww = mw;

            fromDatePicker.Focus();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);

            this.DataContext = DataCont;
        }


        private void InitializeDefaults()
        {
            DataCont.InterestRates = new ObservableCollection<InterestRates>();
            DataCont.SLParameters = new List<SLParameters>();
            DataCont.ControlList = new List<ControlListing>();
        }


        private void InitializeWorkers()
        {
            GenerateInterest.DoWork += GenerateInterest_DoWork;
            //GenerateInterest.ProgressChanged += GenerateInterest_ProgressChanged;
            GenerateInterest.RunWorkerCompleted += GenerateInterest_RunWorkerCompleted;
        }


        private void GenerateInterest_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressBar.Value = e.ProgressPercentage;
        }


        private void GenerateInterest_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnableButtons();

            MessageBox.Show(this, "Module Was Successfully Added!", "Successfully Completed!", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
            this.mww.GenerateControlList();
        }


        public void GenerateInterest_DoWork(object sender, DoWorkEventArgs e)
        {
            DataCont.SLParameters = FunctionGenerate().SLParameters.GroupBy(s => s.SLT).Select(g => g.First()).ToList();
            SelectClientList(DataCont.SLParameters);
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Are you sure to Cancel current work?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            Close();
                            break;
                        case MessageBoxResult.No:
                            break;
                        default:
                            break;

                    } //end switch

                } //end If Key = Escape

                else if (e.Key == Key.F11 || (((Keyboard.IsKeyDown(Key.LeftAlt)) || (Keyboard.IsKeyDown(Key.RightAlt))) && Keyboard.IsKeyDown(Key.G)))
                {
                    if (generateButton.IsEnabled)
                    {
                        this.Generate();
                    }
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void LoadTransType()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/transTypeInterest", "");
            if (Response.Status == "SUCCESS")
            {
                TransactionType transType = new JavaScriptSerializer().Deserialize<TransactionType>(Response.Content);
                DataCont.TransTypeID = transType.TransTypeID;
            }
            else
            {
                MessageBox.Show(this, Response.Content);
            }
        }


        public void LoadInterestRates()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/interestRates", "");
            if (Response.Status == "SUCCESS")
            {
                ObservableCollection<InterestRates> listIntRates = new JavaScriptSerializer().Deserialize<ObservableCollection<InterestRates>>(Response.Content);
                DataCont.InterestRates = listIntRates;
                monthEndDataGrid.CanUserAddRows = false;
            }
            else
            {
                MessageBox.Show(this, Response.Content);
            }
        }


        private void monthEndDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter || e.Key == Key.Space)
                {
                    if ((e.Key == Key.Space || e.Key == Key.Enter) && monthEndDataGrid.CurrentColumn.DisplayIndex == 6)
                    {
                        try
                        {
                            GLAccounts = new GLAccount(glAccounts, this);
                            GLAccounts.Owner = this;
                            GLAccounts.ShowDialog();
                            e.Handled = true;
                        }
                        catch (Exception)
                        {

                            throw;
                        }

                    }

                    else if ((e.Key == Key.Space || e.Key == Key.Enter) && monthEndDataGrid.CurrentColumn.DisplayIndex == 8)
                    {
                        try
                        {
                            GLAccounts = new GLAccount(glAccounts, this);
                            GLAccounts.Owner = this;
                            GLAccounts.ShowDialog();
                            e.Handled = true;
                        }
                        catch (Exception)
                        {

                            throw;
                        }

                    }
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        } //end monthend previewkeydown


        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "fromDatePicker")
            {
                fromDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                fromDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "toDatePicker")
            {
                toDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                toDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
            else if (textName == "transDatePicker")
            {
                transDatePicker.Background = System.Windows.Media.Brushes.Yellow; // WPF
                transDatePicker.IsInactiveSelectionHighlightEnabled = true;
            }
        }


        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string textName = (sender as TextBox).Name.ToString();

            if (textName == "fromDatePicker")
            {
                try
                {
                    DateTime.Parse(fromDatePicker.Text);
                    fromDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    fromDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            }

            else if (textName == "toDatePicker")
            {
                try
                {
                    DateTime.Parse(toDatePicker.Text);
                    toDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    toDatePicker.IsInactiveSelectionHighlightEnabled = true;

                    transDatePicker.Text = toDatePicker.Text;
                }
                catch
                {
                    e.Handled = true;
                }

            } // end else if toDatePicker
            else if (textName == "transDatePicker")
            {
                try
                {
                    DateTime.Parse(transDatePicker.Text);
                    transDatePicker.Background = System.Windows.Media.Brushes.White; // WPF
                    transDatePicker.IsInactiveSelectionHighlightEnabled = true;
                }
                catch
                {
                    e.Handled = true;
                }
            } // end else if toDatePicker
        }


        private MonthEndDataCon FunctionGenerate()
        {

            foreach (InterestRates detail in DataCont.InterestRates)
            {
                DataCont.SLParameters.Add(new SLParameters()
                {
                    SLC = detail.SLC,
                    SLT = detail.SLT,
                    SLE = detail.SLE,
                    SLN = detail.SLN,
                    AcctCode = detail.AcctCode,
                    DateFrom = DataCont.DateFrom,
                    DateTo = DataCont.DateTo,
                    CutOffDate = DataCont.CutOffDate
                });

            }//end if foreach

            return DataCont;
        }


        private List<Models.TransactionDetails> SetGLAcct(int SeqNo, Decimal TotalInterest)
        {
            DateTime TransYear = Convert.ToDateTime(DataCont.CutOffDate);

            List<Models.TransactionDetails> transGL = new List<Models.TransactionDetails>();

            foreach (Models.Scripts.InterestRates transDet in monthEndDataGrid.ItemsSource)
            {
                transGL.Add(new TransactionDetails()
                {
                    AccountCode = transDet.glAccountCode,
                    SequenceNo = SeqNo,
                    Amount = TotalInterest,
                    SLDate = DataCont.CutOffDate,
                    TransYear = TransYear.Year,
                    TransactionCode = Convert.ToInt16(DataCont.TransTypeID)
                });

            }

            return transGL;
        }


        private List<InterestRange> SetupInterestRange()
        {
            List<InterestRange> intRangelist = new List<InterestRange>();
            Decimal maxADB = 0;
            Decimal intRate = 0;

            foreach (InterestRates detail in monthEndDataGrid.ItemsSource)
            {
                try
                {
                    maxADB = Decimal.Parse(detail.MaxADB);
                }
                catch (Exception)
                {
                    maxADB = 999999999999;
                }

                try
                {
                    intRate = Decimal.Parse(detail.InterestRate);
                }
                catch (Exception)
                {
                    intRate = 0;
                }

                intRangelist.Add(new InterestRange()
                {
                    SLC = detail.SLC,
                    SLT = detail.SLT,
                    MinADB = Convert.ToDecimal(detail.MinADB),
                    MaxADB = maxADB,
                    IntRate = intRate,
                    TaxIntRate = Convert.ToDecimal(detail.TaxInterestRate),
                    AcctCode = detail.AcctCode,
                    IntGLAcct = detail.glAccountCode,
                    AcctDesc = detail.COADesc
                });
            }

            return intRangelist;
        }


        private List<InterestRangeForExplanation> SetupInterestExplanation()
        {
            List<InterestRangeForExplanation> intRangeExplist = new List<InterestRangeForExplanation>();
            Decimal maxADB = 0;
            Decimal intRate = 0;

            foreach (InterestRates detail in monthEndDataGrid.ItemsSource)
            {
                try
                {
                    maxADB = Decimal.Parse(detail.MaxADB);
                }
                catch (Exception)
                {
                    maxADB = 0;
                }

                try
                {
                    intRate = Decimal.Parse(detail.InterestRate);
                }
                catch (Exception)
                {
                    intRate = 0;
                }

                intRangeExplist.Add(new InterestRangeForExplanation()
                {
                    COADesc = detail.COADesc,
                    GLAcctCode = detail.glAccountCode,
                    GLAcctDesc = detail.AcctDesc,
                    MinADB = Convert.ToDecimal(detail.MinADB),
                    MaxADB = maxADB,
                    IntRate = intRate,
                    TaxIntRate = Convert.ToDecimal(detail.TaxInterestRate)
                });

            }

            return intRangeExplist;
        }


        private void SelectClientList(List<SLParameters> slParams)
        {
            DateTime TransactionDate = Convert.ToDateTime(DataCont.CutOffDate);

            List<Models.TransactionDetails> TransactionDetailsList = new List<TransactionDetails>();
            List<Models.TransactionDetails> transList = new List<TransactionDetails>();

            List<SLWithADB> slWithADB = new List<SLWithADB>();

            foreach (SLParameters slParam in slParams)
            {
                String parsedSLParam = new JavaScriptSerializer().Serialize(slParam);

                CRUXLib.Response ResponseSL = App.CRUXAPI.request("backoffice/searchClients", parsedSLParam, Int32.MaxValue);

                if (ResponseSL.Status == "SUCCESS")
                {
                    slWithADB = new JavaScriptSerializer().Deserialize<List<SLWithADB>>(ResponseSL.Content);

                    MonthEndScript moInterestScript = new MonthEndScript(DataCont.TransTypeID, DataCont.CutOffDate);

                    foreach (InterestRange intRange in SetupInterestRange().Where(i => i.SLC == slParam.SLC && i.SLT == slParam.SLT).ToList())
                    {
                        TransactionDetailsList = new List<TransactionDetails>();
                        TransactionDetailsList = moInterestScript.SetupInterestOnSavings(intRange, slWithADB);

                        if (TransactionDetailsList.Count != 0)
                        {
                            SetupControlList(slWithADB, intRange, TransactionDetailsList); //add to controlListing list 

                            TransactionDetailContainer _Transactions = new TransactionDetailContainer();

                            TransactionDetailsList.AddRange(SetGLAcct(TransactionDetailsList.Count, moInterestScript.GetTotalInterest()).GroupBy(c => c.AccountCode).Select(s => s.First()).ToList());

                            _Transactions._TransactionDetailsList = TransactionDetailsList;
                            _Transactions._TransactionSummary = TempProcessTransactionSummary(TransactionDate.ToString("yyyyMMdd"), TransactionDate.Year, TransactionDate.ToString("MM/dd/yyyy"));

                            String parsedTransaction = new JavaScriptSerializer().Serialize(_Transactions);
                            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/insertBackoffice", parsedTransaction);

                            if (Response.Status == "SUCCESS")
                            {
                                if (Response.Status != "SUCCESS")
                                {
                                    MessageBox.Show(this, Response.Content, "Module not saved!", MessageBoxButton.OK, MessageBoxImage.Error);
                                }

                            } //end if Response.Status == SUCCESS

                        } //end if (TransactionDetailsList.Count != 0)

                    } //end foreach

                }//end of ResponseADB.Status

                else
                {
                    MessageBox.Show(this, "Failed to Retrieve Data! Please Contact Administrator");
                }

            } // end of foreach

            ExecuteHelper execHelp = new ExecuteHelper(this.mww);
            List<ControlListing> exp = new List<ControlListing>();
            exp.Add(new ControlListing()
            {
                ClientID = 0,
                ClientName = execHelp.CreateInterestOnSavingsExplanation(SetupInterestExplanation(), DataCont.DateFrom, DataCont.DateTo),
                SLC = 0,
                SLT = 0,
                SLE = 0,
                SLN = 0,
                IntRate = 0,
                MinADB = 0,
                MaxADB = 0,
                CurrentBalance = 0,
                ADBValue = 0,
                IncludeTag = 0,
                InterestAmt = 0
            });

            exp.AddRange(DataCont.ControlList);
            String parsedControlList = new JavaScriptSerializer().Serialize(exp);
            CRUXLib.Response ResponseControlList = App.CRUXAPI.request("backoffice/insertControlList", parsedControlList);

            if (ResponseControlList.Status != "SUCCESS")
            {
                MessageBox.Show(this, "Failed to Insert Control list. Please Contact Administrator.Thank you", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }


        private void SetupControlList(List<SLWithADB> slWithADB, InterestRange intRange, List<TransactionDetails> TransDetailsList)
        {

            foreach (TransactionDetails sl in TransDetailsList)
            {
                DataCont.ControlList.Add(new ControlListing()
                {
                    ClientID = sl.ClientID,
                    ClientName = slWithADB.Find(i => i.ClientID == sl.ClientID).ClientName,
                    SLC = Convert.ToByte(sl.SLC_Code),
                    SLT = Convert.ToByte(sl.SLT_Code),
                    SLE = Convert.ToByte(sl.SLE_Code),
                    SLN = Convert.ToByte(sl.SLN_Code),
                    RefNo = sl.ReferenceNo,
                    IntRate = intRange.IntRate,
                    TaxInterestAmt = intRange.TaxIntRate,
                    MinADB = intRange.MinADB,
                    MaxADB = intRange.MaxADB,
                    IntGLAcct = intRange.IntGLAcct.ToString(),
                    AcctCode = intRange.AcctCode,
                    AcctDesc = intRange.AcctDesc,
                    CurrentBalance = slWithADB.Find(i => i.ClientID == sl.ClientID && i.RefNo == sl.ReferenceNo).CurrentBalance,
                    ADBValue = slWithADB.Find(i => i.ClientID == sl.ClientID && i.RefNo == sl.ReferenceNo).ADBValue,
                    IncludeTag = 1,
                    InterestAmt = -1 * sl.Amount
                });
            }
        }


        private void DisableButtons()
        {
            InterestGrid.IsEnabled = false;
            generateButton.IsEnabled = false;
            cancelButton.IsEnabled = false;
        }


        private void EnableButtons()
        {
            InterestGrid.IsEnabled = true;
            generateButton.IsEnabled = true;
            cancelButton.IsEnabled = true;

            ProgressPanel.Visibility = Visibility.Hidden;
        }


        private void Generate()
        {
            MessageBoxResult result = MessageBox.Show(this, "Are you sure want to continue processing?", "Alert", MessageBoxButton.YesNo, MessageBoxImage.Information, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:

                    if (transDatePicker.Text != "__/__/____" && toDatePicker.Text != "__/__/____" && fromDatePicker.Text != "__/__/____")
                    {
                        DateTime From, To, CutOffDate;
                        From = Convert.ToDateTime(fromDatePicker.Text);
                        To = Convert.ToDateTime(toDatePicker.Text);
                        CutOffDate = Convert.ToDateTime(transDatePicker.Text);

                        int days = DateTime.DaysInMonth(CutOffDate.Year, CutOffDate.Month);
                        TimeSpan range = Convert.ToDateTime(To) - Convert.ToDateTime(From);

                        if (days != (range.Days + 1))
                        {
                            MessageBox.Show(this, "Please check period covered! Thank you.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
                            if (From < To && From < CutOffDate)
                            {
                                DisableButtons();
                                ProgressPanel.Visibility = Visibility.Visible;
                                GenerateInterest.RunWorkerAsync();
                            }
                            else
                            {
                                MessageBox.Show(this, "From Date should be less than To Date! Thank you.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Please Enter Date!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                    break;
                case MessageBoxResult.No:

                    break;
            }
        }


        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Generate();
            }
            catch (Exception)
            {
                e.Handled = true;

                throw;
            }

        }


        private void ToFromDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            String name = (sender as TextBox).Name.ToString();

            if (e.Key == Key.Tab)
            {
                try
                {
                    if (name == "fromDatePicker")
                    {
                        DateTime.Parse(fromDatePicker.Text);
                    }
                    else if (name == "toDatePicker")
                    {
                        DateTime.Parse(toDatePicker.Text);
                    }
                    else if (name == "transDatePicker")
                    {
                        DateTime.Parse(transDatePicker.Text);
                    }
                }
                catch
                {
                    e.Handled = true;
                }

            } //end if key = TAB

            else if (e.Key == Key.Enter)
            {
                try
                {
                    if (name == "transDatePicker")
                    {
                        this.Generate();
                    }
                }
                catch
                {
                    e.Handled = true;
                }
            }

        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private TransactionSummary TempProcessTransactionSummary(String batchNo, Int32 TransYear, String TransDate)
        {
            TransactionSummary transactionSummary = new TransactionSummary();
            ExecuteHelper execHelp = new ExecuteHelper(this.mww);

            transactionSummary.TransYear = TransYear;
            transactionSummary.BatchNo = batchNo;
            transactionSummary.TransactionCode = Convert.ToInt16(DataCont.TransTypeID);
            transactionSummary.TransactionDate = TransDate;
            transactionSummary.Explanation = execHelp.CreateInterestOnSavingsExplanation(SetupInterestExplanation(), DataCont.DateFrom, DataCont.DateTo);

            return transactionSummary;

        }


        private TransactionSummary ProcessTransactionSummary(Int16 BranchCode, String batchNo, Int16 transCode, Int64 CtlNo, String DocNo, Int32 TransYear, String TransDate, String RandomToken)
        {
            TransactionSummary transactionSummary = new TransactionSummary();
            ExecuteHelper execHelp = new ExecuteHelper(this.mww);

            transactionSummary.TransYear = TransYear;
            transactionSummary.BranchCode = BranchCode;
            transactionSummary.BatchNo = batchNo;
            transactionSummary.TransactionCode = transCode;
            transactionSummary.ControlNo = CtlNo;
            transactionSummary.DocNo = DocNo;
            transactionSummary.TransactionDate = TransDate;
            transactionSummary.Explanation = execHelp.CreateInterestOnSavingsExplanation(SetupInterestExplanation(), DataCont.DateFrom, DataCont.DateTo);
            transactionSummary.RandomToken = RandomToken;

            return transactionSummary;
        }


        private void useDefaultCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (AllowEdit)
            {
                if (App.CRUXAPI.isAllowed("update_journal"))
                {
                    AllowEdit = false;
                }
            }

            e.Handled = AllowEdit;
        }


        public class MonthEndDataCon : INotifyPropertyChanged
        {
            String _RandomToken;
            public String RandomToken
            {
                get { return _RandomToken; }
                set
                {
                    _RandomToken = value;
                    OnPropertyChanged("RandomToken");
                }
            }

            private int _TransTypeID;
            public int TransTypeID
            {
                get { return _TransTypeID; }
                set
                {
                    _TransTypeID = value;
                    OnPropertyChanged("TransTypeID");
                }
            }

            private String _DateFrom;
            public String DateFrom
            {
                get { return _DateFrom; }
                set
                {
                    _DateFrom = value;
                    OnPropertyChanged("DateFrom");
                }
            }

            private String _DateTo;
            public String DateTo
            {
                get { return _DateTo; }
                set
                {
                    _DateTo = value;
                    OnPropertyChanged("DateTo");
                }
            }

            private String _CutOffDate;
            public String CutOffDate
            {
                get { return _CutOffDate; }
                set
                {
                    _CutOffDate = value;
                    OnPropertyChanged("CutOffDate");
                }
            }

            ObservableCollection<Models.Scripts.InterestRates> _InterestRates;

            public ObservableCollection<Models.Scripts.InterestRates> InterestRates
            {
                get { return _InterestRates; }
                set
                {
                    _InterestRates = value;
                    OnPropertyChanged("InterestRates");
                }
            }

            private List<SLParameters> _SLParameters;
            public List<SLParameters> SLParameters
            {
                get { return _SLParameters; }
                set
                {
                    _SLParameters = value;
                    OnPropertyChanged("SLParameters");
                }
            }

            private List<ControlListing> _ControlList;
            public List<ControlListing> ControlList
            {
                get { return _ControlList; }
                set
                {
                    _ControlList = value;
                    OnPropertyChanged("ControlList");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }


    }
}
