﻿using Back_Office.Helper;
using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for SLEType.xaml
    /// </summary>
    public partial class SLEType : Window
    {
        public Byte temp = 0;
        public MainWindow mww;
        public DataGridFocus dgFocus;

        int currentRow = 0, currentColumn = 1;
        Models.SLEType Selected;
        string SearchText = string.Empty;
        private ICollectionView MyData;

        public SLEType(int slc, byte slt, MainWindow mw)
        {
            InitializeComponent();
            this.PreviewKeyDown += new KeyEventHandler(HandleKeys);
            this.mww = mw;
            FillDataGrid(slc, slt);
            dgFocus = new Helper.DataGridFocus();
        }

        private class SLEParams
        {
            public int SLC { get; set; }
            public Byte SLT { get; set; }
        }


        private void FillDataGrid(Object slc, Object slt)
        {
            SLEParams sle = new SLEParams();
            sle.SLC = Convert.ToInt32(slc);
            sle.SLT = Convert.ToByte(slt);

            string parsed = new JavaScriptSerializer().Serialize(sle);
            CRUXLib.Response Response = App.CRUXAPI.request("sletype/backoffice", parsed);

            if (Response.Status == "SUCCESS")
            {
                List<Models.SLEType> sletype = new JavaScriptSerializer().Deserialize<List<Models.SLEType>>(Response.Content);
                SLETypeGrid.ItemsSource = sletype;

                MyData = CollectionViewSource.GetDefaultView(sletype);
            }
            else
            {
                MessageBox.Show(Response.Content);
            }

        }


        private void myDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void SLETypeGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                function();
                e.Handled = true;
            }
        }


        private void function()
        {
            if (SLETypeGrid.SelectedItem == null) return;
            TransactionDetails tt = (TransactionDetails)this.mww.DG_GeneralJournal.SelectedItem;
            var selectedSLE = SLETypeGrid.SelectedItem as Models.SLEType;
            tt.SLE_Code = selectedSLE.slecode.ToString();
            SLNSet slnset = new SLNSet();

            if (!String.IsNullOrWhiteSpace(tt.SLC))
            {
                this.Close();
                this.mww.DG_GeneralJournal.Focus();
                Keyboard.Focus(this.mww.GetDataGridCell(this.mww.DG_GeneralJournal.SelectedCells[4]));
            }
            else
            {
                slnset = dgFocus.FillSLNDataGrid(tt.SLC_Code, tt.SLT_Code, selectedSLE.slecode.ToString());
                tt.SLN_Code = slnset.SLN.ToString();
                tt.AccountCode = slnset.AccountCode;
                tt.accountDesc = slnset.AccountDesc;

                this.Close();
                this.mww.DG_GeneralJournal.Focus();
                Keyboard.Focus(this.mww.GetDataGridCell(this.mww.DG_GeneralJournal.SelectedCells[7]));
            }

        }


        private void SLETypeGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTextBox.Focus();
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void HandleKeys(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                if (currentRow > 0)
                {
                    try
                    {
                        int previousIndex = SLETypeGrid.SelectedIndex - 1;
                        if (previousIndex < 0) return;
                        SLETypeGrid.SelectedIndex = previousIndex;
                        SLETypeGrid.ScrollIntoView(SLETypeGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }
                }

            }
            else if (e.Key == Key.Down)
            {
                if (currentRow < SLETypeGrid.Items.Count - 1)
                {

                    try
                    {
                        int nextIndex = SLETypeGrid.SelectedIndex + 1;
                        if (nextIndex > SLETypeGrid.Items.Count - 1) return;
                        SLETypeGrid.SelectedIndex = nextIndex;
                        SLETypeGrid.ScrollIntoView(SLETypeGrid.Items[currentRow]);

                    }
                    catch (Exception ex)
                    {
                        e.Handled = true;
                    }

                } // end if (this.SelectedOverride > 0)

            } // end else if (e.Key == Key.Down)

            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            SearchText = t.Text.ToString();

            MyData.Filter = FilterData;
            SLETypeGrid.SelectedIndex = 0;
        }


        private void SearchTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    this.Selected = (Models.SLEType)SLETypeGrid.Items[currentRow];

                    function();
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Selected = (Models.SLEType)SLETypeGrid.Items[currentRow];
                function();
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private bool FilterData(object item)
        {
            var value = (Models.SLEType)item;

            if (value.slecode == 0 || String.IsNullOrEmpty(value.sledescription))
                return false;
            else
                return value.sledescription.ToLower().StartsWith(SearchText.ToLower()) ||
                    Convert.ToString(value.slecode).Contains(SearchText);
        }


        private void SelectedCell_Click(object sender, RoutedEventArgs e)
        {
            DataGridCell cell = sender as DataGridCell;
            if (cell.Column.DisplayIndex != this.currentColumn)
            {
                cell.IsSelected = false;
            }


            DependencyObject dep = (DependencyObject)e.OriginalSource;
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            DataGridRow row = dep as DataGridRow;

            this.currentRow = row.GetIndex();
        }
    }
}
