﻿using Back_Office.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Back_Office
{
    /// <summary>
    /// Interaction logic for TransactionFilter.xaml
    /// </summary>
    public partial class TransactionFilter : Window
    {
        DocumentNoInquiry docNoInq;
        TransactionFilterDataCon DataCon = new TransactionFilterDataCon();
        BackgroundWorker GenerateWorker = new BackgroundWorker();

        public MainWindow mw;

        public void InitializeDefaults()
        {
            this.DataCon.DocInqParams = new DocInquiryParameters();
            //this.DataCon.IsInquiry
            this.DataCon.SystemDate = new DateTime();
        }

        public TransactionFilter(MainWindow mww, String TransType, String TransTypeCode = null)
        {
            InitializeComponent();
            InitializeDefaults();
            loadTransactionType(TransType, TransTypeCode);
            loadOffices();
            InitializeWorkers();

            this.mw = mww;

            if (this.mw.DataCon.TransParameters.TransYear != 0)
            {
                this.DataCon.DocInqParams.TransYear = this.mw.DataCon.TransParameters.TransYear;
                this.DataCon.DocInqParams.ToDate = this.mw.DataCon.TransParameters.ToDate;
                this.DataCon.DocInqParams.FromDate = this.mw.DataCon.TransParameters.FromDate;
              //  this.DataCon.SystemDate = Convert.ToDateTime(this.mw.DataCon.TransParameters.ToDate);
                this.DataCon.DocInqParams.TransCode = this.mw.DataCon.TransParameters.TransCode;
            }
            else
            {
                GetDateTime();
            }

            this.DataCon.DocInqParams.CTLNo = this.mw.DataCon.TransParameters.CTLNo;
            this.DataCon.DocInqParams.DocNo = this.mw.DataCon.TransParameters.DocNo;
            //this.DataCon.OfficeID = 
            this.DataContext = DataCon;

            transTypeComboBox.Focus();
        }


        private void InitializeWorkers()
        {
            GenerateWorker.DoWork += GenerateWorker_DoWork;
            GenerateWorker.RunWorkerCompleted += GenerateWorker_RunWorkCompleted;
        }


        private void GenerateWorker_RunWorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPanel.Visibility = Visibility.Hidden;
            transFilterGrid.IsEnabled = true;

            docNoInq.Owner = this.mw;
            this.Close();
            docNoInq.ShowDialog();

        }


        private void GenerateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                EnterFields();
            }
            catch (Exception)
            {

            }
        }


        private void loadOffices()
        {
            List<Office> Offices = new List<Office>();
            try
            {
                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/Office", "");
                if (Response.Status == "SUCCESS")
                {
                    Offices = new JavaScriptSerializer().Deserialize<List<Office>>(Response.Content);

                    OfficeComboBox.ItemsSource = Offices;
                    OfficeComboBox.SelectedIndex = 0;
                }
            }
            catch (Exception)
            {
                return;
            }
        }


        private void loadTransactionType(String TransType, String TransTypeCode)
        {
            try
            {
                CRUXLib.Response Response = App.CRUXAPI.request("backoffice/transType", "");
                if (Response.Status == "SUCCESS")
                {
                    List<TransactionType> listTransTypes = new JavaScriptSerializer().Deserialize<List<TransactionType>>(Response.Content);

                    if (TransType == "" || TransType == null)
                    {
                        transTypeComboBox.ItemsSource = listTransTypes;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(TransTypeCode))
                        {
                            transTypeComboBox.ItemsSource = listTransTypes.FindAll(t => t.TransTypeModule == TransType).ToList();
                            DataCon.DocInqParams.TransCode = Convert.ToInt16(listTransTypes.Find(t => t.TransTypeCode == TransTypeCode).TransTypeID);
                        }
                        else
                        {
                            transTypeComboBox.ItemsSource = listTransTypes.FindAll(t => t.TransTypeModule == TransType).ToList();
                            listTransTypes.First();
                            //transTypeComboBox.SelectedIndex = 0;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(Response.Content);
                }
            }
            catch (Exception)
            {
                return;
            }

        }


        private void OkButton_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    Generate();
                    e.Handled = true;
                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        private TimeSpan GetDateDifference(String date1, String date2)
        {
            TimeSpan ts = Convert.ToDateTime(Convert.ToDateTime(date1).ToString("MM/dd/yyyy")) - Convert.ToDateTime(Convert.ToDateTime(date2).ToString("MM/dd/yyyy"));

            return ts;
        }


        private void EnterFields()
        {
            try
            {
                //if (DataCon.CTLNo != 0 && String.IsNullOrEmpty(DataCon.DocNo)
                //        && DataCon.TransYear != 0)
                //{
                //    SelectTransactionFilter();
                //}

                //else if (!String.IsNullOrEmpty(DataCon.DocNo) && DataCon.CTLNo == 0
                //         && DataCon.TransYear != 0)
                //{
                //    SelectTransactionFilter();
                //}

                //else if (!String.IsNullOrEmpty(DataCon.DocNo) && DataCon.CTLNo != 0
                //        && DataCon.TransYear != 0)
                //{
                //    SelectTransactionFilter();
                //}

                if (DataCon.DocInqParams.FromDate == "__/__/____" && DataCon.DocInqParams.ToDate == "__/__/____")
                {
                    SelectTransactionFilter();
                }

                else if (!String.IsNullOrEmpty(DataCon.DocInqParams.FromDate) && !String.IsNullOrEmpty(DataCon.DocInqParams.ToDate))
                {
                    TimeSpan range = GetDateDifference(DataCon.DocInqParams.FromDate, DataCon.DocInqParams.ToDate);
                    Int64 EODStatus = 0;

                    if (-(range.Days) <= 60)
                    {

                        if (this.DataCon.DocInqParams.TransCode == 11 || this.DataCon.DocInqParams.TransCode == 12 || this.DataCon.DocInqParams.TransCode == 23 || this.DataCon.DocInqParams.TransCode == 24 || this.DataCon.DocInqParams.TransCode == 25 || this.DataCon.DocInqParams.TransCode == 28)
                        {
                            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/EODStatus", "");
                            if (Response.Status == "SUCCESS")
                            {
                                EODStatus = new JavaScriptSerializer().Deserialize<Int64>(Response.Content);

                                if (EODStatus == 1)
                                {
                                    DataCon.DocInqParams.ToDate = Convert.ToDateTime(DataCon.DocInqParams.ToDate).AddDays(1).ToString();
                                    //dateAdded = dateAdded.AddDays(1);
                                    //SelectTransactionFilter(DataCon.DocInqParams.FromDate, dateAdded.ToString());
                                }
                                //else
                                //{
                                //    SelectTransactionFilter();
                                //}
                            }
                        }
                        else
                        {
                            DataCon.DocInqParams.ToDate = Convert.ToDateTime(DataCon.DocInqParams.ToDate).AddDays(1).ToString("MM/dd/yyyy");
                            //dateAdded = dateAdded.AddDays(1);
                            //SelectTransactionFilter(DataCon.DocInqParams.FromDate, dateAdded.ToString());
                        }

                        SelectTransactionFilter();

                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show("Must specify at most 60 days range, cannot proceed!", "Error!!!", MessageBoxButton.OK);
                    }
                }

                else
                {
                    MessageBox.Show("Must supply valid search in one field");
                }
            }
            catch (Exception ex)
            {
                return;
            }

        }


        private void Generate()
        {
            if ((DataCon.DocInqParams.TransCode != 0 && DataCon.DocInqParams.TransYear != 0 && !String.IsNullOrEmpty(DataCon.DocInqParams.FromDate) && !String.IsNullOrEmpty(DataCon.DocInqParams.ToDate))
                || (DataCon.DocInqParams.TransCode != 0 && DataCon.DocInqParams.TransYear != 0 && (DataCon.DocInqParams.CTLNo != 0 || !String.IsNullOrEmpty(DataCon.DocInqParams.DocNo))))
            {
                ProgressPanel.Visibility = Visibility.Visible;
                transFilterGrid.IsEnabled = false;

                EnterFields();
                //GenerateWorker.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show("Must supply valid search in one field");
            }

        }


        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Generate();
        }


        private void SelectTransactionFilter()
        {

            //if (DataCon.CTLNo != 0 && DataCon.TransYear != 0 && DataCon.TransCode != 0)
            //{
            //    docNoInq = new DocumentNoInquiry(DataCon.TransCode, this.mw, DataCon.CTLNo, null, DataCon.TransYear);
            //}
            //else if (!String.IsNullOrEmpty(DataCon.DocNo) && DataCon.TransYear != 0 && DataCon.TransCode != 0)
            //{
            //    docNoInq = new DocumentNoInquiry(DataCon.TransCode, this.mw, 0, DataCon.DocNo, DataCon.TransYear);
            //}
            //else if (!String.IsNullOrEmpty(DataCon.DocNo) && DataCon.CTLNo != 0 && DataCon.TransYear != 0 && DataCon.TransCode != 0)
            //{
            //    docNoInq = new DocumentNoInquiry(DataCon.TransCode, this.mw, DataCon.CTLNo, DataCon.DocNo, DataCon.TransYear);
            //}
            //else if (!String.IsNullOrEmpty(From) && !String.IsNullOrEmpty(To))
            //{
            //    docNoInq = new DocumentNoInquiry(DataCon.TransCode, this.mw, 0, null, 0, From, To);
            //}
            //else if (DataCon.ORCTLNo != 0 && DataCon.TransYear != 0 && DataCon.TransCode != 0)
            //{
            //    docNoInq = new DocumentNoInquiry(DataCon.TransCode, this.mw, DataCon.TransYear, DataCon.ORCTLNo);
            //}


            docNoInq = new DocumentNoInquiry(DataCon.DocInqParams, this.mw);
            docNoInq.Owner = this.mw;
            this.Close();
            docNoInq.ShowDialog();
        }


        void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }

        //private void SelectTransactionFilter(Int64 CtlNo, Int32 Year)
        //{
        //    TransactionType transType = (TransactionType)transTypeComboBox.SelectedItem;

        //    docNoInq = new DocumentNoInquiry(Convert.ToInt16(transType.TransTypeID), this.mw, CtlNo, null, Year);
        //    docNoInq.Owner = this.mw;
        //    this.Close();
        //    docNoInq.ShowDialog();
        //}


        //private void SelectTransactionFilter(String DocNo, Int32 Year)
        //{
        //    TransactionType transType = (TransactionType)transTypeComboBox.SelectedItem;

        //    docNoInq = new DocumentNoInquiry(Convert.ToInt16(transType.TransTypeID), this.mw, 0, DocNo, Year);
        //    docNoInq.Owner = this.mw;
        //    this.Close();
        //    docNoInq.ShowDialog();
        //}


        //private void SelectTransactionFilter(Int64 CtlNo, String DocNo, Int32 Year)
        //{
        //    TransactionType transType = (TransactionType)transTypeComboBox.SelectedItem;

        //    docNoInq = new DocumentNoInquiry(Convert.ToInt16(transType.TransTypeID), this.mw, CtlNo, DocNo, Year);
        //    docNoInq.Owner = this.mw;
        //    this.Close();
        //    docNoInq.ShowDialog();
        //}


        private void PopulateDate()
        {
            var fromDate = new DateTime(Convert.ToInt32(this.DataCon.DocInqParams.TransYear), this.DataCon.SystemDate.Month, 1);
            var toDate = new DateTime(Convert.ToInt32(this.DataCon.DocInqParams.TransYear), this.DataCon.SystemDate.Month, this.DataCon.SystemDate.Day);
            //fromDate.AddMonths(1).AddDays(-1);

            DataCon.DocInqParams.FromDate = fromDate.ToString("MM/dd/yyyy");
            DataCon.DocInqParams.ToDate = toDate.ToString("MM/dd/yyyy"); ;
        }


        private void GetDateTime()
        {
            CRUXLib.Response Response = App.CRUXAPI.request("backoffice/SystemDate", "");
            if (Response.Status == "SUCCESS")
            {
                SystemConfig systemConfig = new JavaScriptSerializer().Deserialize<SystemConfig>(Response.Content);
                this.DataCon.SystemDate = systemConfig.SystemDate;
                DataCon.DocInqParams.TransYear = Convert.ToInt32(systemConfig.SystemDate.ToString("yyyy"));

                var fromDate = new DateTime(this.DataCon.SystemDate.Year, this.DataCon.SystemDate.Month, 1);
                var toDate = this.DataCon.SystemDate;

                DataCon.DocInqParams.FromDate = fromDate.ToString("MM/dd/yyyy");
                DataCon.DocInqParams.ToDate = toDate.ToString("MM/dd/yyyy");
            }
            else
            {
                MessageBox.Show(Response.Content);
            }
        }


        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void transTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                TransactionType trType = new TransactionType();
                trType = transTypeComboBox.SelectedItem as TransactionType;
                OkButton.IsEnabled = true;

                if (trType != null)
                {
                    DataCon.DocInqParams.TransCode = Convert.ToInt16(trType.TransTypeID);
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }

        }


        private void OfficeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Office office = new Office();
                office = OfficeComboBox.SelectedItem as Office;
                OkButton.IsEnabled = true;

                if (office != null)
                {
                    DataCon.DocInqParams.OfficeID = Convert.ToInt16(office.OfficeID);
                }

            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }


        public void Executed_Close(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }


        private void ControlAndDocNoTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (ctrlNoTextBox.Text != "" || docNoTextBox.Text != "")
            {
                fromLabel.Visibility = Visibility.Hidden;
                toLabel.Visibility = Visibility.Hidden;
                fromDatePicker.Visibility = Visibility.Hidden;
                toDatePicker.Visibility = Visibility.Hidden;

                this.DataCon.DocInqParams.ToDate = null;
                this.DataCon.DocInqParams.FromDate = null;

            }
            else if (!String.IsNullOrEmpty(ORNoTextBox.Text))
            {
                fromLabel.Visibility = Visibility.Hidden;
                toLabel.Visibility = Visibility.Hidden;
                fromDatePicker.Visibility = Visibility.Hidden;
                toDatePicker.Visibility = Visibility.Hidden;
                DateRect.Visibility = Visibility.Hidden;

                OfficeLabel.Visibility = Visibility.Visible;
                OfficeComboBox.Visibility = Visibility.Visible;

                this.DataCon.DocInqParams.ToDate = null;
                this.DataCon.DocInqParams.FromDate = null;
            }
            else
            {
                GetDateTime();
                fromLabel.Visibility = Visibility.Visible;
                toLabel.Visibility = Visibility.Visible;
                fromDatePicker.Visibility = Visibility.Visible;
                toDatePicker.Visibility = Visibility.Visible;
                DateRect.Visibility = Visibility.Visible;

                OfficeLabel.Visibility = Visibility.Hidden;
                OfficeComboBox.Visibility = Visibility.Hidden;
            }
        }


        private void fromDatePicker_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                try
                {
                    DateTime.Parse(fromDatePicker.Text);
                    //toDatePicker.Focus();
                }
                catch
                {
                    e.Handled = true;

                }
            }
            else if (e.Key == Key.Enter)
            {
                try
                {
                    DateTime.Parse(fromDatePicker.Text);
                    Generate();
                    e.Handled = true;
                }
                catch (Exception)
                {
                    MessageBox.Show("Must fill both Date From & To, cannot proceed!");
                    e.Handled = true;
                }

            }
        }


        private void toDatePicker_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                try
                {
                    DateTime.Parse(toDatePicker.Text);
                    //explanationTextBox.Focus();
                }
                catch
                {
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Enter)
            {
                try
                {
                    DateTime.Parse(toDatePicker.Text);
                    Generate();
                    e.Handled = true;
                }
                catch (Exception)
                {
                    MessageBox.Show("Must fill both Date From & To, cannot proceed!");
                    e.Handled = true;
                }
            }
        }


        public class TransactionFilterDataCon : INotifyPropertyChanged
        {
            DocInquiryParameters _DocInqParams;
            public DocInquiryParameters DocInqParams
            {
                get
                {
                    return _DocInqParams;
                }
                set
                {
                    _DocInqParams = value;
                    OnPropertyChanged("DocInqParams");
                }
            }

            //Int16 _TransCode;
            //public Int16 TransCode
            //{
            //    get
            //    {
            //        return _TransCode;
            //    }
            //    set
            //    {
            //        _TransCode = value;
            //        OnPropertyChanged("TransCode");
            //    }
            //}

            //Int32 _TransYear;
            //public Int32 TransYear
            //{
            //    get
            //    {
            //        return _TransYear;
            //    }
            //    set
            //    {
            //        _TransYear = value;
            //        OnPropertyChanged("TransYear");
            //    }
            //}

            //String _FromDate;
            //public String FromDate
            //{
            //    get
            //    {
            //        return this.DateFormat(_FromDate);
            //    }
            //    set
            //    {
            //        _FromDate = value;
            //        OnPropertyChanged("FromDate");
            //    }
            //}

            //String _ToDate;
            //public String ToDate
            //{
            //    get
            //    {
            //        return this.DateFormat(_ToDate);
            //    }
            //    set
            //    {
            //        _ToDate = value;
            //        OnPropertyChanged("ToDate");
            //    }
            //}

            //Int64 _CTLNo;
            //public Int64 CTLNo
            //{
            //    get
            //    {
            //        return _CTLNo;
            //    }
            //    set
            //    {
            //        _CTLNo = value;
            //        OnPropertyChanged("CTLNo");
            //    }
            //}

            //String _DocNo;
            //public String DocNo
            //{
            //    get
            //    {
            //        return _DocNo;
            //    }
            //    set
            //    {
            //        _DocNo = value;
            //        OnPropertyChanged("DocNo");
            //    }
            //}

            //Int64 _ORCTLNo;
            //public Int64 ORCTLNo
            //{
            //    get
            //    {
            //        return _ORCTLNo;
            //    }
            //    set
            //    {
            //        _ORCTLNo = value;
            //        OnPropertyChanged("ORCTLNo");
            //    }
            //}

            //Int64 _OfficeID;
            //public Int64 OfficeID
            //{
            //    get
            //    {
            //        return _OfficeID;
            //    }
            //    set
            //    {
            //        _OfficeID = value;
            //        OnPropertyChanged("OfficeID");
            //    }
            //}

            Boolean _IsInquiry;
            public Boolean IsInquiry
            {
                get
                {
                    return _IsInquiry;
                }
                set
                {
                    _IsInquiry = value;
                    OnPropertyChanged("IsInquiry");
                }
            }

            DateTime _SystemDate;
            public DateTime SystemDate
            {
                get
                {
                    return _SystemDate;
                }
                set
                {
                    _SystemDate = value;
                    OnPropertyChanged("SystemDate");
                }
            }



            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(string property)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            private String DateFormat(String Date)
            {
                if (Date == null)
                {
                    Date = "";
                }

                String temp = Regex.Replace(Date, "[^0-9.]", "");
                if (temp.Length >= 8)
                {
                    temp = temp.Substring(0, 8);
                }
                else
                {
                    int underscore = 8 - temp.Length;

                    while (underscore > 0)
                    {
                        temp += "_";
                        underscore--;
                    }
                }
                temp = temp.Insert(2, "/");
                temp = temp.Insert(5, "/");

                return temp;
            }
        }


        private void Window_Closed(object sender, EventArgs e)
        {
            this.mw.DataCon.TransParameters.TransCode = this.DataCon.DocInqParams.TransCode;
            this.mw.DataCon.TransParameters.TransYear = this.DataCon.DocInqParams.TransYear;
            this.mw.DataCon.TransParameters.CTLNo = this.DataCon.DocInqParams.CTLNo;
            this.mw.DataCon.TransParameters.DocNo = this.DataCon.DocInqParams.DocNo;
            this.mw.DataCon.TransParameters.ToDate = this.DataCon.DocInqParams.ToDate;
            this.mw.DataCon.TransParameters.FromDate = this.DataCon.DocInqParams.FromDate;
            //this.mw.DataCon.s
        }


        private void TextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                String textName = (sender as TextBox).Name.ToString();

                //if (textName == "transYrTextBox")
                //{
                //    PopulateDate();
                //}

                if (textName == "fromDatePicker")
                {
                    try
                    {
                        DateTime.Parse(fromDatePicker.Text);
                    }
                    catch
                    {
                        e.Handled = true;

                    }
                }

                else if (textName == "toDatePicker")
                {
                    try
                    {
                        DateTime.Parse(toDatePicker.Text);
                    }
                    catch
                    {
                        e.Handled = true;
                    }

                }
            }
            catch (Exception)
            {
                e.Handled = true;
            }
        }
    }
}
